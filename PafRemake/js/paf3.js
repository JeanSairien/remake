Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"An AJAX HTTP error occurred.":"Une erreur HTTP AJAX s\u0027est produite.","HTTP Result Code: !status":"Code de statut HTTP : !status","An AJAX HTTP request terminated abnormally.":"Une requ\u00eate HTTP AJAX s\u0027est termin\u00e9e anormalement.","Debugging information follows.":"Informations de d\u00e9bogage ci-dessous.","Path: !uri":"Chemin: !uri","StatusText: !statusText":"StatutTexte: !statusText","ResponseText: !responseText":"ReponseTexte: !responseText","ReadyState: !readyState":"ReadyState: !readyState","(active tab)":"(onglet actif)","Insert this token into your form":"Ins\u00e9rer ce jeton (\u003Cem\u003Etoken\u003C\/em\u003E) dans votre formulaire","First click a text field to insert your tokens into.":"Cliquez d\u0027abord sur un champ de texte pour ins\u00e9rer vos jetons (\u003Cem\u003Etokens\u003C\/em\u003E) dans celui -ci.","Hide":"Masquer","Show":"Afficher","Show shortcuts":"Afficher les raccourcis","Hide shortcuts":"Cacher les raccourcis","Re-order rows by numerical weight instead of dragging.":"R\u00e9-ordonner les lignes avec des poids num\u00e9riques plut\u00f4t qu\u0027en les d\u00e9placant.","Show row weights":"Afficher le rang d\u0027ordre","Hide row weights":"Cacher le rang d\u0027ordre","Drag to re-order":"Cliquer-d\u00e9poser pour r\u00e9-organiser","Changes made in this table will not be saved until the form is submitted.":"Les changements effectu\u00e9s dans ce tableau ne seront pris en compte que lorsque la configuration aura \u00e9t\u00e9 enregistr\u00e9e.","Edit":"Modifier","Description":"Description","Customize dashboard":"Personnaliser le tableau de bord","Enabled":"Activ\u00e9","Fri":"ven","Wednesday":"Mercredi","Tuesday":"Mardi","Sunday":"Dimanche","Monday":"Lundi","Thursday":"Jeudi","Friday":"Vendredi","Saturday":"Samedi","Please wait...":"Veuillez patienter...","Hide summary":"Masquer le r\u00e9sum\u00e9","Edit summary":"Modifier le r\u00e9sum\u00e9","New revision":"Nouvelle r\u00e9vision","No revision":"Aucune r\u00e9vision","By @name on @date":"Par @name le @date","By @name":"Par @name","Not published":"Non publi\u00e9","Next":"Suivant","Disabled":"D\u00e9sactiv\u00e9","Add":"Ajouter","Upload":"Transf\u00e9rer","Configure":"Configurer","Done":"Termin\u00e9","1 hour":"1 heure","@count hours":"@count heures","1 day":"1 jour","@count days":"@count jours","Prev":"Pr\u00e9c.","Owner":"Propri\u00e9taire","Mon":"lun","Tue":"mar","Wed":"mer","Thu":"jeu","Sat":"sam","Sun":"dim","January":"Janvier","February":"f\u00e9vrier","March":"mars","April":"avril","May":"mai","June":"juin","July":"juillet","August":"ao\u00fbt","September":"septembre","October":"octobre","November":"novembre","December":"d\u00e9cembre","Select all rows in this table":"S\u00e9lectionner toutes les lignes du tableau","Deselect all rows in this table":"D\u00e9s\u00e9lectionner toutes les lignes du tableau","Today":"Aujourd\u0027hui","Jan":"Jan","Feb":"f\u00e9v","Mar":"mar","Apr":"avr","Jun":"juin","Jul":"jui","Aug":"ao\u00fb","Sep":"sep","Oct":"oct","Nov":"nov","Dec":"d\u00e9c","Su":"Di","Mo":"Lu","Tu":"Ma","We":"Me","Th":"Je","Fr":"Ve","Sa":"Sa","1 year":"1 ann\u00e9e","@count years":"@count ann\u00e9es","1 week":"1 semaine","@count weeks":"@count semaines","1 min":"1 min","@count min":"@count min","1 sec":"1 sec","@count sec":"@count sec","mm\/dd\/yy":"mm\/jj\/aa","1 month":"1 mois","@count months":"@count mois","Only files with the following extensions are allowed: %files-allowed.":"Seuls les fichiers se terminant par les extensions suivantes sont autoris\u00e9s\u0026nbsp;: %files-allowed.","Not in menu":"Pas dans le menu","Alias: @alias":"Alias : @alias","No alias":"Aucun alias","0 sec":"0\u00a0s","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"N\u0027oubliez pas de cliquer sur \u003Cem\u003EEnregistrer les blocs\u003C\/em\u003E pour confirmer les modifications apport\u00e9es ici.","This permission is inherited from the authenticated user role.":"Ce droit est h\u00e9rit\u00e9e du r\u00f4le de l\u0027utilisateur authentifi\u00e9.","@number comments per page":"@number commentaires par page","Requires a title":"Titre obligatoire","Not restricted":"Non restreint","Not customizable":"Non personnalisable","Restricted to certain pages":"R\u00e9serv\u00e9 \u00e0 certaines pages","The block cannot be placed in this region.":"Le bloc ne peut pas \u00eatre plac\u00e9 dans cette r\u00e9gion.","Don\u0027t display post information":"Ne pas afficher les information de la contribution","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"Le fichier s\u00e9lectionn\u00e9 %filename ne peut pas \u00eatre transf\u00e9r\u00e9. Seulement les fichiers avec les extensions suivantes sont permis : %extensions.","Autocomplete popup":"Popup d\u0027auto-compl\u00e9tion","Searching for matches...":"Recherche de correspondances...","Other":"Autre","File browsing is disabled in directory %dir.":"La navigation de fichiers a \u00e9t\u00e9 d\u00e9sactiv\u00e9e dans le r\u00e9pertoire %dir.","You can not perform this operation.":"Vous ne pouvez pas r\u00e9aliser cette op\u00e9ration.","Do you want to refresh the current directory?":"Voulez-vous rafra\u00eechir ce r\u00e9pertoire?","Delete selected files?":"Voulez-vous vraiment supprimer la s\u00e9lection?","Please select a thumbnail.":"Veuillez choisir une vignette.","Please specify dimensions within the allowed range that is from 1x1 to @dimensions.":"Veuillez sp\u00e9cifier des dimensions qui correspondent \u00e0 la plage allou\u00e9e, soit de 1x1 \u00e0 @dimensions.","unlimited":"illimit\u00e9","Please select a file.":"Veuillez choisir un fichier","Log messages":"Messages du journal","%filename is not an image.":"%filename n\u0027est pas une image.","You must select at least %num files.":"Vous devez s\u00e9lectionner au moins %num fichier(s).","You are not allowed to operate on more than %num files.":"Vous n\u0027\u00eates pas autoris\u00e9(e) \u00e0 effectuer des op\u00e9rations sur plus de %num fichiers.","Close":"Fermer","Insert file":"Ins\u00e9rer un fichier","Remove group":"Supprimer le groupe","Scheduled for publishing":"Planifi\u00e9 pour publication","Scheduled for unpublishing":"Derni\u00e8re action : D\u00e9publi\u00e9. ","Not scheduled":"Non planifi\u00e9","Publishing enabled":"Publication activ\u00e9e","Unpublishing enabled":"D\u00e9publication activ\u00e9e","Resume":"Reprendre","Pause":"Pause","Manage directories":"G\u00e9rer les r\u00e9pertoires","You are not alllowed to create more than %num directories.":"Vous n\u0027\u00eates pas autoris\u00e9 \u00e0 cr\u00e9er plus de %num r\u00e9pertoires.","%dirname is not a valid directory name. It should contain only alphanumeric characters, hyphen and underscore.":"%dirname n\u0027est pas un nom de r\u00e9pertoire valide. Il ne devrait contenir que des caract\u00e8res alphanum\u00e9riques, des traits d\u0027unions et des caract\u00e8res de soulignement.","Subdirectory %dir already exists.":"Le sous-r\u00e9pertoire %dir existe d\u00e9j\u00e0.","Subdirectory %dir does not exist.":"Le sous-r\u00e9pertoire %dir n\u0027existe pas.","Are you sure want to delete this subdirectory with all directories and files in it?":"\u00cates-vous s\u00fbr(e) de vouloir supprimer ce sous-r\u00e9pertoire et tous les dossiers et fichiers qu\u0027il contient ?","Apply (all displays)":"Appliquer (tous les affichages)","Apply (this display)":"Appliquer (cet affichage)","Content limited to @limit characters, remaining: \u003Cstrong\u003E@remaining\u003C\/strong\u003E":"Ce contenu est limit\u00e9 \u00e0 @limit caract\u00e8res, il en reste : \u003Cstrong\u003E@remaining\u003C\/strong\u003E","Last action : Scheduled for publishing":"Derni\u00e8re action : Publi\u00e9.","Last action : Scheduled for unpublishing":"Derni\u00e8re action : D\u00e9publi\u00e9."}} };;
(function(a){Drupal.viewsSlideshow=Drupal.viewsSlideshow||{};Drupal.behaviors.viewsSlideshowControlsText={attach:function(b){a(".views_slideshow_controls_text_previous:not(.views-slideshow-controls-text-previous-processed)",b).addClass("views-slideshow-controls-text-previous-processed").each(function(){var c=a(this).attr("id").replace("views_slideshow_controls_text_previous_","");a(this).click(function(){Drupal.viewsSlideshow.action({action:"previousSlide",slideshowID:c});return false})});a(".views_slideshow_controls_text_next:not(.views-slideshow-controls-text-next-processed)",b).addClass("views-slideshow-controls-text-next-processed").each(function(){var c=a(this).attr("id").replace("views_slideshow_controls_text_next_","");a(this).click(function(){Drupal.viewsSlideshow.action({action:"nextSlide",slideshowID:c});return false})});a(".views_slideshow_controls_text_pause:not(.views-slideshow-controls-text-pause-processed)",b).addClass("views-slideshow-controls-text-pause-processed").each(function(){var c=a(this).attr("id").replace("views_slideshow_controls_text_pause_","");a(this).click(function(){if(Drupal.settings.viewsSlideshow[c].paused){Drupal.viewsSlideshow.action({action:"play",slideshowID:c})}else{Drupal.viewsSlideshow.action({action:"pause",slideshowID:c})}return false})})}};Drupal.viewsSlideshowControlsText=Drupal.viewsSlideshowControlsText||{};Drupal.viewsSlideshowControlsText.pause=function(c){var b=Drupal.theme.prototype.viewsSlideshowControlsPause?Drupal.theme("viewsSlideshowControlsPause"):"";a("#views_slideshow_controls_text_pause_"+c.slideshowID).text(b)};Drupal.viewsSlideshowControlsText.play=function(b){var c=Drupal.theme.prototype.viewsSlideshowControlsPlay?Drupal.theme("viewsSlideshowControlsPlay"):"";a("#views_slideshow_controls_text_pause_"+b.slideshowID).text(c)};Drupal.theme.prototype.viewsSlideshowControlsPause=function(){return Drupal.t("Resume")};Drupal.theme.prototype.viewsSlideshowControlsPlay=function(){return Drupal.t("Pause")};Drupal.behaviors.viewsSlideshowPagerFields={attach:function(b){a(".views_slideshow_pager_field:not(.views-slideshow-pager-field-processed)",b).addClass("views-slideshow-pager-field-processed").each(function(){var e=a(this).attr("id").split("_");var c=e[2];e.splice(0,3);var d=e.join("_");if(Drupal.settings.viewsSlideshowPagerFields[d][c].activatePauseOnHover){a(this).children().each(function(f,g){a(g).hover(function(){Drupal.viewsSlideshow.action({action:"goToSlide",slideshowID:d,slideNum:f});Drupal.viewsSlideshow.action({action:"pause",slideshowID:d})},function(){Drupal.viewsSlideshow.action({action:"play",slideshowID:d})})})}else{a(this).children().each(function(f,g){a(g).click(function(){Drupal.viewsSlideshow.action({action:"goToSlide",slideshowID:d,slideNum:f})})})}})}};Drupal.viewsSlideshowPagerFields=Drupal.viewsSlideshowPagerFields||{};Drupal.viewsSlideshowPagerFields.transitionBegin=function(b){a('[id^="views_slideshow_pager_field_item_'+b.slideshowID+'"]').removeClass("active");a("#views_slideshow_pager_field_item_"+b.slideshowID+"_"+b.slideNum).addClass("active")};Drupal.viewsSlideshowSlideCounter=Drupal.viewsSlideshowSlideCounter||{};Drupal.viewsSlideshowSlideCounter.transitionBegin=function(b){a("#views_slideshow_slide_counter_"+b.slideshowID+" .num").text(b.slideNum+1)};Drupal.viewsSlideshow.action=function(d){var b={value:true,text:""};if(typeof d.action=="undefined"||d.action==""){b.value=false;b.text=Drupal.t("There was no action specified.");return error}if(d.action=="pause"){Drupal.settings.viewsSlideshow[d.slideshowID].paused=1}else{if(d.action=="play"){Drupal.settings.viewsSlideshow[d.slideshowID].paused=0}}switch(d.action){case"goToSlide":case"transitionBegin":case"transitionEnd":if(typeof d.slideNum=="undefined"||typeof d.slideNum!=="number"||parseInt(d.slideNum)!=(d.slideNum-0)){b.value=false;b.text=Drupal.t("An invalid integer was specified for slideNum.")}case"pause":case"play":case"nextSlide":case"previousSlide":var c=Drupal.settings.viewsSlideshow[d.slideshowID]["methods"];var f={};if(typeof d.excludeMethods!=="undefined"){for(var e=0;e<excludeMethods.length;e++){f[excludeMethods[e]]=""}}for(e=0;e<c[d.action].length;e++){if(Drupal[c[d.action][e]]!=undefined&&typeof Drupal[c[d.action][e]][d.action]=="function"&&!(c[d.action][e] in f)){Drupal[c[d.action][e]][d.action](d)}}break;default:b.value=false;b.text=Drupal.t('An invalid action "!action" was specified.',{"!action":d.action})}return b}})(jQuery);;
/*!
 * jQuery Cycle Plugin (with Transition Definitions)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version: 2.9995 (09-AUG-2011)
 * Dual licensed under the MIT and GPL licenses.
 * http://jquery.malsup.com/license.html
 * Requires: jQuery v1.3.2 or later
 */
;(function($) {

var ver = '2.9995';

// if $.support is not defined (pre jQuery 1.3) add what I need
if ($.support == undefined) {
	$.support = {
		opacity: !($.browser.msie)
	};
}

function debug(s) {
	$.fn.cycle.debug && log(s);
}		
function log() {
	window.console && console.log && console.log('[cycle] ' + Array.prototype.join.call(arguments,' '));
}
$.expr[':'].paused = function(el) {
	return el.cyclePause;
}


// the options arg can be...
//   a number  - indicates an immediate transition should occur to the given slide index
//   a string  - 'pause', 'resume', 'toggle', 'next', 'prev', 'stop', 'destroy' or the name of a transition effect (ie, 'fade', 'zoom', etc)
//   an object - properties to control the slideshow
//
// the arg2 arg can be...
//   the name of an fx (only used in conjunction with a numeric value for 'options')
//   the value true (only used in first arg == 'resume') and indicates
//	 that the resume should occur immediately (not wait for next timeout)

$.fn.cycle = function(options, arg2) {
	var o = { s: this.selector, c: this.context };

	// in 1.3+ we can fix mistakes with the ready state
	if (this.length === 0 && options != 'stop') {
		if (!$.isReady && o.s) {
			log('DOM not ready, queuing slideshow');
			$(function() {
				$(o.s,o.c).cycle(options,arg2);
			});
			return this;
		}
		// is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
		log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
		return this;
	}

	// iterate the matched nodeset
	return this.each(function() {
		var opts = handleArguments(this, options, arg2);
		if (opts === false)
			return;

		opts.updateActivePagerLink = opts.updateActivePagerLink || $.fn.cycle.updateActivePagerLink;
		
		// stop existing slideshow for this container (if there is one)
		if (this.cycleTimeout)
			clearTimeout(this.cycleTimeout);
		this.cycleTimeout = this.cyclePause = 0;

		var $cont = $(this);
		var $slides = opts.slideExpr ? $(opts.slideExpr, this) : $cont.children();
		var els = $slides.get();

		var opts2 = buildOptions($cont, $slides, els, opts, o);
		if (opts2 === false)
			return;

		if (els.length < 2) {
			log('terminating; too few slides: ' + els.length);
			return;
		}

		var startTime = opts2.continuous ? 10 : getTimeout(els[opts2.currSlide], els[opts2.nextSlide], opts2, !opts2.backwards);

		// if it's an auto slideshow, kick it off
		if (startTime) {
			startTime += (opts2.delay || 0);
			if (startTime < 10)
				startTime = 10;
			debug('first timeout: ' + startTime);
			this.cycleTimeout = setTimeout(function(){go(els,opts2,0,!opts.backwards)}, startTime);
		}
	});
};

function triggerPause(cont, byHover, onPager) {
	var opts = $(cont).data('cycle.opts');
	var paused = !!cont.cyclePause;
	if (paused && opts.paused)
		opts.paused(cont, opts, byHover, onPager);
	else if (!paused && opts.resumed)
		opts.resumed(cont, opts, byHover, onPager);
}

// process the args that were passed to the plugin fn
function handleArguments(cont, options, arg2) {
	if (cont.cycleStop == undefined)
		cont.cycleStop = 0;
	if (options === undefined || options === null)
		options = {};
	if (options.constructor == String) {
		switch(options) {
		case 'destroy':
		case 'stop':
			var opts = $(cont).data('cycle.opts');
			if (!opts)
				return false;
			cont.cycleStop++; // callbacks look for change
			if (cont.cycleTimeout)
				clearTimeout(cont.cycleTimeout);
			cont.cycleTimeout = 0;
			opts.elements && $(opts.elements).stop();
			$(cont).removeData('cycle.opts');
			if (options == 'destroy')
				destroy(opts);
			return false;
		case 'toggle':
			cont.cyclePause = (cont.cyclePause === 1) ? 0 : 1;
			checkInstantResume(cont.cyclePause, arg2, cont);
			triggerPause(cont);
			return false;
		case 'pause':
			cont.cyclePause = 1;
			triggerPause(cont);
			return false;
		case 'resume':
			cont.cyclePause = 0;
			checkInstantResume(false, arg2, cont);
			triggerPause(cont);
			return false;
		case 'prev':
		case 'next':
			var opts = $(cont).data('cycle.opts');
			if (!opts) {
				log('options not found, "prev/next" ignored');
				return false;
			}
			$.fn.cycle[options](opts);
			return false;
		default:
			options = { fx: options };
		};
		return options;
	}
	else if (options.constructor == Number) {
		// go to the requested slide
		var num = options;
		options = $(cont).data('cycle.opts');
		if (!options) {
			log('options not found, can not advance slide');
			return false;
		}
		if (num < 0 || num >= options.elements.length) {
			log('invalid slide index: ' + num);
			return false;
		}
		options.nextSlide = num;
		if (cont.cycleTimeout) {
			clearTimeout(cont.cycleTimeout);
			cont.cycleTimeout = 0;
		}
		if (typeof arg2 == 'string')
			options.oneTimeFx = arg2;
		go(options.elements, options, 1, num >= options.currSlide);
		return false;
	}
	return options;
	
	function checkInstantResume(isPaused, arg2, cont) {
		if (!isPaused && arg2 === true) { // resume now!
			var options = $(cont).data('cycle.opts');
			if (!options) {
				log('options not found, can not resume');
				return false;
			}
			if (cont.cycleTimeout) {
				clearTimeout(cont.cycleTimeout);
				cont.cycleTimeout = 0;
			}
			go(options.elements, options, 1, !options.backwards);
		}
	}
};

function removeFilter(el, opts) {
	if (!$.support.opacity && opts.cleartype && el.style.filter) {
		try { el.style.removeAttribute('filter'); }
		catch(smother) {} // handle old opera versions
	}
};

// unbind event handlers
function destroy(opts) {
	if (opts.next)
		$(opts.next).unbind(opts.prevNextEvent);
	if (opts.prev)
		$(opts.prev).unbind(opts.prevNextEvent);
	
	if (opts.pager || opts.pagerAnchorBuilder)
		$.each(opts.pagerAnchors || [], function() {
			this.unbind().remove();
		});
	opts.pagerAnchors = null;
	if (opts.destroy) // callback
		opts.destroy(opts);
};

// one-time initialization
function buildOptions($cont, $slides, els, options, o) {
	// support metadata plugin (v1.0 and v2.0)
	var opts = $.extend({}, $.fn.cycle.defaults, options || {}, $.metadata ? $cont.metadata() : $.meta ? $cont.data() : {});
	var meta = $.isFunction($cont.data) ? $cont.data(opts.metaAttr) : null;
	if (meta)
		opts = $.extend(opts, meta);
	if (opts.autostop)
		opts.countdown = opts.autostopCount || els.length;

	var cont = $cont[0];
	$cont.data('cycle.opts', opts);
	opts.$cont = $cont;
	opts.stopCount = cont.cycleStop;
	opts.elements = els;
	opts.before = opts.before ? [opts.before] : [];
	opts.after = opts.after ? [opts.after] : [];

	// push some after callbacks
	if (!$.support.opacity && opts.cleartype)
		opts.after.push(function() { removeFilter(this, opts); });
	if (opts.continuous)
		opts.after.push(function() { go(els,opts,0,!opts.backwards); });

	saveOriginalOpts(opts);

	// clearType corrections
	if (!$.support.opacity && opts.cleartype && !opts.cleartypeNoBg)
		clearTypeFix($slides);

	// container requires non-static position so that slides can be position within
	if ($cont.css('position') == 'static')
		$cont.css('position', 'relative');
	if (opts.width)
		$cont.width(opts.width);
	if (opts.height && opts.height != 'auto')
		$cont.height(opts.height);

	if (opts.startingSlide)
		opts.startingSlide = parseInt(opts.startingSlide,10);
	else if (opts.backwards)
		opts.startingSlide = els.length - 1;

	// if random, mix up the slide array
	if (opts.random) {
		opts.randomMap = [];
		for (var i = 0; i < els.length; i++)
			opts.randomMap.push(i);
		opts.randomMap.sort(function(a,b) {return Math.random() - 0.5;});
		opts.randomIndex = 1;
		opts.startingSlide = opts.randomMap[1];
	}
	else if (opts.startingSlide >= els.length)
		opts.startingSlide = 0; // catch bogus input
	opts.currSlide = opts.startingSlide || 0;
	var first = opts.startingSlide;

	// set position and zIndex on all the slides
	$slides.css({position: 'absolute', top:0, left:0}).hide().each(function(i) {
		var z;
		if (opts.backwards)
			z = first ? i <= first ? els.length + (i-first) : first-i : els.length-i;
		else
			z = first ? i >= first ? els.length - (i-first) : first-i : els.length-i;
		$(this).css('z-index', z)
	});

	// make sure first slide is visible
	$(els[first]).css('opacity',1).show(); // opacity bit needed to handle restart use case
	removeFilter(els[first], opts);

	// stretch slides
	if (opts.fit) {
		if (!opts.aspect) {
	        if (opts.width)
	            $slides.width(opts.width);
	        if (opts.height && opts.height != 'auto')
	            $slides.height(opts.height);
		} else {
			$slides.each(function(){
				var $slide = $(this);
				var ratio = (opts.aspect === true) ? $slide.width()/$slide.height() : opts.aspect;
				if( opts.width && $slide.width() != opts.width ) {
					$slide.width( opts.width );
					$slide.height( opts.width / ratio );
				}

				if( opts.height && $slide.height() < opts.height ) {
					$slide.height( opts.height );
					$slide.width( opts.height * ratio );
				}
			});
		}
	}

	if (opts.center && ((!opts.fit) || opts.aspect)) {
		$slides.each(function(){
			var $slide = $(this);
			$slide.css({
				"margin-left": opts.width ?
					((opts.width - $slide.width()) / 2) + "px" :
					0,
				"margin-top": opts.height ?
					((opts.height - $slide.height()) / 2) + "px" :
					0
			});
		});
	}

	if (opts.center && !opts.fit && !opts.slideResize) {
	  	$slides.each(function(){
	    	var $slide = $(this);
	    	$slide.css({
	      		"margin-left": opts.width ? ((opts.width - $slide.width()) / 2) + "px" : 0,
	      		"margin-top": opts.height ? ((opts.height - $slide.height()) / 2) + "px" : 0
	    	});
	  	});
	}
		
	// stretch container
	var reshape = opts.containerResize && !$cont.innerHeight();
	if (reshape) { // do this only if container has no size http://tinyurl.com/da2oa9
		var maxw = 0, maxh = 0;
		for(var j=0; j < els.length; j++) {
			var $e = $(els[j]), e = $e[0], w = $e.outerWidth(), h = $e.outerHeight();
			if (!w) w = e.offsetWidth || e.width || $e.attr('width');
			if (!h) h = e.offsetHeight || e.height || $e.attr('height');
			maxw = w > maxw ? w : maxw;
			maxh = h > maxh ? h : maxh;
		}
		if (maxw > 0 && maxh > 0)
			$cont.css({width:maxw+'px',height:maxh+'px'});
	}

	var pauseFlag = false;  // https://github.com/malsup/cycle/issues/44
	if (opts.pause)
		$cont.hover(
			function(){
				pauseFlag = true;
				this.cyclePause++;
				triggerPause(cont, true);
			},
			function(){
				pauseFlag && this.cyclePause--;
				triggerPause(cont, true);
			}
		);

	if (supportMultiTransitions(opts) === false)
		return false;

	// apparently a lot of people use image slideshows without height/width attributes on the images.
	// Cycle 2.50+ requires the sizing info for every slide; this block tries to deal with that.
	var requeue = false;
	options.requeueAttempts = options.requeueAttempts || 0;
	$slides.each(function() {
		// try to get height/width of each slide
		var $el = $(this);
		this.cycleH = (opts.fit && opts.height) ? opts.height : ($el.height() || this.offsetHeight || this.height || $el.attr('height') || 0);
		this.cycleW = (opts.fit && opts.width) ? opts.width : ($el.width() || this.offsetWidth || this.width || $el.attr('width') || 0);

		if ( $el.is('img') ) {
			// sigh..  sniffing, hacking, shrugging...  this crappy hack tries to account for what browsers do when
			// an image is being downloaded and the markup did not include sizing info (height/width attributes);
			// there seems to be some "default" sizes used in this situation
			var loadingIE	= ($.browser.msie  && this.cycleW == 28 && this.cycleH == 30 && !this.complete);
			var loadingFF	= ($.browser.mozilla && this.cycleW == 34 && this.cycleH == 19 && !this.complete);
			var loadingOp	= ($.browser.opera && ((this.cycleW == 42 && this.cycleH == 19) || (this.cycleW == 37 && this.cycleH == 17)) && !this.complete);
			var loadingOther = (this.cycleH == 0 && this.cycleW == 0 && !this.complete);
			// don't requeue for images that are still loading but have a valid size
			if (loadingIE || loadingFF || loadingOp || loadingOther) {
				if (o.s && opts.requeueOnImageNotLoaded && ++options.requeueAttempts < 100) { // track retry count so we don't loop forever
					log(options.requeueAttempts,' - img slide not loaded, requeuing slideshow: ', this.src, this.cycleW, this.cycleH);
					setTimeout(function() {$(o.s,o.c).cycle(options)}, opts.requeueTimeout);
					requeue = true;
					return false; // break each loop
				}
				else {
					log('could not determine size of image: '+this.src, this.cycleW, this.cycleH);
				}
			}
		}
		return true;
	});

	if (requeue)
		return false;

	opts.cssBefore = opts.cssBefore || {};
	opts.cssAfter = opts.cssAfter || {};
	opts.cssFirst = opts.cssFirst || {};
	opts.animIn = opts.animIn || {};
	opts.animOut = opts.animOut || {};

	$slides.not(':eq('+first+')').css(opts.cssBefore);
	$($slides[first]).css(opts.cssFirst);

	if (opts.timeout) {
		opts.timeout = parseInt(opts.timeout,10);
		// ensure that timeout and speed settings are sane
		if (opts.speed.constructor == String)
			opts.speed = $.fx.speeds[opts.speed] || parseInt(opts.speed,10);
		if (!opts.sync)
			opts.speed = opts.speed / 2;
		
		var buffer = opts.fx == 'none' ? 0 : opts.fx == 'shuffle' ? 500 : 250;
		while((opts.timeout - opts.speed) < buffer) // sanitize timeout
			opts.timeout += opts.speed;
	}
	if (opts.easing)
		opts.easeIn = opts.easeOut = opts.easing;
	if (!opts.speedIn)
		opts.speedIn = opts.speed;
	if (!opts.speedOut)
		opts.speedOut = opts.speed;

	opts.slideCount = els.length;
	opts.currSlide = opts.lastSlide = first;
	if (opts.random) {
		if (++opts.randomIndex == els.length)
			opts.randomIndex = 0;
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else if (opts.backwards)
		opts.nextSlide = opts.startingSlide == 0 ? (els.length-1) : opts.startingSlide-1;
	else
		opts.nextSlide = opts.startingSlide >= (els.length-1) ? 0 : opts.startingSlide+1;

	// run transition init fn
	if (!opts.multiFx) {
		var init = $.fn.cycle.transitions[opts.fx];
		if ($.isFunction(init))
			init($cont, $slides, opts);
		else if (opts.fx != 'custom' && !opts.multiFx) {
			log('unknown transition: ' + opts.fx,'; slideshow terminating');
			return false;
		}
	}

	// fire artificial events
	var e0 = $slides[first];
	if (!opts.skipInitializationCallbacks) {
		if (opts.before.length)
			opts.before[0].apply(e0, [e0, e0, opts, true]);
		if (opts.after.length)
			opts.after[0].apply(e0, [e0, e0, opts, true]);
	}
	if (opts.next)
		$(opts.next).bind(opts.prevNextEvent,function(){return advance(opts,1)});
	if (opts.prev)
		$(opts.prev).bind(opts.prevNextEvent,function(){return advance(opts,0)});
	if (opts.pager || opts.pagerAnchorBuilder)
		buildPager(els,opts);

	exposeAddSlide(opts, els);

	return opts;
};

// save off original opts so we can restore after clearing state
function saveOriginalOpts(opts) {
	opts.original = { before: [], after: [] };
	opts.original.cssBefore = $.extend({}, opts.cssBefore);
	opts.original.cssAfter  = $.extend({}, opts.cssAfter);
	opts.original.animIn	= $.extend({}, opts.animIn);
	opts.original.animOut   = $.extend({}, opts.animOut);
	$.each(opts.before, function() { opts.original.before.push(this); });
	$.each(opts.after,  function() { opts.original.after.push(this); });
};

function supportMultiTransitions(opts) {
	var i, tx, txs = $.fn.cycle.transitions;
	// look for multiple effects
	if (opts.fx.indexOf(',') > 0) {
		opts.multiFx = true;
		opts.fxs = opts.fx.replace(/\s*/g,'').split(',');
		// discard any bogus effect names
		for (i=0; i < opts.fxs.length; i++) {
			var fx = opts.fxs[i];
			tx = txs[fx];
			if (!tx || !txs.hasOwnProperty(fx) || !$.isFunction(tx)) {
				log('discarding unknown transition: ',fx);
				opts.fxs.splice(i,1);
				i--;
			}
		}
		// if we have an empty list then we threw everything away!
		if (!opts.fxs.length) {
			log('No valid transitions named; slideshow terminating.');
			return false;
		}
	}
	else if (opts.fx == 'all') {  // auto-gen the list of transitions
		opts.multiFx = true;
		opts.fxs = [];
		for (p in txs) {
			tx = txs[p];
			if (txs.hasOwnProperty(p) && $.isFunction(tx))
				opts.fxs.push(p);
		}
	}
	if (opts.multiFx && opts.randomizeEffects) {
		// munge the fxs array to make effect selection random
		var r1 = Math.floor(Math.random() * 20) + 30;
		for (i = 0; i < r1; i++) {
			var r2 = Math.floor(Math.random() * opts.fxs.length);
			opts.fxs.push(opts.fxs.splice(r2,1)[0]);
		}
		debug('randomized fx sequence: ',opts.fxs);
	}
	return true;
};

// provide a mechanism for adding slides after the slideshow has started
function exposeAddSlide(opts, els) {
	opts.addSlide = function(newSlide, prepend) {
		var $s = $(newSlide), s = $s[0];
		if (!opts.autostopCount)
			opts.countdown++;
		els[prepend?'unshift':'push'](s);
		if (opts.els)
			opts.els[prepend?'unshift':'push'](s); // shuffle needs this
		opts.slideCount = els.length;

		$s.css('position','absolute');
		$s[prepend?'prependTo':'appendTo'](opts.$cont);

		if (prepend) {
			opts.currSlide++;
			opts.nextSlide++;
		}

		if (!$.support.opacity && opts.cleartype && !opts.cleartypeNoBg)
			clearTypeFix($s);

		if (opts.fit && opts.width)
			$s.width(opts.width);
		if (opts.fit && opts.height && opts.height != 'auto')
			$s.height(opts.height);
		s.cycleH = (opts.fit && opts.height) ? opts.height : $s.height();
		s.cycleW = (opts.fit && opts.width) ? opts.width : $s.width();

		$s.css(opts.cssBefore);

		if (opts.pager || opts.pagerAnchorBuilder)
			$.fn.cycle.createPagerAnchor(els.length-1, s, $(opts.pager), els, opts);

		if ($.isFunction(opts.onAddSlide))
			opts.onAddSlide($s);
		else
			$s.hide(); // default behavior
	};
}

// reset internal state; we do this on every pass in order to support multiple effects
$.fn.cycle.resetState = function(opts, fx) {
	fx = fx || opts.fx;
	opts.before = []; opts.after = [];
	opts.cssBefore = $.extend({}, opts.original.cssBefore);
	opts.cssAfter  = $.extend({}, opts.original.cssAfter);
	opts.animIn	= $.extend({}, opts.original.animIn);
	opts.animOut   = $.extend({}, opts.original.animOut);
	opts.fxFn = null;
	$.each(opts.original.before, function() { opts.before.push(this); });
	$.each(opts.original.after,  function() { opts.after.push(this); });

	// re-init
	var init = $.fn.cycle.transitions[fx];
	if ($.isFunction(init))
		init(opts.$cont, $(opts.elements), opts);
};

// this is the main engine fn, it handles the timeouts, callbacks and slide index mgmt
function go(els, opts, manual, fwd) {
	// opts.busy is true if we're in the middle of an animation
	if (manual && opts.busy && opts.manualTrump) {
		// let manual transitions requests trump active ones
		debug('manualTrump in go(), stopping active transition');
		$(els).stop(true,true);
		opts.busy = 0;
	}
	// don't begin another timeout-based transition if there is one active
	if (opts.busy) {
		debug('transition active, ignoring new tx request');
		return;
	}

	var p = opts.$cont[0], curr = els[opts.currSlide], next = els[opts.nextSlide];

	// stop cycling if we have an outstanding stop request
	if (p.cycleStop != opts.stopCount || p.cycleTimeout === 0 && !manual)
		return;

	// check to see if we should stop cycling based on autostop options
	if (!manual && !p.cyclePause && !opts.bounce &&
		((opts.autostop && (--opts.countdown <= 0)) ||
		(opts.nowrap && !opts.random && opts.nextSlide < opts.currSlide))) {
		if (opts.end)
			opts.end(opts);
		return;
	}

	// if slideshow is paused, only transition on a manual trigger
	var changed = false;
	if ((manual || !p.cyclePause) && (opts.nextSlide != opts.currSlide)) {
		changed = true;
		var fx = opts.fx;
		// keep trying to get the slide size if we don't have it yet
		curr.cycleH = curr.cycleH || $(curr).height();
		curr.cycleW = curr.cycleW || $(curr).width();
		next.cycleH = next.cycleH || $(next).height();
		next.cycleW = next.cycleW || $(next).width();

		// support multiple transition types
		if (opts.multiFx) {
			if (fwd && (opts.lastFx == undefined || ++opts.lastFx >= opts.fxs.length))
				opts.lastFx = 0;
			else if (!fwd && (opts.lastFx == undefined || --opts.lastFx < 0))
				opts.lastFx = opts.fxs.length - 1;
			fx = opts.fxs[opts.lastFx];
		}

		// one-time fx overrides apply to:  $('div').cycle(3,'zoom');
		if (opts.oneTimeFx) {
			fx = opts.oneTimeFx;
			opts.oneTimeFx = null;
		}

		$.fn.cycle.resetState(opts, fx);

		// run the before callbacks
		if (opts.before.length)
			$.each(opts.before, function(i,o) {
				if (p.cycleStop != opts.stopCount) return;
				o.apply(next, [curr, next, opts, fwd]);
			});

		// stage the after callacks
		var after = function() {
			opts.busy = 0;
			$.each(opts.after, function(i,o) {
				if (p.cycleStop != opts.stopCount) return;
				o.apply(next, [curr, next, opts, fwd]);
			});
		};

		debug('tx firing('+fx+'); currSlide: ' + opts.currSlide + '; nextSlide: ' + opts.nextSlide);
		
		// get ready to perform the transition
		opts.busy = 1;
		if (opts.fxFn) // fx function provided?
			opts.fxFn(curr, next, opts, after, fwd, manual && opts.fastOnEvent);
		else if ($.isFunction($.fn.cycle[opts.fx])) // fx plugin ?
			$.fn.cycle[opts.fx](curr, next, opts, after, fwd, manual && opts.fastOnEvent);
		else
			$.fn.cycle.custom(curr, next, opts, after, fwd, manual && opts.fastOnEvent);
	}

	if (changed || opts.nextSlide == opts.currSlide) {
		// calculate the next slide
		opts.lastSlide = opts.currSlide;
		if (opts.random) {
			opts.currSlide = opts.nextSlide;
			if (++opts.randomIndex == els.length)
				opts.randomIndex = 0;
			opts.nextSlide = opts.randomMap[opts.randomIndex];
			if (opts.nextSlide == opts.currSlide)
				opts.nextSlide = (opts.currSlide == opts.slideCount - 1) ? 0 : opts.currSlide + 1;
		}
		else if (opts.backwards) {
			var roll = (opts.nextSlide - 1) < 0;
			if (roll && opts.bounce) {
				opts.backwards = !opts.backwards;
				opts.nextSlide = 1;
				opts.currSlide = 0;
			}
			else {
				opts.nextSlide = roll ? (els.length-1) : opts.nextSlide-1;
				opts.currSlide = roll ? 0 : opts.nextSlide+1;
			}
		}
		else { // sequence
			var roll = (opts.nextSlide + 1) == els.length;
			if (roll && opts.bounce) {
				opts.backwards = !opts.backwards;
				opts.nextSlide = els.length-2;
				opts.currSlide = els.length-1;
			}
			else {
				opts.nextSlide = roll ? 0 : opts.nextSlide+1;
				opts.currSlide = roll ? els.length-1 : opts.nextSlide-1;
			}
		}
	}
	if (changed && opts.pager)
		opts.updateActivePagerLink(opts.pager, opts.currSlide, opts.activePagerClass);
	
	// stage the next transition
	var ms = 0;
	if (opts.timeout && !opts.continuous)
		ms = getTimeout(els[opts.currSlide], els[opts.nextSlide], opts, fwd);
	else if (opts.continuous && p.cyclePause) // continuous shows work off an after callback, not this timer logic
		ms = 10;
	if (ms > 0)
		p.cycleTimeout = setTimeout(function(){ go(els, opts, 0, !opts.backwards) }, ms);
};

// invoked after transition
$.fn.cycle.updateActivePagerLink = function(pager, currSlide, clsName) {
   $(pager).each(function() {
       $(this).children().removeClass(clsName).eq(currSlide).addClass(clsName);
   });
};

// calculate timeout value for current transition
function getTimeout(curr, next, opts, fwd) {
	if (opts.timeoutFn) {
		// call user provided calc fn
		var t = opts.timeoutFn.call(curr,curr,next,opts,fwd);
		while (opts.fx != 'none' && (t - opts.speed) < 250) // sanitize timeout
			t += opts.speed;
		debug('calculated timeout: ' + t + '; speed: ' + opts.speed);
		if (t !== false)
			return t;
	}
	return opts.timeout;
};

// expose next/prev function, caller must pass in state
$.fn.cycle.next = function(opts) { advance(opts,1); };
$.fn.cycle.prev = function(opts) { advance(opts,0);};

// advance slide forward or back
function advance(opts, moveForward) {
	var val = moveForward ? 1 : -1;
	var els = opts.elements;
	var p = opts.$cont[0], timeout = p.cycleTimeout;
	if (timeout) {
		clearTimeout(timeout);
		p.cycleTimeout = 0;
	}
	if (opts.random && val < 0) {
		// move back to the previously display slide
		opts.randomIndex--;
		if (--opts.randomIndex == -2)
			opts.randomIndex = els.length-2;
		else if (opts.randomIndex == -1)
			opts.randomIndex = els.length-1;
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else if (opts.random) {
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else {
		opts.nextSlide = opts.currSlide + val;
		if (opts.nextSlide < 0) {
			if (opts.nowrap) return false;
			opts.nextSlide = els.length - 1;
		}
		else if (opts.nextSlide >= els.length) {
			if (opts.nowrap) return false;
			opts.nextSlide = 0;
		}
	}

	var cb = opts.onPrevNextEvent || opts.prevNextClick; // prevNextClick is deprecated
	if ($.isFunction(cb))
		cb(val > 0, opts.nextSlide, els[opts.nextSlide]);
	go(els, opts, 1, moveForward);
	return false;
};

function buildPager(els, opts) {
	var $p = $(opts.pager);
	$.each(els, function(i,o) {
		$.fn.cycle.createPagerAnchor(i,o,$p,els,opts);
	});
	opts.updateActivePagerLink(opts.pager, opts.startingSlide, opts.activePagerClass);
};

$.fn.cycle.createPagerAnchor = function(i, el, $p, els, opts) {
	var a;
	if ($.isFunction(opts.pagerAnchorBuilder)) {
		a = opts.pagerAnchorBuilder(i,el);
		debug('pagerAnchorBuilder('+i+', el) returned: ' + a);
	}
	else
		a = '<a href="#">'+(i+1)+'</a>';
		
	if (!a)
		return;
	var $a = $(a);
	// don't reparent if anchor is in the dom
	if ($a.parents('body').length === 0) {
		var arr = [];
		if ($p.length > 1) {
			$p.each(function() {
				var $clone = $a.clone(true);
				$(this).append($clone);
				arr.push($clone[0]);
			});
			$a = $(arr);
		}
		else {
			$a.appendTo($p);
		}
	}

	opts.pagerAnchors =  opts.pagerAnchors || [];
	opts.pagerAnchors.push($a);
	
	var pagerFn = function(e) {
		e.preventDefault();
		opts.nextSlide = i;
		var p = opts.$cont[0], timeout = p.cycleTimeout;
		if (timeout) {
			clearTimeout(timeout);
			p.cycleTimeout = 0;
		}
		var cb = opts.onPagerEvent || opts.pagerClick; // pagerClick is deprecated
		if ($.isFunction(cb))
			cb(opts.nextSlide, els[opts.nextSlide]);
		go(els,opts,1,opts.currSlide < i); // trigger the trans
//		return false; // <== allow bubble
	}
	
	if ( /mouseenter|mouseover/i.test(opts.pagerEvent) ) {
		$a.hover(pagerFn, function(){/* no-op */} );
	}
	else {
		$a.bind(opts.pagerEvent, pagerFn);
	}
	
	if ( ! /^click/.test(opts.pagerEvent) && !opts.allowPagerClickBubble)
		$a.bind('click.cycle', function(){return false;}); // suppress click
	
	var cont = opts.$cont[0];
	var pauseFlag = false; // https://github.com/malsup/cycle/issues/44
	if (opts.pauseOnPagerHover) {
		$a.hover(
			function() { 
				pauseFlag = true;
				cont.cyclePause++; 
				triggerPause(cont,true,true);
			}, function() { 
				pauseFlag && cont.cyclePause--; 
				triggerPause(cont,true,true);
			} 
		);
	}
};

// helper fn to calculate the number of slides between the current and the next
$.fn.cycle.hopsFromLast = function(opts, fwd) {
	var hops, l = opts.lastSlide, c = opts.currSlide;
	if (fwd)
		hops = c > l ? c - l : opts.slideCount - l;
	else
		hops = c < l ? l - c : l + opts.slideCount - c;
	return hops;
};

// fix clearType problems in ie6 by setting an explicit bg color
// (otherwise text slides look horrible during a fade transition)
function clearTypeFix($slides) {
	debug('applying clearType background-color hack');
	function hex(s) {
		s = parseInt(s,10).toString(16);
		return s.length < 2 ? '0'+s : s;
	};
	function getBg(e) {
		for ( ; e && e.nodeName.toLowerCase() != 'html'; e = e.parentNode) {
			var v = $.css(e,'background-color');
			if (v && v.indexOf('rgb') >= 0 ) {
				var rgb = v.match(/\d+/g);
				return '#'+ hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]);
			}
			if (v && v != 'transparent')
				return v;
		}
		return '#ffffff';
	};
	$slides.each(function() { $(this).css('background-color', getBg(this)); });
};

// reset common props before the next transition
$.fn.cycle.commonReset = function(curr,next,opts,w,h,rev) {
	$(opts.elements).not(curr).hide();
	if (typeof opts.cssBefore.opacity == 'undefined')
		opts.cssBefore.opacity = 1;
	opts.cssBefore.display = 'block';
	if (opts.slideResize && w !== false && next.cycleW > 0)
		opts.cssBefore.width = next.cycleW;
	if (opts.slideResize && h !== false && next.cycleH > 0)
		opts.cssBefore.height = next.cycleH;
	opts.cssAfter = opts.cssAfter || {};
	opts.cssAfter.display = 'none';
	$(curr).css('zIndex',opts.slideCount + (rev === true ? 1 : 0));
	$(next).css('zIndex',opts.slideCount + (rev === true ? 0 : 1));
};

// the actual fn for effecting a transition
$.fn.cycle.custom = function(curr, next, opts, cb, fwd, speedOverride) {
	var $l = $(curr), $n = $(next);
	var speedIn = opts.speedIn, speedOut = opts.speedOut, easeIn = opts.easeIn, easeOut = opts.easeOut;
	$n.css(opts.cssBefore);
	if (speedOverride) {
		if (typeof speedOverride == 'number')
			speedIn = speedOut = speedOverride;
		else
			speedIn = speedOut = 1;
		easeIn = easeOut = null;
	}
	var fn = function() {
		$n.animate(opts.animIn, speedIn, easeIn, function() {
			cb();
		});
	};
	$l.animate(opts.animOut, speedOut, easeOut, function() {
		$l.css(opts.cssAfter);
		if (!opts.sync) 
			fn();
	});
	if (opts.sync) fn();
};

// transition definitions - only fade is defined here, transition pack defines the rest
$.fn.cycle.transitions = {
	fade: function($cont, $slides, opts) {
		$slides.not(':eq('+opts.currSlide+')').css('opacity',0);
		opts.before.push(function(curr,next,opts) {
			$.fn.cycle.commonReset(curr,next,opts);
			opts.cssBefore.opacity = 0;
		});
		opts.animIn	   = { opacity: 1 };
		opts.animOut   = { opacity: 0 };
		opts.cssBefore = { top: 0, left: 0 };
	}
};

$.fn.cycle.ver = function() { return ver; };

// override these globally if you like (they are all optional)
$.fn.cycle.defaults = {
	activePagerClass: 'activeSlide', // class name used for the active pager link
	after:		   null,  // transition callback (scope set to element that was shown):  function(currSlideElement, nextSlideElement, options, forwardFlag)
	allowPagerClickBubble: false, // allows or prevents click event on pager anchors from bubbling
	animIn:		   null,  // properties that define how the slide animates in
	animOut:	   null,  // properties that define how the slide animates out
	aspect:		   false,  // preserve aspect ratio during fit resizing, cropping if necessary (must be used with fit option)
	autostop:	   0,	  // true to end slideshow after X transitions (where X == slide count)
	autostopCount: 0,	  // number of transitions (optionally used with autostop to define X)
	backwards:     false, // true to start slideshow at last slide and move backwards through the stack
	before:		   null,  // transition callback (scope set to element to be shown):	 function(currSlideElement, nextSlideElement, options, forwardFlag)
	center: 	   null,  // set to true to have cycle add top/left margin to each slide (use with width and height options)
	cleartype:	   !$.support.opacity,  // true if clearType corrections should be applied (for IE)
	cleartypeNoBg: false, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
	containerResize: 1,	  // resize container to fit largest slide
	continuous:	   0,	  // true to start next transition immediately after current one completes
	cssAfter:	   null,  // properties that defined the state of the slide after transitioning out
	cssBefore:	   null,  // properties that define the initial state of the slide before transitioning in
	delay:		   0,	  // additional delay (in ms) for first transition (hint: can be negative)
	easeIn:		   null,  // easing for "in" transition
	easeOut:	   null,  // easing for "out" transition
	easing:		   null,  // easing method for both in and out transitions
	end:		   null,  // callback invoked when the slideshow terminates (use with autostop or nowrap options): function(options)
	fastOnEvent:   0,	  // force fast transitions when triggered manually (via pager or prev/next); value == time in ms
	fit:		   0,	  // force slides to fit container
	fx:			  'fade', // name of transition effect (or comma separated names, ex: 'fade,scrollUp,shuffle')
	fxFn:		   null,  // function used to control the transition: function(currSlideElement, nextSlideElement, options, afterCalback, forwardFlag)
	height:		  'auto', // container height (if the 'fit' option is true, the slides will be set to this height as well)
	manualTrump:   true,  // causes manual transition to stop an active transition instead of being ignored
	metaAttr:     'cycle',// data- attribute that holds the option data for the slideshow
	next:		   null,  // element, jQuery object, or jQuery selector string for the element to use as event trigger for next slide
	nowrap:		   0,	  // true to prevent slideshow from wrapping
	onPagerEvent:  null,  // callback fn for pager events: function(zeroBasedSlideIndex, slideElement)
	onPrevNextEvent: null,// callback fn for prev/next events: function(isNext, zeroBasedSlideIndex, slideElement)
	pager:		   null,  // element, jQuery object, or jQuery selector string for the element to use as pager container
	pagerAnchorBuilder: null, // callback fn for building anchor links:  function(index, DOMelement)
	pagerEvent:	  'click.cycle', // name of event which drives the pager navigation
	pause:		   0,	  // true to enable "pause on hover"
	pauseOnPagerHover: 0, // true to pause when hovering over pager link
	prev:		   null,  // element, jQuery object, or jQuery selector string for the element to use as event trigger for previous slide
	prevNextEvent:'click.cycle',// event which drives the manual transition to the previous or next slide
	random:		   0,	  // true for random, false for sequence (not applicable to shuffle fx)
	randomizeEffects: 1,  // valid when multiple effects are used; true to make the effect sequence random
	requeueOnImageNotLoaded: true, // requeue the slideshow if any image slides are not yet loaded
	requeueTimeout: 250,  // ms delay for requeue
	rev:		   0,	  // causes animations to transition in reverse (for effects that support it such as scrollHorz/scrollVert/shuffle)
	shuffle:	   null,  // coords for shuffle animation, ex: { top:15, left: 200 }
	skipInitializationCallbacks: false, // set to true to disable the first before/after callback that occurs prior to any transition
	slideExpr:	   null,  // expression for selecting slides (if something other than all children is required)
	slideResize:   1,     // force slide width/height to fixed size before every transition
	speed:		   1000,  // speed of the transition (any valid fx speed value)
	speedIn:	   null,  // speed of the 'in' transition
	speedOut:	   null,  // speed of the 'out' transition
	startingSlide: 0,	  // zero-based index of the first slide to be displayed
	sync:		   1,	  // true if in/out transitions should occur simultaneously
	timeout:	   4000,  // milliseconds between slide transitions (0 to disable auto advance)
	timeoutFn:     null,  // callback for determining per-slide timeout value:  function(currSlideElement, nextSlideElement, options, forwardFlag)
	updateActivePagerLink: null, // callback fn invoked to update the active pager link (adds/removes activePagerClass style)
	width:         null   // container width (if the 'fit' option is true, the slides will be set to this width as well)
};

})(jQuery);


/*!
 * jQuery Cycle Plugin Transition Definitions
 * This script is a plugin for the jQuery Cycle Plugin
 * Examples and documentation at: http://malsup.com/jquery/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version:	 2.73
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
(function($) {

//
// These functions define slide initialization and properties for the named
// transitions. To save file size feel free to remove any of these that you
// don't need.
//
$.fn.cycle.transitions.none = function($cont, $slides, opts) {
	opts.fxFn = function(curr,next,opts,after){
		$(next).show();
		$(curr).hide();
		after();
	};
};

// not a cross-fade, fadeout only fades out the top slide
$.fn.cycle.transitions.fadeout = function($cont, $slides, opts) {
	$slides.not(':eq('+opts.currSlide+')').css({ display: 'block', 'opacity': 1 });
	opts.before.push(function(curr,next,opts,w,h,rev) {
		$(curr).css('zIndex',opts.slideCount + (!rev === true ? 1 : 0));
		$(next).css('zIndex',opts.slideCount + (!rev === true ? 0 : 1));
	});
	opts.animIn.opacity = 1;
	opts.animOut.opacity = 0;
	opts.cssBefore.opacity = 1;
	opts.cssBefore.display = 'block';
	opts.cssAfter.zIndex = 0;
};

// scrollUp/Down/Left/Right
$.fn.cycle.transitions.scrollUp = function($cont, $slides, opts) {
	$cont.css('overflow','hidden');
	opts.before.push($.fn.cycle.commonReset);
	var h = $cont.height();
	opts.cssBefore.top = h;
	opts.cssBefore.left = 0;
	opts.cssFirst.top = 0;
	opts.animIn.top = 0;
	opts.animOut.top = -h;
};
$.fn.cycle.transitions.scrollDown = function($cont, $slides, opts) {
	$cont.css('overflow','hidden');
	opts.before.push($.fn.cycle.commonReset);
	var h = $cont.height();
	opts.cssFirst.top = 0;
	opts.cssBefore.top = -h;
	opts.cssBefore.left = 0;
	opts.animIn.top = 0;
	opts.animOut.top = h;
};
$.fn.cycle.transitions.scrollLeft = function($cont, $slides, opts) {
	$cont.css('overflow','hidden');
	opts.before.push($.fn.cycle.commonReset);
	var w = $cont.width();
	opts.cssFirst.left = 0;
	opts.cssBefore.left = w;
	opts.cssBefore.top = 0;
	opts.animIn.left = 0;
	opts.animOut.left = 0-w;
};
$.fn.cycle.transitions.scrollRight = function($cont, $slides, opts) {
	$cont.css('overflow','hidden');
	opts.before.push($.fn.cycle.commonReset);
	var w = $cont.width();
	opts.cssFirst.left = 0;
	opts.cssBefore.left = -w;
	opts.cssBefore.top = 0;
	opts.animIn.left = 0;
	opts.animOut.left = w;
};
$.fn.cycle.transitions.scrollHorz = function($cont, $slides, opts) {
	$cont.css('overflow','hidden').width();
	opts.before.push(function(curr, next, opts, fwd) {
		if (opts.rev)
			fwd = !fwd;
		$.fn.cycle.commonReset(curr,next,opts);
		opts.cssBefore.left = fwd ? (next.cycleW-1) : (1-next.cycleW);
		opts.animOut.left = fwd ? -curr.cycleW : curr.cycleW;
	});
	opts.cssFirst.left = 0;
	opts.cssBefore.top = 0;
	opts.animIn.left = 0;
	opts.animOut.top = 0;
};
$.fn.cycle.transitions.scrollVert = function($cont, $slides, opts) {
	$cont.css('overflow','hidden');
	opts.before.push(function(curr, next, opts, fwd) {
		if (opts.rev)
			fwd = !fwd;
		$.fn.cycle.commonReset(curr,next,opts);
		opts.cssBefore.top = fwd ? (1-next.cycleH) : (next.cycleH-1);
		opts.animOut.top = fwd ? curr.cycleH : -curr.cycleH;
	});
	opts.cssFirst.top = 0;
	opts.cssBefore.left = 0;
	opts.animIn.top = 0;
	opts.animOut.left = 0;
};

// slideX/slideY
$.fn.cycle.transitions.slideX = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$(opts.elements).not(curr).hide();
		$.fn.cycle.commonReset(curr,next,opts,false,true);
		opts.animIn.width = next.cycleW;
	});
	opts.cssBefore.left = 0;
	opts.cssBefore.top = 0;
	opts.cssBefore.width = 0;
	opts.animIn.width = 'show';
	opts.animOut.width = 0;
};
$.fn.cycle.transitions.slideY = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$(opts.elements).not(curr).hide();
		$.fn.cycle.commonReset(curr,next,opts,true,false);
		opts.animIn.height = next.cycleH;
	});
	opts.cssBefore.left = 0;
	opts.cssBefore.top = 0;
	opts.cssBefore.height = 0;
	opts.animIn.height = 'show';
	opts.animOut.height = 0;
};

// shuffle
$.fn.cycle.transitions.shuffle = function($cont, $slides, opts) {
	var i, w = $cont.css('overflow', 'visible').width();
	$slides.css({left: 0, top: 0});
	opts.before.push(function(curr,next,opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,true,true);
	});
	// only adjust speed once!
	if (!opts.speedAdjusted) {
		opts.speed = opts.speed / 2; // shuffle has 2 transitions
		opts.speedAdjusted = true;
	}
	opts.random = 0;
	opts.shuffle = opts.shuffle || {left:-w, top:15};
	opts.els = [];
	for (i=0; i < $slides.length; i++)
		opts.els.push($slides[i]);

	for (i=0; i < opts.currSlide; i++)
		opts.els.push(opts.els.shift());

	// custom transition fn (hat tip to Benjamin Sterling for this bit of sweetness!)
	opts.fxFn = function(curr, next, opts, cb, fwd) {
		if (opts.rev)
			fwd = !fwd;
		var $el = fwd ? $(curr) : $(next);
		$(next).css(opts.cssBefore);
		var count = opts.slideCount;
		$el.animate(opts.shuffle, opts.speedIn, opts.easeIn, function() {
			var hops = $.fn.cycle.hopsFromLast(opts, fwd);
			for (var k=0; k < hops; k++)
				fwd ? opts.els.push(opts.els.shift()) : opts.els.unshift(opts.els.pop());
			if (fwd) {
				for (var i=0, len=opts.els.length; i < len; i++)
					$(opts.els[i]).css('z-index', len-i+count);
			}
			else {
				var z = $(curr).css('z-index');
				$el.css('z-index', parseInt(z,10)+1+count);
			}
			$el.animate({left:0, top:0}, opts.speedOut, opts.easeOut, function() {
				$(fwd ? this : curr).hide();
				if (cb) cb();
			});
		});
	};
	$.extend(opts.cssBefore, { display: 'block', opacity: 1, top: 0, left: 0 });
};

// turnUp/Down/Left/Right
$.fn.cycle.transitions.turnUp = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,false);
		opts.cssBefore.top = next.cycleH;
		opts.animIn.height = next.cycleH;
		opts.animOut.width = next.cycleW;
	});
	opts.cssFirst.top = 0;
	opts.cssBefore.left = 0;
	opts.cssBefore.height = 0;
	opts.animIn.top = 0;
	opts.animOut.height = 0;
};
$.fn.cycle.transitions.turnDown = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,false);
		opts.animIn.height = next.cycleH;
		opts.animOut.top   = curr.cycleH;
	});
	opts.cssFirst.top = 0;
	opts.cssBefore.left = 0;
	opts.cssBefore.top = 0;
	opts.cssBefore.height = 0;
	opts.animOut.height = 0;
};
$.fn.cycle.transitions.turnLeft = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,true);
		opts.cssBefore.left = next.cycleW;
		opts.animIn.width = next.cycleW;
	});
	opts.cssBefore.top = 0;
	opts.cssBefore.width = 0;
	opts.animIn.left = 0;
	opts.animOut.width = 0;
};
$.fn.cycle.transitions.turnRight = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,true);
		opts.animIn.width = next.cycleW;
		opts.animOut.left = curr.cycleW;
	});
	$.extend(opts.cssBefore, { top: 0, left: 0, width: 0 });
	opts.animIn.left = 0;
	opts.animOut.width = 0;
};

// zoom
$.fn.cycle.transitions.zoom = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,false,true);
		opts.cssBefore.top = next.cycleH/2;
		opts.cssBefore.left = next.cycleW/2;
		$.extend(opts.animIn, { top: 0, left: 0, width: next.cycleW, height: next.cycleH });
		$.extend(opts.animOut, { width: 0, height: 0, top: curr.cycleH/2, left: curr.cycleW/2 });
	});
	opts.cssFirst.top = 0;
	opts.cssFirst.left = 0;
	opts.cssBefore.width = 0;
	opts.cssBefore.height = 0;
};

// fadeZoom
$.fn.cycle.transitions.fadeZoom = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,false);
		opts.cssBefore.left = next.cycleW/2;
		opts.cssBefore.top = next.cycleH/2;
		$.extend(opts.animIn, { top: 0, left: 0, width: next.cycleW, height: next.cycleH });
	});
	opts.cssBefore.width = 0;
	opts.cssBefore.height = 0;
	opts.animOut.opacity = 0;
};

// blindX
$.fn.cycle.transitions.blindX = function($cont, $slides, opts) {
	var w = $cont.css('overflow','hidden').width();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts);
		opts.animIn.width = next.cycleW;
		opts.animOut.left   = curr.cycleW;
	});
	opts.cssBefore.left = w;
	opts.cssBefore.top = 0;
	opts.animIn.left = 0;
	opts.animOut.left = w;
};
// blindY
$.fn.cycle.transitions.blindY = function($cont, $slides, opts) {
	var h = $cont.css('overflow','hidden').height();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts);
		opts.animIn.height = next.cycleH;
		opts.animOut.top   = curr.cycleH;
	});
	opts.cssBefore.top = h;
	opts.cssBefore.left = 0;
	opts.animIn.top = 0;
	opts.animOut.top = h;
};
// blindZ
$.fn.cycle.transitions.blindZ = function($cont, $slides, opts) {
	var h = $cont.css('overflow','hidden').height();
	var w = $cont.width();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts);
		opts.animIn.height = next.cycleH;
		opts.animOut.top   = curr.cycleH;
	});
	opts.cssBefore.top = h;
	opts.cssBefore.left = w;
	opts.animIn.top = 0;
	opts.animIn.left = 0;
	opts.animOut.top = h;
	opts.animOut.left = w;
};

// growX - grow horizontally from centered 0 width
$.fn.cycle.transitions.growX = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,true);
		opts.cssBefore.left = this.cycleW/2;
		opts.animIn.left = 0;
		opts.animIn.width = this.cycleW;
		opts.animOut.left = 0;
	});
	opts.cssBefore.top = 0;
	opts.cssBefore.width = 0;
};
// growY - grow vertically from centered 0 height
$.fn.cycle.transitions.growY = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,false);
		opts.cssBefore.top = this.cycleH/2;
		opts.animIn.top = 0;
		opts.animIn.height = this.cycleH;
		opts.animOut.top = 0;
	});
	opts.cssBefore.height = 0;
	opts.cssBefore.left = 0;
};

// curtainX - squeeze in both edges horizontally
$.fn.cycle.transitions.curtainX = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,false,true,true);
		opts.cssBefore.left = next.cycleW/2;
		opts.animIn.left = 0;
		opts.animIn.width = this.cycleW;
		opts.animOut.left = curr.cycleW/2;
		opts.animOut.width = 0;
	});
	opts.cssBefore.top = 0;
	opts.cssBefore.width = 0;
};
// curtainY - squeeze in both edges vertically
$.fn.cycle.transitions.curtainY = function($cont, $slides, opts) {
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,false,true);
		opts.cssBefore.top = next.cycleH/2;
		opts.animIn.top = 0;
		opts.animIn.height = next.cycleH;
		opts.animOut.top = curr.cycleH/2;
		opts.animOut.height = 0;
	});
	opts.cssBefore.height = 0;
	opts.cssBefore.left = 0;
};

// cover - curr slide covered by next slide
$.fn.cycle.transitions.cover = function($cont, $slides, opts) {
	var d = opts.direction || 'left';
	var w = $cont.css('overflow','hidden').width();
	var h = $cont.height();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts);
		if (d == 'right')
			opts.cssBefore.left = -w;
		else if (d == 'up')
			opts.cssBefore.top = h;
		else if (d == 'down')
			opts.cssBefore.top = -h;
		else
			opts.cssBefore.left = w;
	});
	opts.animIn.left = 0;
	opts.animIn.top = 0;
	opts.cssBefore.top = 0;
	opts.cssBefore.left = 0;
};

// uncover - curr slide moves off next slide
$.fn.cycle.transitions.uncover = function($cont, $slides, opts) {
	var d = opts.direction || 'left';
	var w = $cont.css('overflow','hidden').width();
	var h = $cont.height();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,true,true);
		if (d == 'right')
			opts.animOut.left = w;
		else if (d == 'up')
			opts.animOut.top = -h;
		else if (d == 'down')
			opts.animOut.top = h;
		else
			opts.animOut.left = -w;
	});
	opts.animIn.left = 0;
	opts.animIn.top = 0;
	opts.cssBefore.top = 0;
	opts.cssBefore.left = 0;
};

// toss - move top slide and fade away
$.fn.cycle.transitions.toss = function($cont, $slides, opts) {
	var w = $cont.css('overflow','visible').width();
	var h = $cont.height();
	opts.before.push(function(curr, next, opts) {
		$.fn.cycle.commonReset(curr,next,opts,true,true,true);
		// provide default toss settings if animOut not provided
		if (!opts.animOut.left && !opts.animOut.top)
			$.extend(opts.animOut, { left: w*2, top: -h/2, opacity: 0 });
		else
			opts.animOut.opacity = 0;
	});
	opts.cssBefore.left = 0;
	opts.cssBefore.top = 0;
	opts.animIn.left = 0;
};

// wipe - clip animation
$.fn.cycle.transitions.wipe = function($cont, $slides, opts) {
	var w = $cont.css('overflow','hidden').width();
	var h = $cont.height();
	opts.cssBefore = opts.cssBefore || {};
	var clip;
	if (opts.clip) {
		if (/l2r/.test(opts.clip))
			clip = 'rect(0px 0px '+h+'px 0px)';
		else if (/r2l/.test(opts.clip))
			clip = 'rect(0px '+w+'px '+h+'px '+w+'px)';
		else if (/t2b/.test(opts.clip))
			clip = 'rect(0px '+w+'px 0px 0px)';
		else if (/b2t/.test(opts.clip))
			clip = 'rect('+h+'px '+w+'px '+h+'px 0px)';
		else if (/zoom/.test(opts.clip)) {
			var top = parseInt(h/2,10);
			var left = parseInt(w/2,10);
			clip = 'rect('+top+'px '+left+'px '+top+'px '+left+'px)';
		}
	}

	opts.cssBefore.clip = opts.cssBefore.clip || clip || 'rect(0px 0px 0px 0px)';

	var d = opts.cssBefore.clip.match(/(\d+)/g);
	var t = parseInt(d[0],10), r = parseInt(d[1],10), b = parseInt(d[2],10), l = parseInt(d[3],10);

	opts.before.push(function(curr, next, opts) {
		if (curr == next) return;
		var $curr = $(curr), $next = $(next);
		$.fn.cycle.commonReset(curr,next,opts,true,true,false);
		opts.cssAfter.display = 'block';

		var step = 1, count = parseInt((opts.speedIn / 13),10) - 1;
		(function f() {
			var tt = t ? t - parseInt(step * (t/count),10) : 0;
			var ll = l ? l - parseInt(step * (l/count),10) : 0;
			var bb = b < h ? b + parseInt(step * ((h-b)/count || 1),10) : h;
			var rr = r < w ? r + parseInt(step * ((w-r)/count || 1),10) : w;
			$next.css({ clip: 'rect('+tt+'px '+rr+'px '+bb+'px '+ll+'px)' });
			(step++ <= count) ? setTimeout(f, 13) : $curr.css('display', 'none');
		})();
	});
	$.extend(opts.cssBefore, { display: 'block', opacity: 1, top: 0, left: 0 });
	opts.animIn	   = { left: 0 };
	opts.animOut   = { left: 0 };
};

})(jQuery);
;
(function($){Drupal.behaviors.viewsSlideshowCycle={attach:function(context){$(".views_slideshow_cycle_main:not(.viewsSlideshowCycle-processed)",context).addClass("viewsSlideshowCycle-processed").each(function(){var fullId="#"+$(this).attr("id");var settings=Drupal.settings.viewsSlideshowCycle[fullId];settings.targetId="#"+$(fullId+" :first").attr("id");settings.slideshowId=settings.targetId.replace("#views_slideshow_cycle_teaser_section_","");settings.paused=false;settings.opts={speed:settings.speed,timeout:settings.timeout,delay:settings.delay,sync:settings.sync,random:settings.random,nowrap:settings.nowrap,after:function(curr,next,opts){var slideNum=opts.currSlide;if(typeof settings.processedAfter=="undefined"||!settings.processedAfter){settings.processedAfter=1;slideNum=(typeof settings.opts.startingSlide=="undefined")?0:settings.opts.startingSlide}Drupal.viewsSlideshow.action({action:"transitionEnd",slideshowID:settings.slideshowId,slideNum:slideNum})},before:function(curr,next,opts){if(settings.remember_slide){createCookie(settings.vss_id,opts.currSlide+1,settings.remember_slide_days)}if(!settings.fixed_height){var $ht=$(this).height();$(this).parent().animate({height:$ht})}var slideNum=opts.nextSlide;if(typeof settings.processedBefore=="undefined"||!settings.processedBefore){settings.processedBefore=1;slideNum=(typeof settings.opts.startingSlide=="undefined")?0:settings.opts.startingSlide}Drupal.viewsSlideshow.action({action:"transitionBegin",slideshowID:settings.slideshowId,slideNum:slideNum})},cleartype:(settings.cleartype)?true:false,cleartypeNoBg:(settings.cleartypenobg)?true:false};if(settings.remember_slide){var startSlide=readCookie(settings.vss_id);if(startSlide==null){startSlide=0}settings.opts.startingSlide=startSlide}if(settings.effect=="none"){settings.opts.speed=1}else{settings.opts.fx=settings.effect}var hash=location.hash;if(hash){var hash=hash.replace("#","");var aHash=hash.split(";");var aHashLen=aHash.length;for(var i=0;i<aHashLen;i++){var initialInfo=aHash[i].split(":");if(settings.slideshowId==initialInfo[0]&&settings.num_divs>initialInfo[1]){settings.opts.startingSlide=parseInt(initialInfo[1])}}}if(settings.pause){$("#views_slideshow_cycle_teaser_section_"+settings.vss_id).hover(function(){Drupal.viewsSlideshow.action({action:"pause",slideshowID:settings.slideshowId})},function(){if(!settings.paused){Drupal.viewsSlideshow.action({action:"play",slideshowID:settings.slideshowId})}})}if(settings.pause_on_click){$("#views_slideshow_cycle_teaser_section_"+settings.vss_id).click(function(){Drupal.viewsSlideshow.action({action:"pause",slideshowID:settings.slideshowId})})}if(typeof JSON!="undefined"){var advancedOptions=JSON.parse(settings.advanced_options);for(var option in advancedOptions){advancedOptions[option]=$.trim(advancedOptions[option]);advancedOptions[option]=advancedOptions[option].replace(/\n/g,"");if(!isNaN(parseInt(advancedOptions[option]))){advancedOptions[option]=parseInt(advancedOptions[option])}else{if(advancedOptions[option].toLowerCase()=="true"){advancedOptions[option]=true}else{if(advancedOptions[option].toLowerCase()=="false"){advancedOptions[option]=false}}}switch(option){case"activePagerClass":case"allowPagerClickBubble":case"autostop":case"autostopCount":case"backwards":case"bounce":case"cleartype":case"cleartypeNoBg":case"containerResize":case"continuous":case"delay":case"easeIn":case"easeOut":case"easing":case"fastOnEvent":case"fit":case"fx":case"height":case"manualTrump":case"next":case"nowrap":case"pager":case"pagerEvent":case"pause":case"pauseOnPagerHover":case"prev":case"prevNextEvent":case"random":case"randomizeEffects":case"requeueOnImageNotLoaded":case"requeueTimeout":case"rev":case"slideExpr":case"slideResize":case"speed":case"speedIn":case"speedOut":case"startingSlide":case"sync":case"timeout":settings.opts[option]=advancedOptions[option];break;case"animIn":case"animOut":case"cssBefore":case"cssAfter":case"shuffle":settings.opts[option]=eval("("+advancedOptions[option]+")");break;case"after":settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){eval(advancedOptions[option])};break;case"before":settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){eval(advancedOptions[option])};break;case"end":settings.opts[option]=function(options){eval(advancedOptions[option])};break;case"fxFn":settings.opts[option]=function(currSlideElement,nextSlideElement,options,afterCalback,forwardFlag){eval(advancedOptions[option])};break;case"onPagerEvent":settings.opts[option]=function(zeroBasedSlideIndex,slideElement){eval(advancedOptions[option])};break;case"onPrevNextEvent":settings.opts[option]=function(isNext,zeroBasedSlideIndex,slideElement){eval(advancedOptions[option])};break;case"pagerAnchorBuilder":settings.opts[option]=function(index,DOMelement){eval(advancedOptions[option])};break;case"pagerClick":settings.opts[option]=function(zeroBasedSlideIndex,slideElement){eval(advancedOptions[option])};break;case"timeoutFn":settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){eval(advancedOptions[option])};break;case"updateActivePagerLink":settings.opts[option]=function(pager,currSlideIndex){eval(advancedOptions[option])};break}}}if(settings.wait_for_image_load){settings.totalImages=$(settings.targetId+" img").length;if(settings.totalImages){settings.loadedImages=0;$(settings.targetId+" img").each(function(){var $imageElement=$(this);$imageElement.bind("load",function(){Drupal.viewsSlideshowCycle.imageWait(fullId)});var imgSrc=$imageElement.attr("src");$imageElement.attr("src","");$imageElement.attr("src",imgSrc)})}else{Drupal.viewsSlideshowCycle.load(fullId)}}else{Drupal.viewsSlideshowCycle.load(fullId)}})}};Drupal.viewsSlideshowCycle=Drupal.viewsSlideshowCycle||{};Drupal.viewsSlideshowCycle.imageWait=function(fullId){if(++Drupal.settings.viewsSlideshowCycle[fullId].loadedImages==Drupal.settings.viewsSlideshowCycle[fullId].totalImages){Drupal.viewsSlideshowCycle.load(fullId)}};Drupal.viewsSlideshowCycle.load=function(fullId){var settings=Drupal.settings.viewsSlideshowCycle[fullId];$(settings.targetId).cycle(settings.opts);if(settings.start_paused){Drupal.viewsSlideshow.action({action:"pause",slideshowID:settings.slideshowId})}if(settings.pause_when_hidden){var checkPause=function(settings){var visible=viewsSlideshowCycleIsVisible(settings.targetId,settings.pause_when_hidden_type,settings.amount_allowed_visible);if(visible&&settings.paused){Drupal.viewsSlideshow.action({action:"play",slideshowID:settings.slideshowId})}else{if(!visible&&!settings.paused){Drupal.viewsSlideshow.action({action:"pause",slideshowID:settings.slideshowId})}}};$(window).scroll(function(){checkPause(settings)});$(window).resize(function(){checkPause(settings)})}};Drupal.viewsSlideshowCycle.pause=function(options){$("#views_slideshow_cycle_teaser_section_"+options.slideshowID).cycle("pause")};Drupal.viewsSlideshowCycle.play=function(options){$("#views_slideshow_cycle_teaser_section_"+options.slideshowID).cycle("resume")};Drupal.viewsSlideshowCycle.previousSlide=function(options){$("#views_slideshow_cycle_teaser_section_"+options.slideshowID).cycle("prev")};Drupal.viewsSlideshowCycle.nextSlide=function(options){$("#views_slideshow_cycle_teaser_section_"+options.slideshowID).cycle("next")};Drupal.viewsSlideshowCycle.goToSlide=function(options){$("#views_slideshow_cycle_teaser_section_"+options.slideshowID).cycle(options.slideNum)};function IsNumeric(sText){var ValidChars="0123456789";var IsNumber=true;var Char;for(var i=0;i<sText.length&&IsNumber==true;i++){Char=sText.charAt(i);if(ValidChars.indexOf(Char)==-1){IsNumber=false}}return IsNumber}function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString()}else{var expires=""}document.cookie=name+"="+value+expires+"; path=/"}function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(";");for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==" "){c=c.substring(1,c.length)}if(c.indexOf(nameEQ)==0){return c.substring(nameEQ.length,c.length)}}return null}function eraseCookie(name){createCookie(name,"",-1)}function viewsSlideshowCycleIsVisible(elem,type,amountVisible){var docViewTop=$(window).scrollTop();var docViewBottom=docViewTop+$(window).height();var docViewLeft=$(window).scrollLeft();var docViewRight=docViewLeft+$(window).width();var elemTop=$(elem).offset().top;var elemHeight=$(elem).height();var elemBottom=elemTop+elemHeight;var elemLeft=$(elem).offset().left;var elemWidth=$(elem).width();var elemRight=elemLeft+elemWidth;var elemArea=elemHeight*elemWidth;var missingLeft=0;var missingRight=0;var missingTop=0;var missingBottom=0;if(elemLeft<docViewLeft){missingLeft=docViewLeft-elemLeft}if(elemRight>docViewRight){missingRight=elemRight-docViewRight}if(elemTop<docViewTop){missingTop=docViewTop-elemTop}if(elemBottom>docViewBottom){missingBottom=elemBottom-docViewBottom}if(type=="full"){return((elemBottom>=docViewTop)&&(elemTop<=docViewBottom)&&(elemBottom<=docViewBottom)&&(elemTop>=docViewTop)&&(elemLeft>=docViewLeft)&&(elemRight<=docViewRight)&&(elemLeft<=docViewRight)&&(elemRight>=docViewLeft))}else{if(type=="vertical"){var verticalShowing=elemHeight-missingTop-missingBottom;if(amountVisible.indexOf("%")){return(((verticalShowing/elemHeight)*100)>=parseInt(amountVisible))}else{return(verticalShowing>=parseInt(amountVisible))}}else{if(type=="horizontal"){var horizontalShowing=elemWidth-missingLeft-missingRight;if(amountVisible.indexOf("%")){return(((horizontalShowing/elemWidth)*100)>=parseInt(amountVisible))}else{return(horizontalShowing>=parseInt(amountVisible))}}else{if(type=="area"){var areaShowing=(elemWidth-missingLeft-missingRight)*(elemHeight-missingTop-missingBottom);if(amountVisible.indexOf("%")){return(((areaShowing/elemArea)*100)>=parseInt(amountVisible))}else{return(areaShowing>=parseInt(amountVisible))}}}}}}})(jQuery);;
(function(b){function a(c){var i=/^(([^\/:]+?\.)*)([^\.:]{4,})((\.[a-z]{1,4})*)(:[0-9]{1,5})?$/;var l=window.location.host.replace(i,"$3$4");var e=window.location.host.replace(i,"$1");if(Drupal.settings.extlink.extSubdomains){var f="([^/]*\\.)?"}else{if(e=="www."||e==""){var f="(www\\.)?"}else{var f=e.replace(".","\\.")}}var j=new RegExp("^https?://"+f+l,"i");var d=false;if(Drupal.settings.extlink.extInclude){d=new RegExp(Drupal.settings.extlink.extInclude.replace(/\\/,"\\"))}var g=false;if(Drupal.settings.extlink.extExclude){g=new RegExp(Drupal.settings.extlink.extExclude.replace(/\\/,"\\"))}var k=new Array();var h=new Array();b("a:not(."+Drupal.settings.extlink.extClass+", ."+Drupal.settings.extlink.mailtoClass+")",c).each(function(o){try{var n=this.href.toLowerCase();if(n.indexOf("http")==0&&(!n.match(j)||(d&&n.match(d)))&&!(g&&n.match(g))){k.push(this)}else{if(n.indexOf("mailto:")==0){h.push(this)}}}catch(m){return false}});if(Drupal.settings.extlink.extClass){if(parseFloat(b().jquery)<1.2){b(k).not("[img]").addClass(Drupal.settings.extlink.extClass).each(function(){if(b(this).css("display")=="inline"){b(this).after("<span class="+Drupal.settings.extlink.extClass+"></span>")}})}else{b(k).not(b(k).find("img").parents("a")).addClass(Drupal.settings.extlink.extClass).each(function(){if(b(this).css("display")=="inline"){b(this).after("<span class="+Drupal.settings.extlink.extClass+"></span>")}})}}if(Drupal.settings.extlink.mailtoClass){if(parseFloat(b().jquery)<1.2){b(h).not("[img]").addClass(Drupal.settings.extlink.mailtoClass).each(function(){if(b(this).css("display")=="inline"){b(this).after("<span class="+Drupal.settings.extlink.mailtoClass+"></span>")}})}else{b(h).not(b(h).find("img").parents("a")).addClass(Drupal.settings.extlink.mailtoClass).each(function(){if(b(this).css("display")=="inline"){b(this).after("<span class="+Drupal.settings.extlink.mailtoClass+"></span>")}})}}if(Drupal.settings.extlink.extTarget){b(k).attr("target",Drupal.settings.extlink.extTarget)}if(Drupal.settings.extlink.extAlert){b(k).click(function(m){return confirm(Drupal.settings.extlink.extAlertText)})}}Drupal.behaviors.extlink={attach:function(c){a(c)}}})(jQuery);;
(function(a){Drupal.behaviors.eu_cookie_compliance_popup={attach:function(b,c){a("body").not(".sliding-popup-processed").addClass("sliding-popup-processed").each(function(){try{var g=Drupal.settings.eu_cookie_compliance.popup_enabled;if(!g){return}if(!Drupal.eu_cookie_compliance.cookiesEnabled()){return}var d=Drupal.eu_cookie_compliance.getCurrentStatus();var f=Drupal.settings.eu_cookie_compliance.popup_clicking_confirmation;var h=Drupal.settings.eu_cookie_compliance.popup_agreed_enabled;var j=Drupal.settings.eu_cookie_compliance.popup_hide_agreed;if(d==0){a("body").addClass("margInTop");var k=1;if(f){a("a, input[type=submit]").bind("click.eu_cookie_compliance",function(){if(!h){Drupal.eu_cookie_compliance.setStatus(1);k=2}Drupal.eu_cookie_compliance.changeStatus(k)})}a(".agree-button").click(function(){if(!h){Drupal.eu_cookie_compliance.setStatus(1);k=2}Drupal.eu_cookie_compliance.changeStatus(k)});Drupal.eu_cookie_compliance.createPopup(Drupal.settings.eu_cookie_compliance.popup_html_info)}else{if(d==1){Drupal.eu_cookie_compliance.createPopup(Drupal.settings.eu_cookie_compliance.popup_html_agreed);if(j){a("a, input[type=submit]").bind("click.eu_cookie_compliance_hideagreed",function(){Drupal.eu_cookie_compliance.changeStatus(2)})}}else{return}}}catch(i){return}})}};Drupal.eu_cookie_compliance={};Drupal.eu_cookie_compliance.createPopup=function(d){var c=a(d).attr({id:"sliding-popup"}).height(Drupal.settings.eu_cookie_compliance.popup_height).width(Drupal.settings.eu_cookie_compliance.popup_width).hide();if(Drupal.settings.eu_cookie_compliance.popup_position){c.prependTo("body");var b=c.height();c.show().attr({"class":"sliding-popup-top"}).css({top:-1*b}).animate({top:0},Drupal.settings.eu_cookie_compliance.popup_delay)}else{c.appendTo("body");b=c.height();c.show().attr({"class":"sliding-popup-bottom"}).css({bottom:-1*b}).animate({bottom:0},Drupal.settings.eu_cookie_compliance.popup_delay)}a("#popup-text p").append('<br><a class="find-more-button" title="'+a(".find-more-button").text()+'" href="#">'+a(".find-more-button").text()+"</a>");a("#popup-buttons button:nth-child(2)").remove();Drupal.eu_cookie_compliance.attachEvents()};Drupal.eu_cookie_compliance.attachEvents=function(){var b=Drupal.settings.eu_cookie_compliance.popup_clicking_confirmation;var c=Drupal.settings.eu_cookie_compliance.popup_agreed_enabled;a(".find-more-button").click(function(){if(Drupal.settings.eu_cookie_compliance.popup_link_new_window){window.open(Drupal.settings.eu_cookie_compliance.popup_link)}else{window.location.href=Drupal.settings.eu_cookie_compliance.popup_link}});a(".agree-button").click(function(){var d=1;if(!c){Drupal.eu_cookie_compliance.setStatus(1);d=2}if(b){a("a, input[type=submit]").unbind("click.eu_cookie_compliance")}Drupal.eu_cookie_compliance.changeStatus(d)});a(".hide-popup-button").click(function(){Drupal.eu_cookie_compliance.changeStatus(2)})};Drupal.eu_cookie_compliance.getCurrentStatus=function(){name="cookie-agreed";value=Drupal.eu_cookie_compliance.getCookie(name);return value};Drupal.eu_cookie_compliance.changeStatus=function(c){var b=Drupal.eu_cookie_compliance.getCurrentStatus();if(b==c){return}if(Drupal.settings.eu_cookie_compliance.popup_position){if(b==1){a("body").removeClass("margInTop")}a(".sliding-popup-top").animate({top:a("#sliding-popup").height()*-1},Drupal.settings.eu_cookie_compliance.popup_delay,function(){if(b==0){a("#sliding-popup").html(Drupal.settings.eu_cookie_compliance.popup_html_agreed).animate({top:0},Drupal.settings.eu_cookie_compliance.popup_delay);Drupal.eu_cookie_compliance.attachEvents()}if(b==1){a("#sliding-popup").remove()}})}else{if(b==1){a("body").removeClass("margInTop")}a(".sliding-popup-bottom").animate({bottom:a("#sliding-popup").height()*-1},Drupal.settings.eu_cookie_compliance.popup_delay,function(){if(b==0){a("#sliding-popup").html(Drupal.settings.eu_cookie_compliance.popup_html_agreed).animate({bottom:0},Drupal.settings.eu_cookie_compliance.popup_delay);Drupal.eu_cookie_compliance.attachEvents()}if(b==1){a("#sliding-popup").remove()}})}Drupal.eu_cookie_compliance.setStatus(c)};Drupal.eu_cookie_compliance.setStatus=function(b){var c=new Date();c.setDate(c.getDate()+100);var d="cookie-agreed="+b+";expires="+c.toUTCString()+";path="+Drupal.settings.basePath;if(Drupal.settings.eu_cookie_compliance.domain){d+=";domain="+Drupal.settings.eu_cookie_compliance.domain}document.cookie=d};Drupal.eu_cookie_compliance.hasAgreed=function(){var b=Drupal.eu_cookie_compliance.getCurrentStatus();if(b==1||b==2){return true}return false};Drupal.eu_cookie_compliance.getCookie=function(c){var d=c+"=";var e="";if(document.cookie.length>0){offset=document.cookie.indexOf(d);if(offset!=-1){offset+=d.length;var b=document.cookie.indexOf(";",offset);if(b==-1){b=document.cookie.length}e=decodeURIComponent(document.cookie.substring(offset,b).replace(/\+/g,"%20"))}}return e};Drupal.eu_cookie_compliance.cookiesEnabled=function(){var b=(navigator.cookieEnabled)?true:false;if(typeof navigator.cookieEnabled=="undefined"&&!b){document.cookie="testcookie";b=(document.cookie.indexOf("testcookie")!=-1)?true:false}return(b)}})(jQuery);;
