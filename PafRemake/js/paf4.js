/*! jQuery v1.12.4 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(!l.ownFirst)for(b in a)return k.call(a,b);for(b in a);return void 0===b||k.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(h)return h.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))},d.guid=a.guid=a.guid||n.guid++,d):void 0},now:function(){return+new Date},support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2])return A.find(a);this.length=1,this[0]=f}return this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=!0,c||j.disable(),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll)a.setTimeout(n.ready);else if(d.addEventListener)d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K);else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&d.documentElement}catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")}catch(b){return a.setTimeout(f,50)}J(),n.ready()}}()}return I.promise(b)},n.ready.promise();var L;for(L in n(l))break;l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))}),function(){var a=d.createElement("div");l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}a=null}();var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b},N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0;
}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),"object"!=typeof b&&"function"!=typeof b||(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}}),function(){var a;l.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,e;return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0}}();var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)Y(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)}();var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function fa(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}var ga=/<|&#?\w+;/,ha=/<tbody/i;function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)}function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;o>r;r++)if(g=a[r],g||0===g)if("object"===n.type(g))n.merge(q,g.nodeType?[g]:g);else if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];while(f--)i=i.lastChild;if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;while(f--)n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)}n.merge(q,i.childNodes),i.textContent="";while(i.firstChild)i.removeChild(i.firstChild);i=p.lastChild}else q.push(b.createTextNode(g));i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;while(g=q[r++])if(d&&n.inArray(g,d)>-1)e&&e.push(g);else if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;while(g=i[f++])_.test(g.type||"")&&c.push(g)}return i=null,p}!function(){var b,c,e=d.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1);e=null}();var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;function pa(){return!0}function qa(){return!1}function ra(){try{return d.activeElement}catch(a){}}function sa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)sa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=qa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(G)||[""],h=b.length;while(h--)f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);i;i=i.parentNode)p.push(i),m=i;m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)}o=0;while((i=p[o++])&&!b.isPropagationStopped())b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault());if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;try{e[q]()}catch(s){}n.event.triggered=void 0,m&&(e[h]=m)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)}:function(a,b,c){var d="on"+b;a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0}),n._data(c,"submit",!0))})},postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?("checkbox"!==this.type&&"radio"!==this.type||(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)}),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)}),n._data(b,"change",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)}}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)},one:function(a,b,c,d){return sa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function Ea(a){var b=ya.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Ga(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}}function Ha(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;o>m;m++)g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m);if(h)for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;h>m;m++)g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")));k=e=null}return a}function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=ea(f),h=ea(a),g=0;null!=(e=h[g]);++g)d[g]&&Ga(e,d[g]);if(b)if(c)for(h=h||ea(a),d=d||ea(f),g=0;null!=(e=h[g]);g++)Fa(e,d[g]);else Fa(a,f);return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;null!=(d=a[h]);h++)if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(ea(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0;if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get());return this.pushStack(e)}});var Ja,Ka={HTML:"block",BODY:"block"};function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function Ma(a){var b=d,c=Ka[a];return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c}var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Qa=d.documentElement;!function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f},boxSizingReliable:function(){return null==b&&k(),e},pixelMarginRight:function(){return null==b&&k(),c},pixelPosition:function(){return null==b&&k(),b},reliableMarginRight:function(){return null==b&&k(),g},reliableMarginLeft:function(){return null==b&&k(),h}});function k(){var k,l,m=d.documentElement;m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",j.childNodes[0].style.borderCollapse="separate",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)}}}();var Ra,Sa,Ta=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0===g?g:g+""}):Qa.currentStyle&&(Ra=function(a){return a.currentStyle},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;function bb(a){if(a in ab)return a;var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;while(c--)if(a=_a[c]+b,a in ab)return a}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function db(a,b,c){var d=Ya.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)));return g}function fb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ra(a),g=l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Sa(a,b,f),(0>e||null==e)&&(e=a.style[b]),Oa.test(e))return e;d=g&&(l.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+eb(a,b,c||(g?"border":"content"),d,f)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)}):fb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ra(a);return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)}}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0}),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{
marginLeft:0},function(){return a.getBoundingClientRect().left}):0))+"px":void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+V[d]+b]=f[d]||f[d-2]||f[0];return e}},Na.test(a)||(n.cssHooks[a+b].set=db)}),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ra(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return cb(this,!0)},hide:function(){return cb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()})}});function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)}n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=gb.propHooks[this.prop];return a&&a.get?a.get(this):gb.propHooks._default.get(this)},run:function(a){var b,c=gb.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this}},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;function lb(){return a.setTimeout(function(){hb=void 0}),hb=n.now()}function mb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=V[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(o))"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function pb(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(pb(k,j.opts.specialEasing);g>f;f++)if(d=qb.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return X(c.elem,a,U.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&kb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)}}),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(hb=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),hb=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(ib),ib=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value}();var rb=/\r/g,sb=/[\x20\t\r\n\f]+/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a)).replace(sb," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>-1)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var tb,ub,vb=n.expr.attrHandle,wb=/^(?:checked|selected)$/i,xb=l.getSetAttribute,yb=l.input;n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ub:tb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?yb&&xb||!wb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(xb?c:d)}}),ub={set:function(a,b,c){return b===!1?n.removeAttr(a,c):yb&&xb||!wb.test(c)?a.setAttribute(!xb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=vb[b]||n.find.attr;yb&&xb||!wb.test(b)?vb[b]=function(a,b,d){var e,f;return d||(f=vb[b],vb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,vb[b]=f),e}:vb[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),yb&&xb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):tb&&tb.set(a,b,c)}}),xb||(tb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},vb.id=vb.name=vb.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:tb.set},n.attrHooks.contenteditable={set:function(a,b,c){tb.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var zb=/^(?:input|select|textarea|button|object)$/i,Ab=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):zb.test(a.nodeName)||Ab.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var Bb=/[\t\r\n\f]/g;function Cb(a){return n.attr(a,"class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,Cb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,Cb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Cb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=Cb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+Cb(c)+" ").replace(Bb," ").indexOf(b)>-1)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Db=a.location,Eb=n.now(),Fb=/\?/,Gb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(Gb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var Hb=/#.*$/,Ib=/([?&])_=[^&]*/,Jb=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Kb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Lb=/^(?:GET|HEAD)$/,Mb=/^\/\//,Nb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Ob={},Pb={},Qb="*/".concat("*"),Rb=Db.href,Sb=Nb.exec(Rb.toLowerCase())||[];function Tb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Ub(a,b,c,d){var e={},f=a===Pb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Vb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Wb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Xb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Rb,type:"GET",isLocal:Kb.test(Sb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Qb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Vb(Vb(a,n.ajaxSettings),b):Vb(n.ajaxSettings,a)},ajaxPrefilter:Tb(Ob),ajaxTransport:Tb(Pb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;if(2===u){if(!k){k={};while(b=Jb.exec(g))k[b[1].toLowerCase()]=b[2]}b=k[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===u?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return u||(a=t[c]=t[c]||a,s[a]=b),this},overrideMimeType:function(a){return u||(l.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>u)for(b in a)r[b]=[r[b],a[b]];else w.always(a[w.status]);return this},abort:function(a){var b=a||v;return j&&j.abort(b),y(0,b),this}};if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Rb)+"").replace(Hb,"").replace(Mb,Sb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Nb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Sb[1]&&d[2]===Sb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Sb[3]||("http:"===Sb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Ub(Ob,l,c,w),2===u)return w;i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Lb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Fb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Ib.test(f)?f.replace(Ib,"$1_="+Eb++):f+(Fb.test(f)?"&":"?")+"_="+Eb++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Qb+"; q=0.01":""):l.accepts["*"]);for(e in l.headers)w.setRequestHeader(e,l.headers[e]);if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u))return w.abort();v="abort";for(e in{success:1,error:1,complete:1})w[e](l[e]);if(j=Ub(Pb,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u)return w;l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")},l.timeout));try{u=1,j.send(s,y)}catch(x){if(!(2>u))throw x;y(-1,x)}}else y(-1,"No Transport");function y(b,c,d,e){var k,s,t,v,x,y=c;2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Wb(l,w,d)),v=Xb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))}return w},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}});function Yb(a){return a.style&&a.style.display||n.css(a,"display")}function Zb(a){if(!n.contains(a.ownerDocument||d,a))return!0;while(a&&1===a.nodeType){if("none"===Yb(a)||"hidden"===a.type)return!0;a=a.parentNode}return!1}n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Zb(a)},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var $b=/%20/g,_b=/\[\]$/,ac=/\r?\n/g,bc=/^(?:submit|button|image|reset|file)$/i,cc=/^(?:input|select|textarea|keygen)/i;function dc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||_b.test(a)?d(a,e):dc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)dc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)dc(c,a[c],b,e);return d.join("&").replace($b,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&cc.test(this.nodeName)&&!bc.test(a)&&(this.checked||!Z.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(ac,"\r\n")}}):{name:b.name,value:c.replace(ac,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?ic():d.documentMode>8?hc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&hc()||ic()}:hc;var ec=0,fc={},gc=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in fc)fc[a](void 0,!0)}),l.cors=!!gc&&"withCredentials"in gc,gc=l.ajax=!!gc,gc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;return{send:function(d,e){var f,g=b.xhr(),h=++ec;if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(f in b.xhrFields)g[f]=b.xhrFields[f];b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");for(f in d)void 0!==d[f]&&g.setRequestHeader(f,d[f]+"");g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;if(c&&(d||4===g.readyState))if(delete fc[h],c=void 0,g.onreadystatechange=n.noop,d)4!==g.readyState&&g.abort();else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);try{i=g.statusText}catch(k){i=""}f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404}j&&e(f,i,j,g.getAllResponseHeaders())},b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=fc[h]=c:c()},abort:function(){c&&c(void 0,!0)}}}});function hc(){try{return new a.XMLHttpRequest}catch(b){}}function ic(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var jc=[],kc=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=jc.pop()||n.expando+"_"+Eb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(kc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&kc.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(kc,"$1"+e):b.jsonp!==!1&&(b.url+=(Fb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,jc.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||d;var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var lc=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&lc)return lc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function mc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=mc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0),c.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Qa})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return Y(this,function(a,d,e){var f=mc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({
padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return Y(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var nc=a.jQuery,oc=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=oc),b&&a.jQuery===n&&(a.jQuery=nc),n},b||(a.jQuery=a.$=n),n});
;
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 2)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.6
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.6
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.6'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.6
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.6'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target)
      if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"]') || $(e.target).is('input[type="checkbox"]'))) e.preventDefault()
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.6
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.6'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.6
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.6'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.6
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.6'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.6
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.6'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.6
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.6'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element
        .removeAttr('aria-describedby')
        .trigger('hidden.bs.' + that.type)
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var elOffset  = isBody ? { top: 0, left: 0 } : $element.offset()
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.6
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.6'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div title="Fermer" class="glyphicon glyphicon-remove popover-btn-close"></div><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.6
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.6'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.6
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.6'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.6
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.6'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);
;
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2017 Twitter, Inc.
 * Licensed under the MIT license
 */
function getCookieGeoloc(){for(var a="; "+document.cookie,b=a.split("; "),c=!1,d=0;d<b.length;d++){var e=b[d].split("=");if("session_caf"==e[0]){c=!0;var f=e[1],g=window.location.pathname,h=g.split("/"),i=h[2],j=jQuery("#liste_all_caf_name_by_caf_id .caf_id_"+f+"_url").text();if(i!=j&&"allocataires"==h[1]&&"undefined"!=typeof h[3]&&h[3].length>1){var k=jQuery("#caf_id_cookie_to_set").text();return"undefined"!=typeof k&&k.length>0?(createCookie(k),k):f}return f}}if(!c){var g=window.location.pathname,h=g.split("/");"undefined"!=typeof h[2]&&jQuery("#liste_all_caf_name_by_caf_id div").each(function(){if(/caf\_id\_([0-9])*\_url/.test($(this).attr("class"))&&h[2]==$(this).text()){var a=$(this).attr("class").replace("caf_id_","").replace("_url","");return createCookie(a),a}})}return"cnaf"}function createCookie(a){var b=3,c=new Date;c.setTime(c.getTime()+24*b*60*60*1e3);var d="; expires="+c.toGMTString(),e=window.location.hostname;e=/caf\-fr/.test(e)?".caf-fr.local":".caf.fr",null!=a&&(document.cookie=encodeURIComponent("session_caf")+"="+encodeURIComponent(a)+d+"; domain= "+e+"; path=/")}function accueil_allocataire_pave_actus_height(){if(jQuery(window).width()>959){for(var a=["#allocataire-actu-paves-accueil .col-md-4 .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:first-child .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:first-child .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body"],b=Math.max(jQuery(a[0]).height(),jQuery(a[1]).height(),jQuery(a[2]).height()),c=0;c<a.length;c++)jQuery(a[c]).parent().css("height",b+227+"px");a=["#allocataire-actu-paves-accueil .col-md-4 .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:nth-child(2) .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:nth-child(2) .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body"],b=Math.max(jQuery(a[0]).height(),jQuery(a[1]).height(),jQuery(a[2]).height());for(var c=0;c<a.length;c++)jQuery(a[c]).parent().css("height",b+227+"px")}else if(jQuery(window).width()<960&&jQuery(window).width()>639){for(var a=["#allocataire-actu-paves-accueil .col-md-4 .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-4 .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body"],b=Math.max(jQuery(a[0]).height(),jQuery(a[1]).height()),d=jQuery(a[1]).parent().find("img").height(),c=0;c<a.length;c++)jQuery(a[c]).parent().css("height",b+d+70+"px");a=["#allocataire-actu-paves-accueil .col-md-8 .row:first-child .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:first-child .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body"],b=Math.max(jQuery(a[0]).height(),jQuery(a[1]).height()),d=jQuery(a[1]).parent().find("img").height();for(var c=0;c<a.length;c++)jQuery(a[c]).parent().css("height",b+d+70+"px");a=["#allocataire-actu-paves-accueil .col-md-8 .row:nth-child(2) .col-sm-6:first-child .panel-actu-content .panel-actu-body","#allocataire-actu-paves-accueil .col-md-8 .row:nth-child(2) .col-sm-6:nth-child(2) .panel-actu-content .panel-actu-body"],b=Math.max(jQuery(a[0]).height(),jQuery(a[1]).height()),d=jQuery(a[1]).parent().find("img").height();for(var c=0;c<a.length;c++)jQuery(a[c]).parent().css("height",b+d+70+"px")}else jQuery("#allocataire-actu-paves-accueil .panel-actu-content").css("height","auto")}function drawMaCarteCaf(){function a(a){var b,c;if(a)if(m!=a.properties.CODE_DEPT){var g=h.centroid(a);b=g[0],c=g[1],n="11"==a.properties.CODE_REG?9:5,m=a.properties.CODE_DEPT,q.selectAll("path").classed("dept-active",function(a){return m==a.properties.CODE_DEPT}),q.selectAll("text.dept-text").attr("text-anchor",function(a){var b=a.properties.CODE_DEPT,c="middle";return"92"==b&&(c="end"),"75"==b&&(c="start"),c}).attr("font-size",function(a){var b=(3.6*f).toString()+"px",c=a.properties.CODE_REG;return"11"==c&&(b=(2*f).toString()+"px"),b}).text(function(a){return l[a.properties.CODE_DEPT]}).append("svg:title"),q.selectAll("title").text(function(a){var b="";return m==a.properties.CODE_DEPT&&(b="Cliquer pour accÃ©der Ã  la "),b+l[a.properties.CODE_DEPT]});var j="translate("+d/2+","+e/2+")scale("+n+")translate("+-b+","+-c+")";q.transition().duration(500).attr("transform",j),o=i.scale()}else{var k=a.properties.CODE_DEPT;"2A"==k&&(k="200"),"2B"==k&&(k="202"),k+=2==k.length?"000":"00",jQuery("#zipcode").val(k),jQuery("#block-zipcode").submit()}}function b(){var a=i.scale();if(m&&1!=n&&a!=o){m=null,q.selectAll("path").classed("dept-active",!1),q.selectAll("title").text(function(a){return a.properties.NOM_DEPT}),q.selectAll("text.dept-text").attr("text-anchor","middle").attr("font-size",(11*f).toString()+"px").text(function(a){return"647"==a.properties.CODE_DEPT||"648"==a.properties.CODE_DEPT?"64":a.properties.CODE_DEPT}).append("svg:title"),q.selectAll("title").text(function(a){return"647"==a.properties.CODE_DEPT||"648"==a.properties.CODE_DEPT?"PYRENEES-ATLANTIQUES":a.properties.NOM_DEPT});var b="translate(0,0)scale(1)";q.transition().duration(500).attr("transform",b)}o=a}function c(a){var b=a.properties.CODE_DEPT;1==n?(q.selectAll("path").classed("dept-stroke-z5",!1),q.selectAll("path").classed("dept-stroke-z9",!1),q.selectAll("path").classed("dept-stroke",function(a){return b==a.properties.CODE_DEPT})):5==n?(q.selectAll("path").classed("dept-stroke",!1),q.selectAll("path").classed("dept-stroke-z9",!1),q.selectAll("path").classed("dept-stroke-z5",function(a){return b==a.properties.CODE_DEPT})):(q.selectAll("path").classed("dept-stroke",!1),q.selectAll("path").classed("dept-stroke-z5",!1),q.selectAll("path").classed("dept-stroke-z9",function(a){return b==a.properties.CODE_DEPT}))}var d=parseInt(d3.select("#map_carte_de_france").style("width"))-5,e=.95*d,f=(d/708).toFixed(2),g=d3.geo.conicConformal().center([3,46.599722]).scale(6*d).translate([d/2,e/2]),h=d3.geo.path().projection(g),i=d3.behavior.zoom().translate([d/2,e/2]).scale(1).scaleExtent([0,10]).on("zoom",b),j={bleu:["#3182bd","#6baed6","#9ecae1","#c6dbef"],orange:["#e6550d","#fd8d3c","#fdae6b","#fdd0a2"],vert:["#31a354","#74c476","#a1d99b","#c7e9c0"],violet:["#756bb1","#9e9ac8","#bcbddc","#dadaeb"],gris:["#636363","#969696","#bdbdbd","#d9d9d9"],foin:["#637939","#8ca252","#b5cf6b","#cedb9c"],ocre:["#8c6d31","#bd9e39","#e7ba52","#e7cb94"],rouge:["#843c39","#ad494a","#d6616b","#e7969c"],mauve:["#7b4173","#a55194","#ce6dbd","#de9ed6"]},k={"01":{abbr:"GUAD",color:"bleu"},"02":{abbr:"MART",color:"orange"},"03":{abbr:"GUYA",color:"vert"},"04":{abbr:"REUN",color:"mauve"},"06":{abbr:"MAYO",color:"gris"},11:{abbr:"IDF",color:"mauve"},24:{abbr:"CVDL",color:"bleu"},27:{abbr:"BFC",color:"vert"},28:{abbr:"NMD",color:"orange"},32:{abbr:"NPCP",color:"bleu"},44:{abbr:"ACAL",color:"orange"},52:{abbr:"PDLL",color:"mauve"},53:{abbr:"BTG",color:"vert"},75:{abbr:"ALPC",color:"orange"},76:{abbr:"LRMP",color:"vert"},84:{abbr:"ARA",color:"mauve"},93:{abbr:"PACA",color:"bleu"},94:{abbr:"CRS",color:"violet"}},l={"01":"CAF de lâ€™Ain","02":"CAF de l'Aisne","03":"CAF de lâ€™Allier","04":"CAF des Alpes-de-Haute-Provence","05":"CAF des Hautes-Alpes","06":"CAF des Alpes-Maritimes","07":"CAF de l'ArdÃ¨che","08":"CAF des Ardennes","09":"CAF de lâ€™AriÃ¨ge",10:"CAF de lâ€™Aube",11:"CAF de lâ€™Aude",12:"CAF de lâ€™Aveyron",13:"CAF des Bouches-du-RhÃ´ne",14:"CAF du Calvados",15:"CAF du Cantal",16:"CAF de la Charente",17:"CAF de la Charente-Maritime",18:"CAF du Cher",19:"CAF de la CorrÃ¨ze","2A":"CAF de la Corse-du-Sud","2B":"CAF de la Haute-Corse",21:"CAF de la CÃ´te-dâ€™Or",22:"CAF des CÃ´tes-dâ€™Armor",23:"CAF de la Creuse",24:"CAF de la Dordogne",25:"CAF du Doubs",26:"CAF de la DrÃ´me",27:"CAF de lâ€™Eure",28:"CAF de lâ€™Eure-et-Loir",29:"CAF du FinistÃ¨re",30:"CAF du Gard",31:"CAF de la Haute-Garonne",32:"CAF du Gers",33:"CAF de la Gironde",34:"CAF de l'HÃ©rault",35:"CAF d'Ille-et-Vilaine",36:"CAF de lâ€™Indre",37:"CAF de l'Indre-et-Loire",38:"CAF de l'IsÃ¨re",39:"CAF du Jura",40:"CAF des Landes",41:"CAF du Loir et Cher",42:"CAF de la Loire",43:"CAF de la Haute-Loire",44:"CAF de la Loire-Atlantique",45:"CAF du Loiret",46:"CAF du Lot",47:"CAF de Lot-et-Garonne",48:"CAF de la LozÃ¨re",49:"CAF de Maine-et-Loire",50:"CAF de la Manche",51:"CAF de la Marne",52:"CAF de la Haute-Marne",53:"CAF de la Mayenne",54:"CAF de la Meurthe-et-Moselle",55:"CAF de la Meuse",56:"CAF du Morbihan",57:"CAF de la Moselle",58:"CAF de la NiÃ¨vre",59:"CAF du Nord",60:"CAF de l'Oise",61:"CAF de lâ€™Orne",62:"CAF du Pas-de-Calais",63:"CAF du Puy-de-DÃ´me",64:"CAF des PyrÃ©nÃ©es-Atlantiques",647:"CAF du Pays Basque et du Seignanx",648:"CAF BÃ©arn et Soule",65:"CAF des Hautes-PyrÃ©nÃ©es",66:"CAF des PyrÃ©nÃ©es-Orientales",67:"CAF du Bas-Rhin",68:"CAF du Haut-Rhin",69:"CAF du RhÃ´ne",70:"CAF de la Haute-SaÃ´ne",71:"CAF de la SaÃ´ne-et-Loire",72:"CAF de la Sarthe",73:"CAF de la Savoie",74:"CAF de la Haute-Savoie",75:"CAF de Paris",76:"CAF de la Seine-Maritime",77:"CAF de la Seine-et-Marne",78:"CAF des Yvelines",79:"CAF des Deux-SÃ¨vres",80:"CAF de la Somme",81:"CAF du Tarn",82:"CAF de Tarn-et-Garonne",83:"CAF du Var",84:"CAF de Vaucluse",85:"CAF de la VendÃ©e",86:"CAF de la Vienne",87:"CAF de la Haute-Vienne",88:"CAF des Vosges",89:"CAF de lâ€™Yonne",90:"CAF du Territoire-de-Belfort",91:"CAF de lâ€™Essonne",92:"CAF des Hauts-de-Seine",93:"CAF de la Seine-Saint-Denis",94:"CAF du Val-de-Marne",95:"CAF du Val-dâ€™Oise",971:"CAF de la Guadeloupe",972:"CAF de la Martinique",973:"CAF de la Guyane",974:"CAF de La RÃ©union",976:"CAF de Mayotte"},m=null,n=1,o=1,p=d3.select("#map_carte_de_france").append("svg").attr("overflow","hidden").attr("viewBox","0 0 "+d+" "+e).attr("preserveAspectRatio","xMinYMin meet").call(i),q=p.append("g").attr("id","departements");d3.json(window.location.protocol+"//"+window.location.host+"/sites/all/themes/caf_v2/js/caf/dept2016.topojson",function(b,d){if(b)throw jQuery("#map_carte_de_france").css("display","none"),jQuery("#map_caf_liste").css("display","block"),b;q.selectAll("path").data(topojson.feature(d,d.objects.dept2016).features.filter(function(a){return"64"!=a.properties.CODE_DEPT})).enter().append("path").attr("class","departement").attr("fill",function(a){return j[k[a.properties.CODE_REG].color][2]}).attr("d",h).on("click",a).on("mouseover",c).append("svg:title").text(function(a){return a.properties.NOM_DEPT}),q.selectAll("text.dept-text").data(topojson.feature(d,d.objects.dept2016).features.filter(function(a){return"64"!=a.properties.CODE_DEPT})).enter().append("svg:text").text(function(a){return"647"==a.properties.CODE_DEPT||"648"==a.properties.CODE_DEPT?"64":a.properties.CODE_DEPT}).attr("class","dept-text").attr("x",function(a){return h.centroid(a)[0]}).attr("y",function(a){return h.centroid(a)[1]}).attr("text-anchor","middle").attr("font-size",(11*f).toString()+"px").on("click",a).on("mouseover",c).append("svg:title").text(function(a){return"647"==a.properties.CODE_DEPT||"648"==a.properties.CODE_DEPT?"PYRENEES-ATLANTIQUES":a.properties.NOM_DEPT}),q.selectAll("text.dept-nom").data(topojson.feature(d,d.objects.dept2016).features.filter(function(a){return"971"==a.properties.CODE_DEPT||"972"==a.properties.CODE_DEPT||"973"==a.properties.CODE_DEPT||"974"==a.properties.CODE_DEPT||"976"==a.properties.CODE_DEPT})).enter().append("svg:text").text(function(a){return a.properties.NOM_DEPT.charAt(0)+a.properties.NOM_DEPT.slice(1).toLowerCase()}).attr("class","dept-nom").attr("x",function(a){return h.bounds(a)[1][0]}).attr("y",function(a){return h.bounds(a)[1][1]+12}).attr("text-anchor","end").attr("font-size",(11*f).toString()+"px")})}function resize_pave_services_en_ligne(){if(jQuery(window).width()>639&&jQuery(window).width()<960){var a=370,b=0,c=0;jQuery(".pave-services-en-ligne .text-center").each(function(){jQuery(this).css("height","");var a=parseInt(jQuery(this).css("height").replace("px",""));c<a&&(c=a),b+=a});var d=a-60-parseInt(b);if(d>0)for(;d>0;)jQuery(".pave-services-en-ligne .text-center").each(function(){var a=parseInt(jQuery(this).css("height").replace("px",""));c>a?(jQuery(this).css("height",a+1+"px"),d-=1):c==a&&(jQuery(this).css("height",a+1+"px"),c+=1,d-=1)})}else jQuery(window).width()>959?jQuery(".pave-services-en-ligne .col-md-6").each(function(){jQuery(this).find(".text-center").css("height","")}):jQuery(window).width()<640&&jQuery(".pave-services-en-ligne .col-md-6").each(function(){jQuery(this).find(".text-center").css("height","")})}if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1||b[0]>2)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")}(jQuery),+function(a){"use strict";if(a(".img_interac_map").length>0){var b=a(".img_interac_map").attr("id").split("img_interac_nid_");b=b[1],0==a('map[name="img_interac_nid_'+b+'"').length&&jQuery.ajax({url:"/admin/images-interactives/get-by-nid/"+b,success:function(c){if("undefined"!=typeof c.zones&&c.zones.length>0){if(c.data.uri==a("#img_interac_nid_"+b).attr("src")){for(var d='<map name="map_image_interac_'+b+'">',e=0;e<c.zones.length;e++){var f=c.zones[e].text_hover,g=c.zones[e].list_coords,h=c.zones[e].link;d+='<area shape="poly" target="_blank" coords="'+g+'" href="'+h+'" title="'+f.replace(/"/g,"&quot;")+'">'}d+="</map>",a("#img_interac_nid_"+b).after(d)}else a("#img_interac_nid_"+b).hide();a("#img_interac_nid_"+b).rwdImageMaps()}else a("#img_interac_nid_"+b).hide()},error:function(){a("#img_interac_nid_"+b).hide()}})}if(a(".accueil-block-simulateur").length>0&&a(".accueil-block-simulateur .btn-incremental button[type='button']").click(function(){var b=a(this).parent().find("input").val(),c=0;c="+"==a(this).text()?b<8?parseFloat(b)+1:b:b>1?parseFloat(b)-1:1,a(this).parent().find("input").val(c)}),a("#allocataire-actu-paves-accueil").length>0){var c=getCookieGeoloc();a.ajax({type:"GET",url:"/get/ajax/allocataire/actus",data:{caf_id:c},success:function(b){if("undefined"==typeof b.ERROR){var d='<div class="col-sm-12 col-md-4"><div class="row">';a.each(b.tab_actu_VDF,function(a,b){var c="col-sm-6 col-md-12";a>0&&(c="col-sm-6 col-md-12 hidden-xs hidden-md hidden-lg"),d+='<div class="'+c+'"><div class="panel-actu-content"><div class="panel-actu-image"><a href="'+b.url+'"><img src="'+b.uri_image+'" alt=""></a><div class="panel-actu-cartouche">Vies de famille</div></div><div class="text-left panel-actu-body"><div class="infos-txt date-actu">'+b.date_value+'</div><a href="'+b.url+'" class="subtitle-txt">'+b.title+"</a><div>"+b.resume+'</div><a href="'+b.url+'" class="btn btn-info">Lire la suite</a></div></div></div>',a++}),d+='</div><a class="btn btn-primary btn-block" href="/allocataires/vies-de-famille">+ de vies de famille</a></div><div class="col-sm-12 col-md-8">',a.each(b.tab_actu,function(b,e){var f="Allocataires";"cnaf"!=c&&e.caf_id&&(f="Caf "+a("#liste_all_caf_name_by_caf_id .caf_id_"+c+"_short").text()),b%2==0&&(d+='<div class="row">'),d+='<div class="col-sm-6"><div class="panel-actu-content"><div class="panel-actu-image"><a href="'+e.url+'"><img src="'+e.uri_image+'"></a><div class="panel-actu-cartouche">'+f+'</div></div><div class="text-left panel-actu-body"><div class="infos-txt date-actu">'+e.date_value+'</div><a href="'+e.url+'" class="subtitle-txt">'+e.title+"</a><div>"+e.resume+'</div><a href="'+e.url+'" class="btn btn-info">Lire la suite</a></div></div></div>',b%2==1&&(d+="</div>")});var e="/allocataires/actualites";if("cnaf"!=c){var f=a("#liste_all_caf_name_by_caf_id .caf_id_"+c+"_url").text();e="/allocataires/"+f+"/actualites"}d+='<a class="btn btn-primary btn-block" href="'+e+"\">+ d'actualitÃ©s</a>",a("#allocataire-actu-paves-accueil").html(d)}setTimeout(function(){accueil_allocataire_pave_actus_height()},300)}})}}(jQuery),+function(a){"use strict";var b=getCookieGeoloc();if(a(".bloc-ma-caf").length>0){var c=window.location.pathname.split("/"),d=c[1];if("partenaires"==d?a(".icon-pic-geoloc-allocataire ").attr("class","icon-pic-geoloc-partenaire pull-left"):"presse-institutionnel"==d&&a(".icon-pic-geoloc-allocataire ").attr("class","icon-pic-geoloc-institutionnel pull-left"),null!=b&&"cnaf"!=b){var e="Caf "+a("#liste_all_caf_name_by_caf_id .caf_id_"+b+"_short").text();if("partenaires"==d)var f="/allocataires/"+a("#liste_all_caf_name_by_caf_id .caf_id_"+b+"_url").text()+"/partenaires";else var f="/allocataires/"+a("#liste_all_caf_name_by_caf_id .caf_id_"+b+"_url").text()+"/actualites";a(".icon-pic-geoloc-allocataire").bind("click",function(){window.location.href="/allocataires/ma-caf-recherche"}),a(".icon-pic-geoloc-partenaire").bind("click",function(){window.location.href="/partenaires/ma-caf-recherche"}),a(".icon-pic-geoloc-institutionnel").bind("click",function(){window.location.href="/presse-institutionnel/ma-caf-recherche"}),a(".icon-pic-geoloc-allocataire").css("cursor","pointer"),a(".icon-pic-geoloc-partenaire").css("cursor","pointer"),a(".icon-pic-geoloc-institutionnel").css("cursor","pointer"),a(".bloc-ma-caf .caf-name").html(e),a(".bloc-ma-caf .caf-name").show(),a(".bloc-ma-caf .titre p").hide(),a(".btn-bloc-ma-caf ").attr("href",f),a(".bloc-ma-caf .contenu-non-geoloc").hide(),a(".bloc-ma-caf .contenu-geoloc").show()}else a(".icon-pic-geoloc-allocataire").bind("click",function(){window.location.href="/allocataires/ma-caf-recherche"}),a(".icon-pic-geoloc-partenaire").bind("click",function(){window.location.href="/partenaires/ma-caf-recherche"}),a(".icon-pic-geoloc-institutionnel").bind("click",function(){window.location.href="/presse-institutionnel/ma-caf-recherche"}),a(".icon-pic-geoloc-allocataire").css("cursor","pointer"),a(".icon-pic-geoloc-partenaire").css("cursor","pointer"),a(".icon-pic-geoloc-institutionnel").css("cursor","pointer"),a(".bloc-ma-caf .caf-name").hide(),a(".bloc-ma-caf .titre p").show(),a(".bloc-ma-caf .contenu-non-geoloc").show(),a(".bloc-ma-caf .contenu-geoloc").hide()}if(a(".bloc-vdf-dernieres-actus").length>0&&a.ajax({type:"GET",url:"/get/ajax/get-last-actus",dataType:"json",data:{caf_id:b},success:function(b){var c="";if("undefined"!=typeof b.vdf_pave_actu_importante&&b.vdf_pave_actu_importante.length>0&&(c+='<div class="date-actu infos-txt">'+b.vdf_pave_actu_importante[0].published+'<span class="btn-important">IMPORTANT</span></div><a href="'+b.vdf_pave_actu_importante[0].url+'">'+b.vdf_pave_actu_importante[0].title+"</a><hr>"),"undefined"!=typeof b.vdf_pave_actu&&b.vdf_pave_actu.length>0)for(var d=0;d<b.vdf_pave_actu.length;d++)c+='<div class="date-actu infos-txt">'+b.vdf_pave_actu[d].published+'</div><a href="'+b.vdf_pave_actu[d].url+'"> '+b.vdf_pave_actu[d].title+"</a><hr>";c+='<div class="text-center"><a type="button"  href="'+b.vdf_pave_actu_plus+'" class="btn btn-primary">Voir plus d\'actualitÃ©s</a></div>',a(".bloc-vdf-dernieres-actus .bloc-vdf-dernieres-actus-content").html(c);var c="";if(a(".vdf_accueil_droit_contenu .vdf_pave_actualites").length>0)if("undefined"==typeof b.vdf_pave_actu_importante&&"undefined"==typeof b.vdf_pave_actu)a(".vdf_accueil_droit_contenu").hide();else{if("undefined"!=typeof b.vdf_pave_actu_importante&&b.vdf_pave_actu_importante.length>0&&(c+="<div><span>IMPORTANT</span></div>",c+="<div><span>"+b.vdf_pave_actu_importante[0].published+'</span><span><a href="'+b.vdf_pave_actu_importante[0].url+'"> '+b.vdf_pave_actu_importante[0].title+"</a></span><div>"+b.vdf_pave_actu_importante[0].resume+'<span><a href="'+b.vdf_pave_actu_importante[0].url+'"> Lire</a></span></div></div>'),"undefined"!=typeof b.vdf_pave_actu&&b.vdf_pave_actu.length>0)for(var d=0;d<b.vdf_pave_actu.length;d++)c+="<div><span>"+b.vdf_pave_actu[d].published+'</span><span><a href="'+b.vdf_pave_actu[d].url+'"> '+b.vdf_pave_actu[d].title+"</a></span><div>"+b.vdf_pave_actu[d].resume+'<span><a href="'+b.vdf_pave_actu[d].url+'"> Lire</a></span></div></div>';c+='<div><a type="button" href="'+b.vdf_pave_actu_plus+'" class="btn btn-primary btn-voir-plus-actus">Voir plus d\'actualitÃ©s</a></div>',a(".vdf_accueil_droit_contenu .vdf_pave_actualites").html(c)}}}),a(".bloc-vdf-articles-les-plus-lus .bloc-vdf-articles-les-plus-lus-content").length>0&&a.ajax({type:"GET",url:"/get/ajax/get-articles-most-view",dataType:"json",data:{caf_id:b},success:function(b){if("undefined"==typeof b.vdf_art_plus_lus||0==b.vdf_art_plus_lus.length)a(".bloc-vdf-articles-les-plus-lus").parent().hide();else{var c="";if("undefined"!=typeof b.vdf_art_plus_lus&&b.vdf_art_plus_lus.length>0)for(var d=0;d<5;d++)if("undefined"!=typeof b.vdf_art_plus_lus[d]){var e=b.vdf_art_plus_lus[d];c+="<hr><div>",c+='<a href="'+e.alias+'" title="'+e.title+'">',c+='<div class="pull-left"><img src="'+e.img_path+'" alt="" /></div>',c+="<div>"+e.title+"</div>",c+='</a></div><div class="clearfix"></div>'}a(".bloc-vdf-articles-les-plus-lus .bloc-vdf-articles-les-plus-lus-content").html(c)}var c="";if("undefined"==typeof b.vdf_art_plus_lus||0==b.vdf_art_plus_lus.length)a(".vdf_accueil_gauche_contenu").hide();else if(a(".vdf_accueil_gauche_contenu .vdf_accueil_article_plus_lus").length>0){if("undefined"!=typeof b.vdf_art_plus_lus&&b.vdf_art_plus_lus.length>0){c+="<ul>";for(var d=0;d<b.vdf_art_plus_lus.length;d++)c+='<hr><div><a href="/'+b.vdf_art_plus_lus[d].alias+'">'+b.vdf_art_plus_lus[d].title+"</a></div>";c+="</ul>"}a(".vdf_accueil_gauche_contenu .vdf_accueil_article_plus_lus").html(c)}}}),a(".node-sommaire-prestation").length>0||a(".node-sommaire-q-r").length>0||a(".node-question-reponse").length>0||a(".node-fiche-prestation").length>0){var g=a(".page-content-node").attr("class").split(" "),h=g[2].replace("node-","").replace(/\-/g,"_"),i="node-promoted"!=g[3]?g[3].replace("node-",""):g[4].replace("node-","");"cnaf"!=b&&a.ajax({type:"GET",url:"/ajax/get-bloc-ma-caf-from-sidoc",dataType:"json",data:{caf_id:b,node_nid:i,node_type:h},success:function(b){"undefined"==typeof b.erreur&&a(".page-sidebar-right").append(b)}})}if(a(".bloc-vdf-articles-recommandes").length>0){if(a(".page-content-node").length>0){var g=a(".page-content-node").attr("class").split(" ");if(g.length>1)var i="node-promoted"!=g[3]?g[3].replace("node-",""):g[4].replace("node-","")}else if(a(".node-pages-vdf-accueil").length>0)var g=a(".node-pages-vdf-accueil").attr("class").split(" "),i="node-promoted"!=g[3]?g[3].replace("node-",""):g[4].replace("node-","");a.ajax({type:"GET",url:"/get/ajax/get-bloc-articles-recommandes",dataType:"json",data:{caf_id:b,node_nid:i},success:function(b){if(b.isBlocVisible){for(var c="",d=0;d<b.vdf_art_recommandes.length;d++){var e=b.vdf_art_recommandes[d];c+="<hr><div>",c+='<a href="'+e.alias+'" title="'+e.node_title+'">',c+='<div class="pull-left"><img src="'+e.img_path+'" alt="" /></div>',c+="<div>"+e.node_title+"</div>",c+='</a></div><div class="clearfix"></div>'}a(".bloc-vdf-articles-recommandes h3").after(c),a(".bloc-vdf-articles-recommandes").show()}else a(".bloc-vdf-articles-recommandes").parent().hide()}})}}(jQuery),/*!
  * Bowser - a browser detector
  * https://github.com/ded/bowser
  * MIT License | (c) Dustin Diaz 2015
  */
!function(a,b){"undefined"!=typeof module&&module.exports?module.exports=b():"function"==typeof define&&define.amd?define(b):this[a]=b()}("bowser",function(){function a(a){function c(b){var c=a.match(b);return c&&c.length>1&&c[1]||""}function d(b){var c=a.match(b);return c&&c.length>1&&c[2]||""}var e,f=c(/(ipod|iphone|ipad)/i).toLowerCase(),g=/like android/i.test(a),h=!g&&/android/i.test(a),i=/CrOS/.test(a),j=c(/edge\/(\d+(\.\d+)?)/i),k=c(/version\/(\d+(\.\d+)?)/i),l=/tablet/i.test(a),m=!l&&/[^-]mobi/i.test(a);/opera|opr/i.test(a)?e={name:"Opera",opera:b,version:k||c(/(?:opera|opr)[\s\/](\d+(\.\d+)?)/i)}:/yabrowser/i.test(a)?e={name:"Yandex Browser",yandexbrowser:b,version:k||c(/(?:yabrowser)[\s\/](\d+(\.\d+)?)/i)}:/windows phone/i.test(a)?(e={name:"Windows Phone",windowsphone:b},j?(e.msedge=b,e.version=j):(e.msie=b,e.version=c(/iemobile\/(\d+(\.\d+)?)/i))):/msie|trident/i.test(a)?e={name:"Internet Explorer",msie:b,version:c(/(?:msie |rv:)(\d+(\.\d+)?)/i)}:i?e={name:"Chrome",chromeBook:b,chrome:b,version:c(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)}:/chrome.+? edge/i.test(a)?e={name:"Microsoft Edge",msedge:b,version:j}:/chrome|crios|crmo/i.test(a)?e={name:"Chrome",chrome:b,version:c(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)}:f?(e={name:"iphone"==f?"iPhone":"ipad"==f?"iPad":"iPod"},k&&(e.version=k)):/sailfish/i.test(a)?e={name:"Sailfish",sailfish:b,version:c(/sailfish\s?browser\/(\d+(\.\d+)?)/i)}:/seamonkey\//i.test(a)?e={name:"SeaMonkey",seamonkey:b,version:c(/seamonkey\/(\d+(\.\d+)?)/i)}:/firefox|iceweasel/i.test(a)?(e={name:"Firefox",firefox:b,version:c(/(?:firefox|iceweasel)[ \/](\d+(\.\d+)?)/i)},/\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(a)&&(e.firefoxos=b)):/silk/i.test(a)?e={name:"Amazon Silk",silk:b,version:c(/silk\/(\d+(\.\d+)?)/i)}:h?e={name:"Android",version:k}:/phantom/i.test(a)?e={name:"PhantomJS",phantom:b,version:c(/phantomjs\/(\d+(\.\d+)?)/i)}:/blackberry|\bbb\d+/i.test(a)||/rim\stablet/i.test(a)?e={name:"BlackBerry",blackberry:b,version:k||c(/blackberry[\d]+\/(\d+(\.\d+)?)/i)}:/(web|hpw)os/i.test(a)?(e={name:"WebOS",webos:b,version:k||c(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)},/touchpad\//i.test(a)&&(e.touchpad=b)):e=/bada/i.test(a)?{name:"Bada",bada:b,version:c(/dolfin\/(\d+(\.\d+)?)/i)}:/tizen/i.test(a)?{name:"Tizen",tizen:b,version:c(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i)||k}:/safari/i.test(a)?{name:"Safari",safari:b,version:k}:{name:c(/^(.*)\/(.*) /),version:d(/^(.*)\/(.*) /)},!e.msedge&&/(apple)?webkit/i.test(a)?(e.name=e.name||"Webkit",e.webkit=b,!e.version&&k&&(e.version=k)):!e.opera&&/gecko\//i.test(a)&&(e.name=e.name||"Gecko",e.gecko=b,e.version=e.version||c(/gecko\/(\d+(\.\d+)?)/i)),e.msedge||!h&&!e.silk?f&&(e[f]=b,e.ios=b):e.android=b;var n="";e.windowsphone?n=c(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i):f?(n=c(/os (\d+([_\s]\d+)*) like mac os x/i),n=n.replace(/[_\s]/g,".")):h?n=c(/android[ \/-](\d+(\.\d+)*)/i):e.webos?n=c(/(?:web|hpw)os\/(\d+(\.\d+)*)/i):e.blackberry?n=c(/rim\stablet\sos\s(\d+(\.\d+)*)/i):e.bada?n=c(/bada\/(\d+(\.\d+)*)/i):e.tizen&&(n=c(/tizen[\/\s](\d+(\.\d+)*)/i)),n&&(e.osversion=n);var o=n.split(".")[0];return l||"ipad"==f||h&&(3==o||4==o&&!m)||e.silk?e.tablet=b:(m||"iphone"==f||"ipod"==f||h||e.blackberry||e.webos||e.bada)&&(e.mobile=b),e.msedge||e.msie&&e.version>=10||e.yandexbrowser&&e.version>=15||e.chrome&&e.version>=20||e.firefox&&e.version>=20||e.safari&&e.version>=6||e.opera&&e.version>=10||e.ios&&e.osversion&&e.osversion.split(".")[0]>=6||e.blackberry&&e.version>=10.1?e.a=b:e.msie&&e.version<10||e.chrome&&e.version<20||e.firefox&&e.version<20||e.safari&&e.version<6||e.opera&&e.version<10||e.ios&&e.osversion&&e.osversion.split(".")[0]<6?e.c=b:e.x=b,e}var b=!0,c=a("undefined"!=typeof navigator?navigator.userAgent:"");return c.test=function(a){for(var b=0;b<a.length;++b){var d=a[b];if("string"==typeof d&&d in c)return!0}return!1},c._detect=a,c})+function(a){"use strict";function b(){jQuery("a[href*='.pdf']").attr("target","_blank"),jQuery("a[href*='.doc']").attr("target","_blank"),jQuery("a[href*='.docx']").attr("target","_blank"),jQuery("a[href*='.xls']").attr("target","_blank"),jQuery("a[href*='.xlsx']").attr("target","_blank"),jQuery("a[href*='.ppt']").attr("target","_blank"),jQuery("a[href*='.pptx']").attr("target","_blank"),jQuery("a[href*='.jpg']").attr("target","_blank"),jQuery("a[href*='.png']").attr("target","_blank"),jQuery("a[href*='.gif']").attr("target","_blank"),jQuery("a[href*='.tiff']").attr("target","_blank")}(bowser.msie&&bowser.version<11||bowser.chrome&&bowser.version<26||bowser.firefox&&bowser.version<1.5||bowser.safari&&bowser.version<6)&&window.location.replace("/navigateur-obsolete.html"),a(".panel-accordeon h3").click(function(){a(this).parent().find(".panel-body").first().toggle(400,function(){a(this).parent().toggleClass("active")})});var c=[".subtitle-txt",".label-txt",".infos-txt",".infos-txt1"];a("#AplusBtn").on("click",function(){var b=parseInt(a("body").css("font-size"));b=b+2+"px",a("body").css({"font-size":b});for(var d=1;d<6;d++){var b=parseInt(a("h"+d).css("font-size"));b=b+2+"px",a("h"+d).css({"font-size":b})}c.forEach(function(b){var c=parseInt(a(b).css("font-size"));c=c+2+"px",a(b).css({"font-size":c})})}),a("#AmoinsBtn").on("click",function(){var b=parseInt(a("body").css("font-size"));b=b-2+"px",a("body").css({"font-size":b});for(var d=1;d<6;d++){var b=parseInt(a("h"+d).css("font-size"));b=b-2+"px",a("h"+d).css({"font-size":b})}c.forEach(function(b){var c=parseInt(a(b).css("font-size"));c=c-2+"px",a(b).css({"font-size":c})})}),b(),setTimeout(function(){if(a(".row-same-height").length>0&&a(".row-same-height .panel").length>0&&a(window).width()>959){a(".row-same-height").each(function(){if(a(this).find(".col-md-6:first-child .panel").length>0){var b=a(this).find(".col-md-6:first-child .panel"),c=a(this).find(".col-md-6:last-child .panel"),d=a(b).find(".title.subtitle-txt").height(),e=a(c).find(".title.subtitle-txt").height();if(d>e?a(c).find(".title.subtitle-txt").css("height",d+"px"):e>d&&a(b).find(".title.subtitle-txt").css("height",e+"px"),a(b).height()>a(c).height()){var f=a(b).height();a(c).css("height",f+2+"px");var g=a(c).find("h3").height();if(null==g)var g=a(c).find(".title").height();g+=30,a(c).find(".panel-body-white").css("height",f-g+"px")}else if(a(b).height()<a(c).height()){var f=a(c).height();a(b).css("height",f+2+"px");var g=a(b).find("h3").height();if(null==g)var g=a(c).find(".title").height();g+=30,a(b).find(".panel-body-white").css("height",f-g+"px")}}})}},100)}(jQuery),!function(){function a(a){return a&&(a.ownerDocument||a.document||a).documentElement}function b(a){return a&&(a.ownerDocument&&a.ownerDocument.defaultView||a.document&&a||a.defaultView)}function c(a,b){return b>a?-1:a>b?1:a>=b?0:NaN}function d(a){return null===a?NaN:+a}function e(a){return!isNaN(a)}function f(a){return{left:function(b,c,d,e){for(arguments.length<3&&(d=0),arguments.length<4&&(e=b.length);e>d;){var f=d+e>>>1;a(b[f],c)<0?d=f+1:e=f}return d},right:function(b,c,d,e){for(arguments.length<3&&(d=0),arguments.length<4&&(e=b.length);e>d;){var f=d+e>>>1;a(b[f],c)>0?e=f:d=f+1}return d}}}function g(a){return a.length}function h(a){for(var b=1;a*b%1;)b*=10;return b}function i(a,b){for(var c in b)Object.defineProperty(a.prototype,c,{value:b[c],enumerable:!1})}function j(){this._=Object.create(null)}function k(a){return(a+="")===ug||a[0]===vg?vg+a:a}function l(a){return(a+="")[0]===vg?a.slice(1):a}function m(a){return k(a)in this._}function n(a){return(a=k(a))in this._&&delete this._[a]}function o(){var a=[];for(var b in this._)a.push(l(b));return a}function p(){var a=0;for(var b in this._)++a;return a}function q(){for(var a in this._)return!1;return!0}function r(){this._=Object.create(null)}function s(a){return a}function t(a,b,c){return function(){var d=c.apply(b,arguments);return d===b?a:d}}function u(a,b){if(b in a)return b;b=b.charAt(0).toUpperCase()+b.slice(1);for(var c=0,d=wg.length;d>c;++c){var e=wg[c]+b;if(e in a)return e}}function v(){}function w(){}function x(a){function b(){for(var b,d=c,e=-1,f=d.length;++e<f;)(b=d[e].on)&&b.apply(this,arguments);return a}var c=[],d=new j;return b.on=function(b,e){var f,g=d.get(b);return arguments.length<2?g&&g.on:(g&&(g.on=null,c=c.slice(0,f=c.indexOf(g)).concat(c.slice(f+1)),d.remove(b)),e&&c.push(d.set(b,{on:e})),a)},b}function y(){hg.event.preventDefault()}function z(){for(var a,b=hg.event;a=b.sourceEvent;)b=a;return b}function A(a){for(var b=new w,c=0,d=arguments.length;++c<d;)b[arguments[c]]=x(b);return b.of=function(c,d){return function(e){try{var f=e.sourceEvent=hg.event;e.target=a,hg.event=e,b[e.type].apply(c,d)}finally{hg.event=f}}},b}function B(a){return yg(a,Cg),a}function C(a){return"function"==typeof a?a:function(){return zg(a,this)}}function D(a){return"function"==typeof a?a:function(){return Ag(a,this)}}function E(a,b){function c(){this.removeAttribute(a)}function d(){this.removeAttributeNS(a.space,a.local)}function e(){this.setAttribute(a,b)}function f(){this.setAttributeNS(a.space,a.local,b)}function g(){var c=b.apply(this,arguments);null==c?this.removeAttribute(a):this.setAttribute(a,c)}function h(){var c=b.apply(this,arguments);null==c?this.removeAttributeNS(a.space,a.local):this.setAttributeNS(a.space,a.local,c)}return a=hg.ns.qualify(a),null==b?a.local?d:c:"function"==typeof b?a.local?h:g:a.local?f:e}function F(a){return a.trim().replace(/\s+/g," ")}function G(a){return new RegExp("(?:^|\\s+)"+hg.requote(a)+"(?:\\s+|$)","g")}function H(a){return(a+"").trim().split(/^|\s+/)}function I(a,b){function c(){for(var c=-1;++c<e;)a[c](this,b)}function d(){for(var c=-1,d=b.apply(this,arguments);++c<e;)a[c](this,d)}a=H(a).map(J);var e=a.length;return"function"==typeof b?d:c}function J(a){var b=G(a);return function(c,d){if(e=c.classList)return d?e.add(a):e.remove(a);var e=c.getAttribute("class")||"";d?(b.lastIndex=0,b.test(e)||c.setAttribute("class",F(e+" "+a))):c.setAttribute("class",F(e.replace(b," ")))}}function K(a,b,c){function d(){this.style.removeProperty(a)}function e(){this.style.setProperty(a,b,c)}function f(){var d=b.apply(this,arguments);null==d?this.style.removeProperty(a):this.style.setProperty(a,d,c)}return null==b?d:"function"==typeof b?f:e}function L(a,b){function c(){delete this[a]}function d(){this[a]=b}function e(){var c=b.apply(this,arguments);null==c?delete this[a]:this[a]=c}return null==b?c:"function"==typeof b?e:d}function M(a){function b(){var b=this.ownerDocument,c=this.namespaceURI;return c?b.createElementNS(c,a):b.createElement(a)}function c(){return this.ownerDocument.createElementNS(a.space,a.local)}return"function"==typeof a?a:(a=hg.ns.qualify(a)).local?c:b}function N(){var a=this.parentNode;a&&a.removeChild(this)}function O(a){return{__data__:a}}function P(a){return function(){return Bg(this,a)}}function Q(a){return arguments.length||(a=c),function(b,c){return b&&c?a(b.__data__,c.__data__):!b-!c}}function R(a,b){for(var c=0,d=a.length;d>c;c++)for(var e,f=a[c],g=0,h=f.length;h>g;g++)(e=f[g])&&b(e,g,c);return a}function S(a){return yg(a,Eg),a}function T(a){var b,c;return function(d,e,f){var g,h=a[f].update,i=h.length;for(f!=c&&(c=f,b=0),e>=b&&(b=e+1);!(g=h[b])&&++b<i;);return g}}function U(a,b,c){function d(){var b=this[g];b&&(this.removeEventListener(a,b,b.$),delete this[g])}function e(){var e=i(b,jg(arguments));d.call(this),this.addEventListener(a,this[g]=e,e.$=c),e._=b}function f(){var b,c=new RegExp("^__on([^.]+)"+hg.requote(a)+"$");for(var d in this)if(b=d.match(c)){var e=this[d];this.removeEventListener(b[1],e,e.$),delete this[d]}}var g="__on"+a,h=a.indexOf("."),i=V;h>0&&(a=a.slice(0,h));var j=Fg.get(a);return j&&(a=j,i=W),h?b?e:d:b?v:f}function V(a,b){return function(c){var d=hg.event;hg.event=c,b[0]=this.__data__;try{a.apply(this,b)}finally{hg.event=d}}}function W(a,b){var c=V(a,b);return function(a){var b=this,d=a.relatedTarget;d&&(d===b||8&d.compareDocumentPosition(b))||c.call(b,a)}}function X(c){var d=".dragsuppress-"+ ++Hg,e="click"+d,f=hg.select(b(c)).on("touchmove"+d,y).on("dragstart"+d,y).on("selectstart"+d,y);if(null==Gg&&(Gg=!("onselectstart"in c)&&u(c.style,"userSelect")),Gg){var g=a(c).style,h=g[Gg];g[Gg]="none"}return function(a){if(f.on(d,null),Gg&&(g[Gg]=h),a){var b=function(){f.on(e,null)};f.on(e,function(){y(),b()},!0),setTimeout(b,0)}}}function Y(a,c){c.changedTouches&&(c=c.changedTouches[0]);var d=a.ownerSVGElement||a;if(d.createSVGPoint){var e=d.createSVGPoint();if(0>Ig){var f=b(a);if(f.scrollX||f.scrollY){d=hg.select("body").append("svg").style({position:"absolute",top:0,left:0,margin:0,padding:0,border:"none"},"important");var g=d[0][0].getScreenCTM();Ig=!(g.f||g.e),d.remove()}}return Ig?(e.x=c.pageX,e.y=c.pageY):(e.x=c.clientX,e.y=c.clientY),e=e.matrixTransform(a.getScreenCTM().inverse()),[e.x,e.y]}var h=a.getBoundingClientRect();return[c.clientX-h.left-a.clientLeft,c.clientY-h.top-a.clientTop]}function Z(){return hg.event.changedTouches[0].identifier}function $(a){return a>0?1:0>a?-1:0}function _(a,b,c){return(b[0]-a[0])*(c[1]-a[1])-(b[1]-a[1])*(c[0]-a[0])}function aa(a){return a>1?0:-1>a?Lg:Math.acos(a)}function ba(a){return a>1?Og:-1>a?-Og:Math.asin(a)}function ca(a){return((a=Math.exp(a))-1/a)/2}function da(a){return((a=Math.exp(a))+1/a)/2}function ea(a){return((a=Math.exp(2*a))-1)/(a+1)}function fa(a){return(a=Math.sin(a/2))*a}function ga(){}function ha(a,b,c){return this instanceof ha?(this.h=+a,this.s=+b,void(this.l=+c)):arguments.length<2?a instanceof ha?new ha(a.h,a.s,a.l):va(""+a,wa,ha):new ha(a,b,c)}function ia(a,b,c){function d(a){return a>360?a-=360:0>a&&(a+=360),60>a?f+(g-f)*a/60:180>a?g:240>a?f+(g-f)*(240-a)/60:f}function e(a){return Math.round(255*d(a))}var f,g;return a=isNaN(a)?0:(a%=360)<0?a+360:a,b=isNaN(b)?0:0>b?0:b>1?1:b,c=0>c?0:c>1?1:c,g=.5>=c?c*(1+b):c+b-c*b,f=2*c-g,new ra(e(a+120),e(a),e(a-120))}function ja(a,b,c){return this instanceof ja?(this.h=+a,this.c=+b,void(this.l=+c)):arguments.length<2?a instanceof ja?new ja(a.h,a.c,a.l):a instanceof la?na(a.l,a.a,a.b):na((a=xa((a=hg.rgb(a)).r,a.g,a.b)).l,a.a,a.b):new ja(a,b,c)}function ka(a,b,c){return isNaN(a)&&(a=0),isNaN(b)&&(b=0),new la(c,Math.cos(a*=Pg)*b,Math.sin(a)*b)}function la(a,b,c){return this instanceof la?(this.l=+a,this.a=+b,void(this.b=+c)):arguments.length<2?a instanceof la?new la(a.l,a.a,a.b):a instanceof ja?ka(a.h,a.c,a.l):xa((a=ra(a)).r,a.g,a.b):new la(a,b,c)}function ma(a,b,c){var d=(a+16)/116,e=d+b/500,f=d-c/200;return e=oa(e)*$g,d=oa(d)*_g,f=oa(f)*ah,new ra(qa(3.2404542*e-1.5371385*d-.4985314*f),qa(-.969266*e+1.8760108*d+.041556*f),qa(.0556434*e-.2040259*d+1.0572252*f))}function na(a,b,c){return a>0?new ja(Math.atan2(c,b)*Qg,Math.sqrt(b*b+c*c),a):new ja(NaN,NaN,a)}function oa(a){return a>.206893034?a*a*a:(a-4/29)/7.787037}function pa(a){return a>.008856?Math.pow(a,1/3):7.787037*a+4/29}function qa(a){return Math.round(255*(.00304>=a?12.92*a:1.055*Math.pow(a,1/2.4)-.055))}function ra(a,b,c){return this instanceof ra?(this.r=~~a,this.g=~~b,void(this.b=~~c)):arguments.length<2?a instanceof ra?new ra(a.r,a.g,a.b):va(""+a,ra,ia):new ra(a,b,c)}function sa(a){return new ra(a>>16,a>>8&255,255&a)}function ta(a){return sa(a)+""}function ua(a){return 16>a?"0"+Math.max(0,a).toString(16):Math.min(255,a).toString(16)}function va(a,b,c){var d,e,f,g=0,h=0,i=0;if(d=/([a-z]+)\((.*)\)/.exec(a=a.toLowerCase()))switch(e=d[2].split(","),d[1]){case"hsl":return c(parseFloat(e[0]),parseFloat(e[1])/100,parseFloat(e[2])/100);case"rgb":return b(za(e[0]),za(e[1]),za(e[2]))}return(f=dh.get(a))?b(f.r,f.g,f.b):(null==a||"#"!==a.charAt(0)||isNaN(f=parseInt(a.slice(1),16))||(4===a.length?(g=(3840&f)>>4,g=g>>4|g,h=240&f,h=h>>4|h,i=15&f,i=i<<4|i):7===a.length&&(g=(16711680&f)>>16,h=(65280&f)>>8,i=255&f)),b(g,h,i))}function wa(a,b,c){var d,e,f=Math.min(a/=255,b/=255,c/=255),g=Math.max(a,b,c),h=g-f,i=(g+f)/2;return h?(e=.5>i?h/(g+f):h/(2-g-f),d=a==g?(b-c)/h+(c>b?6:0):b==g?(c-a)/h+2:(a-b)/h+4,d*=60):(d=NaN,e=i>0&&1>i?0:d),new ha(d,e,i)}function xa(a,b,c){a=ya(a),b=ya(b),c=ya(c);var d=pa((.4124564*a+.3575761*b+.1804375*c)/$g),e=pa((.2126729*a+.7151522*b+.072175*c)/_g),f=pa((.0193339*a+.119192*b+.9503041*c)/ah);return la(116*e-16,500*(d-e),200*(e-f))}function ya(a){return(a/=255)<=.04045?a/12.92:Math.pow((a+.055)/1.055,2.4)}function za(a){var b=parseFloat(a);return"%"===a.charAt(a.length-1)?Math.round(2.55*b):b}function Aa(a){return"function"==typeof a?a:function(){return a}}function Ba(a){return function(b,c,d){return 2===arguments.length&&"function"==typeof c&&(d=c,c=null),Ca(b,c,a,d)}}function Ca(a,b,c,d){function e(){var a,b=i.status;if(!b&&Ea(i)||b>=200&&300>b||304===b){try{a=c.call(f,i)}catch(d){return void g.error.call(f,d)}g.load.call(f,a)}else g.error.call(f,i)}var f={},g=hg.dispatch("beforesend","progress","load","error"),h={},i=new XMLHttpRequest,j=null;return!this.XDomainRequest||"withCredentials"in i||!/^(http(s)?:)?\/\//.test(a)||(i=new XDomainRequest),"onload"in i?i.onload=i.onerror=e:i.onreadystatechange=function(){i.readyState>3&&e()},i.onprogress=function(a){var b=hg.event;hg.event=a;try{g.progress.call(f,i)}finally{hg.event=b}},f.header=function(a,b){return a=(a+"").toLowerCase(),arguments.length<2?h[a]:(null==b?delete h[a]:h[a]=b+"",f)},f.mimeType=function(a){return arguments.length?(b=null==a?null:a+"",f):b},f.responseType=function(a){return arguments.length?(j=a,f):j},f.response=function(a){return c=a,f},["get","post"].forEach(function(a){f[a]=function(){return f.send.apply(f,[a].concat(jg(arguments)))}}),f.send=function(c,d,e){if(2===arguments.length&&"function"==typeof d&&(e=d,d=null),i.open(c,a,!0),null==b||"accept"in h||(h.accept=b+",*/*"),i.setRequestHeader)for(var k in h)i.setRequestHeader(k,h[k]);return null!=b&&i.overrideMimeType&&i.overrideMimeType(b),null!=j&&(i.responseType=j),null!=e&&f.on("error",e).on("load",function(a){e(null,a)}),g.beforesend.call(f,i),i.send(null==d?null:d),f},f.abort=function(){return i.abort(),f},hg.rebind(f,g,"on"),null==d?f:f.get(Da(d))}function Da(a){return 1===a.length?function(b,c){a(null==b?c:null)}:a}function Ea(a){var b=a.responseType;return b&&"text"!==b?a.response:a.responseText}function Fa(a,b,c){var d=arguments.length;2>d&&(b=0),3>d&&(c=Date.now());var e=c+b,f={c:a,t:e,n:null};return fh?fh.n=f:eh=f,fh=f,gh||(hh=clearTimeout(hh),gh=1,ih(Ga)),f}function Ga(){var a=Ha(),b=Ia()-a;b>24?(isFinite(b)&&(clearTimeout(hh),hh=setTimeout(Ga,b)),gh=0):(gh=1,ih(Ga))}function Ha(){for(var a=Date.now(),b=eh;b;)a>=b.t&&b.c(a-b.t)&&(b.c=null),b=b.n;return a}function Ia(){for(var a,b=eh,c=1/0;b;)b.c?(b.t<c&&(c=b.t),b=(a=b).n):b=a?a.n=b.n:eh=b.n;return fh=a,c}function Ja(a,b){return b-(a?Math.ceil(Math.log(a)/Math.LN10):1)}function Ka(a,b){var c=Math.pow(10,3*tg(8-b));return{scale:b>8?function(a){return a/c}:function(a){return a*c},symbol:a}}function La(a){var b=a.decimal,c=a.thousands,d=a.grouping,e=a.currency,f=d&&c?function(a,b){for(var e=a.length,f=[],g=0,h=d[0],i=0;e>0&&h>0&&(i+h+1>b&&(h=Math.max(1,b-i)),f.push(a.substring(e-=h,e+h)),!((i+=h+1)>b));)h=d[g=(g+1)%d.length];return f.reverse().join(c)}:s;return function(a){var c=kh.exec(a),d=c[1]||" ",g=c[2]||">",h=c[3]||"-",i=c[4]||"",j=c[5],k=+c[6],l=c[7],m=c[8],n=c[9],o=1,p="",q="",r=!1,s=!0;switch(m&&(m=+m.substring(1)),(j||"0"===d&&"="===g)&&(j=d="0",g="="),n){case"n":l=!0,n="g";break;case"%":o=100,q="%",n="f";break;case"p":o=100,q="%",n="r";break;case"b":case"o":case"x":case"X":"#"===i&&(p="0"+n.toLowerCase());case"c":s=!1;case"d":r=!0,m=0;break;case"s":o=-1,n="r"}"$"===i&&(p=e[0],q=e[1]),"r"!=n||m||(n="g"),null!=m&&("g"==n?m=Math.max(1,Math.min(21,m)):("e"==n||"f"==n)&&(m=Math.max(0,Math.min(20,m)))),n=lh.get(n)||Ma;var t=j&&l;return function(a){var c=q;if(r&&a%1)return"";var e=0>a||0===a&&0>1/a?(a=-a,"-"):"-"===h?"":h;if(0>o){var i=hg.formatPrefix(a,m);a=i.scale(a),c=i.symbol+q}else a*=o;a=n(a,m);var u,v,w=a.lastIndexOf(".");if(0>w){var x=s?a.lastIndexOf("e"):-1;0>x?(u=a,v=""):(u=a.substring(0,x),v=a.substring(x))}else u=a.substring(0,w),v=b+a.substring(w+1);!j&&l&&(u=f(u,1/0));var y=p.length+u.length+v.length+(t?0:e.length),z=k>y?new Array(y=k-y+1).join(d):"";return t&&(u=f(z+u,z.length?k-v.length:1/0)),e+=p,a=u+v,("<"===g?e+a+z:">"===g?z+e+a:"^"===g?z.substring(0,y>>=1)+e+a+z.substring(y):e+(t?a:z+a))+c}}}function Ma(a){return a+""}function Na(){this._=new Date(arguments.length>1?Date.UTC.apply(this,arguments):arguments[0])}function Oa(a,b,c){function d(b){var c=a(b),d=f(c,1);return d-b>b-c?c:d}function e(c){return b(c=a(new nh(c-1)),1),c}function f(a,c){return b(a=new nh((+a)),c),a}function g(a,d,f){var g=e(a),h=[];if(f>1)for(;d>g;)c(g)%f||h.push(new Date((+g))),b(g,1);else for(;d>g;)h.push(new Date((+g))),b(g,1);return h}function h(a,b,c){try{nh=Na;var d=new Na;return d._=a,g(d,b,c)}finally{nh=Date}}a.floor=a,a.round=d,a.ceil=e,a.offset=f,a.range=g;var i=a.utc=Pa(a);return i.floor=i,i.round=Pa(d),i.ceil=Pa(e),i.offset=Pa(f),i.range=h,a}function Pa(a){return function(b,c){try{nh=Na;var d=new Na;return d._=b,a(d,c)._}finally{nh=Date}}}function Qa(a){function b(a){function b(b){for(var c,e,f,g=[],h=-1,i=0;++h<d;)37===a.charCodeAt(h)&&(g.push(a.slice(i,h)),null!=(e=ph[c=a.charAt(++h)])&&(c=a.charAt(++h)),(f=C[c])&&(c=f(b,null==e?"e"===c?" ":"0":e)),g.push(c),i=h+1);return g.push(a.slice(i,h)),g.join("")}var d=a.length;return b.parse=function(b){var d={y:1900,m:0,d:1,H:0,M:0,S:0,L:0,Z:null},e=c(d,a,b,0);if(e!=b.length)return null;"p"in d&&(d.H=d.H%12+12*d.p);var f=null!=d.Z&&nh!==Na,g=new(f?Na:nh);return"j"in d?g.setFullYear(d.y,0,d.j):"W"in d||"U"in d?("w"in d||(d.w="W"in d?1:0),g.setFullYear(d.y,0,1),g.setFullYear(d.y,0,"W"in d?(d.w+6)%7+7*d.W-(g.getDay()+5)%7:d.w+7*d.U-(g.getDay()+6)%7)):g.setFullYear(d.y,d.m,d.d),g.setHours(d.H+(d.Z/100|0),d.M+d.Z%100,d.S,d.L),f?g._:g},b.toString=function(){return a},b}function c(a,b,c,d){for(var e,f,g,h=0,i=b.length,j=c.length;i>h;){if(d>=j)return-1;if(e=b.charCodeAt(h++),37===e){if(g=b.charAt(h++),f=D[g in ph?b.charAt(h++):g],!f||(d=f(a,c,d))<0)return-1}else if(e!=c.charCodeAt(d++))return-1}return d}function d(a,b,c){w.lastIndex=0;var d=w.exec(b.slice(c));return d?(a.w=x.get(d[0].toLowerCase()),c+d[0].length):-1}function e(a,b,c){u.lastIndex=0;var d=u.exec(b.slice(c));return d?(a.w=v.get(d[0].toLowerCase()),c+d[0].length):-1}function f(a,b,c){A.lastIndex=0;var d=A.exec(b.slice(c));return d?(a.m=B.get(d[0].toLowerCase()),c+d[0].length):-1}function g(a,b,c){y.lastIndex=0;var d=y.exec(b.slice(c));return d?(a.m=z.get(d[0].toLowerCase()),c+d[0].length):-1}function h(a,b,d){return c(a,C.c.toString(),b,d)}function i(a,b,d){return c(a,C.x.toString(),b,d)}function j(a,b,d){return c(a,C.X.toString(),b,d)}function k(a,b,c){var d=t.get(b.slice(c,c+=2).toLowerCase());return null==d?-1:(a.p=d,c)}var l=a.dateTime,m=a.date,n=a.time,o=a.periods,p=a.days,q=a.shortDays,r=a.months,s=a.shortMonths;b.utc=function(a){function c(a){try{nh=Na;var b=new nh;return b._=a,d(b)}finally{nh=Date}}var d=b(a);return c.parse=function(a){try{nh=Na;var b=d.parse(a);return b&&b._}finally{nh=Date}},c.toString=d.toString,c},b.multi=b.utc.multi=ib;var t=hg.map(),u=Sa(p),v=Ta(p),w=Sa(q),x=Ta(q),y=Sa(r),z=Ta(r),A=Sa(s),B=Ta(s);o.forEach(function(a,b){t.set(a.toLowerCase(),b)});var C={a:function(a){return q[a.getDay()]},A:function(a){return p[a.getDay()]},b:function(a){return s[a.getMonth()]},B:function(a){return r[a.getMonth()]},c:b(l),d:function(a,b){return Ra(a.getDate(),b,2)},e:function(a,b){return Ra(a.getDate(),b,2)},H:function(a,b){return Ra(a.getHours(),b,2)},I:function(a,b){return Ra(a.getHours()%12||12,b,2)},j:function(a,b){return Ra(1+mh.dayOfYear(a),b,3)},L:function(a,b){return Ra(a.getMilliseconds(),b,3)},m:function(a,b){return Ra(a.getMonth()+1,b,2)},M:function(a,b){return Ra(a.getMinutes(),b,2)},p:function(a){return o[+(a.getHours()>=12)]},S:function(a,b){return Ra(a.getSeconds(),b,2)},U:function(a,b){return Ra(mh.sundayOfYear(a),b,2)},w:function(a){return a.getDay()},W:function(a,b){return Ra(mh.mondayOfYear(a),b,2)},x:b(m),X:b(n),y:function(a,b){return Ra(a.getFullYear()%100,b,2)},Y:function(a,b){return Ra(a.getFullYear()%1e4,b,4)},Z:gb,"%":function(){return"%"}},D={a:d,A:e,b:f,B:g,c:h,d:ab,e:ab,H:cb,I:cb,j:bb,L:fb,m:_a,M:db,p:k,S:eb,U:Va,w:Ua,W:Wa,x:i,X:j,y:Ya,Y:Xa,Z:Za,"%":hb};return b}function Ra(a,b,c){var d=0>a?"-":"",e=(d?-a:a)+"",f=e.length;return d+(c>f?new Array(c-f+1).join(b)+e:e)}function Sa(a){return new RegExp("^(?:"+a.map(hg.requote).join("|")+")","i")}function Ta(a){for(var b=new j,c=-1,d=a.length;++c<d;)b.set(a[c].toLowerCase(),c);return b}function Ua(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+1));return d?(a.w=+d[0],c+d[0].length):-1}function Va(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c));return d?(a.U=+d[0],c+d[0].length):-1}function Wa(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c));return d?(a.W=+d[0],c+d[0].length):-1}function Xa(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+4));return d?(a.y=+d[0],c+d[0].length):-1}function Ya(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.y=$a(+d[0]),c+d[0].length):-1}function Za(a,b,c){return/^[+-]\d{4}$/.test(b=b.slice(c,c+5))?(a.Z=-b,c+5):-1}function $a(a){return a+(a>68?1900:2e3)}function _a(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.m=d[0]-1,c+d[0].length):-1}function ab(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.d=+d[0],c+d[0].length):-1}function bb(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+3));return d?(a.j=+d[0],c+d[0].length):-1}function cb(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.H=+d[0],c+d[0].length):-1}function db(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.M=+d[0],c+d[0].length):-1}function eb(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+2));return d?(a.S=+d[0],c+d[0].length):-1}function fb(a,b,c){qh.lastIndex=0;var d=qh.exec(b.slice(c,c+3));return d?(a.L=+d[0],c+d[0].length):-1}function gb(a){var b=a.getTimezoneOffset(),c=b>0?"-":"+",d=tg(b)/60|0,e=tg(b)%60;return c+Ra(d,"0",2)+Ra(e,"0",2)}function hb(a,b,c){rh.lastIndex=0;var d=rh.exec(b.slice(c,c+1));return d?c+d[0].length:-1}function ib(a){for(var b=a.length,c=-1;++c<b;)a[c][0]=this(a[c][0]);return function(b){for(var c=0,d=a[c];!d[1](b);)d=a[++c];return d[0](b)}}function jb(){}function kb(a,b,c){var d=c.s=a+b,e=d-a,f=d-e;c.t=a-f+(b-e)}function lb(a,b){a&&vh.hasOwnProperty(a.type)&&vh[a.type](a,b)}function mb(a,b,c){var d,e=-1,f=a.length-c;for(b.lineStart();++e<f;)d=a[e],b.point(d[0],d[1],d[2]);b.lineEnd()}function nb(a,b){var c=-1,d=a.length;for(b.polygonStart();++c<d;)mb(a[c],b,1);b.polygonEnd()}function ob(){function a(a,b){a*=Pg,b=b*Pg/2+Lg/4;var c=a-d,g=c>=0?1:-1,h=g*c,i=Math.cos(b),j=Math.sin(b),k=f*j,l=e*i+k*Math.cos(h),m=k*g*Math.sin(h);xh.add(Math.atan2(m,l)),d=a,e=i,f=j}var b,c,d,e,f;yh.point=function(g,h){yh.point=a,d=(b=g)*Pg,e=Math.cos(h=(c=h)*Pg/2+Lg/4),f=Math.sin(h)},yh.lineEnd=function(){a(b,c)}}function pb(a){var b=a[0],c=a[1],d=Math.cos(c);return[d*Math.cos(b),d*Math.sin(b),Math.sin(c)]}function qb(a,b){return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]}function rb(a,b){return[a[1]*b[2]-a[2]*b[1],a[2]*b[0]-a[0]*b[2],a[0]*b[1]-a[1]*b[0]]}function sb(a,b){a[0]+=b[0],a[1]+=b[1],a[2]+=b[2]}function tb(a,b){return[a[0]*b,a[1]*b,a[2]*b]}function ub(a){var b=Math.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);a[0]/=b,a[1]/=b,a[2]/=b}function vb(a){return[Math.atan2(a[1],a[0]),ba(a[2])]}function wb(a,b){return tg(a[0]-b[0])<Jg&&tg(a[1]-b[1])<Jg}function xb(a,b){a*=Pg;var c=Math.cos(b*=Pg);yb(c*Math.cos(a),c*Math.sin(a),Math.sin(b))}function yb(a,b,c){++zh,Bh+=(a-Bh)/zh,Ch+=(b-Ch)/zh,Dh+=(c-Dh)/zh}function zb(){function a(a,e){a*=Pg;var f=Math.cos(e*=Pg),g=f*Math.cos(a),h=f*Math.sin(a),i=Math.sin(e),j=Math.atan2(Math.sqrt((j=c*i-d*h)*j+(j=d*g-b*i)*j+(j=b*h-c*g)*j),b*g+c*h+d*i);Ah+=j,Eh+=j*(b+(b=g)),Fh+=j*(c+(c=h)),Gh+=j*(d+(d=i)),yb(b,c,d)}var b,c,d;Kh.point=function(e,f){e*=Pg;var g=Math.cos(f*=Pg);b=g*Math.cos(e),c=g*Math.sin(e),d=Math.sin(f),Kh.point=a,yb(b,c,d)}}function Ab(){Kh.point=xb}function Bb(){function a(a,b){a*=Pg;var c=Math.cos(b*=Pg),g=c*Math.cos(a),h=c*Math.sin(a),i=Math.sin(b),j=e*i-f*h,k=f*g-d*i,l=d*h-e*g,m=Math.sqrt(j*j+k*k+l*l),n=d*g+e*h+f*i,o=m&&-aa(n)/m,p=Math.atan2(m,n);Hh+=o*j,Ih+=o*k,Jh+=o*l,Ah+=p,Eh+=p*(d+(d=g)),Fh+=p*(e+(e=h)),Gh+=p*(f+(f=i)),yb(d,e,f)}var b,c,d,e,f;Kh.point=function(g,h){b=g,c=h,Kh.point=a,g*=Pg;var i=Math.cos(h*=Pg);d=i*Math.cos(g),e=i*Math.sin(g),f=Math.sin(h),yb(d,e,f)},Kh.lineEnd=function(){a(b,c),Kh.lineEnd=Ab,Kh.point=xb}}function Cb(a,b){function c(c,d){return c=a(c,d),b(c[0],c[1])}return a.invert&&b.invert&&(c.invert=function(c,d){return c=b.invert(c,d),c&&a.invert(c[0],c[1])}),c}function Db(){return!0}function Eb(a,b,c,d,e){var f=[],g=[];if(a.forEach(function(a){if(!((b=a.length-1)<=0)){var b,c=a[0],d=a[b];if(wb(c,d)){e.lineStart();for(var h=0;b>h;++h)e.point((c=a[h])[0],c[1]);return void e.lineEnd()}var i=new Gb(c,a,null,(!0)),j=new Gb(c,null,i,(!1));i.o=j,f.push(i),g.push(j),i=new Gb(d,a,null,(!1)),j=new Gb(d,null,i,(!0)),i.o=j,f.push(i),g.push(j)}}),g.sort(b),Fb(f),Fb(g),f.length){for(var h=0,i=c,j=g.length;j>h;++h)g[h].e=i=!i;for(var k,l,m=f[0];;){for(var n=m,o=!0;n.v;)if((n=n.n)===m)return;k=n.z,e.lineStart();do{if(n.v=n.o.v=!0,n.e){if(o)for(var h=0,j=k.length;j>h;++h)e.point((l=k[h])[0],l[1]);else d(n.x,n.n.x,1,e);n=n.n}else{if(o){k=n.p.z;for(var h=k.length-1;h>=0;--h)e.point((l=k[h])[0],l[1])}else d(n.x,n.p.x,-1,e);n=n.p}n=n.o,k=n.z,o=!o}while(!n.v);e.lineEnd()}}}function Fb(a){if(b=a.length){for(var b,c,d=0,e=a[0];++d<b;)e.n=c=a[d],c.p=e,e=c;e.n=c=a[0],c.p=e}}function Gb(a,b,c,d){this.x=a,this.z=b,this.o=c,this.e=d,this.v=!1,this.n=this.p=null}function Hb(a,b,c,d){return function(e,f){function g(b,c){var d=e(b,c);a(b=d[0],c=d[1])&&f.point(b,c)}function h(a,b){var c=e(a,b);q.point(c[0],c[1])}function i(){s.point=h,q.lineStart()}function j(){s.point=g,q.lineEnd()}function k(a,b){p.push([a,b]);var c=e(a,b);u.point(c[0],c[1])}function l(){u.lineStart(),p=[]}function m(){k(p[0][0],p[0][1]),u.lineEnd();var a,b=u.clean(),c=t.buffer(),d=c.length;if(p.pop(),o.push(p),p=null,d)if(1&b){a=c[0];var e,d=a.length-1,g=-1;if(d>0){for(v||(f.polygonStart(),v=!0),f.lineStart();++g<d;)f.point((e=a[g])[0],e[1]);f.lineEnd()}}else d>1&&2&b&&c.push(c.pop().concat(c.shift())),n.push(c.filter(Ib))}var n,o,p,q=b(f),r=e.invert(d[0],d[1]),s={point:g,lineStart:i,lineEnd:j,polygonStart:function(){s.point=k,s.lineStart=l,s.lineEnd=m,n=[],o=[]},polygonEnd:function(){s.point=g,s.lineStart=i,s.lineEnd=j,n=hg.merge(n);var a=Ob(r,o);n.length?(v||(f.polygonStart(),v=!0),Eb(n,Kb,a,c,f)):a&&(v||(f.polygonStart(),v=!0),f.lineStart(),c(null,null,1,f),f.lineEnd()),v&&(f.polygonEnd(),v=!1),n=o=null},sphere:function(){f.polygonStart(),f.lineStart(),c(null,null,1,f),f.lineEnd(),f.polygonEnd()}},t=Jb(),u=b(t),v=!1;return s}}function Ib(a){return a.length>1}function Jb(){var a,b=[];return{lineStart:function(){b.push(a=[])},point:function(b,c){a.push([b,c])},lineEnd:v,buffer:function(){var c=b;return b=[],a=null,c},rejoin:function(){b.length>1&&b.push(b.pop().concat(b.shift()))}}}function Kb(a,b){return((a=a.x)[0]<0?a[1]-Og-Jg:Og-a[1])-((b=b.x)[0]<0?b[1]-Og-Jg:Og-b[1])}function Lb(a){var b,c=NaN,d=NaN,e=NaN;return{lineStart:function(){a.lineStart(),b=1},point:function(f,g){var h=f>0?Lg:-Lg,i=tg(f-c);tg(i-Lg)<Jg?(a.point(c,d=(d+g)/2>0?Og:-Og),a.point(e,d),a.lineEnd(),a.lineStart(),a.point(h,d),a.point(f,d),b=0):e!==h&&i>=Lg&&(tg(c-e)<Jg&&(c-=e*Jg),tg(f-h)<Jg&&(f-=h*Jg),d=Mb(c,d,f,g),a.point(e,d),a.lineEnd(),a.lineStart(),a.point(h,d),b=0),a.point(c=f,d=g),e=h},lineEnd:function(){a.lineEnd(),c=d=NaN},clean:function(){return 2-b}}}function Mb(a,b,c,d){var e,f,g=Math.sin(a-c);return tg(g)>Jg?Math.atan((Math.sin(b)*(f=Math.cos(d))*Math.sin(c)-Math.sin(d)*(e=Math.cos(b))*Math.sin(a))/(e*f*g)):(b+d)/2}function Nb(a,b,c,d){var e;if(null==a)e=c*Og,d.point(-Lg,e),d.point(0,e),d.point(Lg,e),d.point(Lg,0),d.point(Lg,-e),d.point(0,-e),d.point(-Lg,-e),d.point(-Lg,0),d.point(-Lg,e);else if(tg(a[0]-b[0])>Jg){var f=a[0]<b[0]?Lg:-Lg;e=c*f/2,d.point(-f,e),d.point(0,e),d.point(f,e)}else d.point(b[0],b[1])}function Ob(a,b){var c=a[0],d=a[1],e=[Math.sin(c),-Math.cos(c),0],f=0,g=0;xh.reset();for(var h=0,i=b.length;i>h;++h){
var j=b[h],k=j.length;if(k)for(var l=j[0],m=l[0],n=l[1]/2+Lg/4,o=Math.sin(n),p=Math.cos(n),q=1;;){q===k&&(q=0),a=j[q];var r=a[0],s=a[1]/2+Lg/4,t=Math.sin(s),u=Math.cos(s),v=r-m,w=v>=0?1:-1,x=w*v,y=x>Lg,z=o*t;if(xh.add(Math.atan2(z*w*Math.sin(x),p*u+z*Math.cos(x))),f+=y?v+w*Mg:v,y^m>=c^r>=c){var A=rb(pb(l),pb(a));ub(A);var B=rb(e,A);ub(B);var C=(y^v>=0?-1:1)*ba(B[2]);(d>C||d===C&&(A[0]||A[1]))&&(g+=y^v>=0?1:-1)}if(!q++)break;m=r,o=t,p=u,l=a}}return(-Jg>f||Jg>f&&0>xh)^1&g}function Pb(a){function b(a,b){return Math.cos(a)*Math.cos(b)>f}function c(a){var c,f,i,j,k;return{lineStart:function(){j=i=!1,k=1},point:function(l,m){var n,o=[l,m],p=b(l,m),q=g?p?0:e(l,m):p?e(l+(0>l?Lg:-Lg),m):0;if(!c&&(j=i=p)&&a.lineStart(),p!==i&&(n=d(c,o),(wb(c,n)||wb(o,n))&&(o[0]+=Jg,o[1]+=Jg,p=b(o[0],o[1]))),p!==i)k=0,p?(a.lineStart(),n=d(o,c),a.point(n[0],n[1])):(n=d(c,o),a.point(n[0],n[1]),a.lineEnd()),c=n;else if(h&&c&&g^p){var r;q&f||!(r=d(o,c,!0))||(k=0,g?(a.lineStart(),a.point(r[0][0],r[0][1]),a.point(r[1][0],r[1][1]),a.lineEnd()):(a.point(r[1][0],r[1][1]),a.lineEnd(),a.lineStart(),a.point(r[0][0],r[0][1])))}!p||c&&wb(c,o)||a.point(o[0],o[1]),c=o,i=p,f=q},lineEnd:function(){i&&a.lineEnd(),c=null},clean:function(){return k|(j&&i)<<1}}}function d(a,b,c){var d=pb(a),e=pb(b),g=[1,0,0],h=rb(d,e),i=qb(h,h),j=h[0],k=i-j*j;if(!k)return!c&&a;var l=f*i/k,m=-f*j/k,n=rb(g,h),o=tb(g,l),p=tb(h,m);sb(o,p);var q=n,r=qb(o,q),s=qb(q,q),t=r*r-s*(qb(o,o)-1);if(!(0>t)){var u=Math.sqrt(t),v=tb(q,(-r-u)/s);if(sb(v,o),v=vb(v),!c)return v;var w,x=a[0],y=b[0],z=a[1],A=b[1];x>y&&(w=x,x=y,y=w);var B=y-x,C=tg(B-Lg)<Jg,D=C||Jg>B;if(!C&&z>A&&(w=z,z=A,A=w),D?C?z+A>0^v[1]<(tg(v[0]-x)<Jg?z:A):z<=v[1]&&v[1]<=A:B>Lg^(x<=v[0]&&v[0]<=y)){var E=tb(q,(-r+u)/s);return sb(E,o),[v,vb(E)]}}}function e(b,c){var d=g?a:Lg-a,e=0;return-d>b?e|=1:b>d&&(e|=2),-d>c?e|=4:c>d&&(e|=8),e}var f=Math.cos(a),g=f>0,h=tg(f)>Jg,i=oc(a,6*Pg);return Hb(b,c,i,g?[0,-a]:[-Lg,a-Lg])}function Qb(a,b,c,d){return function(e){var f,g=e.a,h=e.b,i=g.x,j=g.y,k=h.x,l=h.y,m=0,n=1,o=k-i,p=l-j;if(f=a-i,o||!(f>0)){if(f/=o,0>o){if(m>f)return;n>f&&(n=f)}else if(o>0){if(f>n)return;f>m&&(m=f)}if(f=c-i,o||!(0>f)){if(f/=o,0>o){if(f>n)return;f>m&&(m=f)}else if(o>0){if(m>f)return;n>f&&(n=f)}if(f=b-j,p||!(f>0)){if(f/=p,0>p){if(m>f)return;n>f&&(n=f)}else if(p>0){if(f>n)return;f>m&&(m=f)}if(f=d-j,p||!(0>f)){if(f/=p,0>p){if(f>n)return;f>m&&(m=f)}else if(p>0){if(m>f)return;n>f&&(n=f)}return m>0&&(e.a={x:i+m*o,y:j+m*p}),1>n&&(e.b={x:i+n*o,y:j+n*p}),e}}}}}}function Rb(a,b,c,d){function e(d,e){return tg(d[0]-a)<Jg?e>0?0:3:tg(d[0]-c)<Jg?e>0?2:1:tg(d[1]-b)<Jg?e>0?1:0:e>0?3:2}function f(a,b){return g(a.x,b.x)}function g(a,b){var c=e(a,1),d=e(b,1);return c!==d?c-d:0===c?b[1]-a[1]:1===c?a[0]-b[0]:2===c?a[1]-b[1]:b[0]-a[0]}return function(h){function i(a){for(var b=0,c=q.length,d=a[1],e=0;c>e;++e)for(var f,g=1,h=q[e],i=h.length,j=h[0];i>g;++g)f=h[g],j[1]<=d?f[1]>d&&_(j,f,a)>0&&++b:f[1]<=d&&_(j,f,a)<0&&--b,j=f;return 0!==b}function j(f,h,i,j){var k=0,l=0;if(null==f||(k=e(f,i))!==(l=e(h,i))||g(f,h)<0^i>0){do j.point(0===k||3===k?a:c,k>1?d:b);while((k=(k+i+4)%4)!==l)}else j.point(h[0],h[1])}function k(e,f){return e>=a&&c>=e&&f>=b&&d>=f}function l(a,b){k(a,b)&&h.point(a,b)}function m(){D.point=o,q&&q.push(r=[]),y=!0,x=!1,v=w=NaN}function n(){p&&(o(s,t),u&&x&&B.rejoin(),p.push(B.buffer())),D.point=l,x&&h.lineEnd()}function o(a,b){a=Math.max(-Mh,Math.min(Mh,a)),b=Math.max(-Mh,Math.min(Mh,b));var c=k(a,b);if(q&&r.push([a,b]),y)s=a,t=b,u=c,y=!1,c&&(h.lineStart(),h.point(a,b));else if(c&&x)h.point(a,b);else{var d={a:{x:v,y:w},b:{x:a,y:b}};C(d)?(x||(h.lineStart(),h.point(d.a.x,d.a.y)),h.point(d.b.x,d.b.y),c||h.lineEnd(),z=!1):c&&(h.lineStart(),h.point(a,b),z=!1)}v=a,w=b,x=c}var p,q,r,s,t,u,v,w,x,y,z,A=h,B=Jb(),C=Qb(a,b,c,d),D={point:l,lineStart:m,lineEnd:n,polygonStart:function(){h=B,p=[],q=[],z=!0},polygonEnd:function(){h=A,p=hg.merge(p);var b=i([a,d]),c=z&&b,e=p.length;(c||e)&&(h.polygonStart(),c&&(h.lineStart(),j(null,null,1,h),h.lineEnd()),e&&Eb(p,f,b,j,h),h.polygonEnd()),p=q=r=null}};return D}}function Sb(a){var b=0,c=Lg/3,d=gc(a),e=d(b,c);return e.parallels=function(a){return arguments.length?d(b=a[0]*Lg/180,c=a[1]*Lg/180):[b/Lg*180,c/Lg*180]},e}function Tb(a,b){function c(a,b){var c=Math.sqrt(f-2*e*Math.sin(b))/e;return[c*Math.sin(a*=e),g-c*Math.cos(a)]}var d=Math.sin(a),e=(d+Math.sin(b))/2,f=1+d*(2*e-d),g=Math.sqrt(f)/e;return c.invert=function(a,b){var c=g-b;return[Math.atan2(a,c)/e,ba((f-(a*a+c*c)*e*e)/(2*e))]},c}function Ub(){function a(a,b){Oh+=e*a-d*b,d=a,e=b}var b,c,d,e;Th.point=function(f,g){Th.point=a,b=d=f,c=e=g},Th.lineEnd=function(){a(b,c)}}function Vb(a,b){Ph>a&&(Ph=a),a>Rh&&(Rh=a),Qh>b&&(Qh=b),b>Sh&&(Sh=b)}function Wb(){function a(a,b){g.push("M",a,",",b,f)}function b(a,b){g.push("M",a,",",b),h.point=c}function c(a,b){g.push("L",a,",",b)}function d(){h.point=a}function e(){g.push("Z")}var f=Xb(4.5),g=[],h={point:a,lineStart:function(){h.point=b},lineEnd:d,polygonStart:function(){h.lineEnd=e},polygonEnd:function(){h.lineEnd=d,h.point=a},pointRadius:function(a){return f=Xb(a),h},result:function(){if(g.length){var a=g.join("");return g=[],a}}};return h}function Xb(a){return"m0,"+a+"a"+a+","+a+" 0 1,1 0,"+-2*a+"a"+a+","+a+" 0 1,1 0,"+2*a+"z"}function Yb(a,b){Bh+=a,Ch+=b,++Dh}function Zb(){function a(a,d){var e=a-b,f=d-c,g=Math.sqrt(e*e+f*f);Eh+=g*(b+a)/2,Fh+=g*(c+d)/2,Gh+=g,Yb(b=a,c=d)}var b,c;Vh.point=function(d,e){Vh.point=a,Yb(b=d,c=e)}}function $b(){Vh.point=Yb}function _b(){function a(a,b){var c=a-d,f=b-e,g=Math.sqrt(c*c+f*f);Eh+=g*(d+a)/2,Fh+=g*(e+b)/2,Gh+=g,g=e*a-d*b,Hh+=g*(d+a),Ih+=g*(e+b),Jh+=3*g,Yb(d=a,e=b)}var b,c,d,e;Vh.point=function(f,g){Vh.point=a,Yb(b=d=f,c=e=g)},Vh.lineEnd=function(){a(b,c)}}function ac(a){function b(b,c){a.moveTo(b+g,c),a.arc(b,c,g,0,Mg)}function c(b,c){a.moveTo(b,c),h.point=d}function d(b,c){a.lineTo(b,c)}function e(){h.point=b}function f(){a.closePath()}var g=4.5,h={point:b,lineStart:function(){h.point=c},lineEnd:e,polygonStart:function(){h.lineEnd=f},polygonEnd:function(){h.lineEnd=e,h.point=b},pointRadius:function(a){return g=a,h},result:v};return h}function bc(a){function b(a){return(h?d:c)(a)}function c(b){return ec(b,function(c,d){c=a(c,d),b.point(c[0],c[1])})}function d(b){function c(c,d){c=a(c,d),b.point(c[0],c[1])}function d(){t=NaN,y.point=f,b.lineStart()}function f(c,d){var f=pb([c,d]),g=a(c,d);e(t,u,s,v,w,x,t=g[0],u=g[1],s=c,v=f[0],w=f[1],x=f[2],h,b),b.point(t,u)}function g(){y.point=c,b.lineEnd()}function i(){d(),y.point=j,y.lineEnd=k}function j(a,b){f(l=a,m=b),n=t,o=u,p=v,q=w,r=x,y.point=f}function k(){e(t,u,s,v,w,x,n,o,l,p,q,r,h,b),y.lineEnd=g,g()}var l,m,n,o,p,q,r,s,t,u,v,w,x,y={point:c,lineStart:d,lineEnd:g,polygonStart:function(){b.polygonStart(),y.lineStart=i},polygonEnd:function(){b.polygonEnd(),y.lineStart=d}};return y}function e(b,c,d,h,i,j,k,l,m,n,o,p,q,r){var s=k-b,t=l-c,u=s*s+t*t;if(u>4*f&&q--){var v=h+n,w=i+o,x=j+p,y=Math.sqrt(v*v+w*w+x*x),z=Math.asin(x/=y),A=tg(tg(x)-1)<Jg||tg(d-m)<Jg?(d+m)/2:Math.atan2(w,v),B=a(A,z),C=B[0],D=B[1],E=C-b,F=D-c,G=t*E-s*F;(G*G/u>f||tg((s*E+t*F)/u-.5)>.3||g>h*n+i*o+j*p)&&(e(b,c,d,h,i,j,C,D,A,v/=y,w/=y,x,q,r),r.point(C,D),e(C,D,A,v,w,x,k,l,m,n,o,p,q,r))}}var f=.5,g=Math.cos(30*Pg),h=16;return b.precision=function(a){return arguments.length?(h=(f=a*a)>0&&16,b):Math.sqrt(f)},b}function cc(a){var b=bc(function(b,c){return a([b*Qg,c*Qg])});return function(a){return hc(b(a))}}function dc(a){this.stream=a}function ec(a,b){return{point:b,sphere:function(){a.sphere()},lineStart:function(){a.lineStart()},lineEnd:function(){a.lineEnd()},polygonStart:function(){a.polygonStart()},polygonEnd:function(){a.polygonEnd()}}}function fc(a){return gc(function(){return a})()}function gc(a){function b(a){return a=h(a[0]*Pg,a[1]*Pg),[a[0]*m+i,j-a[1]*m]}function c(a){return a=h.invert((a[0]-i)/m,(j-a[1])/m),a&&[a[0]*Qg,a[1]*Qg]}function d(){h=Cb(g=kc(r,t,u),f);var a=f(p,q);return i=n-a[0]*m,j=o+a[1]*m,e()}function e(){return k&&(k.valid=!1,k=null),b}var f,g,h,i,j,k,l=bc(function(a,b){return a=f(a,b),[a[0]*m+i,j-a[1]*m]}),m=150,n=480,o=250,p=0,q=0,r=0,t=0,u=0,v=Lh,w=s,x=null,y=null;return b.stream=function(a){return k&&(k.valid=!1),k=hc(v(g,l(w(a)))),k.valid=!0,k},b.clipAngle=function(a){return arguments.length?(v=null==a?(x=a,Lh):Pb((x=+a)*Pg),e()):x},b.clipExtent=function(a){return arguments.length?(y=a,w=a?Rb(a[0][0],a[0][1],a[1][0],a[1][1]):s,e()):y},b.scale=function(a){return arguments.length?(m=+a,d()):m},b.translate=function(a){return arguments.length?(n=+a[0],o=+a[1],d()):[n,o]},b.center=function(a){return arguments.length?(p=a[0]%360*Pg,q=a[1]%360*Pg,d()):[p*Qg,q*Qg]},b.rotate=function(a){return arguments.length?(r=a[0]%360*Pg,t=a[1]%360*Pg,u=a.length>2?a[2]%360*Pg:0,d()):[r*Qg,t*Qg,u*Qg]},hg.rebind(b,l,"precision"),function(){return f=a.apply(this,arguments),b.invert=f.invert&&c,d()}}function hc(a){return ec(a,function(b,c){a.point(b*Pg,c*Pg)})}function ic(a,b){return[a,b]}function jc(a,b){return[a>Lg?a-Mg:-Lg>a?a+Mg:a,b]}function kc(a,b,c){return a?b||c?Cb(mc(a),nc(b,c)):mc(a):b||c?nc(b,c):jc}function lc(a){return function(b,c){return b+=a,[b>Lg?b-Mg:-Lg>b?b+Mg:b,c]}}function mc(a){var b=lc(a);return b.invert=lc(-a),b}function nc(a,b){function c(a,b){var c=Math.cos(b),h=Math.cos(a)*c,i=Math.sin(a)*c,j=Math.sin(b),k=j*d+h*e;return[Math.atan2(i*f-k*g,h*d-j*e),ba(k*f+i*g)]}var d=Math.cos(a),e=Math.sin(a),f=Math.cos(b),g=Math.sin(b);return c.invert=function(a,b){var c=Math.cos(b),h=Math.cos(a)*c,i=Math.sin(a)*c,j=Math.sin(b),k=j*f-i*g;return[Math.atan2(i*f+j*g,h*d+k*e),ba(k*d-h*e)]},c}function oc(a,b){var c=Math.cos(a),d=Math.sin(a);return function(e,f,g,h){var i=g*b;null!=e?(e=pc(c,e),f=pc(c,f),(g>0?f>e:e>f)&&(e+=g*Mg)):(e=a+g*Mg,f=a-.5*i);for(var j,k=e;g>0?k>f:f>k;k-=i)h.point((j=vb([c,-d*Math.cos(k),-d*Math.sin(k)]))[0],j[1])}}function pc(a,b){var c=pb(b);c[0]-=a,ub(c);var d=aa(-c[1]);return((-c[2]<0?-d:d)+2*Math.PI-Jg)%(2*Math.PI)}function qc(a,b,c){var d=hg.range(a,b-Jg,c).concat(b);return function(a){return d.map(function(b){return[a,b]})}}function rc(a,b,c){var d=hg.range(a,b-Jg,c).concat(b);return function(a){return d.map(function(b){return[b,a]})}}function sc(a){return a.source}function tc(a){return a.target}function uc(a,b,c,d){var e=Math.cos(b),f=Math.sin(b),g=Math.cos(d),h=Math.sin(d),i=e*Math.cos(a),j=e*Math.sin(a),k=g*Math.cos(c),l=g*Math.sin(c),m=2*Math.asin(Math.sqrt(fa(d-b)+e*g*fa(c-a))),n=1/Math.sin(m),o=m?function(a){var b=Math.sin(a*=m)*n,c=Math.sin(m-a)*n,d=c*i+b*k,e=c*j+b*l,g=c*f+b*h;return[Math.atan2(e,d)*Qg,Math.atan2(g,Math.sqrt(d*d+e*e))*Qg]}:function(){return[a*Qg,b*Qg]};return o.distance=m,o}function vc(){function a(a,e){var f=Math.sin(e*=Pg),g=Math.cos(e),h=tg((a*=Pg)-b),i=Math.cos(h);Wh+=Math.atan2(Math.sqrt((h=g*Math.sin(h))*h+(h=d*f-c*g*i)*h),c*f+d*g*i),b=a,c=f,d=g}var b,c,d;Xh.point=function(e,f){b=e*Pg,c=Math.sin(f*=Pg),d=Math.cos(f),Xh.point=a},Xh.lineEnd=function(){Xh.point=Xh.lineEnd=v}}function wc(a,b){function c(b,c){var d=Math.cos(b),e=Math.cos(c),f=a(d*e);return[f*e*Math.sin(b),f*Math.sin(c)]}return c.invert=function(a,c){var d=Math.sqrt(a*a+c*c),e=b(d),f=Math.sin(e),g=Math.cos(e);return[Math.atan2(a*f,d*g),Math.asin(d&&c*f/d)]},c}function xc(a,b){function c(a,b){g>0?-Og+Jg>b&&(b=-Og+Jg):b>Og-Jg&&(b=Og-Jg);var c=g/Math.pow(e(b),f);return[c*Math.sin(f*a),g-c*Math.cos(f*a)]}var d=Math.cos(a),e=function(a){return Math.tan(Lg/4+a/2)},f=a===b?Math.sin(a):Math.log(d/Math.cos(b))/Math.log(e(b)/e(a)),g=d*Math.pow(e(a),f)/f;return f?(c.invert=function(a,b){var c=g-b,d=$(f)*Math.sqrt(a*a+c*c);return[Math.atan2(a,c)/f,2*Math.atan(Math.pow(g/d,1/f))-Og]},c):zc}function yc(a,b){function c(a,b){var c=f-b;return[c*Math.sin(e*a),f-c*Math.cos(e*a)]}var d=Math.cos(a),e=a===b?Math.sin(a):(d-Math.cos(b))/(b-a),f=d/e+a;return tg(e)<Jg?ic:(c.invert=function(a,b){var c=f-b;return[Math.atan2(a,c)/e,f-$(e)*Math.sqrt(a*a+c*c)]},c)}function zc(a,b){return[a,Math.log(Math.tan(Lg/4+b/2))]}function Ac(a){var b,c=fc(a),d=c.scale,e=c.translate,f=c.clipExtent;return c.scale=function(){var a=d.apply(c,arguments);return a===c?b?c.clipExtent(null):c:a},c.translate=function(){var a=e.apply(c,arguments);return a===c?b?c.clipExtent(null):c:a},c.clipExtent=function(a){var g=f.apply(c,arguments);if(g===c){if(b=null==a){var h=Lg*d(),i=e();f([[i[0]-h,i[1]-h],[i[0]+h,i[1]+h]])}}else b&&(g=null);return g},c.clipExtent(null)}function Bc(a,b){return[Math.log(Math.tan(Lg/4+b/2)),-a]}function Cc(a){return a[0]}function Dc(a){return a[1]}function Ec(a){for(var b=a.length,c=[0,1],d=2,e=2;b>e;e++){for(;d>1&&_(a[c[d-2]],a[c[d-1]],a[e])<=0;)--d;c[d++]=e}return c.slice(0,d)}function Fc(a,b){return a[0]-b[0]||a[1]-b[1]}function Gc(a,b,c){return(c[0]-b[0])*(a[1]-b[1])<(c[1]-b[1])*(a[0]-b[0])}function Hc(a,b,c,d){var e=a[0],f=c[0],g=b[0]-e,h=d[0]-f,i=a[1],j=c[1],k=b[1]-i,l=d[1]-j,m=(h*(i-j)-l*(e-f))/(l*g-h*k);return[e+m*g,i+m*k]}function Ic(a){var b=a[0],c=a[a.length-1];return!(b[0]-c[0]||b[1]-c[1])}function Jc(){cd(this),this.edge=this.site=this.circle=null}function Kc(a){var b=hi.pop()||new Jc;return b.site=a,b}function Lc(a){Vc(a),ei.remove(a),hi.push(a),cd(a)}function Mc(a){var b=a.circle,c=b.x,d=b.cy,e={x:c,y:d},f=a.P,g=a.N,h=[a];Lc(a);for(var i=f;i.circle&&tg(c-i.circle.x)<Jg&&tg(d-i.circle.cy)<Jg;)f=i.P,h.unshift(i),Lc(i),i=f;h.unshift(i),Vc(i);for(var j=g;j.circle&&tg(c-j.circle.x)<Jg&&tg(d-j.circle.cy)<Jg;)g=j.N,h.push(j),Lc(j),j=g;h.push(j),Vc(j);var k,l=h.length;for(k=1;l>k;++k)j=h[k],i=h[k-1],_c(j.edge,i.site,j.site,e);i=h[0],j=h[l-1],j.edge=Zc(i.site,j.site,null,e),Uc(i),Uc(j)}function Nc(a){for(var b,c,d,e,f=a.x,g=a.y,h=ei._;h;)if(d=Oc(h,g)-f,d>Jg)h=h.L;else{if(e=f-Pc(h,g),!(e>Jg)){d>-Jg?(b=h.P,c=h):e>-Jg?(b=h,c=h.N):b=c=h;break}if(!h.R){b=h;break}h=h.R}var i=Kc(a);if(ei.insert(b,i),b||c){if(b===c)return Vc(b),c=Kc(b.site),ei.insert(i,c),i.edge=c.edge=Zc(b.site,i.site),Uc(b),void Uc(c);if(!c)return void(i.edge=Zc(b.site,i.site));Vc(b),Vc(c);var j=b.site,k=j.x,l=j.y,m=a.x-k,n=a.y-l,o=c.site,p=o.x-k,q=o.y-l,r=2*(m*q-n*p),s=m*m+n*n,t=p*p+q*q,u={x:(q*s-n*t)/r+k,y:(m*t-p*s)/r+l};_c(c.edge,j,o,u),i.edge=Zc(j,a,null,u),c.edge=Zc(a,o,null,u),Uc(b),Uc(c)}}function Oc(a,b){var c=a.site,d=c.x,e=c.y,f=e-b;if(!f)return d;var g=a.P;if(!g)return-(1/0);c=g.site;var h=c.x,i=c.y,j=i-b;if(!j)return h;var k=h-d,l=1/f-1/j,m=k/j;return l?(-m+Math.sqrt(m*m-2*l*(k*k/(-2*j)-i+j/2+e-f/2)))/l+d:(d+h)/2}function Pc(a,b){var c=a.N;if(c)return Oc(c,b);var d=a.site;return d.y===b?d.x:1/0}function Qc(a){this.site=a,this.edges=[]}function Rc(a){for(var b,c,d,e,f,g,h,i,j,k,l=a[0][0],m=a[1][0],n=a[0][1],o=a[1][1],p=di,q=p.length;q--;)if(f=p[q],f&&f.prepare())for(h=f.edges,i=h.length,g=0;i>g;)k=h[g].end(),d=k.x,e=k.y,j=h[++g%i].start(),b=j.x,c=j.y,(tg(d-b)>Jg||tg(e-c)>Jg)&&(h.splice(g,0,new ad($c(f.site,k,tg(d-l)<Jg&&o-e>Jg?{x:l,y:tg(b-l)<Jg?c:o}:tg(e-o)<Jg&&m-d>Jg?{x:tg(c-o)<Jg?b:m,y:o}:tg(d-m)<Jg&&e-n>Jg?{x:m,y:tg(b-m)<Jg?c:n}:tg(e-n)<Jg&&d-l>Jg?{x:tg(c-n)<Jg?b:l,y:n}:null),f.site,null)),++i)}function Sc(a,b){return b.angle-a.angle}function Tc(){cd(this),this.x=this.y=this.arc=this.site=this.cy=null}function Uc(a){var b=a.P,c=a.N;if(b&&c){var d=b.site,e=a.site,f=c.site;if(d!==f){var g=e.x,h=e.y,i=d.x-g,j=d.y-h,k=f.x-g,l=f.y-h,m=2*(i*l-j*k);if(!(m>=-Kg)){var n=i*i+j*j,o=k*k+l*l,p=(l*n-j*o)/m,q=(i*o-k*n)/m,l=q+h,r=ii.pop()||new Tc;r.arc=a,r.site=e,r.x=p+g,r.y=l+Math.sqrt(p*p+q*q),r.cy=l,a.circle=r;for(var s=null,t=gi._;t;)if(r.y<t.y||r.y===t.y&&r.x<=t.x){if(!t.L){s=t.P;break}t=t.L}else{if(!t.R){s=t;break}t=t.R}gi.insert(s,r),s||(fi=r)}}}}function Vc(a){var b=a.circle;b&&(b.P||(fi=b.N),gi.remove(b),ii.push(b),cd(b),a.circle=null)}function Wc(a){for(var b,c=ci,d=Qb(a[0][0],a[0][1],a[1][0],a[1][1]),e=c.length;e--;)b=c[e],(!Xc(b,a)||!d(b)||tg(b.a.x-b.b.x)<Jg&&tg(b.a.y-b.b.y)<Jg)&&(b.a=b.b=null,c.splice(e,1))}function Xc(a,b){var c=a.b;if(c)return!0;var d,e,f=a.a,g=b[0][0],h=b[1][0],i=b[0][1],j=b[1][1],k=a.l,l=a.r,m=k.x,n=k.y,o=l.x,p=l.y,q=(m+o)/2,r=(n+p)/2;if(p===n){if(g>q||q>=h)return;if(m>o){if(f){if(f.y>=j)return}else f={x:q,y:i};c={x:q,y:j}}else{if(f){if(f.y<i)return}else f={x:q,y:j};c={x:q,y:i}}}else if(d=(m-o)/(p-n),e=r-d*q,-1>d||d>1)if(m>o){if(f){if(f.y>=j)return}else f={x:(i-e)/d,y:i};c={x:(j-e)/d,y:j}}else{if(f){if(f.y<i)return}else f={x:(j-e)/d,y:j};c={x:(i-e)/d,y:i}}else if(p>n){if(f){if(f.x>=h)return}else f={x:g,y:d*g+e};c={x:h,y:d*h+e}}else{if(f){if(f.x<g)return}else f={x:h,y:d*h+e};c={x:g,y:d*g+e}}return a.a=f,a.b=c,!0}function Yc(a,b){this.l=a,this.r=b,this.a=this.b=null}function Zc(a,b,c,d){var e=new Yc(a,b);return ci.push(e),c&&_c(e,a,b,c),d&&_c(e,b,a,d),di[a.i].edges.push(new ad(e,a,b)),di[b.i].edges.push(new ad(e,b,a)),e}function $c(a,b,c){var d=new Yc(a,null);return d.a=b,d.b=c,ci.push(d),d}function _c(a,b,c,d){a.a||a.b?a.l===c?a.b=d:a.a=d:(a.a=d,a.l=b,a.r=c)}function ad(a,b,c){var d=a.a,e=a.b;this.edge=a,this.site=b,this.angle=c?Math.atan2(c.y-b.y,c.x-b.x):a.l===b?Math.atan2(e.x-d.x,d.y-e.y):Math.atan2(d.x-e.x,e.y-d.y)}function bd(){this._=null}function cd(a){a.U=a.C=a.L=a.R=a.P=a.N=null}function dd(a,b){var c=b,d=b.R,e=c.U;e?e.L===c?e.L=d:e.R=d:a._=d,d.U=e,c.U=d,c.R=d.L,c.R&&(c.R.U=c),d.L=c}function ed(a,b){var c=b,d=b.L,e=c.U;e?e.L===c?e.L=d:e.R=d:a._=d,d.U=e,c.U=d,c.L=d.R,c.L&&(c.L.U=c),d.R=c}function fd(a){for(;a.L;)a=a.L;return a}function gd(a,b){var c,d,e,f=a.sort(hd).pop();for(ci=[],di=new Array(a.length),ei=new bd,gi=new bd;;)if(e=fi,f&&(!e||f.y<e.y||f.y===e.y&&f.x<e.x))(f.x!==c||f.y!==d)&&(di[f.i]=new Qc(f),Nc(f),c=f.x,d=f.y),f=a.pop();else{if(!e)break;Mc(e.arc)}b&&(Wc(b),Rc(b));var g={cells:di,edges:ci};return ei=gi=ci=di=null,g}function hd(a,b){return b.y-a.y||b.x-a.x}function id(a,b,c){return(a.x-c.x)*(b.y-a.y)-(a.x-b.x)*(c.y-a.y)}function jd(a){return a.x}function kd(a){return a.y}function ld(){return{leaf:!0,nodes:[],point:null,x:null,y:null}}function md(a,b,c,d,e,f){if(!a(b,c,d,e,f)){var g=.5*(c+e),h=.5*(d+f),i=b.nodes;i[0]&&md(a,i[0],c,d,g,h),i[1]&&md(a,i[1],g,d,e,h),i[2]&&md(a,i[2],c,h,g,f),i[3]&&md(a,i[3],g,h,e,f)}}function nd(a,b,c,d,e,f,g){var h,i=1/0;return function j(a,k,l,m,n){if(!(k>f||l>g||d>m||e>n)){if(o=a.point){var o,p=b-a.x,q=c-a.y,r=p*p+q*q;if(i>r){var s=Math.sqrt(i=r);d=b-s,e=c-s,f=b+s,g=c+s,h=o}}for(var t=a.nodes,u=.5*(k+m),v=.5*(l+n),w=b>=u,x=c>=v,y=x<<1|w,z=y+4;z>y;++y)if(a=t[3&y])switch(3&y){case 0:j(a,k,l,u,v);break;case 1:j(a,u,l,m,v);break;case 2:j(a,k,v,u,n);break;case 3:j(a,u,v,m,n)}}}(a,d,e,f,g),h}function od(a,b){a=hg.rgb(a),b=hg.rgb(b);var c=a.r,d=a.g,e=a.b,f=b.r-c,g=b.g-d,h=b.b-e;return function(a){return"#"+ua(Math.round(c+f*a))+ua(Math.round(d+g*a))+ua(Math.round(e+h*a))}}function pd(a,b){var c,d={},e={};for(c in a)c in b?d[c]=sd(a[c],b[c]):e[c]=a[c];for(c in b)c in a||(e[c]=b[c]);return function(a){for(c in d)e[c]=d[c](a);return e}}function qd(a,b){return a=+a,b=+b,function(c){return a*(1-c)+b*c}}function rd(a,b){var c,d,e,f=ki.lastIndex=li.lastIndex=0,g=-1,h=[],i=[];for(a+="",b+="";(c=ki.exec(a))&&(d=li.exec(b));)(e=d.index)>f&&(e=b.slice(f,e),h[g]?h[g]+=e:h[++g]=e),(c=c[0])===(d=d[0])?h[g]?h[g]+=d:h[++g]=d:(h[++g]=null,i.push({i:g,x:qd(c,d)})),f=li.lastIndex;return f<b.length&&(e=b.slice(f),h[g]?h[g]+=e:h[++g]=e),h.length<2?i[0]?(b=i[0].x,function(a){return b(a)+""}):function(){return b}:(b=i.length,function(a){for(var c,d=0;b>d;++d)h[(c=i[d]).i]=c.x(a);return h.join("")})}function sd(a,b){for(var c,d=hg.interpolators.length;--d>=0&&!(c=hg.interpolators[d](a,b)););return c}function td(a,b){var c,d=[],e=[],f=a.length,g=b.length,h=Math.min(a.length,b.length);for(c=0;h>c;++c)d.push(sd(a[c],b[c]));for(;f>c;++c)e[c]=a[c];for(;g>c;++c)e[c]=b[c];return function(a){for(c=0;h>c;++c)e[c]=d[c](a);return e}}function ud(a){return function(b){return 0>=b?0:b>=1?1:a(b)}}function vd(a){return function(b){return 1-a(1-b)}}function wd(a){return function(b){return.5*(.5>b?a(2*b):2-a(2-2*b))}}function xd(a){return a*a}function yd(a){return a*a*a}function zd(a){if(0>=a)return 0;if(a>=1)return 1;var b=a*a,c=b*a;return 4*(.5>a?c:3*(a-b)+c-.75)}function Ad(a){return function(b){return Math.pow(b,a)}}function Bd(a){return 1-Math.cos(a*Og)}function Cd(a){return Math.pow(2,10*(a-1))}function Dd(a){return 1-Math.sqrt(1-a*a)}function Ed(a,b){var c;return arguments.length<2&&(b=.45),arguments.length?c=b/Mg*Math.asin(1/a):(a=1,c=b/4),function(d){return 1+a*Math.pow(2,-10*d)*Math.sin((d-c)*Mg/b)}}function Fd(a){return a||(a=1.70158),function(b){return b*b*((a+1)*b-a)}}function Gd(a){return 1/2.75>a?7.5625*a*a:2/2.75>a?7.5625*(a-=1.5/2.75)*a+.75:2.5/2.75>a?7.5625*(a-=2.25/2.75)*a+.9375:7.5625*(a-=2.625/2.75)*a+.984375}function Hd(a,b){a=hg.hcl(a),b=hg.hcl(b);var c=a.h,d=a.c,e=a.l,f=b.h-c,g=b.c-d,h=b.l-e;return isNaN(g)&&(g=0,d=isNaN(d)?b.c:d),isNaN(f)?(f=0,c=isNaN(c)?b.h:c):f>180?f-=360:-180>f&&(f+=360),function(a){return ka(c+f*a,d+g*a,e+h*a)+""}}function Id(a,b){a=hg.hsl(a),b=hg.hsl(b);var c=a.h,d=a.s,e=a.l,f=b.h-c,g=b.s-d,h=b.l-e;return isNaN(g)&&(g=0,d=isNaN(d)?b.s:d),isNaN(f)?(f=0,c=isNaN(c)?b.h:c):f>180?f-=360:-180>f&&(f+=360),function(a){return ia(c+f*a,d+g*a,e+h*a)+""}}function Jd(a,b){a=hg.lab(a),b=hg.lab(b);var c=a.l,d=a.a,e=a.b,f=b.l-c,g=b.a-d,h=b.b-e;return function(a){return ma(c+f*a,d+g*a,e+h*a)+""}}function Kd(a,b){return b-=a,function(c){return Math.round(a+b*c)}}function Ld(a){var b=[a.a,a.b],c=[a.c,a.d],d=Nd(b),e=Md(b,c),f=Nd(Od(c,b,-e))||0;b[0]*c[1]<c[0]*b[1]&&(b[0]*=-1,b[1]*=-1,d*=-1,e*=-1),this.rotate=(d?Math.atan2(b[1],b[0]):Math.atan2(-c[0],c[1]))*Qg,this.translate=[a.e,a.f],this.scale=[d,f],this.skew=f?Math.atan2(e,f)*Qg:0}function Md(a,b){return a[0]*b[0]+a[1]*b[1]}function Nd(a){var b=Math.sqrt(Md(a,a));return b&&(a[0]/=b,a[1]/=b),b}function Od(a,b,c){return a[0]+=c*b[0],a[1]+=c*b[1],a}function Pd(a){return a.length?a.pop()+",":""}function Qd(a,b,c,d){if(a[0]!==b[0]||a[1]!==b[1]){var e=c.push("translate(",null,",",null,")");d.push({i:e-4,x:qd(a[0],b[0])},{i:e-2,x:qd(a[1],b[1])})}else(b[0]||b[1])&&c.push("translate("+b+")")}function Rd(a,b,c,d){a!==b?(a-b>180?b+=360:b-a>180&&(a+=360),d.push({i:c.push(Pd(c)+"rotate(",null,")")-2,x:qd(a,b)})):b&&c.push(Pd(c)+"rotate("+b+")")}function Sd(a,b,c,d){a!==b?d.push({i:c.push(Pd(c)+"skewX(",null,")")-2,x:qd(a,b)}):b&&c.push(Pd(c)+"skewX("+b+")")}function Td(a,b,c,d){if(a[0]!==b[0]||a[1]!==b[1]){var e=c.push(Pd(c)+"scale(",null,",",null,")");d.push({i:e-4,x:qd(a[0],b[0])},{i:e-2,x:qd(a[1],b[1])})}else(1!==b[0]||1!==b[1])&&c.push(Pd(c)+"scale("+b+")")}function Ud(a,b){var c=[],d=[];return a=hg.transform(a),b=hg.transform(b),Qd(a.translate,b.translate,c,d),Rd(a.rotate,b.rotate,c,d),Sd(a.skew,b.skew,c,d),Td(a.scale,b.scale,c,d),a=b=null,function(a){for(var b,e=-1,f=d.length;++e<f;)c[(b=d[e]).i]=b.x(a);return c.join("")}}function Vd(a,b){return b=(b-=a=+a)||1/b,function(c){return(c-a)/b}}function Wd(a,b){return b=(b-=a=+a)||1/b,function(c){return Math.max(0,Math.min(1,(c-a)/b))}}function Xd(a){for(var b=a.source,c=a.target,d=Zd(b,c),e=[b];b!==d;)b=b.parent,e.push(b);for(var f=e.length;c!==d;)e.splice(f,0,c),c=c.parent;return e}function Yd(a){for(var b=[],c=a.parent;null!=c;)b.push(a),a=c,c=c.parent;return b.push(a),b}function Zd(a,b){if(a===b)return a;for(var c=Yd(a),d=Yd(b),e=c.pop(),f=d.pop(),g=null;e===f;)g=e,e=c.pop(),f=d.pop();return g}function $d(a){a.fixed|=2}function _d(a){a.fixed&=-7}function ae(a){a.fixed|=4,a.px=a.x,a.py=a.y}function be(a){a.fixed&=-5}function ce(a,b,c){var d=0,e=0;if(a.charge=0,!a.leaf)for(var f,g=a.nodes,h=g.length,i=-1;++i<h;)f=g[i],null!=f&&(ce(f,b,c),a.charge+=f.charge,d+=f.charge*f.cx,e+=f.charge*f.cy);if(a.point){a.leaf||(a.point.x+=Math.random()-.5,a.point.y+=Math.random()-.5);var j=b*c[a.point.index];a.charge+=a.pointCharge=j,d+=j*a.point.x,e+=j*a.point.y}a.cx=d/a.charge,a.cy=e/a.charge}function de(a,b){return hg.rebind(a,b,"sort","children","value"),a.nodes=a,a.links=je,a}function ee(a,b){for(var c=[a];null!=(a=c.pop());)if(b(a),(e=a.children)&&(d=e.length))for(var d,e;--d>=0;)c.push(e[d])}function fe(a,b){for(var c=[a],d=[];null!=(a=c.pop());)if(d.push(a),(f=a.children)&&(e=f.length))for(var e,f,g=-1;++g<e;)c.push(f[g]);for(;null!=(a=d.pop());)b(a)}function ge(a){return a.children}function he(a){return a.value}function ie(a,b){return b.value-a.value}function je(a){return hg.merge(a.map(function(a){return(a.children||[]).map(function(b){return{source:a,target:b}})}))}function ke(a){return a.x}function le(a){return a.y}function me(a,b,c){a.y0=b,a.y=c}function ne(a){return hg.range(a.length)}function oe(a){for(var b=-1,c=a[0].length,d=[];++b<c;)d[b]=0;return d}function pe(a){for(var b,c=1,d=0,e=a[0][1],f=a.length;f>c;++c)(b=a[c][1])>e&&(d=c,e=b);return d}function qe(a){return a.reduce(re,0)}function re(a,b){return a+b[1]}function se(a,b){return te(a,Math.ceil(Math.log(b.length)/Math.LN2+1))}function te(a,b){for(var c=-1,d=+a[0],e=(a[1]-d)/b,f=[];++c<=b;)f[c]=e*c+d;return f}function ue(a){return[hg.min(a),hg.max(a)]}function ve(a,b){return a.value-b.value}function we(a,b){var c=a._pack_next;a._pack_next=b,b._pack_prev=a,b._pack_next=c,c._pack_prev=b}function xe(a,b){a._pack_next=b,b._pack_prev=a}function ye(a,b){var c=b.x-a.x,d=b.y-a.y,e=a.r+b.r;return.999*e*e>c*c+d*d}function ze(a){function b(a){k=Math.min(a.x-a.r,k),l=Math.max(a.x+a.r,l),m=Math.min(a.y-a.r,m),n=Math.max(a.y+a.r,n)}if((c=a.children)&&(j=c.length)){var c,d,e,f,g,h,i,j,k=1/0,l=-(1/0),m=1/0,n=-(1/0);if(c.forEach(Ae),d=c[0],d.x=-d.r,d.y=0,b(d),j>1&&(e=c[1],e.x=e.r,e.y=0,b(e),j>2))for(f=c[2],De(d,e,f),b(f),we(d,f),d._pack_prev=f,we(f,e),e=d._pack_next,g=3;j>g;g++){De(d,e,f=c[g]);var o=0,p=1,q=1;for(h=e._pack_next;h!==e;h=h._pack_next,p++)if(ye(h,f)){o=1;break}if(1==o)for(i=d._pack_prev;i!==h._pack_prev&&!ye(i,f);i=i._pack_prev,q++);o?(q>p||p==q&&e.r<d.r?xe(d,e=h):xe(d=i,e),g--):(we(d,f),e=f,b(f))}var r=(k+l)/2,s=(m+n)/2,t=0;for(g=0;j>g;g++)f=c[g],f.x-=r,f.y-=s,t=Math.max(t,f.r+Math.sqrt(f.x*f.x+f.y*f.y));a.r=t,c.forEach(Be)}}function Ae(a){a._pack_next=a._pack_prev=a}function Be(a){delete a._pack_next,delete a._pack_prev}function Ce(a,b,c,d){var e=a.children;if(a.x=b+=d*a.x,a.y=c+=d*a.y,a.r*=d,e)for(var f=-1,g=e.length;++f<g;)Ce(e[f],b,c,d)}function De(a,b,c){var d=a.r+c.r,e=b.x-a.x,f=b.y-a.y;if(d&&(e||f)){var g=b.r+c.r,h=e*e+f*f;g*=g,d*=d;var i=.5+(d-g)/(2*h),j=Math.sqrt(Math.max(0,2*g*(d+h)-(d-=h)*d-g*g))/(2*h);c.x=a.x+i*e+j*f,c.y=a.y+i*f-j*e}else c.x=a.x+d,c.y=a.y}function Ee(a,b){return a.parent==b.parent?1:2}function Fe(a){var b=a.children;return b.length?b[0]:a.t}function Ge(a){var b,c=a.children;return(b=c.length)?c[b-1]:a.t}function He(a,b,c){var d=c/(b.i-a.i);b.c-=d,b.s+=c,a.c+=d,b.z+=c,b.m+=c}function Ie(a){for(var b,c=0,d=0,e=a.children,f=e.length;--f>=0;)b=e[f],b.z+=c,b.m+=c,c+=b.s+(d+=b.c)}function Je(a,b,c){return a.a.parent===b.parent?a.a:c}function Ke(a){return 1+hg.max(a,function(a){return a.y})}function Le(a){return a.reduce(function(a,b){return a+b.x},0)/a.length}function Me(a){var b=a.children;return b&&b.length?Me(b[0]):a}function Ne(a){var b,c=a.children;return c&&(b=c.length)?Ne(c[b-1]):a}function Oe(a){return{x:a.x,y:a.y,dx:a.dx,dy:a.dy}}function Pe(a,b){var c=a.x+b[3],d=a.y+b[0],e=a.dx-b[1]-b[3],f=a.dy-b[0]-b[2];return 0>e&&(c+=e/2,e=0),0>f&&(d+=f/2,f=0),{x:c,y:d,dx:e,dy:f}}function Qe(a){var b=a[0],c=a[a.length-1];return c>b?[b,c]:[c,b]}function Re(a){return a.rangeExtent?a.rangeExtent():Qe(a.range())}function Se(a,b,c,d){var e=c(a[0],a[1]),f=d(b[0],b[1]);return function(a){return f(e(a))}}function Te(a,b){var c,d=0,e=a.length-1,f=a[d],g=a[e];return f>g&&(c=d,d=e,e=c,c=f,f=g,g=c),a[d]=b.floor(f),a[e]=b.ceil(g),a}function Ue(a){return a?{floor:function(b){return Math.floor(b/a)*a},ceil:function(b){return Math.ceil(b/a)*a}}:wi}function Ve(a,b,c,d){var e=[],f=[],g=0,h=Math.min(a.length,b.length)-1;for(a[h]<a[0]&&(a=a.slice().reverse(),b=b.slice().reverse());++g<=h;)e.push(c(a[g-1],a[g])),f.push(d(b[g-1],b[g]));return function(b){var c=hg.bisect(a,b,1,h)-1;return f[c](e[c](b))}}function We(a,b,c,d){function e(){var e=Math.min(a.length,b.length)>2?Ve:Se,i=d?Wd:Vd;return g=e(a,b,i,c),h=e(b,a,i,sd),f}function f(a){return g(a)}var g,h;return f.invert=function(a){return h(a)},f.domain=function(b){return arguments.length?(a=b.map(Number),e()):a},f.range=function(a){return arguments.length?(b=a,e()):b},f.rangeRound=function(a){return f.range(a).interpolate(Kd)},f.clamp=function(a){return arguments.length?(d=a,e()):d},f.interpolate=function(a){return arguments.length?(c=a,e()):c},f.ticks=function(b){return $e(a,b)},f.tickFormat=function(b,c){return _e(a,b,c)},f.nice=function(b){return Ye(a,b),e()},f.copy=function(){return We(a,b,c,d)},e()}function Xe(a,b){return hg.rebind(a,b,"range","rangeRound","interpolate","clamp")}function Ye(a,b){return Te(a,Ue(Ze(a,b)[2])),Te(a,Ue(Ze(a,b)[2])),a}function Ze(a,b){null==b&&(b=10);var c=Qe(a),d=c[1]-c[0],e=Math.pow(10,Math.floor(Math.log(d/b)/Math.LN10)),f=b/d*e;return.15>=f?e*=10:.35>=f?e*=5:.75>=f&&(e*=2),c[0]=Math.ceil(c[0]/e)*e,c[1]=Math.floor(c[1]/e)*e+.5*e,c[2]=e,c}function $e(a,b){return hg.range.apply(hg,Ze(a,b))}function _e(a,b,c){var d=Ze(a,b);if(c){var e=kh.exec(c);if(e.shift(),"s"===e[8]){var f=hg.formatPrefix(Math.max(tg(d[0]),tg(d[1])));return e[7]||(e[7]="."+af(f.scale(d[2]))),e[8]="f",c=hg.format(e.join("")),function(a){return c(f.scale(a))+f.symbol}}e[7]||(e[7]="."+bf(e[8],d)),c=e.join("")}else c=",."+af(d[2])+"f";return hg.format(c)}function af(a){return-Math.floor(Math.log(a)/Math.LN10+.01)}function bf(a,b){var c=af(b[2]);return a in xi?Math.abs(c-af(Math.max(tg(b[0]),tg(b[1]))))+ +("e"!==a):c-2*("%"===a)}function cf(a,b,c,d){function e(a){return(c?Math.log(0>a?0:a):-Math.log(a>0?0:-a))/Math.log(b)}function f(a){return c?Math.pow(b,a):-Math.pow(b,-a)}function g(b){return a(e(b))}return g.invert=function(b){return f(a.invert(b))},g.domain=function(b){return arguments.length?(c=b[0]>=0,a.domain((d=b.map(Number)).map(e)),g):d},g.base=function(c){return arguments.length?(b=+c,a.domain(d.map(e)),g):b},g.nice=function(){var b=Te(d.map(e),c?Math:zi);return a.domain(b),d=b.map(f),g},g.ticks=function(){var a=Qe(d),g=[],h=a[0],i=a[1],j=Math.floor(e(h)),k=Math.ceil(e(i)),l=b%1?2:b;if(isFinite(k-j)){if(c){for(;k>j;j++)for(var m=1;l>m;m++)g.push(f(j)*m);g.push(f(j))}else for(g.push(f(j));j++<k;)for(var m=l-1;m>0;m--)g.push(f(j)*m);for(j=0;g[j]<h;j++);for(k=g.length;g[k-1]>i;k--);g=g.slice(j,k)}return g},g.tickFormat=function(a,c){if(!arguments.length)return yi;arguments.length<2?c=yi:"function"!=typeof c&&(c=hg.format(c));var d=Math.max(1,b*a/g.ticks().length);return function(a){var g=a/f(Math.round(e(a)));return b-.5>g*b&&(g*=b),d>=g?c(a):""}},g.copy=function(){return cf(a.copy(),b,c,d)},Xe(g,a)}function df(a,b,c){function d(b){return a(e(b))}var e=ef(b),f=ef(1/b);return d.invert=function(b){return f(a.invert(b))},d.domain=function(b){return arguments.length?(a.domain((c=b.map(Number)).map(e)),d):c},d.ticks=function(a){return $e(c,a)},d.tickFormat=function(a,b){return _e(c,a,b)},d.nice=function(a){return d.domain(Ye(c,a))},d.exponent=function(g){return arguments.length?(e=ef(b=g),f=ef(1/b),a.domain(c.map(e)),d):b},d.copy=function(){return df(a.copy(),b,c)},Xe(d,a)}function ef(a){return function(b){return 0>b?-Math.pow(-b,a):Math.pow(b,a)}}function ff(a,b){function c(c){return f[((e.get(c)||("range"===b.t?e.set(c,a.push(c)):NaN))-1)%f.length]}function d(b,c){return hg.range(a.length).map(function(a){return b+c*a})}var e,f,g;return c.domain=function(d){if(!arguments.length)return a;a=[],e=new j;for(var f,g=-1,h=d.length;++g<h;)e.has(f=d[g])||e.set(f,a.push(f));return c[b.t].apply(c,b.a)},c.range=function(a){return arguments.length?(f=a,g=0,b={t:"range",a:arguments},c):f},c.rangePoints=function(e,h){arguments.length<2&&(h=0);var i=e[0],j=e[1],k=a.length<2?(i=(i+j)/2,0):(j-i)/(a.length-1+h);return f=d(i+k*h/2,k),g=0,b={t:"rangePoints",a:arguments},c},c.rangeRoundPoints=function(e,h){arguments.length<2&&(h=0);var i=e[0],j=e[1],k=a.length<2?(i=j=Math.round((i+j)/2),0):(j-i)/(a.length-1+h)|0;return f=d(i+Math.round(k*h/2+(j-i-(a.length-1+h)*k)/2),k),g=0,b={t:"rangeRoundPoints",a:arguments},c},c.rangeBands=function(e,h,i){arguments.length<2&&(h=0),arguments.length<3&&(i=h);var j=e[1]<e[0],k=e[j-0],l=e[1-j],m=(l-k)/(a.length-h+2*i);
return f=d(k+m*i,m),j&&f.reverse(),g=m*(1-h),b={t:"rangeBands",a:arguments},c},c.rangeRoundBands=function(e,h,i){arguments.length<2&&(h=0),arguments.length<3&&(i=h);var j=e[1]<e[0],k=e[j-0],l=e[1-j],m=Math.floor((l-k)/(a.length-h+2*i));return f=d(k+Math.round((l-k-(a.length-h)*m)/2),m),j&&f.reverse(),g=Math.round(m*(1-h)),b={t:"rangeRoundBands",a:arguments},c},c.rangeBand=function(){return g},c.rangeExtent=function(){return Qe(b.a[0])},c.copy=function(){return ff(a,b)},c.domain(a)}function gf(a,b){function f(){var c=0,d=b.length;for(h=[];++c<d;)h[c-1]=hg.quantile(a,c/d);return g}function g(a){return isNaN(a=+a)?void 0:b[hg.bisect(h,a)]}var h;return g.domain=function(b){return arguments.length?(a=b.map(d).filter(e).sort(c),f()):a},g.range=function(a){return arguments.length?(b=a,f()):b},g.quantiles=function(){return h},g.invertExtent=function(c){return c=b.indexOf(c),0>c?[NaN,NaN]:[c>0?h[c-1]:a[0],c<h.length?h[c]:a[a.length-1]]},g.copy=function(){return gf(a,b)},f()}function hf(a,b,c){function d(b){return c[Math.max(0,Math.min(g,Math.floor(f*(b-a))))]}function e(){return f=c.length/(b-a),g=c.length-1,d}var f,g;return d.domain=function(c){return arguments.length?(a=+c[0],b=+c[c.length-1],e()):[a,b]},d.range=function(a){return arguments.length?(c=a,e()):c},d.invertExtent=function(b){return b=c.indexOf(b),b=0>b?NaN:b/f+a,[b,b+1/f]},d.copy=function(){return hf(a,b,c)},e()}function jf(a,b){function c(c){return c>=c?b[hg.bisect(a,c)]:void 0}return c.domain=function(b){return arguments.length?(a=b,c):a},c.range=function(a){return arguments.length?(b=a,c):b},c.invertExtent=function(c){return c=b.indexOf(c),[a[c-1],a[c]]},c.copy=function(){return jf(a,b)},c}function kf(a){function b(a){return+a}return b.invert=b,b.domain=b.range=function(c){return arguments.length?(a=c.map(b),b):a},b.ticks=function(b){return $e(a,b)},b.tickFormat=function(b,c){return _e(a,b,c)},b.copy=function(){return kf(a)},b}function lf(){return 0}function mf(a){return a.innerRadius}function nf(a){return a.outerRadius}function of(a){return a.startAngle}function pf(a){return a.endAngle}function qf(a){return a&&a.padAngle}function rf(a,b,c,d){return(a-c)*b-(b-d)*a>0?0:1}function sf(a,b,c,d,e){var f=a[0]-b[0],g=a[1]-b[1],h=(e?d:-d)/Math.sqrt(f*f+g*g),i=h*g,j=-h*f,k=a[0]+i,l=a[1]+j,m=b[0]+i,n=b[1]+j,o=(k+m)/2,p=(l+n)/2,q=m-k,r=n-l,s=q*q+r*r,t=c-d,u=k*n-m*l,v=(0>r?-1:1)*Math.sqrt(Math.max(0,t*t*s-u*u)),w=(u*r-q*v)/s,x=(-u*q-r*v)/s,y=(u*r+q*v)/s,z=(-u*q+r*v)/s,A=w-o,B=x-p,C=y-o,D=z-p;return A*A+B*B>C*C+D*D&&(w=y,x=z),[[w-i,x-j],[w*c/t,x*c/t]]}function tf(a){function b(b){function g(){j.push("M",f(a(k),h))}for(var i,j=[],k=[],l=-1,m=b.length,n=Aa(c),o=Aa(d);++l<m;)e.call(this,i=b[l],l)?k.push([+n.call(this,i,l),+o.call(this,i,l)]):k.length&&(g(),k=[]);return k.length&&g(),j.length?j.join(""):null}var c=Cc,d=Dc,e=Db,f=uf,g=f.key,h=.7;return b.x=function(a){return arguments.length?(c=a,b):c},b.y=function(a){return arguments.length?(d=a,b):d},b.defined=function(a){return arguments.length?(e=a,b):e},b.interpolate=function(a){return arguments.length?(g="function"==typeof a?f=a:(f=Fi.get(a)||uf).key,b):g},b.tension=function(a){return arguments.length?(h=a,b):h},b}function uf(a){return a.length>1?a.join("L"):a+"Z"}function vf(a){return a.join("L")+"Z"}function wf(a){for(var b=0,c=a.length,d=a[0],e=[d[0],",",d[1]];++b<c;)e.push("H",(d[0]+(d=a[b])[0])/2,"V",d[1]);return c>1&&e.push("H",d[0]),e.join("")}function xf(a){for(var b=0,c=a.length,d=a[0],e=[d[0],",",d[1]];++b<c;)e.push("V",(d=a[b])[1],"H",d[0]);return e.join("")}function yf(a){for(var b=0,c=a.length,d=a[0],e=[d[0],",",d[1]];++b<c;)e.push("H",(d=a[b])[0],"V",d[1]);return e.join("")}function zf(a,b){return a.length<4?uf(a):a[1]+Cf(a.slice(1,-1),Df(a,b))}function Af(a,b){return a.length<3?vf(a):a[0]+Cf((a.push(a[0]),a),Df([a[a.length-2]].concat(a,[a[1]]),b))}function Bf(a,b){return a.length<3?uf(a):a[0]+Cf(a,Df(a,b))}function Cf(a,b){if(b.length<1||a.length!=b.length&&a.length!=b.length+2)return uf(a);var c=a.length!=b.length,d="",e=a[0],f=a[1],g=b[0],h=g,i=1;if(c&&(d+="Q"+(f[0]-2*g[0]/3)+","+(f[1]-2*g[1]/3)+","+f[0]+","+f[1],e=a[1],i=2),b.length>1){h=b[1],f=a[i],i++,d+="C"+(e[0]+g[0])+","+(e[1]+g[1])+","+(f[0]-h[0])+","+(f[1]-h[1])+","+f[0]+","+f[1];for(var j=2;j<b.length;j++,i++)f=a[i],h=b[j],d+="S"+(f[0]-h[0])+","+(f[1]-h[1])+","+f[0]+","+f[1]}if(c){var k=a[i];d+="Q"+(f[0]+2*h[0]/3)+","+(f[1]+2*h[1]/3)+","+k[0]+","+k[1]}return d}function Df(a,b){for(var c,d=[],e=(1-b)/2,f=a[0],g=a[1],h=1,i=a.length;++h<i;)c=f,f=g,g=a[h],d.push([e*(g[0]-c[0]),e*(g[1]-c[1])]);return d}function Ef(a){if(a.length<3)return uf(a);var b=1,c=a.length,d=a[0],e=d[0],f=d[1],g=[e,e,e,(d=a[1])[0]],h=[f,f,f,d[1]],i=[e,",",f,"L",If(Ii,g),",",If(Ii,h)];for(a.push(a[c-1]);++b<=c;)d=a[b],g.shift(),g.push(d[0]),h.shift(),h.push(d[1]),Jf(i,g,h);return a.pop(),i.push("L",d),i.join("")}function Ff(a){if(a.length<4)return uf(a);for(var b,c=[],d=-1,e=a.length,f=[0],g=[0];++d<3;)b=a[d],f.push(b[0]),g.push(b[1]);for(c.push(If(Ii,f)+","+If(Ii,g)),--d;++d<e;)b=a[d],f.shift(),f.push(b[0]),g.shift(),g.push(b[1]),Jf(c,f,g);return c.join("")}function Gf(a){for(var b,c,d=-1,e=a.length,f=e+4,g=[],h=[];++d<4;)c=a[d%e],g.push(c[0]),h.push(c[1]);for(b=[If(Ii,g),",",If(Ii,h)],--d;++d<f;)c=a[d%e],g.shift(),g.push(c[0]),h.shift(),h.push(c[1]),Jf(b,g,h);return b.join("")}function Hf(a,b){var c=a.length-1;if(c)for(var d,e,f=a[0][0],g=a[0][1],h=a[c][0]-f,i=a[c][1]-g,j=-1;++j<=c;)d=a[j],e=j/c,d[0]=b*d[0]+(1-b)*(f+e*h),d[1]=b*d[1]+(1-b)*(g+e*i);return Ef(a)}function If(a,b){return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3]}function Jf(a,b,c){a.push("C",If(Gi,b),",",If(Gi,c),",",If(Hi,b),",",If(Hi,c),",",If(Ii,b),",",If(Ii,c))}function Kf(a,b){return(b[1]-a[1])/(b[0]-a[0])}function Lf(a){for(var b=0,c=a.length-1,d=[],e=a[0],f=a[1],g=d[0]=Kf(e,f);++b<c;)d[b]=(g+(g=Kf(e=f,f=a[b+1])))/2;return d[b]=g,d}function Mf(a){for(var b,c,d,e,f=[],g=Lf(a),h=-1,i=a.length-1;++h<i;)b=Kf(a[h],a[h+1]),tg(b)<Jg?g[h]=g[h+1]=0:(c=g[h]/b,d=g[h+1]/b,e=c*c+d*d,e>9&&(e=3*b/Math.sqrt(e),g[h]=e*c,g[h+1]=e*d));for(h=-1;++h<=i;)e=(a[Math.min(i,h+1)][0]-a[Math.max(0,h-1)][0])/(6*(1+g[h]*g[h])),f.push([e||0,g[h]*e||0]);return f}function Nf(a){return a.length<3?uf(a):a[0]+Cf(a,Mf(a))}function Of(a){for(var b,c,d,e=-1,f=a.length;++e<f;)b=a[e],c=b[0],d=b[1]-Og,b[0]=c*Math.cos(d),b[1]=c*Math.sin(d);return a}function Pf(a){function b(b){function i(){p.push("M",h(a(r),l),k,j(a(q.reverse()),l),"Z")}for(var m,n,o,p=[],q=[],r=[],s=-1,t=b.length,u=Aa(c),v=Aa(e),w=c===d?function(){return n}:Aa(d),x=e===f?function(){return o}:Aa(f);++s<t;)g.call(this,m=b[s],s)?(q.push([n=+u.call(this,m,s),o=+v.call(this,m,s)]),r.push([+w.call(this,m,s),+x.call(this,m,s)])):q.length&&(i(),q=[],r=[]);return q.length&&i(),p.length?p.join(""):null}var c=Cc,d=Cc,e=0,f=Dc,g=Db,h=uf,i=h.key,j=h,k="L",l=.7;return b.x=function(a){return arguments.length?(c=d=a,b):d},b.x0=function(a){return arguments.length?(c=a,b):c},b.x1=function(a){return arguments.length?(d=a,b):d},b.y=function(a){return arguments.length?(e=f=a,b):f},b.y0=function(a){return arguments.length?(e=a,b):e},b.y1=function(a){return arguments.length?(f=a,b):f},b.defined=function(a){return arguments.length?(g=a,b):g},b.interpolate=function(a){return arguments.length?(i="function"==typeof a?h=a:(h=Fi.get(a)||uf).key,j=h.reverse||h,k=h.closed?"M":"L",b):i},b.tension=function(a){return arguments.length?(l=a,b):l},b}function Qf(a){return a.radius}function Rf(a){return[a.x,a.y]}function Sf(a){return function(){var b=a.apply(this,arguments),c=b[0],d=b[1]-Og;return[c*Math.cos(d),c*Math.sin(d)]}}function Tf(){return 64}function Uf(){return"circle"}function Vf(a){var b=Math.sqrt(a/Lg);return"M0,"+b+"A"+b+","+b+" 0 1,1 0,"+-b+"A"+b+","+b+" 0 1,1 0,"+b+"Z"}function Wf(a){return function(){var b,c,d;(b=this[a])&&(d=b[c=b.active])&&(d.timer.c=null,d.timer.t=NaN,--b.count?delete b[c]:delete this[a],b.active+=.5,d.event&&d.event.interrupt.call(this,this.__data__,d.index))}}function Xf(a,b,c){return yg(a,Pi),a.namespace=b,a.id=c,a}function Yf(a,b,c,d){var e=a.id,f=a.namespace;return R(a,"function"==typeof c?function(a,g,h){a[f][e].tween.set(b,d(c.call(a,a.__data__,g,h)))}:(c=d(c),function(a){a[f][e].tween.set(b,c)}))}function Zf(a){return null==a&&(a=""),function(){this.textContent=a}}function $f(a){return null==a?"__transition__":"__transition_"+a+"__"}function _f(a,b,c,d,e){function f(a){var b=p.delay;return k.t=b+i,a>=b?g(a-b):void(k.c=g)}function g(c){var e=o.active,f=o[e];f&&(f.timer.c=null,f.timer.t=NaN,--o.count,delete o[e],f.event&&f.event.interrupt.call(a,a.__data__,f.index));for(var g in o)if(d>+g){var j=o[g];j.timer.c=null,j.timer.t=NaN,--o.count,delete o[g]}k.c=h,Fa(function(){return k.c&&h(c||1)&&(k.c=null,k.t=NaN),1},0,i),o.active=d,p.event&&p.event.start.call(a,a.__data__,b),n=[],p.tween.forEach(function(c,d){(d=d.call(a,a.__data__,b))&&n.push(d)}),m=p.ease,l=p.duration}function h(e){for(var f=e/l,g=m(f),h=n.length;h>0;)n[--h].call(a,g);return f>=1?(p.event&&p.event.end.call(a,a.__data__,b),--o.count?delete o[d]:delete a[c],1):void 0}var i,k,l,m,n,o=a[c]||(a[c]={active:0,count:0}),p=o[d];p||(i=e.time,k=Fa(f,0,i),p=o[d]={tween:new j,time:i,timer:k,delay:e.delay,duration:e.duration,ease:e.ease,index:b},e=null,++o.count)}function ag(a,b,c){a.attr("transform",function(a){var d=b(a);return"translate("+(isFinite(d)?d:c(a))+",0)"})}function bg(a,b,c){a.attr("transform",function(a){var d=b(a);return"translate(0,"+(isFinite(d)?d:c(a))+")"})}function cg(a){return a.toISOString()}function dg(a,b,c){function d(b){return a(b)}function e(a,c){var d=a[1]-a[0],e=d/c,f=hg.bisect(Yi,e);return f==Yi.length?[b.year,Ze(a.map(function(a){return a/31536e6}),c)[2]]:f?b[e/Yi[f-1]<Yi[f]/e?f-1:f]:[_i,Ze(a,c)[2]]}return d.invert=function(b){return eg(a.invert(b))},d.domain=function(b){return arguments.length?(a.domain(b),d):a.domain().map(eg)},d.nice=function(a,b){function c(c){return!isNaN(c)&&!a.range(c,eg(+c+1),b).length}var f=d.domain(),g=Qe(f),h=null==a?e(g,10):"number"==typeof a&&e(g,a);return h&&(a=h[0],b=h[1]),d.domain(Te(f,b>1?{floor:function(b){for(;c(b=a.floor(b));)b=eg(b-1);return b},ceil:function(b){for(;c(b=a.ceil(b));)b=eg(+b+1);return b}}:a))},d.ticks=function(a,b){var c=Qe(d.domain()),f=null==a?e(c,10):"number"==typeof a?e(c,a):!a.range&&[{range:a},b];return f&&(a=f[0],b=f[1]),a.range(c[0],eg(+c[1]+1),1>b?1:b)},d.tickFormat=function(){return c},d.copy=function(){return dg(a.copy(),b,c)},Xe(d,a)}function eg(a){return new Date(a)}function fg(a){return JSON.parse(a.responseText)}function gg(a){var b=kg.createRange();return b.selectNode(kg.body),b.createContextualFragment(a.responseText)}var hg={version:"3.5.12"},ig=[].slice,jg=function(a){return ig.call(a)},kg=this.document;if(kg)try{jg(kg.documentElement.childNodes)[0].nodeType}catch(lg){jg=function(a){for(var b=a.length,c=new Array(b);b--;)c[b]=a[b];return c}}if(Date.now||(Date.now=function(){return+new Date}),kg)try{kg.createElement("DIV").style.setProperty("opacity",0,"")}catch(mg){var ng=this.Element.prototype,og=ng.setAttribute,pg=ng.setAttributeNS,qg=this.CSSStyleDeclaration.prototype,rg=qg.setProperty;ng.setAttribute=function(a,b){og.call(this,a,b+"")},ng.setAttributeNS=function(a,b,c){pg.call(this,a,b,c+"")},qg.setProperty=function(a,b,c){rg.call(this,a,b+"",c)}}hg.ascending=c,hg.descending=function(a,b){return a>b?-1:b>a?1:b>=a?0:NaN},hg.min=function(a,b){var c,d,e=-1,f=a.length;if(1===arguments.length){for(;++e<f;)if(null!=(d=a[e])&&d>=d){c=d;break}for(;++e<f;)null!=(d=a[e])&&c>d&&(c=d)}else{for(;++e<f;)if(null!=(d=b.call(a,a[e],e))&&d>=d){c=d;break}for(;++e<f;)null!=(d=b.call(a,a[e],e))&&c>d&&(c=d)}return c},hg.max=function(a,b){var c,d,e=-1,f=a.length;if(1===arguments.length){for(;++e<f;)if(null!=(d=a[e])&&d>=d){c=d;break}for(;++e<f;)null!=(d=a[e])&&d>c&&(c=d)}else{for(;++e<f;)if(null!=(d=b.call(a,a[e],e))&&d>=d){c=d;break}for(;++e<f;)null!=(d=b.call(a,a[e],e))&&d>c&&(c=d)}return c},hg.extent=function(a,b){var c,d,e,f=-1,g=a.length;if(1===arguments.length){for(;++f<g;)if(null!=(d=a[f])&&d>=d){c=e=d;break}for(;++f<g;)null!=(d=a[f])&&(c>d&&(c=d),d>e&&(e=d))}else{for(;++f<g;)if(null!=(d=b.call(a,a[f],f))&&d>=d){c=e=d;break}for(;++f<g;)null!=(d=b.call(a,a[f],f))&&(c>d&&(c=d),d>e&&(e=d))}return[c,e]},hg.sum=function(a,b){var c,d=0,f=a.length,g=-1;if(1===arguments.length)for(;++g<f;)e(c=+a[g])&&(d+=c);else for(;++g<f;)e(c=+b.call(a,a[g],g))&&(d+=c);return d},hg.mean=function(a,b){var c,f=0,g=a.length,h=-1,i=g;if(1===arguments.length)for(;++h<g;)e(c=d(a[h]))?f+=c:--i;else for(;++h<g;)e(c=d(b.call(a,a[h],h)))?f+=c:--i;return i?f/i:void 0},hg.quantile=function(a,b){var c=(a.length-1)*b+1,d=Math.floor(c),e=+a[d-1],f=c-d;return f?e+f*(a[d]-e):e},hg.median=function(a,b){var f,g=[],h=a.length,i=-1;if(1===arguments.length)for(;++i<h;)e(f=d(a[i]))&&g.push(f);else for(;++i<h;)e(f=d(b.call(a,a[i],i)))&&g.push(f);return g.length?hg.quantile(g.sort(c),.5):void 0},hg.variance=function(a,b){var c,f,g=a.length,h=0,i=0,j=-1,k=0;if(1===arguments.length)for(;++j<g;)e(c=d(a[j]))&&(f=c-h,h+=f/++k,i+=f*(c-h));else for(;++j<g;)e(c=d(b.call(a,a[j],j)))&&(f=c-h,h+=f/++k,i+=f*(c-h));return k>1?i/(k-1):void 0},hg.deviation=function(){var a=hg.variance.apply(this,arguments);return a?Math.sqrt(a):a};var sg=f(c);hg.bisectLeft=sg.left,hg.bisect=hg.bisectRight=sg.right,hg.bisector=function(a){return f(1===a.length?function(b,d){return c(a(b),d)}:a)},hg.shuffle=function(a,b,c){(f=arguments.length)<3&&(c=a.length,2>f&&(b=0));for(var d,e,f=c-b;f;)e=Math.random()*f--|0,d=a[f+b],a[f+b]=a[e+b],a[e+b]=d;return a},hg.permute=function(a,b){for(var c=b.length,d=new Array(c);c--;)d[c]=a[b[c]];return d},hg.pairs=function(a){for(var b,c=0,d=a.length-1,e=a[0],f=new Array(0>d?0:d);d>c;)f[c]=[b=e,e=a[++c]];return f},hg.zip=function(){if(!(d=arguments.length))return[];for(var a=-1,b=hg.min(arguments,g),c=new Array(b);++a<b;)for(var d,e=-1,f=c[a]=new Array(d);++e<d;)f[e]=arguments[e][a];return c},hg.transpose=function(a){return hg.zip.apply(hg,a)},hg.keys=function(a){var b=[];for(var c in a)b.push(c);return b},hg.values=function(a){var b=[];for(var c in a)b.push(a[c]);return b},hg.entries=function(a){var b=[];for(var c in a)b.push({key:c,value:a[c]});return b},hg.merge=function(a){for(var b,c,d,e=a.length,f=-1,g=0;++f<e;)g+=a[f].length;for(c=new Array(g);--e>=0;)for(d=a[e],b=d.length;--b>=0;)c[--g]=d[b];return c};var tg=Math.abs;hg.range=function(a,b,c){if(arguments.length<3&&(c=1,arguments.length<2&&(b=a,a=0)),(b-a)/c===1/0)throw new Error("infinite range");var d,e=[],f=h(tg(c)),g=-1;if(a*=f,b*=f,c*=f,0>c)for(;(d=a+c*++g)>b;)e.push(d/f);else for(;(d=a+c*++g)<b;)e.push(d/f);return e},hg.map=function(a,b){var c=new j;if(a instanceof j)a.forEach(function(a,b){c.set(a,b)});else if(Array.isArray(a)){var d,e=-1,f=a.length;if(1===arguments.length)for(;++e<f;)c.set(e,a[e]);else for(;++e<f;)c.set(b.call(a,d=a[e],e),d)}else for(var g in a)c.set(g,a[g]);return c};var ug="__proto__",vg="\0";i(j,{has:m,get:function(a){return this._[k(a)]},set:function(a,b){return this._[k(a)]=b},remove:n,keys:o,values:function(){var a=[];for(var b in this._)a.push(this._[b]);return a},entries:function(){var a=[];for(var b in this._)a.push({key:l(b),value:this._[b]});return a},size:p,empty:q,forEach:function(a){for(var b in this._)a.call(this,l(b),this._[b])}}),hg.nest=function(){function a(b,g,h){if(h>=f.length)return d?d.call(e,g):c?g.sort(c):g;for(var i,k,l,m,n=-1,o=g.length,p=f[h++],q=new j;++n<o;)(m=q.get(i=p(k=g[n])))?m.push(k):q.set(i,[k]);return b?(k=b(),l=function(c,d){k.set(c,a(b,d,h))}):(k={},l=function(c,d){k[c]=a(b,d,h)}),q.forEach(l),k}function b(a,c){if(c>=f.length)return a;var d=[],e=g[c++];return a.forEach(function(a,e){d.push({key:a,values:b(e,c)})}),e?d.sort(function(a,b){return e(a.key,b.key)}):d}var c,d,e={},f=[],g=[];return e.map=function(b,c){return a(c,b,0)},e.entries=function(c){return b(a(hg.map,c,0),0)},e.key=function(a){return f.push(a),e},e.sortKeys=function(a){return g[f.length-1]=a,e},e.sortValues=function(a){return c=a,e},e.rollup=function(a){return d=a,e},e},hg.set=function(a){var b=new r;if(a)for(var c=0,d=a.length;d>c;++c)b.add(a[c]);return b},i(r,{has:m,add:function(a){return this._[k(a+="")]=!0,a},remove:n,values:o,size:p,empty:q,forEach:function(a){for(var b in this._)a.call(this,l(b))}}),hg.behavior={},hg.rebind=function(a,b){for(var c,d=1,e=arguments.length;++d<e;)a[c=arguments[d]]=t(a,b,b[c]);return a};var wg=["webkit","ms","moz","Moz","o","O"];hg.dispatch=function(){for(var a=new w,b=-1,c=arguments.length;++b<c;)a[arguments[b]]=x(a);return a},w.prototype.on=function(a,b){var c=a.indexOf("."),d="";if(c>=0&&(d=a.slice(c+1),a=a.slice(0,c)),a)return arguments.length<2?this[a].on(d):this[a].on(d,b);if(2===arguments.length){if(null==b)for(a in this)this.hasOwnProperty(a)&&this[a].on(d,null);return this}},hg.event=null,hg.requote=function(a){return a.replace(xg,"\\$&")};var xg=/[\\\^\$\*\+\?\|\[\]\(\)\.\{\}]/g,yg={}.__proto__?function(a,b){a.__proto__=b}:function(a,b){for(var c in b)a[c]=b[c]},zg=function(a,b){return b.querySelector(a)},Ag=function(a,b){return b.querySelectorAll(a)},Bg=function(a,b){var c=a.matches||a[u(a,"matchesSelector")];return(Bg=function(a,b){return c.call(a,b)})(a,b)};"function"==typeof Sizzle&&(zg=function(a,b){return Sizzle(a,b)[0]||null},Ag=Sizzle,Bg=Sizzle.matchesSelector),hg.selection=function(){return hg.select(kg.documentElement)};var Cg=hg.selection.prototype=[];Cg.select=function(a){var b,c,d,e,f=[];a=C(a);for(var g=-1,h=this.length;++g<h;){f.push(b=[]),b.parentNode=(d=this[g]).parentNode;for(var i=-1,j=d.length;++i<j;)(e=d[i])?(b.push(c=a.call(e,e.__data__,i,g)),c&&"__data__"in e&&(c.__data__=e.__data__)):b.push(null)}return B(f)},Cg.selectAll=function(a){var b,c,d=[];a=D(a);for(var e=-1,f=this.length;++e<f;)for(var g=this[e],h=-1,i=g.length;++h<i;)(c=g[h])&&(d.push(b=jg(a.call(c,c.__data__,h,e))),b.parentNode=c);return B(d)};var Dg={svg:"http://www.w3.org/2000/svg",xhtml:"http://www.w3.org/1999/xhtml",xlink:"http://www.w3.org/1999/xlink",xml:"http://www.w3.org/XML/1998/namespace",xmlns:"http://www.w3.org/2000/xmlns/"};hg.ns={prefix:Dg,qualify:function(a){var b=a.indexOf(":"),c=a;return b>=0&&"xmlns"!==(c=a.slice(0,b))&&(a=a.slice(b+1)),Dg.hasOwnProperty(c)?{space:Dg[c],local:a}:a}},Cg.attr=function(a,b){if(arguments.length<2){if("string"==typeof a){var c=this.node();return a=hg.ns.qualify(a),a.local?c.getAttributeNS(a.space,a.local):c.getAttribute(a)}for(b in a)this.each(E(b,a[b]));return this}return this.each(E(a,b))},Cg.classed=function(a,b){if(arguments.length<2){if("string"==typeof a){var c=this.node(),d=(a=H(a)).length,e=-1;if(b=c.classList){for(;++e<d;)if(!b.contains(a[e]))return!1}else for(b=c.getAttribute("class");++e<d;)if(!G(a[e]).test(b))return!1;return!0}for(b in a)this.each(I(b,a[b]));return this}return this.each(I(a,b))},Cg.style=function(a,c,d){var e=arguments.length;if(3>e){if("string"!=typeof a){2>e&&(c="");for(d in a)this.each(K(d,a[d],c));return this}if(2>e){var f=this.node();return b(f).getComputedStyle(f,null).getPropertyValue(a)}d=""}return this.each(K(a,c,d))},Cg.property=function(a,b){if(arguments.length<2){if("string"==typeof a)return this.node()[a];for(b in a)this.each(L(b,a[b]));return this}return this.each(L(a,b))},Cg.text=function(a){return arguments.length?this.each("function"==typeof a?function(){var b=a.apply(this,arguments);this.textContent=null==b?"":b}:null==a?function(){this.textContent=""}:function(){this.textContent=a}):this.node().textContent},Cg.html=function(a){return arguments.length?this.each("function"==typeof a?function(){var b=a.apply(this,arguments);this.innerHTML=null==b?"":b}:null==a?function(){this.innerHTML=""}:function(){this.innerHTML=a}):this.node().innerHTML},Cg.append=function(a){return a=M(a),this.select(function(){return this.appendChild(a.apply(this,arguments))})},Cg.insert=function(a,b){return a=M(a),b=C(b),this.select(function(){return this.insertBefore(a.apply(this,arguments),b.apply(this,arguments)||null)})},Cg.remove=function(){return this.each(N)},Cg.data=function(a,b){function c(a,c){var d,e,f,g=a.length,l=c.length,m=Math.min(g,l),n=new Array(l),o=new Array(l),p=new Array(g);if(b){var q,r=new j,s=new Array(g);for(d=-1;++d<g;)(e=a[d])&&(r.has(q=b.call(e,e.__data__,d))?p[d]=e:r.set(q,e),s[d]=q);for(d=-1;++d<l;)(e=r.get(q=b.call(c,f=c[d],d)))?e!==!0&&(n[d]=e,e.__data__=f):o[d]=O(f),r.set(q,!0);for(d=-1;++d<g;)d in s&&r.get(s[d])!==!0&&(p[d]=a[d])}else{for(d=-1;++d<m;)e=a[d],f=c[d],e?(e.__data__=f,n[d]=e):o[d]=O(f);for(;l>d;++d)o[d]=O(c[d]);for(;g>d;++d)p[d]=a[d]}o.update=n,o.parentNode=n.parentNode=p.parentNode=a.parentNode,h.push(o),i.push(n),k.push(p)}var d,e,f=-1,g=this.length;if(!arguments.length){for(a=new Array(g=(d=this[0]).length);++f<g;)(e=d[f])&&(a[f]=e.__data__);return a}var h=S([]),i=B([]),k=B([]);if("function"==typeof a)for(;++f<g;)c(d=this[f],a.call(d,d.parentNode.__data__,f));else for(;++f<g;)c(d=this[f],a);return i.enter=function(){return h},i.exit=function(){return k},i},Cg.datum=function(a){return arguments.length?this.property("__data__",a):this.property("__data__")},Cg.filter=function(a){var b,c,d,e=[];"function"!=typeof a&&(a=P(a));for(var f=0,g=this.length;g>f;f++){e.push(b=[]),b.parentNode=(c=this[f]).parentNode;for(var h=0,i=c.length;i>h;h++)(d=c[h])&&a.call(d,d.__data__,h,f)&&b.push(d)}return B(e)},Cg.order=function(){for(var a=-1,b=this.length;++a<b;)for(var c,d=this[a],e=d.length-1,f=d[e];--e>=0;)(c=d[e])&&(f&&f!==c.nextSibling&&f.parentNode.insertBefore(c,f),f=c);return this},Cg.sort=function(a){a=Q.apply(this,arguments);for(var b=-1,c=this.length;++b<c;)this[b].sort(a);return this.order()},Cg.each=function(a){return R(this,function(b,c,d){a.call(b,b.__data__,c,d)})},Cg.call=function(a){var b=jg(arguments);return a.apply(b[0]=this,b),this},Cg.empty=function(){return!this.node()},Cg.node=function(){for(var a=0,b=this.length;b>a;a++)for(var c=this[a],d=0,e=c.length;e>d;d++){var f=c[d];if(f)return f}return null},Cg.size=function(){var a=0;return R(this,function(){++a}),a};var Eg=[];hg.selection.enter=S,hg.selection.enter.prototype=Eg,Eg.append=Cg.append,Eg.empty=Cg.empty,Eg.node=Cg.node,Eg.call=Cg.call,Eg.size=Cg.size,Eg.select=function(a){for(var b,c,d,e,f,g=[],h=-1,i=this.length;++h<i;){d=(e=this[h]).update,g.push(b=[]),b.parentNode=e.parentNode;for(var j=-1,k=e.length;++j<k;)(f=e[j])?(b.push(d[j]=c=a.call(e.parentNode,f.__data__,j,h)),c.__data__=f.__data__):b.push(null)}return B(g)},Eg.insert=function(a,b){return arguments.length<2&&(b=T(this)),Cg.insert.call(this,a,b)},hg.select=function(b){var c;return"string"==typeof b?(c=[zg(b,kg)],c.parentNode=kg.documentElement):(c=[b],c.parentNode=a(b)),B([c])},hg.selectAll=function(a){var b;return"string"==typeof a?(b=jg(Ag(a,kg)),b.parentNode=kg.documentElement):(b=jg(a),b.parentNode=null),B([b])},Cg.on=function(a,b,c){var d=arguments.length;if(3>d){if("string"!=typeof a){2>d&&(b=!1);for(c in a)this.each(U(c,a[c],b));return this}if(2>d)return(d=this.node()["__on"+a])&&d._;c=!1}return this.each(U(a,b,c))};var Fg=hg.map({mouseenter:"mouseover",mouseleave:"mouseout"});kg&&Fg.forEach(function(a){"on"+a in kg&&Fg.remove(a)});var Gg,Hg=0;hg.mouse=function(a){return Y(a,z())};var Ig=this.navigator&&/WebKit/.test(this.navigator.userAgent)?-1:0;hg.touch=function(a,b,c){if(arguments.length<3&&(c=b,b=z().changedTouches),b)for(var d,e=0,f=b.length;f>e;++e)if((d=b[e]).identifier===c)return Y(a,d)},hg.behavior.drag=function(){function a(){this.on("mousedown.drag",f).on("touchstart.drag",g)}function c(a,b,c,f,g){return function(){function h(){var a,c,d=b(m,p);d&&(a=d[0]-t[0],c=d[1]-t[1],o|=a|c,t=d,n({type:"drag",x:d[0]+j[0],y:d[1]+j[1],dx:a,dy:c}))}function i(){b(m,p)&&(r.on(f+q,null).on(g+q,null),s(o),n({type:"dragend"}))}var j,k=this,l=hg.event.target,m=k.parentNode,n=d.of(k,arguments),o=0,p=a(),q=".drag"+(null==p?"":"-"+p),r=hg.select(c(l)).on(f+q,h).on(g+q,i),s=X(l),t=b(m,p);e?(j=e.apply(k,arguments),j=[j.x-t[0],j.y-t[1]]):j=[0,0],n({type:"dragstart"})}}var d=A(a,"drag","dragstart","dragend"),e=null,f=c(v,hg.mouse,b,"mousemove","mouseup"),g=c(Z,hg.touch,s,"touchmove","touchend");return a.origin=function(b){return arguments.length?(e=b,a):e},hg.rebind(a,d,"on")},hg.touches=function(a,b){return arguments.length<2&&(b=z().touches),b?jg(b).map(function(b){var c=Y(a,b);return c.identifier=b.identifier,c}):[]};var Jg=1e-6,Kg=Jg*Jg,Lg=Math.PI,Mg=2*Lg,Ng=Mg-Jg,Og=Lg/2,Pg=Lg/180,Qg=180/Lg,Rg=Math.SQRT2,Sg=2,Tg=4;hg.interpolateZoom=function(a,b){var c,d,e=a[0],f=a[1],g=a[2],h=b[0],i=b[1],j=b[2],k=h-e,l=i-f,m=k*k+l*l;if(Kg>m)d=Math.log(j/g)/Rg,c=function(a){return[e+a*k,f+a*l,g*Math.exp(Rg*a*d)]};else{var n=Math.sqrt(m),o=(j*j-g*g+Tg*m)/(2*g*Sg*n),p=(j*j-g*g-Tg*m)/(2*j*Sg*n),q=Math.log(Math.sqrt(o*o+1)-o),r=Math.log(Math.sqrt(p*p+1)-p);d=(r-q)/Rg,c=function(a){var b=a*d,c=da(q),h=g/(Sg*n)*(c*ea(Rg*b+q)-ca(q));return[e+h*k,f+h*l,g*c/da(Rg*b+q)]}}return c.duration=1e3*d,c},hg.behavior.zoom=function(){function a(a){a.on(F,l).on(Vg+".zoom",n).on("dblclick.zoom",o).on(I,m)}function c(a){return[(a[0]-z.x)/z.k,(a[1]-z.y)/z.k]}function d(a){return[a[0]*z.k+z.x,a[1]*z.k+z.y]}function e(a){z.k=Math.max(C[0],Math.min(C[1],a))}function f(a,b){b=d(b),z.x+=a[0]-b[0],z.y+=a[1]-b[1]}function g(b,c,d,g){b.__chart__={x:z.x,y:z.y,k:z.k},e(Math.pow(2,g)),f(q=c,d),b=hg.select(b),D>0&&(b=b.transition().duration(D)),b.call(a.event)}function h(){v&&v.domain(u.range().map(function(a){return(a-z.x)/z.k}).map(u.invert)),x&&x.domain(w.range().map(function(a){return(a-z.y)/z.k}).map(w.invert))}function i(a){E++||a({type:"zoomstart"})}function j(a){h(),a({type:"zoom",scale:z.k,translate:[z.x,z.y]})}function k(a){--E||(a({type:"zoomend"}),q=null)}function l(){function a(){h=1,f(hg.mouse(e),m),j(g)}function d(){l.on(G,null).on(H,null),n(h),k(g)}var e=this,g=J.of(e,arguments),h=0,l=hg.select(b(e)).on(G,a).on(H,d),m=c(hg.mouse(e)),n=X(e);Oi.call(e),i(g)}function m(){function a(){var a=hg.touches(o);return n=z.k,a.forEach(function(a){a.identifier in q&&(q[a.identifier]=c(a))}),a}function b(){var b=hg.event.target;hg.select(b).on(u,d).on(v,h),w.push(b);for(var c=hg.event.changedTouches,e=0,f=c.length;f>e;++e)q[c[e].identifier]=null;var i=a(),j=Date.now();if(1===i.length){if(500>j-t){var k=i[0];g(o,k,q[k.identifier],Math.floor(Math.log(z.k)/Math.LN2)+1),y()}t=j}else if(i.length>1){var k=i[0],l=i[1],m=k[0]-l[0],n=k[1]-l[1];r=m*m+n*n}}function d(){var a,b,c,d,g=hg.touches(o);Oi.call(o);for(var h=0,i=g.length;i>h;++h,d=null)if(c=g[h],d=q[c.identifier]){if(b)break;a=c,b=d}if(d){var k=(k=c[0]-a[0])*k+(k=c[1]-a[1])*k,l=r&&Math.sqrt(k/r);a=[(a[0]+c[0])/2,(a[1]+c[1])/2],b=[(b[0]+d[0])/2,(b[1]+d[1])/2],e(l*n)}t=null,f(a,b),j(p)}function h(){if(hg.event.touches.length){for(var b=hg.event.changedTouches,c=0,d=b.length;d>c;++c)delete q[b[c].identifier];for(var e in q)return void a()}hg.selectAll(w).on(s,null),x.on(F,l).on(I,m),A(),k(p)}var n,o=this,p=J.of(o,arguments),q={},r=0,s=".zoom-"+hg.event.changedTouches[0].identifier,u="touchmove"+s,v="touchend"+s,w=[],x=hg.select(o),A=X(o);b(),i(p),x.on(F,null).on(I,b)}function n(){var a=J.of(this,arguments);s?clearTimeout(s):(Oi.call(this),p=c(q=r||hg.mouse(this)),i(a)),s=setTimeout(function(){s=null,k(a)},50),y(),e(Math.pow(2,.002*Ug())*z.k),f(q,p),j(a)}function o(){var a=hg.mouse(this),b=Math.log(z.k)/Math.LN2;g(this,a,c(a),hg.event.shiftKey?Math.ceil(b)-1:Math.floor(b)+1)}var p,q,r,s,t,u,v,w,x,z={x:0,y:0,k:1},B=[960,500],C=Wg,D=250,E=0,F="mousedown.zoom",G="mousemove.zoom",H="mouseup.zoom",I="touchstart.zoom",J=A(a,"zoomstart","zoom","zoomend");return Vg||(Vg="onwheel"in kg?(Ug=function(){return-hg.event.deltaY*(hg.event.deltaMode?120:1)},"wheel"):"onmousewheel"in kg?(Ug=function(){return hg.event.wheelDelta},"mousewheel"):(Ug=function(){return-hg.event.detail},"MozMousePixelScroll")),a.event=function(a){a.each(function(){var a=J.of(this,arguments),b=z;Mi?hg.select(this).transition().each("start.zoom",function(){z=this.__chart__||{x:0,y:0,k:1},i(a)}).tween("zoom:zoom",function(){var c=B[0],d=B[1],e=q?q[0]:c/2,f=q?q[1]:d/2,g=hg.interpolateZoom([(e-z.x)/z.k,(f-z.y)/z.k,c/z.k],[(e-b.x)/b.k,(f-b.y)/b.k,c/b.k]);return function(b){var d=g(b),h=c/d[2];this.__chart__=z={x:e-d[0]*h,y:f-d[1]*h,k:h},j(a)}}).each("interrupt.zoom",function(){k(a)}).each("end.zoom",function(){k(a)}):(this.__chart__=z,i(a),j(a),k(a))})},a.translate=function(b){return arguments.length?(z={x:+b[0],y:+b[1],k:z.k},h(),a):[z.x,z.y]},a.scale=function(b){return arguments.length?(z={x:z.x,y:z.y,k:null},e(+b),h(),a):z.k},a.scaleExtent=function(b){return arguments.length?(C=null==b?Wg:[+b[0],+b[1]],a):C},a.center=function(b){return arguments.length?(r=b&&[+b[0],+b[1]],a):r},a.size=function(b){return arguments.length?(B=b&&[+b[0],+b[1]],a):B},a.duration=function(b){return arguments.length?(D=+b,a):D},a.x=function(b){return arguments.length?(v=b,u=b.copy(),z={x:0,y:0,k:1},a):v},a.y=function(b){return arguments.length?(x=b,w=b.copy(),z={x:0,y:0,k:1},a):x},hg.rebind(a,J,"on")};var Ug,Vg,Wg=[0,1/0];hg.color=ga,ga.prototype.toString=function(){return this.rgb()+""},hg.hsl=ha;var Xg=ha.prototype=new ga;Xg.brighter=function(a){return a=Math.pow(.7,arguments.length?a:1),new ha(this.h,this.s,this.l/a)},Xg.darker=function(a){return a=Math.pow(.7,arguments.length?a:1),new ha(this.h,this.s,a*this.l)},Xg.rgb=function(){return ia(this.h,this.s,this.l)},hg.hcl=ja;var Yg=ja.prototype=new ga;Yg.brighter=function(a){return new ja(this.h,this.c,Math.min(100,this.l+Zg*(arguments.length?a:1)))},Yg.darker=function(a){return new ja(this.h,this.c,Math.max(0,this.l-Zg*(arguments.length?a:1)))},Yg.rgb=function(){return ka(this.h,this.c,this.l).rgb()},hg.lab=la;var Zg=18,$g=.95047,_g=1,ah=1.08883,bh=la.prototype=new ga;bh.brighter=function(a){return new la(Math.min(100,this.l+Zg*(arguments.length?a:1)),this.a,this.b)},bh.darker=function(a){return new la(Math.max(0,this.l-Zg*(arguments.length?a:1)),this.a,this.b)},bh.rgb=function(){return ma(this.l,this.a,this.b)},hg.rgb=ra;var ch=ra.prototype=new ga;ch.brighter=function(a){a=Math.pow(.7,arguments.length?a:1);var b=this.r,c=this.g,d=this.b,e=30;return b||c||d?(b&&e>b&&(b=e),c&&e>c&&(c=e),d&&e>d&&(d=e),new ra(Math.min(255,b/a),Math.min(255,c/a),Math.min(255,d/a))):new ra(e,e,e)},ch.darker=function(a){return a=Math.pow(.7,arguments.length?a:1),new ra(a*this.r,a*this.g,a*this.b)},ch.hsl=function(){return wa(this.r,this.g,this.b)},ch.toString=function(){return"#"+ua(this.r)+ua(this.g)+ua(this.b)};var dh=hg.map({aliceblue:15792383,antiquewhite:16444375,aqua:65535,aquamarine:8388564,azure:15794175,beige:16119260,bisque:16770244,black:0,blanchedalmond:16772045,blue:255,blueviolet:9055202,brown:10824234,burlywood:14596231,cadetblue:6266528,chartreuse:8388352,chocolate:13789470,coral:16744272,cornflowerblue:6591981,cornsilk:16775388,crimson:14423100,cyan:65535,darkblue:139,darkcyan:35723,darkgoldenrod:12092939,darkgray:11119017,darkgreen:25600,darkgrey:11119017,darkkhaki:12433259,darkmagenta:9109643,darkolivegreen:5597999,darkorange:16747520,darkorchid:10040012,darkred:9109504,darksalmon:15308410,darkseagreen:9419919,darkslateblue:4734347,darkslategray:3100495,darkslategrey:3100495,darkturquoise:52945,darkviolet:9699539,deeppink:16716947,deepskyblue:49151,dimgray:6908265,dimgrey:6908265,dodgerblue:2003199,firebrick:11674146,floralwhite:16775920,forestgreen:2263842,fuchsia:16711935,gainsboro:14474460,ghostwhite:16316671,gold:16766720,goldenrod:14329120,gray:8421504,green:32768,greenyellow:11403055,grey:8421504,honeydew:15794160,hotpink:16738740,indianred:13458524,indigo:4915330,ivory:16777200,khaki:15787660,lavender:15132410,lavenderblush:16773365,lawngreen:8190976,lemonchiffon:16775885,lightblue:11393254,lightcoral:15761536,lightcyan:14745599,lightgoldenrodyellow:16448210,lightgray:13882323,lightgreen:9498256,lightgrey:13882323,lightpink:16758465,lightsalmon:16752762,lightseagreen:2142890,lightskyblue:8900346,lightslategray:7833753,lightslategrey:7833753,lightsteelblue:11584734,lightyellow:16777184,lime:65280,limegreen:3329330,linen:16445670,magenta:16711935,maroon:8388608,mediumaquamarine:6737322,mediumblue:205,mediumorchid:12211667,
mediumpurple:9662683,mediumseagreen:3978097,mediumslateblue:8087790,mediumspringgreen:64154,mediumturquoise:4772300,mediumvioletred:13047173,midnightblue:1644912,mintcream:16121850,mistyrose:16770273,moccasin:16770229,navajowhite:16768685,navy:128,oldlace:16643558,olive:8421376,olivedrab:7048739,orange:16753920,orangered:16729344,orchid:14315734,palegoldenrod:15657130,palegreen:10025880,paleturquoise:11529966,palevioletred:14381203,papayawhip:16773077,peachpuff:16767673,peru:13468991,pink:16761035,plum:14524637,powderblue:11591910,purple:8388736,rebeccapurple:6697881,red:16711680,rosybrown:12357519,royalblue:4286945,saddlebrown:9127187,salmon:16416882,sandybrown:16032864,seagreen:3050327,seashell:16774638,sienna:10506797,silver:12632256,skyblue:8900331,slateblue:6970061,slategray:7372944,slategrey:7372944,snow:16775930,springgreen:65407,steelblue:4620980,tan:13808780,teal:32896,thistle:14204888,tomato:16737095,turquoise:4251856,violet:15631086,wheat:16113331,white:16777215,whitesmoke:16119285,yellow:16776960,yellowgreen:10145074});dh.forEach(function(a,b){dh.set(a,sa(b))}),hg.functor=Aa,hg.xhr=Ba(s),hg.dsv=function(a,b){function c(a,c,f){arguments.length<3&&(f=c,c=null);var g=Ca(a,b,null==c?d:e(c),f);return g.row=function(a){return arguments.length?g.response(null==(c=a)?d:e(a)):c},g}function d(a){return c.parse(a.responseText)}function e(a){return function(b){return c.parse(b.responseText,a)}}function f(b){return b.map(g).join(a)}function g(a){return h.test(a)?'"'+a.replace(/\"/g,'""')+'"':a}var h=new RegExp('["'+a+"\n]"),i=a.charCodeAt(0);return c.parse=function(a,b){var d;return c.parseRows(a,function(a,c){if(d)return d(a,c-1);var e=new Function("d","return {"+a.map(function(a,b){return JSON.stringify(a)+": d["+b+"]"}).join(",")+"}");d=b?function(a,c){return b(e(a),c)}:e})},c.parseRows=function(a,b){function c(){if(k>=j)return g;if(e)return e=!1,f;var b=k;if(34===a.charCodeAt(b)){for(var c=b;c++<j;)if(34===a.charCodeAt(c)){if(34!==a.charCodeAt(c+1))break;++c}k=c+2;var d=a.charCodeAt(c+1);return 13===d?(e=!0,10===a.charCodeAt(c+2)&&++k):10===d&&(e=!0),a.slice(b+1,c).replace(/""/g,'"')}for(;j>k;){var d=a.charCodeAt(k++),h=1;if(10===d)e=!0;else if(13===d)e=!0,10===a.charCodeAt(k)&&(++k,++h);else if(d!==i)continue;return a.slice(b,k-h)}return a.slice(b)}for(var d,e,f={},g={},h=[],j=a.length,k=0,l=0;(d=c())!==g;){for(var m=[];d!==f&&d!==g;)m.push(d),d=c();b&&null==(m=b(m,l++))||h.push(m)}return h},c.format=function(b){if(Array.isArray(b[0]))return c.formatRows(b);var d=new r,e=[];return b.forEach(function(a){for(var b in a)d.has(b)||e.push(d.add(b))}),[e.map(g).join(a)].concat(b.map(function(b){return e.map(function(a){return g(b[a])}).join(a)})).join("\n")},c.formatRows=function(a){return a.map(f).join("\n")},c},hg.csv=hg.dsv(",","text/csv"),hg.tsv=hg.dsv("\t","text/tab-separated-values");var eh,fh,gh,hh,ih=this[u(this,"requestAnimationFrame")]||function(a){setTimeout(a,17)};hg.timer=function(){Fa.apply(this,arguments)},hg.timer.flush=function(){Ha(),Ia()},hg.round=function(a,b){return b?Math.round(a*(b=Math.pow(10,b)))/b:Math.round(a)};var jh=["y","z","a","f","p","n","Âµ","m","","k","M","G","T","P","E","Z","Y"].map(Ka);hg.formatPrefix=function(a,b){var c=0;return(a=+a)&&(0>a&&(a*=-1),b&&(a=hg.round(a,Ja(a,b))),c=1+Math.floor(1e-12+Math.log(a)/Math.LN10),c=Math.max(-24,Math.min(24,3*Math.floor((c-1)/3)))),jh[8+c/3]};var kh=/(?:([^{])?([<>=^]))?([+\- ])?([$#])?(0)?(\d+)?(,)?(\.-?\d+)?([a-z%])?/i,lh=hg.map({b:function(a){return a.toString(2)},c:function(a){return String.fromCharCode(a)},o:function(a){return a.toString(8)},x:function(a){return a.toString(16)},X:function(a){return a.toString(16).toUpperCase()},g:function(a,b){return a.toPrecision(b)},e:function(a,b){return a.toExponential(b)},f:function(a,b){return a.toFixed(b)},r:function(a,b){return(a=hg.round(a,Ja(a,b))).toFixed(Math.max(0,Math.min(20,Ja(a*(1+1e-15),b))))}}),mh=hg.time={},nh=Date;Na.prototype={getDate:function(){return this._.getUTCDate()},getDay:function(){return this._.getUTCDay()},getFullYear:function(){return this._.getUTCFullYear()},getHours:function(){return this._.getUTCHours()},getMilliseconds:function(){return this._.getUTCMilliseconds()},getMinutes:function(){return this._.getUTCMinutes()},getMonth:function(){return this._.getUTCMonth()},getSeconds:function(){return this._.getUTCSeconds()},getTime:function(){return this._.getTime()},getTimezoneOffset:function(){return 0},valueOf:function(){return this._.valueOf()},setDate:function(){oh.setUTCDate.apply(this._,arguments)},setDay:function(){oh.setUTCDay.apply(this._,arguments)},setFullYear:function(){oh.setUTCFullYear.apply(this._,arguments)},setHours:function(){oh.setUTCHours.apply(this._,arguments)},setMilliseconds:function(){oh.setUTCMilliseconds.apply(this._,arguments)},setMinutes:function(){oh.setUTCMinutes.apply(this._,arguments)},setMonth:function(){oh.setUTCMonth.apply(this._,arguments)},setSeconds:function(){oh.setUTCSeconds.apply(this._,arguments)},setTime:function(){oh.setTime.apply(this._,arguments)}};var oh=Date.prototype;mh.year=Oa(function(a){return a=mh.day(a),a.setMonth(0,1),a},function(a,b){a.setFullYear(a.getFullYear()+b)},function(a){return a.getFullYear()}),mh.years=mh.year.range,mh.years.utc=mh.year.utc.range,mh.day=Oa(function(a){var b=new nh(2e3,0);return b.setFullYear(a.getFullYear(),a.getMonth(),a.getDate()),b},function(a,b){a.setDate(a.getDate()+b)},function(a){return a.getDate()-1}),mh.days=mh.day.range,mh.days.utc=mh.day.utc.range,mh.dayOfYear=function(a){var b=mh.year(a);return Math.floor((a-b-6e4*(a.getTimezoneOffset()-b.getTimezoneOffset()))/864e5)},["sunday","monday","tuesday","wednesday","thursday","friday","saturday"].forEach(function(a,b){b=7-b;var c=mh[a]=Oa(function(a){return(a=mh.day(a)).setDate(a.getDate()-(a.getDay()+b)%7),a},function(a,b){a.setDate(a.getDate()+7*Math.floor(b))},function(a){var c=mh.year(a).getDay();return Math.floor((mh.dayOfYear(a)+(c+b)%7)/7)-(c!==b)});mh[a+"s"]=c.range,mh[a+"s"].utc=c.utc.range,mh[a+"OfYear"]=function(a){var c=mh.year(a).getDay();return Math.floor((mh.dayOfYear(a)+(c+b)%7)/7)}}),mh.week=mh.sunday,mh.weeks=mh.sunday.range,mh.weeks.utc=mh.sunday.utc.range,mh.weekOfYear=mh.sundayOfYear;var ph={"-":"",_:" ",0:"0"},qh=/^\s*\d+/,rh=/^%/;hg.locale=function(a){return{numberFormat:La(a),timeFormat:Qa(a)}};var sh=hg.locale({decimal:".",thousands:",",grouping:[3],currency:["$",""],dateTime:"%a %b %e %X %Y",date:"%m/%d/%Y",time:"%H:%M:%S",periods:["AM","PM"],days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],shortDays:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],shortMonths:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]});hg.format=sh.numberFormat,hg.geo={},jb.prototype={s:0,t:0,add:function(a){kb(a,this.t,th),kb(th.s,this.s,this),this.s?this.t+=th.t:this.s=th.t},reset:function(){this.s=this.t=0},valueOf:function(){return this.s}};var th=new jb;hg.geo.stream=function(a,b){a&&uh.hasOwnProperty(a.type)?uh[a.type](a,b):lb(a,b)};var uh={Feature:function(a,b){lb(a.geometry,b)},FeatureCollection:function(a,b){for(var c=a.features,d=-1,e=c.length;++d<e;)lb(c[d].geometry,b)}},vh={Sphere:function(a,b){b.sphere()},Point:function(a,b){a=a.coordinates,b.point(a[0],a[1],a[2])},MultiPoint:function(a,b){for(var c=a.coordinates,d=-1,e=c.length;++d<e;)a=c[d],b.point(a[0],a[1],a[2])},LineString:function(a,b){mb(a.coordinates,b,0)},MultiLineString:function(a,b){for(var c=a.coordinates,d=-1,e=c.length;++d<e;)mb(c[d],b,0)},Polygon:function(a,b){nb(a.coordinates,b)},MultiPolygon:function(a,b){for(var c=a.coordinates,d=-1,e=c.length;++d<e;)nb(c[d],b)},GeometryCollection:function(a,b){for(var c=a.geometries,d=-1,e=c.length;++d<e;)lb(c[d],b)}};hg.geo.area=function(a){return wh=0,hg.geo.stream(a,yh),wh};var wh,xh=new jb,yh={sphere:function(){wh+=4*Lg},point:v,lineStart:v,lineEnd:v,polygonStart:function(){xh.reset(),yh.lineStart=ob},polygonEnd:function(){var a=2*xh;wh+=0>a?4*Lg+a:a,yh.lineStart=yh.lineEnd=yh.point=v}};hg.geo.bounds=function(){function a(a,b){t.push(u=[k=a,m=a]),l>b&&(l=b),b>n&&(n=b)}function b(b,c){var d=pb([b*Pg,c*Pg]);if(r){var e=rb(r,d),f=[e[1],-e[0],0],g=rb(f,e);ub(g),g=vb(g);var i=b-o,j=i>0?1:-1,p=g[0]*Qg*j,q=tg(i)>180;if(q^(p>j*o&&j*b>p)){var s=g[1]*Qg;s>n&&(n=s)}else if(p=(p+360)%360-180,q^(p>j*o&&j*b>p)){var s=-g[1]*Qg;l>s&&(l=s)}else l>c&&(l=c),c>n&&(n=c);q?o>b?h(k,b)>h(k,m)&&(m=b):h(b,m)>h(k,m)&&(k=b):m>=k?(k>b&&(k=b),b>m&&(m=b)):b>o?h(k,b)>h(k,m)&&(m=b):h(b,m)>h(k,m)&&(k=b)}else a(b,c);r=d,o=b}function c(){v.point=b}function d(){u[0]=k,u[1]=m,v.point=a,r=null}function e(a,c){if(r){var d=a-o;s+=tg(d)>180?d+(d>0?360:-360):d}else p=a,q=c;yh.point(a,c),b(a,c)}function f(){yh.lineStart()}function g(){e(p,q),yh.lineEnd(),tg(s)>Jg&&(k=-(m=180)),u[0]=k,u[1]=m,r=null}function h(a,b){return(b-=a)<0?b+360:b}function i(a,b){return a[0]-b[0]}function j(a,b){return b[0]<=b[1]?b[0]<=a&&a<=b[1]:a<b[0]||b[1]<a}var k,l,m,n,o,p,q,r,s,t,u,v={point:a,lineStart:c,lineEnd:d,polygonStart:function(){v.point=e,v.lineStart=f,v.lineEnd=g,s=0,yh.polygonStart()},polygonEnd:function(){yh.polygonEnd(),v.point=a,v.lineStart=c,v.lineEnd=d,0>xh?(k=-(m=180),l=-(n=90)):s>Jg?n=90:-Jg>s&&(l=-90),u[0]=k,u[1]=m}};return function(a){n=m=-(k=l=1/0),t=[],hg.geo.stream(a,v);var b=t.length;if(b){t.sort(i);for(var c,d=1,e=t[0],f=[e];b>d;++d)c=t[d],j(c[0],e)||j(c[1],e)?(h(e[0],c[1])>h(e[0],e[1])&&(e[1]=c[1]),h(c[0],e[1])>h(e[0],e[1])&&(e[0]=c[0])):f.push(e=c);for(var g,c,o=-(1/0),b=f.length-1,d=0,e=f[b];b>=d;e=c,++d)c=f[d],(g=h(e[1],c[0]))>o&&(o=g,k=c[0],m=e[1])}return t=u=null,k===1/0||l===1/0?[[NaN,NaN],[NaN,NaN]]:[[k,l],[m,n]]}}(),hg.geo.centroid=function(a){zh=Ah=Bh=Ch=Dh=Eh=Fh=Gh=Hh=Ih=Jh=0,hg.geo.stream(a,Kh);var b=Hh,c=Ih,d=Jh,e=b*b+c*c+d*d;return Kg>e&&(b=Eh,c=Fh,d=Gh,Jg>Ah&&(b=Bh,c=Ch,d=Dh),e=b*b+c*c+d*d,Kg>e)?[NaN,NaN]:[Math.atan2(c,b)*Qg,ba(d/Math.sqrt(e))*Qg]};var zh,Ah,Bh,Ch,Dh,Eh,Fh,Gh,Hh,Ih,Jh,Kh={sphere:v,point:xb,lineStart:zb,lineEnd:Ab,polygonStart:function(){Kh.lineStart=Bb},polygonEnd:function(){Kh.lineStart=zb}},Lh=Hb(Db,Lb,Nb,[-Lg,-Lg/2]),Mh=1e9;hg.geo.clipExtent=function(){var a,b,c,d,e,f,g={stream:function(a){return e&&(e.valid=!1),e=f(a),e.valid=!0,e},extent:function(h){return arguments.length?(f=Rb(a=+h[0][0],b=+h[0][1],c=+h[1][0],d=+h[1][1]),e&&(e.valid=!1,e=null),g):[[a,b],[c,d]]}};return g.extent([[0,0],[960,500]])},(hg.geo.conicEqualArea=function(){return Sb(Tb)}).raw=Tb,hg.geo.albers=function(){return hg.geo.conicEqualArea().rotate([96,0]).center([-.6,38.7]).parallels([29.5,45.5]).scale(1070)},hg.geo.albersUsa=function(){function a(a){var f=a[0],g=a[1];return b=null,c(f,g),b||(d(f,g),b)||e(f,g),b}var b,c,d,e,f=hg.geo.albers(),g=hg.geo.conicEqualArea().rotate([154,0]).center([-2,58.5]).parallels([55,65]),h=hg.geo.conicEqualArea().rotate([157,0]).center([-3,19.9]).parallels([8,18]),i={point:function(a,c){b=[a,c]}};return a.invert=function(a){var b=f.scale(),c=f.translate(),d=(a[0]-c[0])/b,e=(a[1]-c[1])/b;return(e>=.12&&.234>e&&d>=-.425&&-.214>d?g:e>=.166&&.234>e&&d>=-.214&&-.115>d?h:f).invert(a)},a.stream=function(a){var b=f.stream(a),c=g.stream(a),d=h.stream(a);return{point:function(a,e){b.point(a,e),c.point(a,e),d.point(a,e)},sphere:function(){b.sphere(),c.sphere(),d.sphere()},lineStart:function(){b.lineStart(),c.lineStart(),d.lineStart()},lineEnd:function(){b.lineEnd(),c.lineEnd(),d.lineEnd()},polygonStart:function(){b.polygonStart(),c.polygonStart(),d.polygonStart()},polygonEnd:function(){b.polygonEnd(),c.polygonEnd(),d.polygonEnd()}}},a.precision=function(b){return arguments.length?(f.precision(b),g.precision(b),h.precision(b),a):f.precision()},a.scale=function(b){return arguments.length?(f.scale(b),g.scale(.35*b),h.scale(b),a.translate(f.translate())):f.scale()},a.translate=function(b){if(!arguments.length)return f.translate();var j=f.scale(),k=+b[0],l=+b[1];return c=f.translate(b).clipExtent([[k-.455*j,l-.238*j],[k+.455*j,l+.238*j]]).stream(i).point,d=g.translate([k-.307*j,l+.201*j]).clipExtent([[k-.425*j+Jg,l+.12*j+Jg],[k-.214*j-Jg,l+.234*j-Jg]]).stream(i).point,e=h.translate([k-.205*j,l+.212*j]).clipExtent([[k-.214*j+Jg,l+.166*j+Jg],[k-.115*j-Jg,l+.234*j-Jg]]).stream(i).point,a},a.scale(1070)};var Nh,Oh,Ph,Qh,Rh,Sh,Th={point:v,lineStart:v,lineEnd:v,polygonStart:function(){Oh=0,Th.lineStart=Ub},polygonEnd:function(){Th.lineStart=Th.lineEnd=Th.point=v,Nh+=tg(Oh/2)}},Uh={point:Vb,lineStart:v,lineEnd:v,polygonStart:v,polygonEnd:v},Vh={point:Yb,lineStart:Zb,lineEnd:$b,polygonStart:function(){Vh.lineStart=_b},polygonEnd:function(){Vh.point=Yb,Vh.lineStart=Zb,Vh.lineEnd=$b}};hg.geo.path=function(){function a(a){return a&&("function"==typeof h&&f.pointRadius(+h.apply(this,arguments)),g&&g.valid||(g=e(f)),hg.geo.stream(a,g)),f.result()}function b(){return g=null,a}var c,d,e,f,g,h=4.5;return a.area=function(a){return Nh=0,hg.geo.stream(a,e(Th)),Nh},a.centroid=function(a){return Bh=Ch=Dh=Eh=Fh=Gh=Hh=Ih=Jh=0,hg.geo.stream(a,e(Vh)),Jh?[Hh/Jh,Ih/Jh]:Gh?[Eh/Gh,Fh/Gh]:Dh?[Bh/Dh,Ch/Dh]:[NaN,NaN]},a.bounds=function(a){return Rh=Sh=-(Ph=Qh=1/0),hg.geo.stream(a,e(Uh)),[[Ph,Qh],[Rh,Sh]]},a.projection=function(a){return arguments.length?(e=(c=a)?a.stream||cc(a):s,b()):c},a.context=function(a){return arguments.length?(f=null==(d=a)?new Wb:new ac(a),"function"!=typeof h&&f.pointRadius(h),b()):d},a.pointRadius=function(b){return arguments.length?(h="function"==typeof b?b:(f.pointRadius(+b),+b),a):h},a.projection(hg.geo.albersUsa()).context(null)},hg.geo.transform=function(a){return{stream:function(b){var c=new dc(b);for(var d in a)c[d]=a[d];return c}}},dc.prototype={point:function(a,b){this.stream.point(a,b)},sphere:function(){this.stream.sphere()},lineStart:function(){this.stream.lineStart()},lineEnd:function(){this.stream.lineEnd()},polygonStart:function(){this.stream.polygonStart()},polygonEnd:function(){this.stream.polygonEnd()}},hg.geo.projection=fc,hg.geo.projectionMutator=gc,(hg.geo.equirectangular=function(){return fc(ic)}).raw=ic.invert=ic,hg.geo.rotation=function(a){function b(b){return b=a(b[0]*Pg,b[1]*Pg),b[0]*=Qg,b[1]*=Qg,b}return a=kc(a[0]%360*Pg,a[1]*Pg,a.length>2?a[2]*Pg:0),b.invert=function(b){return b=a.invert(b[0]*Pg,b[1]*Pg),b[0]*=Qg,b[1]*=Qg,b},b},jc.invert=ic,hg.geo.circle=function(){function a(){var a="function"==typeof d?d.apply(this,arguments):d,b=kc(-a[0]*Pg,-a[1]*Pg,0).invert,e=[];return c(null,null,1,{point:function(a,c){e.push(a=b(a,c)),a[0]*=Qg,a[1]*=Qg}}),{type:"Polygon",coordinates:[e]}}var b,c,d=[0,0],e=6;return a.origin=function(b){return arguments.length?(d=b,a):d},a.angle=function(d){return arguments.length?(c=oc((b=+d)*Pg,e*Pg),a):b},a.precision=function(d){return arguments.length?(c=oc(b*Pg,(e=+d)*Pg),a):e},a.angle(90)},hg.geo.distance=function(a,b){var c,d=(b[0]-a[0])*Pg,e=a[1]*Pg,f=b[1]*Pg,g=Math.sin(d),h=Math.cos(d),i=Math.sin(e),j=Math.cos(e),k=Math.sin(f),l=Math.cos(f);return Math.atan2(Math.sqrt((c=l*g)*c+(c=j*k-i*l*h)*c),i*k+j*l*h)},hg.geo.graticule=function(){function a(){return{type:"MultiLineString",coordinates:b()}}function b(){return hg.range(Math.ceil(f/q)*q,e,q).map(m).concat(hg.range(Math.ceil(j/r)*r,i,r).map(n)).concat(hg.range(Math.ceil(d/o)*o,c,o).filter(function(a){return tg(a%q)>Jg}).map(k)).concat(hg.range(Math.ceil(h/p)*p,g,p).filter(function(a){return tg(a%r)>Jg}).map(l))}var c,d,e,f,g,h,i,j,k,l,m,n,o=10,p=o,q=90,r=360,s=2.5;return a.lines=function(){return b().map(function(a){return{type:"LineString",coordinates:a}})},a.outline=function(){return{type:"Polygon",coordinates:[m(f).concat(n(i).slice(1),m(e).reverse().slice(1),n(j).reverse().slice(1))]}},a.extent=function(b){return arguments.length?a.majorExtent(b).minorExtent(b):a.minorExtent()},a.majorExtent=function(b){return arguments.length?(f=+b[0][0],e=+b[1][0],j=+b[0][1],i=+b[1][1],f>e&&(b=f,f=e,e=b),j>i&&(b=j,j=i,i=b),a.precision(s)):[[f,j],[e,i]]},a.minorExtent=function(b){return arguments.length?(d=+b[0][0],c=+b[1][0],h=+b[0][1],g=+b[1][1],d>c&&(b=d,d=c,c=b),h>g&&(b=h,h=g,g=b),a.precision(s)):[[d,h],[c,g]]},a.step=function(b){return arguments.length?a.majorStep(b).minorStep(b):a.minorStep()},a.majorStep=function(b){return arguments.length?(q=+b[0],r=+b[1],a):[q,r]},a.minorStep=function(b){return arguments.length?(o=+b[0],p=+b[1],a):[o,p]},a.precision=function(b){return arguments.length?(s=+b,k=qc(h,g,90),l=rc(d,c,s),m=qc(j,i,90),n=rc(f,e,s),a):s},a.majorExtent([[-180,-90+Jg],[180,90-Jg]]).minorExtent([[-180,-80-Jg],[180,80+Jg]])},hg.geo.greatArc=function(){function a(){return{type:"LineString",coordinates:[b||d.apply(this,arguments),c||e.apply(this,arguments)]}}var b,c,d=sc,e=tc;return a.distance=function(){return hg.geo.distance(b||d.apply(this,arguments),c||e.apply(this,arguments))},a.source=function(c){return arguments.length?(d=c,b="function"==typeof c?null:c,a):d},a.target=function(b){return arguments.length?(e=b,c="function"==typeof b?null:b,a):e},a.precision=function(){return arguments.length?a:0},a},hg.geo.interpolate=function(a,b){return uc(a[0]*Pg,a[1]*Pg,b[0]*Pg,b[1]*Pg)},hg.geo.length=function(a){return Wh=0,hg.geo.stream(a,Xh),Wh};var Wh,Xh={sphere:v,point:v,lineStart:vc,lineEnd:v,polygonStart:v,polygonEnd:v},Yh=wc(function(a){return Math.sqrt(2/(1+a))},function(a){return 2*Math.asin(a/2)});(hg.geo.azimuthalEqualArea=function(){return fc(Yh)}).raw=Yh;var Zh=wc(function(a){var b=Math.acos(a);return b&&b/Math.sin(b)},s);(hg.geo.azimuthalEquidistant=function(){return fc(Zh)}).raw=Zh,(hg.geo.conicConformal=function(){return Sb(xc)}).raw=xc,(hg.geo.conicEquidistant=function(){return Sb(yc)}).raw=yc;var $h=wc(function(a){return 1/a},Math.atan);(hg.geo.gnomonic=function(){return fc($h)}).raw=$h,zc.invert=function(a,b){return[a,2*Math.atan(Math.exp(b))-Og]},(hg.geo.mercator=function(){return Ac(zc)}).raw=zc;var _h=wc(function(){return 1},Math.asin);(hg.geo.orthographic=function(){return fc(_h)}).raw=_h;var ai=wc(function(a){return 1/(1+a)},function(a){return 2*Math.atan(a)});(hg.geo.stereographic=function(){return fc(ai)}).raw=ai,Bc.invert=function(a,b){return[-b,2*Math.atan(Math.exp(a))-Og]},(hg.geo.transverseMercator=function(){var a=Ac(Bc),b=a.center,c=a.rotate;return a.center=function(a){return a?b([-a[1],a[0]]):(a=b(),[a[1],-a[0]])},a.rotate=function(a){return a?c([a[0],a[1],a.length>2?a[2]+90:90]):(a=c(),[a[0],a[1],a[2]-90])},c([0,0,90])}).raw=Bc,hg.geom={},hg.geom.hull=function(a){function b(a){if(a.length<3)return[];var b,e=Aa(c),f=Aa(d),g=a.length,h=[],i=[];for(b=0;g>b;b++)h.push([+e.call(this,a[b],b),+f.call(this,a[b],b),b]);for(h.sort(Fc),b=0;g>b;b++)i.push([h[b][0],-h[b][1]]);var j=Ec(h),k=Ec(i),l=k[0]===j[0],m=k[k.length-1]===j[j.length-1],n=[];for(b=j.length-1;b>=0;--b)n.push(a[h[j[b]][2]]);for(b=+l;b<k.length-m;++b)n.push(a[h[k[b]][2]]);return n}var c=Cc,d=Dc;return arguments.length?b(a):(b.x=function(a){return arguments.length?(c=a,b):c},b.y=function(a){return arguments.length?(d=a,b):d},b)},hg.geom.polygon=function(a){return yg(a,bi),a};var bi=hg.geom.polygon.prototype=[];bi.area=function(){for(var a,b=-1,c=this.length,d=this[c-1],e=0;++b<c;)a=d,d=this[b],e+=a[1]*d[0]-a[0]*d[1];return.5*e},bi.centroid=function(a){var b,c,d=-1,e=this.length,f=0,g=0,h=this[e-1];for(arguments.length||(a=-1/(6*this.area()));++d<e;)b=h,h=this[d],c=b[0]*h[1]-h[0]*b[1],f+=(b[0]+h[0])*c,g+=(b[1]+h[1])*c;return[f*a,g*a]},bi.clip=function(a){for(var b,c,d,e,f,g,h=Ic(a),i=-1,j=this.length-Ic(this),k=this[j-1];++i<j;){for(b=a.slice(),a.length=0,e=this[i],f=b[(d=b.length-h)-1],c=-1;++c<d;)g=b[c],Gc(g,k,e)?(Gc(f,k,e)||a.push(Hc(f,g,k,e)),a.push(g)):Gc(f,k,e)&&a.push(Hc(f,g,k,e)),f=g;h&&a.push(a[0]),k=e}return a};var ci,di,ei,fi,gi,hi=[],ii=[];Qc.prototype.prepare=function(){for(var a,b=this.edges,c=b.length;c--;)a=b[c].edge,a.b&&a.a||b.splice(c,1);return b.sort(Sc),b.length},ad.prototype={start:function(){return this.edge.l===this.site?this.edge.a:this.edge.b},end:function(){return this.edge.l===this.site?this.edge.b:this.edge.a}},bd.prototype={insert:function(a,b){var c,d,e;if(a){if(b.P=a,b.N=a.N,a.N&&(a.N.P=b),a.N=b,a.R){for(a=a.R;a.L;)a=a.L;a.L=b}else a.R=b;c=a}else this._?(a=fd(this._),b.P=null,b.N=a,a.P=a.L=b,c=a):(b.P=b.N=null,this._=b,c=null);for(b.L=b.R=null,b.U=c,b.C=!0,a=b;c&&c.C;)d=c.U,c===d.L?(e=d.R,e&&e.C?(c.C=e.C=!1,d.C=!0,a=d):(a===c.R&&(dd(this,c),a=c,c=a.U),c.C=!1,d.C=!0,ed(this,d))):(e=d.L,e&&e.C?(c.C=e.C=!1,d.C=!0,a=d):(a===c.L&&(ed(this,c),a=c,c=a.U),c.C=!1,d.C=!0,dd(this,d))),c=a.U;this._.C=!1},remove:function(a){a.N&&(a.N.P=a.P),a.P&&(a.P.N=a.N),a.N=a.P=null;var b,c,d,e=a.U,f=a.L,g=a.R;if(c=f?g?fd(g):f:g,e?e.L===a?e.L=c:e.R=c:this._=c,f&&g?(d=c.C,c.C=a.C,c.L=f,f.U=c,c!==g?(e=c.U,c.U=a.U,a=c.R,e.L=a,c.R=g,g.U=c):(c.U=e,e=c,a=c.R)):(d=a.C,a=c),a&&(a.U=e),!d){if(a&&a.C)return void(a.C=!1);do{if(a===this._)break;if(a===e.L){if(b=e.R,b.C&&(b.C=!1,e.C=!0,dd(this,e),b=e.R),b.L&&b.L.C||b.R&&b.R.C){b.R&&b.R.C||(b.L.C=!1,b.C=!0,ed(this,b),b=e.R),b.C=e.C,e.C=b.R.C=!1,dd(this,e),a=this._;break}}else if(b=e.L,b.C&&(b.C=!1,e.C=!0,ed(this,e),b=e.L),b.L&&b.L.C||b.R&&b.R.C){b.L&&b.L.C||(b.R.C=!1,b.C=!0,dd(this,b),b=e.L),b.C=e.C,e.C=b.L.C=!1,ed(this,e),a=this._;break}b.C=!0,a=e,e=e.U}while(!a.C);a&&(a.C=!1)}}},hg.geom.voronoi=function(a){function b(a){var b=new Array(a.length),d=h[0][0],e=h[0][1],f=h[1][0],g=h[1][1];return gd(c(a),h).cells.forEach(function(c,h){var i=c.edges,j=c.site,k=b[h]=i.length?i.map(function(a){var b=a.start();return[b.x,b.y]}):j.x>=d&&j.x<=f&&j.y>=e&&j.y<=g?[[d,g],[f,g],[f,e],[d,e]]:[];k.point=a[h]}),b}function c(a){return a.map(function(a,b){return{x:Math.round(f(a,b)/Jg)*Jg,y:Math.round(g(a,b)/Jg)*Jg,i:b}})}var d=Cc,e=Dc,f=d,g=e,h=ji;return a?b(a):(b.links=function(a){return gd(c(a)).edges.filter(function(a){return a.l&&a.r}).map(function(b){return{source:a[b.l.i],target:a[b.r.i]}})},b.triangles=function(a){var b=[];return gd(c(a)).cells.forEach(function(c,d){for(var e,f,g=c.site,h=c.edges.sort(Sc),i=-1,j=h.length,k=h[j-1].edge,l=k.l===g?k.r:k.l;++i<j;)e=k,f=l,k=h[i].edge,l=k.l===g?k.r:k.l,d<f.i&&d<l.i&&id(g,f,l)<0&&b.push([a[d],a[f.i],a[l.i]])}),b},b.x=function(a){return arguments.length?(f=Aa(d=a),b):d},b.y=function(a){return arguments.length?(g=Aa(e=a),b):e},b.clipExtent=function(a){return arguments.length?(h=null==a?ji:a,b):h===ji?null:h},b.size=function(a){return arguments.length?b.clipExtent(a&&[[0,0],a]):h===ji?null:h&&h[1]},b)};var ji=[[-1e6,-1e6],[1e6,1e6]];hg.geom.delaunay=function(a){return hg.geom.voronoi().triangles(a)},hg.geom.quadtree=function(a,b,c,d,e){function f(a){function f(a,b,c,d,e,f,g,h){if(!isNaN(c)&&!isNaN(d))if(a.leaf){var i=a.x,k=a.y;if(null!=i)if(tg(i-c)+tg(k-d)<.01)j(a,b,c,d,e,f,g,h);else{var l=a.point;a.x=a.y=a.point=null,j(a,l,i,k,e,f,g,h),j(a,b,c,d,e,f,g,h)}else a.x=c,a.y=d,a.point=b}else j(a,b,c,d,e,f,g,h)}function j(a,b,c,d,e,g,h,i){var j=.5*(e+h),k=.5*(g+i),l=c>=j,m=d>=k,n=m<<1|l;a.leaf=!1,a=a.nodes[n]||(a.nodes[n]=ld()),l?e=j:h=j,m?g=k:i=k,f(a,b,c,d,e,g,h,i)}var k,l,m,n,o,p,q,r,s,t=Aa(h),u=Aa(i);if(null!=b)p=b,q=c,r=d,s=e;else if(r=s=-(p=q=1/0),l=[],m=[],o=a.length,g)for(n=0;o>n;++n)k=a[n],k.x<p&&(p=k.x),k.y<q&&(q=k.y),k.x>r&&(r=k.x),k.y>s&&(s=k.y),l.push(k.x),m.push(k.y);else for(n=0;o>n;++n){var v=+t(k=a[n],n),w=+u(k,n);p>v&&(p=v),q>w&&(q=w),v>r&&(r=v),w>s&&(s=w),l.push(v),m.push(w)}var x=r-p,y=s-q;x>y?s=q+x:r=p+y;var z=ld();if(z.add=function(a){f(z,a,+t(a,++n),+u(a,n),p,q,r,s)},z.visit=function(a){md(a,z,p,q,r,s)},z.find=function(a){return nd(z,a[0],a[1],p,q,r,s)},n=-1,null==b){for(;++n<o;)f(z,a[n],l[n],m[n],p,q,r,s);--n}else a.forEach(z.add);return l=m=a=k=null,z}var g,h=Cc,i=Dc;return(g=arguments.length)?(h=jd,i=kd,3===g&&(e=c,d=b,c=b=0),f(a)):(f.x=function(a){return arguments.length?(h=a,f):h},f.y=function(a){return arguments.length?(i=a,f):i},f.extent=function(a){return arguments.length?(null==a?b=c=d=e=null:(b=+a[0][0],c=+a[0][1],d=+a[1][0],e=+a[1][1]),f):null==b?null:[[b,c],[d,e]]},f.size=function(a){return arguments.length?(null==a?b=c=d=e=null:(b=c=0,d=+a[0],e=+a[1]),f):null==b?null:[d-b,e-c]},f)},hg.interpolateRgb=od,hg.interpolateObject=pd,hg.interpolateNumber=qd,hg.interpolateString=rd;var ki=/[-+]?(?:\d+\.?\d*|\.?\d+)(?:[eE][-+]?\d+)?/g,li=new RegExp(ki.source,"g");hg.interpolate=sd,hg.interpolators=[function(a,b){var c=typeof b;return("string"===c?dh.has(b.toLowerCase())||/^(#|rgb\(|hsl\()/i.test(b)?od:rd:b instanceof ga?od:Array.isArray(b)?td:"object"===c&&isNaN(b)?pd:qd)(a,b)}],hg.interpolateArray=td;var mi=function(){return s},ni=hg.map({linear:mi,poly:Ad,quad:function(){return xd},cubic:function(){return yd},sin:function(){return Bd},exp:function(){return Cd},circle:function(){return Dd},elastic:Ed,back:Fd,bounce:function(){return Gd}}),oi=hg.map({"in":s,out:vd,"in-out":wd,"out-in":function(a){return wd(vd(a))}});hg.ease=function(a){var b=a.indexOf("-"),c=b>=0?a.slice(0,b):a,d=b>=0?a.slice(b+1):"in";return c=ni.get(c)||mi,d=oi.get(d)||s,ud(d(c.apply(null,ig.call(arguments,1))))},hg.interpolateHcl=Hd,hg.interpolateHsl=Id,hg.interpolateLab=Jd,hg.interpolateRound=Kd,hg.transform=function(a){var b=kg.createElementNS(hg.ns.prefix.svg,"g");return(hg.transform=function(a){if(null!=a){b.setAttribute("transform",a);var c=b.transform.baseVal.consolidate()}return new Ld(c?c.matrix:pi)})(a)},Ld.prototype.toString=function(){return"translate("+this.translate+")rotate("+this.rotate+")skewX("+this.skew+")scale("+this.scale+")"};var pi={a:1,b:0,c:0,d:1,e:0,f:0};hg.interpolateTransform=Ud,hg.layout={},hg.layout.bundle=function(){return function(a){for(var b=[],c=-1,d=a.length;++c<d;)b.push(Xd(a[c]));return b}},hg.layout.chord=function(){function a(){var a,j,l,m,n,o={},p=[],q=hg.range(f),r=[];for(c=[],d=[],a=0,m=-1;++m<f;){for(j=0,n=-1;++n<f;)j+=e[m][n];p.push(j),r.push(hg.range(f)),a+=j}for(g&&q.sort(function(a,b){return g(p[a],p[b])}),h&&r.forEach(function(a,b){a.sort(function(a,c){return h(e[b][a],e[b][c])})}),a=(Mg-k*f)/a,j=0,m=-1;++m<f;){for(l=j,n=-1;++n<f;){var s=q[m],t=r[s][n],u=e[s][t],v=j,w=j+=u*a;o[s+"-"+t]={index:s,subindex:t,startAngle:v,endAngle:w,value:u}}d[s]={index:s,startAngle:l,endAngle:j,value:p[s]},j+=k}for(m=-1;++m<f;)for(n=m-1;++n<f;){var x=o[m+"-"+n],y=o[n+"-"+m];(x.value||y.value)&&c.push(x.value<y.value?{source:y,target:x}:{source:x,target:y})}i&&b()}function b(){c.sort(function(a,b){return i((a.source.value+a.target.value)/2,(b.source.value+b.target.value)/2)})}var c,d,e,f,g,h,i,j={},k=0;return j.matrix=function(a){return arguments.length?(f=(e=a)&&e.length,c=d=null,j):e},j.padding=function(a){return arguments.length?(k=a,c=d=null,j):k},j.sortGroups=function(a){return arguments.length?(g=a,c=d=null,j):g},j.sortSubgroups=function(a){return arguments.length?(h=a,c=null,j):h},j.sortChords=function(a){return arguments.length?(i=a,c&&b(),j):i},j.chords=function(){return c||a(),c},j.groups=function(){return d||a(),d},j},hg.layout.force=function(){function a(a){return function(b,c,d,e){if(b.point!==a){var f=b.cx-a.x,g=b.cy-a.y,h=e-c,i=f*f+g*g;if(i>h*h/r){if(p>i){var j=b.charge/i;a.px-=f*j,a.py-=g*j}return!0}if(b.point&&i&&p>i){var j=b.pointCharge/i;a.px-=f*j,a.py-=g*j}}return!b.charge}}function b(a){a.px=hg.event.x,a.py=hg.event.y,i.resume()}var c,d,e,f,g,h,i={},j=hg.dispatch("start","tick","end"),k=[1,1],l=.9,m=qi,n=ri,o=-30,p=si,q=.1,r=.64,t=[],u=[];return i.tick=function(){if((e*=.99)<.005)return c=null,j.end({type:"end",alpha:e=0}),!0;var b,d,i,m,n,p,r,s,v,w=t.length,x=u.length;for(d=0;x>d;++d)i=u[d],m=i.source,n=i.target,s=n.x-m.x,v=n.y-m.y,(p=s*s+v*v)&&(p=e*g[d]*((p=Math.sqrt(p))-f[d])/p,s*=p,v*=p,n.x-=s*(r=m.weight+n.weight?m.weight/(m.weight+n.weight):.5),n.y-=v*r,m.x+=s*(r=1-r),m.y+=v*r);if((r=e*q)&&(s=k[0]/2,v=k[1]/2,d=-1,r))for(;++d<w;)i=t[d],i.x+=(s-i.x)*r,i.y+=(v-i.y)*r;if(o)for(ce(b=hg.geom.quadtree(t),e,h),d=-1;++d<w;)(i=t[d]).fixed||b.visit(a(i));for(d=-1;++d<w;)i=t[d],i.fixed?(i.x=i.px,i.y=i.py):(i.x-=(i.px-(i.px=i.x))*l,i.y-=(i.py-(i.py=i.y))*l);j.tick({type:"tick",alpha:e})},i.nodes=function(a){return arguments.length?(t=a,i):t},i.links=function(a){return arguments.length?(u=a,i):u},i.size=function(a){return arguments.length?(k=a,i):k},i.linkDistance=function(a){return arguments.length?(m="function"==typeof a?a:+a,i):m},i.distance=i.linkDistance,i.linkStrength=function(a){return arguments.length?(n="function"==typeof a?a:+a,i):n},i.friction=function(a){return arguments.length?(l=+a,i):l},i.charge=function(a){return arguments.length?(o="function"==typeof a?a:+a,i):o},i.chargeDistance=function(a){return arguments.length?(p=a*a,i):Math.sqrt(p)},i.gravity=function(a){return arguments.length?(q=+a,i):q},i.theta=function(a){return arguments.length?(r=a*a,i):Math.sqrt(r)},i.alpha=function(a){return arguments.length?(a=+a,e?a>0?e=a:(c.c=null,c.t=NaN,c=null,j.end({type:"end",alpha:e=0})):a>0&&(j.start({type:"start",alpha:e=a}),c=Fa(i.tick)),i):e},i.start=function(){function a(a,d){if(!c){for(c=new Array(e),i=0;e>i;++i)c[i]=[];for(i=0;j>i;++i){var f=u[i];c[f.source.index].push(f.target),c[f.target.index].push(f.source)}}for(var g,h=c[b],i=-1,k=h.length;++i<k;)if(!isNaN(g=h[i][a]))return g;return Math.random()*d}var b,c,d,e=t.length,j=u.length,l=k[0],p=k[1];for(b=0;e>b;++b)(d=t[b]).index=b,d.weight=0;for(b=0;j>b;++b)d=u[b],"number"==typeof d.source&&(d.source=t[d.source]),"number"==typeof d.target&&(d.target=t[d.target]),++d.source.weight,++d.target.weight;for(b=0;e>b;++b)d=t[b],isNaN(d.x)&&(d.x=a("x",l)),isNaN(d.y)&&(d.y=a("y",p)),isNaN(d.px)&&(d.px=d.x),isNaN(d.py)&&(d.py=d.y);if(f=[],"function"==typeof m)for(b=0;j>b;++b)f[b]=+m.call(this,u[b],b);else for(b=0;j>b;++b)f[b]=m;if(g=[],"function"==typeof n)for(b=0;j>b;++b)g[b]=+n.call(this,u[b],b);else for(b=0;j>b;++b)g[b]=n;if(h=[],"function"==typeof o)for(b=0;e>b;++b)h[b]=+o.call(this,t[b],b);else for(b=0;e>b;++b)h[b]=o;return i.resume()},i.resume=function(){return i.alpha(.1)},i.stop=function(){return i.alpha(0)},i.drag=function(){return d||(d=hg.behavior.drag().origin(s).on("dragstart.force",$d).on("drag.force",b).on("dragend.force",_d)),arguments.length?void this.on("mouseover.force",ae).on("mouseout.force",be).call(d):d},hg.rebind(i,j,"on")};var qi=20,ri=1,si=1/0;hg.layout.hierarchy=function(){function a(e){var f,g=[e],h=[];for(e.depth=0;null!=(f=g.pop());)if(h.push(f),(j=c.call(a,f,f.depth))&&(i=j.length)){for(var i,j,k;--i>=0;)g.push(k=j[i]),k.parent=f,k.depth=f.depth+1;d&&(f.value=0),f.children=j}else d&&(f.value=+d.call(a,f,f.depth)||0),delete f.children;return fe(e,function(a){var c,e;b&&(c=a.children)&&c.sort(b),d&&(e=a.parent)&&(e.value+=a.value)}),h}var b=ie,c=ge,d=he;return a.sort=function(c){return arguments.length?(b=c,a):b},a.children=function(b){return arguments.length?(c=b,a):c},a.value=function(b){return arguments.length?(d=b,a):d},a.revalue=function(b){return d&&(ee(b,function(a){a.children&&(a.value=0)}),fe(b,function(b){var c;b.children||(b.value=+d.call(a,b,b.depth)||0),(c=b.parent)&&(c.value+=b.value)})),b},a},hg.layout.partition=function(){function a(b,c,d,e){var f=b.children;if(b.x=c,b.y=b.depth*e,b.dx=d,b.dy=e,f&&(g=f.length)){var g,h,i,j=-1;for(d=b.value?d/b.value:0;++j<g;)a(h=f[j],c,i=h.value*d,e),c+=i}}function b(a){var c=a.children,d=0;if(c&&(e=c.length))for(var e,f=-1;++f<e;)d=Math.max(d,b(c[f]));return 1+d}function c(c,f){var g=d.call(this,c,f);return a(g[0],0,e[0],e[1]/b(g[0])),g}var d=hg.layout.hierarchy(),e=[1,1];return c.size=function(a){return arguments.length?(e=a,c):e},de(c,d)},hg.layout.pie=function(){function a(g){var h,i=g.length,j=g.map(function(c,d){return+b.call(a,c,d)}),k=+("function"==typeof d?d.apply(this,arguments):d),l=("function"==typeof e?e.apply(this,arguments):e)-k,m=Math.min(Math.abs(l)/i,+("function"==typeof f?f.apply(this,arguments):f)),n=m*(0>l?-1:1),o=hg.sum(j),p=o?(l-i*n)/o:0,q=hg.range(i),r=[];return null!=c&&q.sort(c===ti?function(a,b){return j[b]-j[a]}:function(a,b){return c(g[a],g[b])}),q.forEach(function(a){r[a]={data:g[a],value:h=j[a],startAngle:k,endAngle:k+=h*p+n,padAngle:m}}),r}var b=Number,c=ti,d=0,e=Mg,f=0;return a.value=function(c){return arguments.length?(b=c,a):b},a.sort=function(b){return arguments.length?(c=b,a):c},a.startAngle=function(b){
return arguments.length?(d=b,a):d},a.endAngle=function(b){return arguments.length?(e=b,a):e},a.padAngle=function(b){return arguments.length?(f=b,a):f},a};var ti={};hg.layout.stack=function(){function a(h,i){if(!(m=h.length))return h;var j=h.map(function(c,d){return b.call(a,c,d)}),k=j.map(function(b){return b.map(function(b,c){return[f.call(a,b,c),g.call(a,b,c)]})}),l=c.call(a,k,i);j=hg.permute(j,l),k=hg.permute(k,l);var m,n,o,p,q=d.call(a,k,i),r=j[0].length;for(o=0;r>o;++o)for(e.call(a,j[0][o],p=q[o],k[0][o][1]),n=1;m>n;++n)e.call(a,j[n][o],p+=k[n-1][o][1],k[n][o][1]);return h}var b=s,c=ne,d=oe,e=me,f=ke,g=le;return a.values=function(c){return arguments.length?(b=c,a):b},a.order=function(b){return arguments.length?(c="function"==typeof b?b:ui.get(b)||ne,a):c},a.offset=function(b){return arguments.length?(d="function"==typeof b?b:vi.get(b)||oe,a):d},a.x=function(b){return arguments.length?(f=b,a):f},a.y=function(b){return arguments.length?(g=b,a):g},a.out=function(b){return arguments.length?(e=b,a):e},a};var ui=hg.map({"inside-out":function(a){var b,c,d=a.length,e=a.map(pe),f=a.map(qe),g=hg.range(d).sort(function(a,b){return e[a]-e[b]}),h=0,i=0,j=[],k=[];for(b=0;d>b;++b)c=g[b],i>h?(h+=f[c],j.push(c)):(i+=f[c],k.push(c));return k.reverse().concat(j)},reverse:function(a){return hg.range(a.length).reverse()},"default":ne}),vi=hg.map({silhouette:function(a){var b,c,d,e=a.length,f=a[0].length,g=[],h=0,i=[];for(c=0;f>c;++c){for(b=0,d=0;e>b;b++)d+=a[b][c][1];d>h&&(h=d),g.push(d)}for(c=0;f>c;++c)i[c]=(h-g[c])/2;return i},wiggle:function(a){var b,c,d,e,f,g,h,i,j,k=a.length,l=a[0],m=l.length,n=[];for(n[0]=i=j=0,c=1;m>c;++c){for(b=0,e=0;k>b;++b)e+=a[b][c][1];for(b=0,f=0,h=l[c][0]-l[c-1][0];k>b;++b){for(d=0,g=(a[b][c][1]-a[b][c-1][1])/(2*h);b>d;++d)g+=(a[d][c][1]-a[d][c-1][1])/h;f+=g*a[b][c][1]}n[c]=i-=e?f/e*h:0,j>i&&(j=i)}for(c=0;m>c;++c)n[c]-=j;return n},expand:function(a){var b,c,d,e=a.length,f=a[0].length,g=1/e,h=[];for(c=0;f>c;++c){for(b=0,d=0;e>b;b++)d+=a[b][c][1];if(d)for(b=0;e>b;b++)a[b][c][1]/=d;else for(b=0;e>b;b++)a[b][c][1]=g}for(c=0;f>c;++c)h[c]=0;return h},zero:oe});hg.layout.histogram=function(){function a(a,f){for(var g,h,i=[],j=a.map(c,this),k=d.call(this,j,f),l=e.call(this,k,j,f),f=-1,m=j.length,n=l.length-1,o=b?1:1/m;++f<n;)g=i[f]=[],g.dx=l[f+1]-(g.x=l[f]),g.y=0;if(n>0)for(f=-1;++f<m;)h=j[f],h>=k[0]&&h<=k[1]&&(g=i[hg.bisect(l,h,1,n)-1],g.y+=o,g.push(a[f]));return i}var b=!0,c=Number,d=ue,e=se;return a.value=function(b){return arguments.length?(c=b,a):c},a.range=function(b){return arguments.length?(d=Aa(b),a):d},a.bins=function(b){return arguments.length?(e="number"==typeof b?function(a){return te(a,b)}:Aa(b),a):e},a.frequency=function(c){return arguments.length?(b=!!c,a):b},a},hg.layout.pack=function(){function a(a,f){var g=c.call(this,a,f),h=g[0],i=e[0],j=e[1],k=null==b?Math.sqrt:"function"==typeof b?b:function(){return b};if(h.x=h.y=0,fe(h,function(a){a.r=+k(a.value)}),fe(h,ze),d){var l=d*(b?1:Math.max(2*h.r/i,2*h.r/j))/2;fe(h,function(a){a.r+=l}),fe(h,ze),fe(h,function(a){a.r-=l})}return Ce(h,i/2,j/2,b?1:1/Math.max(2*h.r/i,2*h.r/j)),g}var b,c=hg.layout.hierarchy().sort(ve),d=0,e=[1,1];return a.size=function(b){return arguments.length?(e=b,a):e},a.radius=function(c){return arguments.length?(b=null==c||"function"==typeof c?c:+c,a):b},a.padding=function(b){return arguments.length?(d=+b,a):d},de(a,c)},hg.layout.tree=function(){function a(a,e){var k=g.call(this,a,e),l=k[0],m=b(l);if(fe(m,c),m.parent.m=-m.z,ee(m,d),j)ee(l,f);else{var n=l,o=l,p=l;ee(l,function(a){a.x<n.x&&(n=a),a.x>o.x&&(o=a),a.depth>p.depth&&(p=a)});var q=h(n,o)/2-n.x,r=i[0]/(o.x+h(o,n)/2+q),s=i[1]/(p.depth||1);ee(l,function(a){a.x=(a.x+q)*r,a.y=a.depth*s})}return k}function b(a){for(var b,c={A:null,children:[a]},d=[c];null!=(b=d.pop());)for(var e,f=b.children,g=0,h=f.length;h>g;++g)d.push((f[g]=e={_:f[g],parent:b,children:(e=f[g].children)&&e.slice()||[],A:null,a:null,z:0,m:0,c:0,s:0,t:null,i:g}).a=e);return c.children[0]}function c(a){var b=a.children,c=a.parent.children,d=a.i?c[a.i-1]:null;if(b.length){Ie(a);var f=(b[0].z+b[b.length-1].z)/2;d?(a.z=d.z+h(a._,d._),a.m=a.z-f):a.z=f}else d&&(a.z=d.z+h(a._,d._));a.parent.A=e(a,d,a.parent.A||c[0])}function d(a){a._.x=a.z+a.parent.m,a.m+=a.parent.m}function e(a,b,c){if(b){for(var d,e=a,f=a,g=b,i=e.parent.children[0],j=e.m,k=f.m,l=g.m,m=i.m;g=Ge(g),e=Fe(e),g&&e;)i=Fe(i),f=Ge(f),f.a=a,d=g.z+l-e.z-j+h(g._,e._),d>0&&(He(Je(g,a,c),a,d),j+=d,k+=d),l+=g.m,j+=e.m,m+=i.m,k+=f.m;g&&!Ge(f)&&(f.t=g,f.m+=l-k),e&&!Fe(i)&&(i.t=e,i.m+=j-m,c=a)}return c}function f(a){a.x*=i[0],a.y=a.depth*i[1]}var g=hg.layout.hierarchy().sort(null).value(null),h=Ee,i=[1,1],j=null;return a.separation=function(b){return arguments.length?(h=b,a):h},a.size=function(b){return arguments.length?(j=null==(i=b)?f:null,a):j?null:i},a.nodeSize=function(b){return arguments.length?(j=null==(i=b)?null:f,a):j?i:null},de(a,g)},hg.layout.cluster=function(){function a(a,f){var g,h=b.call(this,a,f),i=h[0],j=0;fe(i,function(a){var b=a.children;b&&b.length?(a.x=Le(b),a.y=Ke(b)):(a.x=g?j+=c(a,g):0,a.y=0,g=a)});var k=Me(i),l=Ne(i),m=k.x-c(k,l)/2,n=l.x+c(l,k)/2;return fe(i,e?function(a){a.x=(a.x-i.x)*d[0],a.y=(i.y-a.y)*d[1]}:function(a){a.x=(a.x-m)/(n-m)*d[0],a.y=(1-(i.y?a.y/i.y:1))*d[1]}),h}var b=hg.layout.hierarchy().sort(null).value(null),c=Ee,d=[1,1],e=!1;return a.separation=function(b){return arguments.length?(c=b,a):c},a.size=function(b){return arguments.length?(e=null==(d=b),a):e?null:d},a.nodeSize=function(b){return arguments.length?(e=null!=(d=b),a):e?d:null},de(a,b)},hg.layout.treemap=function(){function a(a,b){for(var c,d,e=-1,f=a.length;++e<f;)d=(c=a[e]).value*(0>b?0:b),c.area=isNaN(d)||0>=d?0:d}function b(c){var f=c.children;if(f&&f.length){var g,h,i,j=l(c),k=[],m=f.slice(),o=1/0,p="slice"===n?j.dx:"dice"===n?j.dy:"slice-dice"===n?1&c.depth?j.dy:j.dx:Math.min(j.dx,j.dy);for(a(m,j.dx*j.dy/c.value),k.area=0;(i=m.length)>0;)k.push(g=m[i-1]),k.area+=g.area,"squarify"!==n||(h=d(k,p))<=o?(m.pop(),o=h):(k.area-=k.pop().area,e(k,p,j,!1),p=Math.min(j.dx,j.dy),k.length=k.area=0,o=1/0);k.length&&(e(k,p,j,!0),k.length=k.area=0),f.forEach(b)}}function c(b){var d=b.children;if(d&&d.length){var f,g=l(b),h=d.slice(),i=[];for(a(h,g.dx*g.dy/b.value),i.area=0;f=h.pop();)i.push(f),i.area+=f.area,null!=f.z&&(e(i,f.z?g.dx:g.dy,g,!h.length),i.length=i.area=0);d.forEach(c)}}function d(a,b){for(var c,d=a.area,e=0,f=1/0,g=-1,h=a.length;++g<h;)(c=a[g].area)&&(f>c&&(f=c),c>e&&(e=c));return d*=d,b*=b,d?Math.max(b*e*o/d,d/(b*f*o)):1/0}function e(a,b,c,d){var e,f=-1,g=a.length,h=c.x,j=c.y,k=b?i(a.area/b):0;if(b==c.dx){for((d||k>c.dy)&&(k=c.dy);++f<g;)e=a[f],e.x=h,e.y=j,e.dy=k,h+=e.dx=Math.min(c.x+c.dx-h,k?i(e.area/k):0);e.z=!0,e.dx+=c.x+c.dx-h,c.y+=k,c.dy-=k}else{for((d||k>c.dx)&&(k=c.dx);++f<g;)e=a[f],e.x=h,e.y=j,e.dx=k,j+=e.dy=Math.min(c.y+c.dy-j,k?i(e.area/k):0);e.z=!1,e.dy+=c.y+c.dy-j,c.x+=k,c.dx-=k}}function f(d){var e=g||h(d),f=e[0];return f.x=f.y=0,f.value?(f.dx=j[0],f.dy=j[1]):f.dx=f.dy=0,g&&h.revalue(f),a([f],f.dx*f.dy/f.value),(g?c:b)(f),m&&(g=e),e}var g,h=hg.layout.hierarchy(),i=Math.round,j=[1,1],k=null,l=Oe,m=!1,n="squarify",o=.5*(1+Math.sqrt(5));return f.size=function(a){return arguments.length?(j=a,f):j},f.padding=function(a){function b(b){var c=a.call(f,b,b.depth);return null==c?Oe(b):Pe(b,"number"==typeof c?[c,c,c,c]:c)}function c(b){return Pe(b,a)}if(!arguments.length)return k;var d;return l=null==(k=a)?Oe:"function"==(d=typeof a)?b:"number"===d?(a=[a,a,a,a],c):c,f},f.round=function(a){return arguments.length?(i=a?Math.round:Number,f):i!=Number},f.sticky=function(a){return arguments.length?(m=a,g=null,f):m},f.ratio=function(a){return arguments.length?(o=a,f):o},f.mode=function(a){return arguments.length?(n=a+"",f):n},de(f,h)},hg.random={normal:function(a,b){var c=arguments.length;return 2>c&&(b=1),1>c&&(a=0),function(){var c,d,e;do c=2*Math.random()-1,d=2*Math.random()-1,e=c*c+d*d;while(!e||e>1);return a+b*c*Math.sqrt(-2*Math.log(e)/e)}},logNormal:function(){var a=hg.random.normal.apply(hg,arguments);return function(){return Math.exp(a())}},bates:function(a){var b=hg.random.irwinHall(a);return function(){return b()/a}},irwinHall:function(a){return function(){for(var b=0,c=0;a>c;c++)b+=Math.random();return b}}},hg.scale={};var wi={floor:s,ceil:s};hg.scale.linear=function(){return We([0,1],[0,1],sd,!1)};var xi={s:1,g:1,p:1,r:1,e:1};hg.scale.log=function(){return cf(hg.scale.linear().domain([0,1]),10,!0,[1,10])};var yi=hg.format(".0e"),zi={floor:function(a){return-Math.ceil(-a)},ceil:function(a){return-Math.floor(-a)}};hg.scale.pow=function(){return df(hg.scale.linear(),1,[0,1])},hg.scale.sqrt=function(){return hg.scale.pow().exponent(.5)},hg.scale.ordinal=function(){return ff([],{t:"range",a:[[]]})},hg.scale.category10=function(){return hg.scale.ordinal().range(Ai)},hg.scale.category20=function(){return hg.scale.ordinal().range(Bi)},hg.scale.category20b=function(){return hg.scale.ordinal().range(Ci)},hg.scale.category20c=function(){return hg.scale.ordinal().range(Di)};var Ai=[2062260,16744206,2924588,14034728,9725885,9197131,14907330,8355711,12369186,1556175].map(ta),Bi=[2062260,11454440,16744206,16759672,2924588,10018698,14034728,16750742,9725885,12955861,9197131,12885140,14907330,16234194,8355711,13092807,12369186,14408589,1556175,10410725].map(ta),Ci=[3750777,5395619,7040719,10264286,6519097,9216594,11915115,13556636,9202993,12426809,15186514,15190932,8666169,11356490,14049643,15177372,8077683,10834324,13528509,14589654].map(ta),Di=[3244733,7057110,10406625,13032431,15095053,16616764,16625259,16634018,3253076,7652470,10607003,13101504,7695281,10394312,12369372,14342891,6513507,9868950,12434877,14277081].map(ta);hg.scale.quantile=function(){return gf([],[])},hg.scale.quantize=function(){return hf(0,1,[0,1])},hg.scale.threshold=function(){return jf([.5],[0,1])},hg.scale.identity=function(){return kf([0,1])},hg.svg={},hg.svg.arc=function(){function a(){var a=Math.max(0,+c.apply(this,arguments)),j=Math.max(0,+d.apply(this,arguments)),k=g.apply(this,arguments)-Og,l=h.apply(this,arguments)-Og,m=Math.abs(l-k),n=k>l?0:1;if(a>j&&(o=j,j=a,a=o),m>=Ng)return b(j,n)+(a?b(a,1-n):"")+"Z";var o,p,q,r,s,t,u,v,w,x,y,z,A=0,B=0,C=[];if((r=(+i.apply(this,arguments)||0)/2)&&(q=f===Ei?Math.sqrt(a*a+j*j):+f.apply(this,arguments),n||(B*=-1),j&&(B=ba(q/j*Math.sin(r))),a&&(A=ba(q/a*Math.sin(r)))),j){s=j*Math.cos(k+B),t=j*Math.sin(k+B),u=j*Math.cos(l-B),v=j*Math.sin(l-B);var D=Math.abs(l-k-2*B)<=Lg?0:1;if(B&&rf(s,t,u,v)===n^D){var E=(k+l)/2;s=j*Math.cos(E),t=j*Math.sin(E),u=v=null}}else s=t=0;if(a){w=a*Math.cos(l-A),x=a*Math.sin(l-A),y=a*Math.cos(k+A),z=a*Math.sin(k+A);var F=Math.abs(k-l+2*A)<=Lg?0:1;if(A&&rf(w,x,y,z)===1-n^F){var G=(k+l)/2;w=a*Math.cos(G),x=a*Math.sin(G),y=z=null}}else w=x=0;if(m>Jg&&(o=Math.min(Math.abs(j-a)/2,+e.apply(this,arguments)))>.001){p=j>a^n?0:1;var H=o,I=o;if(Lg>m){var J=null==y?[w,x]:null==u?[s,t]:Hc([s,t],[y,z],[u,v],[w,x]),K=s-J[0],L=t-J[1],M=u-J[0],N=v-J[1],O=1/Math.sin(Math.acos((K*M+L*N)/(Math.sqrt(K*K+L*L)*Math.sqrt(M*M+N*N)))/2),P=Math.sqrt(J[0]*J[0]+J[1]*J[1]);I=Math.min(o,(a-P)/(O-1)),H=Math.min(o,(j-P)/(O+1))}if(null!=u){var Q=sf(null==y?[w,x]:[y,z],[s,t],j,H,n),R=sf([u,v],[w,x],j,H,n);o===H?C.push("M",Q[0],"A",H,",",H," 0 0,",p," ",Q[1],"A",j,",",j," 0 ",1-n^rf(Q[1][0],Q[1][1],R[1][0],R[1][1]),",",n," ",R[1],"A",H,",",H," 0 0,",p," ",R[0]):C.push("M",Q[0],"A",H,",",H," 0 1,",p," ",R[0])}else C.push("M",s,",",t);if(null!=y){var S=sf([s,t],[y,z],a,-I,n),T=sf([w,x],null==u?[s,t]:[u,v],a,-I,n);o===I?C.push("L",T[0],"A",I,",",I," 0 0,",p," ",T[1],"A",a,",",a," 0 ",n^rf(T[1][0],T[1][1],S[1][0],S[1][1]),",",1-n," ",S[1],"A",I,",",I," 0 0,",p," ",S[0]):C.push("L",T[0],"A",I,",",I," 0 0,",p," ",S[0])}else C.push("L",w,",",x)}else C.push("M",s,",",t),null!=u&&C.push("A",j,",",j," 0 ",D,",",n," ",u,",",v),C.push("L",w,",",x),null!=y&&C.push("A",a,",",a," 0 ",F,",",1-n," ",y,",",z);return C.push("Z"),C.join("")}function b(a,b){return"M0,"+a+"A"+a+","+a+" 0 1,"+b+" 0,"+-a+"A"+a+","+a+" 0 1,"+b+" 0,"+a}var c=mf,d=nf,e=lf,f=Ei,g=of,h=pf,i=qf;return a.innerRadius=function(b){return arguments.length?(c=Aa(b),a):c},a.outerRadius=function(b){return arguments.length?(d=Aa(b),a):d},a.cornerRadius=function(b){return arguments.length?(e=Aa(b),a):e},a.padRadius=function(b){return arguments.length?(f=b==Ei?Ei:Aa(b),a):f},a.startAngle=function(b){return arguments.length?(g=Aa(b),a):g},a.endAngle=function(b){return arguments.length?(h=Aa(b),a):h},a.padAngle=function(b){return arguments.length?(i=Aa(b),a):i},a.centroid=function(){var a=(+c.apply(this,arguments)+ +d.apply(this,arguments))/2,b=(+g.apply(this,arguments)+ +h.apply(this,arguments))/2-Og;return[Math.cos(b)*a,Math.sin(b)*a]},a};var Ei="auto";hg.svg.line=function(){return tf(s)};var Fi=hg.map({linear:uf,"linear-closed":vf,step:wf,"step-before":xf,"step-after":yf,basis:Ef,"basis-open":Ff,"basis-closed":Gf,bundle:Hf,cardinal:Bf,"cardinal-open":zf,"cardinal-closed":Af,monotone:Nf});Fi.forEach(function(a,b){b.key=a,b.closed=/-closed$/.test(a)});var Gi=[0,2/3,1/3,0],Hi=[0,1/3,2/3,0],Ii=[0,1/6,2/3,1/6];hg.svg.line.radial=function(){var a=tf(Of);return a.radius=a.x,delete a.x,a.angle=a.y,delete a.y,a},xf.reverse=yf,yf.reverse=xf,hg.svg.area=function(){return Pf(s)},hg.svg.area.radial=function(){var a=Pf(Of);return a.radius=a.x,delete a.x,a.innerRadius=a.x0,delete a.x0,a.outerRadius=a.x1,delete a.x1,a.angle=a.y,delete a.y,a.startAngle=a.y0,delete a.y0,a.endAngle=a.y1,delete a.y1,a},hg.svg.chord=function(){function a(a,h){var i=b(this,f,a,h),j=b(this,g,a,h);return"M"+i.p0+d(i.r,i.p1,i.a1-i.a0)+(c(i,j)?e(i.r,i.p1,i.r,i.p0):e(i.r,i.p1,j.r,j.p0)+d(j.r,j.p1,j.a1-j.a0)+e(j.r,j.p1,i.r,i.p0))+"Z"}function b(a,b,c,d){var e=b.call(a,c,d),f=h.call(a,e,d),g=i.call(a,e,d)-Og,k=j.call(a,e,d)-Og;return{r:f,a0:g,a1:k,p0:[f*Math.cos(g),f*Math.sin(g)],p1:[f*Math.cos(k),f*Math.sin(k)]}}function c(a,b){return a.a0==b.a0&&a.a1==b.a1}function d(a,b,c){return"A"+a+","+a+" 0 "+ +(c>Lg)+",1 "+b}function e(a,b,c,d){return"Q 0,0 "+d}var f=sc,g=tc,h=Qf,i=of,j=pf;return a.radius=function(b){return arguments.length?(h=Aa(b),a):h},a.source=function(b){return arguments.length?(f=Aa(b),a):f},a.target=function(b){return arguments.length?(g=Aa(b),a):g},a.startAngle=function(b){return arguments.length?(i=Aa(b),a):i},a.endAngle=function(b){return arguments.length?(j=Aa(b),a):j},a},hg.svg.diagonal=function(){function a(a,e){var f=b.call(this,a,e),g=c.call(this,a,e),h=(f.y+g.y)/2,i=[f,{x:f.x,y:h},{x:g.x,y:h},g];return i=i.map(d),"M"+i[0]+"C"+i[1]+" "+i[2]+" "+i[3]}var b=sc,c=tc,d=Rf;return a.source=function(c){return arguments.length?(b=Aa(c),a):b},a.target=function(b){return arguments.length?(c=Aa(b),a):c},a.projection=function(b){return arguments.length?(d=b,a):d},a},hg.svg.diagonal.radial=function(){var a=hg.svg.diagonal(),b=Rf,c=a.projection;return a.projection=function(a){return arguments.length?c(Sf(b=a)):b},a},hg.svg.symbol=function(){function a(a,d){return(Ji.get(b.call(this,a,d))||Vf)(c.call(this,a,d))}var b=Uf,c=Tf;return a.type=function(c){return arguments.length?(b=Aa(c),a):b},a.size=function(b){return arguments.length?(c=Aa(b),a):c},a};var Ji=hg.map({circle:Vf,cross:function(a){var b=Math.sqrt(a/5)/2;return"M"+-3*b+","+-b+"H"+-b+"V"+-3*b+"H"+b+"V"+-b+"H"+3*b+"V"+b+"H"+b+"V"+3*b+"H"+-b+"V"+b+"H"+-3*b+"Z"},diamond:function(a){var b=Math.sqrt(a/(2*Li)),c=b*Li;return"M0,"+-b+"L"+c+",0 0,"+b+" "+-c+",0Z"},square:function(a){var b=Math.sqrt(a)/2;return"M"+-b+","+-b+"L"+b+","+-b+" "+b+","+b+" "+-b+","+b+"Z"},"triangle-down":function(a){var b=Math.sqrt(a/Ki),c=b*Ki/2;return"M0,"+c+"L"+b+","+-c+" "+-b+","+-c+"Z"},"triangle-up":function(a){var b=Math.sqrt(a/Ki),c=b*Ki/2;return"M0,"+-c+"L"+b+","+c+" "+-b+","+c+"Z"}});hg.svg.symbolTypes=Ji.keys();var Ki=Math.sqrt(3),Li=Math.tan(30*Pg);Cg.transition=function(a){for(var b,c,d=Mi||++Qi,e=$f(a),f=[],g=Ni||{time:Date.now(),ease:zd,delay:0,duration:250},h=-1,i=this.length;++h<i;){f.push(b=[]);for(var j=this[h],k=-1,l=j.length;++k<l;)(c=j[k])&&_f(c,k,e,d,g),b.push(c)}return Xf(f,e,d)},Cg.interrupt=function(a){return this.each(null==a?Oi:Wf($f(a)))};var Mi,Ni,Oi=Wf($f()),Pi=[],Qi=0;Pi.call=Cg.call,Pi.empty=Cg.empty,Pi.node=Cg.node,Pi.size=Cg.size,hg.transition=function(a,b){return a&&a.transition?Mi?a.transition(b):a:hg.selection().transition(a)},hg.transition.prototype=Pi,Pi.select=function(a){var b,c,d,e=this.id,f=this.namespace,g=[];a=C(a);for(var h=-1,i=this.length;++h<i;){g.push(b=[]);for(var j=this[h],k=-1,l=j.length;++k<l;)(d=j[k])&&(c=a.call(d,d.__data__,k,h))?("__data__"in d&&(c.__data__=d.__data__),_f(c,k,f,e,d[f][e]),b.push(c)):b.push(null)}return Xf(g,f,e)},Pi.selectAll=function(a){var b,c,d,e,f,g=this.id,h=this.namespace,i=[];a=D(a);for(var j=-1,k=this.length;++j<k;)for(var l=this[j],m=-1,n=l.length;++m<n;)if(d=l[m]){f=d[h][g],c=a.call(d,d.__data__,m,j),i.push(b=[]);for(var o=-1,p=c.length;++o<p;)(e=c[o])&&_f(e,o,h,g,f),b.push(e)}return Xf(i,h,g)},Pi.filter=function(a){var b,c,d,e=[];"function"!=typeof a&&(a=P(a));for(var f=0,g=this.length;g>f;f++){e.push(b=[]);for(var c=this[f],h=0,i=c.length;i>h;h++)(d=c[h])&&a.call(d,d.__data__,h,f)&&b.push(d)}return Xf(e,this.namespace,this.id)},Pi.tween=function(a,b){var c=this.id,d=this.namespace;return arguments.length<2?this.node()[d][c].tween.get(a):R(this,null==b?function(b){b[d][c].tween.remove(a)}:function(e){e[d][c].tween.set(a,b)})},Pi.attr=function(a,b){function c(){this.removeAttribute(h)}function d(){this.removeAttributeNS(h.space,h.local)}function e(a){return null==a?c:(a+="",function(){var b,c=this.getAttribute(h);return c!==a&&(b=g(c,a),function(a){this.setAttribute(h,b(a))})})}function f(a){return null==a?d:(a+="",function(){var b,c=this.getAttributeNS(h.space,h.local);return c!==a&&(b=g(c,a),function(a){this.setAttributeNS(h.space,h.local,b(a))})})}if(arguments.length<2){for(b in a)this.attr(b,a[b]);return this}var g="transform"==a?Ud:sd,h=hg.ns.qualify(a);return Yf(this,"attr."+a,b,h.local?f:e)},Pi.attrTween=function(a,b){function c(a,c){var d=b.call(this,a,c,this.getAttribute(e));return d&&function(a){this.setAttribute(e,d(a))}}function d(a,c){var d=b.call(this,a,c,this.getAttributeNS(e.space,e.local));return d&&function(a){this.setAttributeNS(e.space,e.local,d(a))}}var e=hg.ns.qualify(a);return this.tween("attr."+a,e.local?d:c)},Pi.style=function(a,c,d){function e(){this.style.removeProperty(a)}function f(c){return null==c?e:(c+="",function(){var e,f=b(this).getComputedStyle(this,null).getPropertyValue(a);return f!==c&&(e=sd(f,c),function(b){this.style.setProperty(a,e(b),d)})})}var g=arguments.length;if(3>g){if("string"!=typeof a){2>g&&(c="");for(d in a)this.style(d,a[d],c);return this}d=""}return Yf(this,"style."+a,c,f)},Pi.styleTween=function(a,c,d){function e(e,f){var g=c.call(this,e,f,b(this).getComputedStyle(this,null).getPropertyValue(a));return g&&function(b){this.style.setProperty(a,g(b),d)}}return arguments.length<3&&(d=""),this.tween("style."+a,e)},Pi.text=function(a){return Yf(this,"text",a,Zf)},Pi.remove=function(){var a=this.namespace;return this.each("end.transition",function(){var b;this[a].count<2&&(b=this.parentNode)&&b.removeChild(this)})},Pi.ease=function(a){var b=this.id,c=this.namespace;return arguments.length<1?this.node()[c][b].ease:("function"!=typeof a&&(a=hg.ease.apply(hg,arguments)),R(this,function(d){d[c][b].ease=a}))},Pi.delay=function(a){var b=this.id,c=this.namespace;return arguments.length<1?this.node()[c][b].delay:R(this,"function"==typeof a?function(d,e,f){d[c][b].delay=+a.call(d,d.__data__,e,f)}:(a=+a,function(d){d[c][b].delay=a}))},Pi.duration=function(a){var b=this.id,c=this.namespace;return arguments.length<1?this.node()[c][b].duration:R(this,"function"==typeof a?function(d,e,f){d[c][b].duration=Math.max(1,a.call(d,d.__data__,e,f))}:(a=Math.max(1,a),function(d){d[c][b].duration=a}))},Pi.each=function(a,b){var c=this.id,d=this.namespace;if(arguments.length<2){var e=Ni,f=Mi;try{Mi=c,R(this,function(b,e,f){Ni=b[d][c],a.call(b,b.__data__,e,f)})}finally{Ni=e,Mi=f}}else R(this,function(e){var f=e[d][c];(f.event||(f.event=hg.dispatch("start","end","interrupt"))).on(a,b)});return this},Pi.transition=function(){for(var a,b,c,d,e=this.id,f=++Qi,g=this.namespace,h=[],i=0,j=this.length;j>i;i++){h.push(a=[]);for(var b=this[i],k=0,l=b.length;l>k;k++)(c=b[k])&&(d=c[g][e],_f(c,k,g,f,{time:d.time,ease:d.ease,delay:d.delay+d.duration,duration:d.duration})),a.push(c)}return Xf(h,g,f)},hg.svg.axis=function(){function a(a){a.each(function(){var a,j=hg.select(this),k=this.__chart__||c,l=this.__chart__=c.copy(),m=null==i?l.ticks?l.ticks.apply(l,h):l.domain():i,n=null==b?l.tickFormat?l.tickFormat.apply(l,h):s:b,o=j.selectAll(".tick").data(m,l),p=o.enter().insert("g",".domain").attr("class","tick").style("opacity",Jg),q=hg.transition(o.exit()).style("opacity",Jg).remove(),r=hg.transition(o.order()).style("opacity",1),t=Math.max(e,0)+g,u=Re(l),v=j.selectAll(".domain").data([0]),w=(v.enter().append("path").attr("class","domain"),hg.transition(v));p.append("line"),p.append("text");var x,y,z,A,B=p.select("line"),C=r.select("line"),D=o.select("text").text(n),E=p.select("text"),F=r.select("text"),G="top"===d||"left"===d?-1:1;if("bottom"===d||"top"===d?(a=ag,x="x",z="y",y="x2",A="y2",D.attr("dy",0>G?"0em":".71em").style("text-anchor","middle"),w.attr("d","M"+u[0]+","+G*f+"V0H"+u[1]+"V"+G*f)):(a=bg,x="y",z="x",y="y2",A="x2",D.attr("dy",".32em").style("text-anchor",0>G?"end":"start"),w.attr("d","M"+G*f+","+u[0]+"H0V"+u[1]+"H"+G*f)),B.attr(A,G*e),E.attr(z,G*t),C.attr(y,0).attr(A,G*e),F.attr(x,0).attr(z,G*t),l.rangeBand){var H=l,I=H.rangeBand()/2;k=l=function(a){return H(a)+I}}else k.rangeBand?k=l:q.call(a,l,k);p.call(a,k,l),r.call(a,l,l)})}var b,c=hg.scale.linear(),d=Ri,e=6,f=6,g=3,h=[10],i=null;return a.scale=function(b){return arguments.length?(c=b,a):c},a.orient=function(b){return arguments.length?(d=b in Si?b+"":Ri,a):d},a.ticks=function(){return arguments.length?(h=jg(arguments),a):h},a.tickValues=function(b){return arguments.length?(i=b,a):i},a.tickFormat=function(c){return arguments.length?(b=c,a):b},a.tickSize=function(b){var c=arguments.length;return c?(e=+b,f=+arguments[c-1],a):e},a.innerTickSize=function(b){return arguments.length?(e=+b,a):e},a.outerTickSize=function(b){return arguments.length?(f=+b,a):f},a.tickPadding=function(b){return arguments.length?(g=+b,a):g},a.tickSubdivide=function(){return arguments.length&&a},a};var Ri="bottom",Si={top:1,right:1,bottom:1,left:1};hg.svg.brush=function(){function a(b){b.each(function(){var b=hg.select(this).style("pointer-events","all").style("-webkit-tap-highlight-color","rgba(0,0,0,0)").on("mousedown.brush",f).on("touchstart.brush",f),g=b.selectAll(".background").data([0]);g.enter().append("rect").attr("class","background").style("visibility","hidden").style("cursor","crosshair"),b.selectAll(".extent").data([0]).enter().append("rect").attr("class","extent").style("cursor","move");var h=b.selectAll(".resize").data(p,s);h.exit().remove(),h.enter().append("g").attr("class",function(a){return"resize "+a}).style("cursor",function(a){return Ti[a]}).append("rect").attr("x",function(a){return/[ew]$/.test(a)?-3:null}).attr("y",function(a){return/^[ns]/.test(a)?-3:null}).attr("width",6).attr("height",6).style("visibility","hidden"),h.style("display",a.empty()?"none":null);var i,l=hg.transition(b),m=hg.transition(g);j&&(i=Re(j),m.attr("x",i[0]).attr("width",i[1]-i[0]),d(l)),k&&(i=Re(k),m.attr("y",i[0]).attr("height",i[1]-i[0]),e(l)),c(l)})}function c(a){a.selectAll(".resize").attr("transform",function(a){return"translate("+l[+/e$/.test(a)]+","+m[+/^s/.test(a)]+")"})}function d(a){a.select(".extent").attr("x",l[0]),a.selectAll(".extent,.n>rect,.s>rect").attr("width",l[1]-l[0])}function e(a){a.select(".extent").attr("y",m[0]),a.selectAll(".extent,.e>rect,.w>rect").attr("height",m[1]-m[0])}function f(){function f(){32==hg.event.keyCode&&(D||(t=null,F[0]-=l[1],F[1]-=m[1],D=2),y())}function p(){32==hg.event.keyCode&&2==D&&(F[0]+=l[1],F[1]+=m[1],D=0,y())}function q(){var a=hg.mouse(v),b=!1;u&&(a[0]+=u[0],a[1]+=u[1]),D||(hg.event.altKey?(t||(t=[(l[0]+l[1])/2,(m[0]+m[1])/2]),F[0]=l[+(a[0]<t[0])],F[1]=m[+(a[1]<t[1])]):t=null),B&&r(a,j,0)&&(d(z),b=!0),C&&r(a,k,1)&&(e(z),b=!0),b&&(c(z),x({type:"brush",mode:D?"move":"resize"}))}function r(a,b,c){var d,e,f=Re(b),i=f[0],j=f[1],k=F[c],p=c?m:l,q=p[1]-p[0];return D&&(i-=k,j-=q+k),d=(c?o:n)?Math.max(i,Math.min(j,a[c])):a[c],D?e=(d+=k)+q:(t&&(k=Math.max(i,Math.min(j,2*t[c]-d))),d>k?(e=d,d=k):e=k),p[0]!=d||p[1]!=e?(c?h=null:g=null,p[0]=d,p[1]=e,!0):void 0}function s(){q(),z.style("pointer-events","all").selectAll(".resize").style("display",a.empty()?"none":null),hg.select("body").style("cursor",null),G.on("mousemove.brush",null).on("mouseup.brush",null).on("touchmove.brush",null).on("touchend.brush",null).on("keydown.brush",null).on("keyup.brush",null),E(),x({type:"brushend"})}var t,u,v=this,w=hg.select(hg.event.target),x=i.of(v,arguments),z=hg.select(v),A=w.datum(),B=!/^(n|s)$/.test(A)&&j,C=!/^(e|w)$/.test(A)&&k,D=w.classed("extent"),E=X(v),F=hg.mouse(v),G=hg.select(b(v)).on("keydown.brush",f).on("keyup.brush",p);if(hg.event.changedTouches?G.on("touchmove.brush",q).on("touchend.brush",s):G.on("mousemove.brush",q).on("mouseup.brush",s),z.interrupt().selectAll("*").interrupt(),D)F[0]=l[0]-F[0],F[1]=m[0]-F[1];else if(A){var H=+/w$/.test(A),I=+/^n/.test(A);u=[l[1-H]-F[0],m[1-I]-F[1]],F[0]=l[H],F[1]=m[I]}else hg.event.altKey&&(t=F.slice());z.style("pointer-events","none").selectAll(".resize").style("display",null),hg.select("body").style("cursor",w.style("cursor")),x({type:"brushstart"}),q()}var g,h,i=A(a,"brushstart","brush","brushend"),j=null,k=null,l=[0,0],m=[0,0],n=!0,o=!0,p=Ui[0];return a.event=function(a){a.each(function(){var a=i.of(this,arguments),b={x:l,y:m,i:g,j:h},c=this.__chart__||b;this.__chart__=b,Mi?hg.select(this).transition().each("start.brush",function(){g=c.i,h=c.j,l=c.x,m=c.y,a({type:"brushstart"})}).tween("brush:brush",function(){var c=td(l,b.x),d=td(m,b.y);return g=h=null,function(e){l=b.x=c(e),m=b.y=d(e),a({type:"brush",mode:"resize"})}}).each("end.brush",function(){g=b.i,h=b.j,a({type:"brush",mode:"resize"}),a({type:"brushend"})}):(a({type:"brushstart"}),a({type:"brush",mode:"resize"}),a({type:"brushend"}))})},a.x=function(b){return arguments.length?(j=b,p=Ui[!j<<1|!k],a):j},a.y=function(b){return arguments.length?(k=b,p=Ui[!j<<1|!k],a):k},a.clamp=function(b){return arguments.length?(j&&k?(n=!!b[0],o=!!b[1]):j?n=!!b:k&&(o=!!b),a):j&&k?[n,o]:j?n:k?o:null},a.extent=function(b){var c,d,e,f,i;return arguments.length?(j&&(c=b[0],d=b[1],k&&(c=c[0],d=d[0]),g=[c,d],j.invert&&(c=j(c),d=j(d)),c>d&&(i=c,c=d,d=i),(c!=l[0]||d!=l[1])&&(l=[c,d])),k&&(e=b[0],f=b[1],j&&(e=e[1],f=f[1]),h=[e,f],k.invert&&(e=k(e),f=k(f)),e>f&&(i=e,e=f,f=i),(e!=m[0]||f!=m[1])&&(m=[e,f])),a):(j&&(g?(c=g[0],d=g[1]):(c=l[0],d=l[1],j.invert&&(c=j.invert(c),d=j.invert(d)),c>d&&(i=c,c=d,d=i))),k&&(h?(e=h[0],f=h[1]):(e=m[0],f=m[1],k.invert&&(e=k.invert(e),f=k.invert(f)),e>f&&(i=e,e=f,f=i))),j&&k?[[c,e],[d,f]]:j?[c,d]:k&&[e,f])},a.clear=function(){return a.empty()||(l=[0,0],m=[0,0],g=h=null),a},a.empty=function(){return!!j&&l[0]==l[1]||!!k&&m[0]==m[1]},hg.rebind(a,i,"on")};var Ti={n:"ns-resize",e:"ew-resize",s:"ns-resize",w:"ew-resize",nw:"nwse-resize",ne:"nesw-resize",se:"nwse-resize",sw:"nesw-resize"},Ui=[["n","e","s","w","nw","ne","se","sw"],["e","w"],["n","s"],[]],Vi=mh.format=sh.timeFormat,Wi=Vi.utc,Xi=Wi("%Y-%m-%dT%H:%M:%S.%LZ");Vi.iso=Date.prototype.toISOString&&+new Date("2000-01-01T00:00:00.000Z")?cg:Xi,cg.parse=function(a){var b=new Date(a);return isNaN(b)?null:b},cg.toString=Xi.toString,mh.second=Oa(function(a){return new nh(1e3*Math.floor(a/1e3))},function(a,b){a.setTime(a.getTime()+1e3*Math.floor(b))},function(a){return a.getSeconds()}),mh.seconds=mh.second.range,mh.seconds.utc=mh.second.utc.range,mh.minute=Oa(function(a){return new nh(6e4*Math.floor(a/6e4))},function(a,b){a.setTime(a.getTime()+6e4*Math.floor(b))},function(a){return a.getMinutes()}),mh.minutes=mh.minute.range,mh.minutes.utc=mh.minute.utc.range,mh.hour=Oa(function(a){var b=a.getTimezoneOffset()/60;return new nh(36e5*(Math.floor(a/36e5-b)+b))},function(a,b){a.setTime(a.getTime()+36e5*Math.floor(b))},function(a){return a.getHours()}),mh.hours=mh.hour.range,mh.hours.utc=mh.hour.utc.range,mh.month=Oa(function(a){return a=mh.day(a),a.setDate(1),a},function(a,b){a.setMonth(a.getMonth()+b)},function(a){return a.getMonth()}),mh.months=mh.month.range,mh.months.utc=mh.month.utc.range;var Yi=[1e3,5e3,15e3,3e4,6e4,3e5,9e5,18e5,36e5,108e5,216e5,432e5,864e5,1728e5,6048e5,2592e6,7776e6,31536e6],Zi=[[mh.second,1],[mh.second,5],[mh.second,15],[mh.second,30],[mh.minute,1],[mh.minute,5],[mh.minute,15],[mh.minute,30],[mh.hour,1],[mh.hour,3],[mh.hour,6],[mh.hour,12],[mh.day,1],[mh.day,2],[mh.week,1],[mh.month,1],[mh.month,3],[mh.year,1]],$i=Vi.multi([[".%L",function(a){return a.getMilliseconds()}],[":%S",function(a){return a.getSeconds()}],["%I:%M",function(a){return a.getMinutes()}],["%I %p",function(a){return a.getHours()}],["%a %d",function(a){return a.getDay()&&1!=a.getDate()}],["%b %d",function(a){return 1!=a.getDate()}],["%B",function(a){return a.getMonth()}],["%Y",Db]]),_i={range:function(a,b,c){return hg.range(Math.ceil(a/c)*c,+b,c).map(eg)},floor:s,ceil:s};Zi.year=mh.year,mh.scale=function(){return dg(hg.scale.linear(),Zi,$i)};var aj=Zi.map(function(a){return[a[0].utc,a[1]]}),bj=Wi.multi([[".%L",function(a){return a.getUTCMilliseconds()}],[":%S",function(a){return a.getUTCSeconds()}],["%I:%M",function(a){return a.getUTCMinutes()}],["%I %p",function(a){return a.getUTCHours()}],["%a %d",function(a){return a.getUTCDay()&&1!=a.getUTCDate()}],["%b %d",function(a){return 1!=a.getUTCDate()}],["%B",function(a){return a.getUTCMonth()}],["%Y",Db]]);aj.year=mh.year.utc,mh.scale.utc=function(){return dg(hg.scale.linear(),aj,bj)},hg.text=Ba(function(a){return a.responseText}),hg.json=function(a,b){return Ca(a,"application/json",fg,b)},hg.html=function(a,b){return Ca(a,"text/html",gg,b)},hg.xml=Ba(function(a){return a.responseXML}),"function"==typeof define&&define.amd?(this.d3=hg,define(hg)):"object"==typeof module&&module.exports?module.exports=hg:this.d3=hg}(),+function(a){"use strict";function b(){a(".navbar-menu-burger-content .menu-links > div").not(":has(a)").not("div[id='menu-tt-rub']").click(function(b){var c=a(this).attr("data-toggle-id");a(this).parent().animate({"margin-left":"-320px"},"slow"),a("#"+c).show().animate({"margin-left":"0px"},"slow")}),a(".navbar-menu-burger-content .menu-sub-links .title-level-1").click(function(b){var c=a(this).parent().parent().attr("id");a("[data-toggle-id='"+c+"']").parent().animate({"margin-left":"0px"},"slow"),a("#"+c).animate({"margin-left":"280px"},"slow",function(){a("#"+c).hide()})}),a(".navbar-menu-burger-content .menu-sub-links .submenu-lvl2").click(function(b){var c=a(this).attr("data-toggle-id");a(this).parent().animate({"margin-left":"-320px"},"slow"),a("#"+c).show().animate({"margin-left":"0px"},"slow")}),a('.navbar-menu-burger-content a[href="'+location.pathname+'"]').css("font-weight","bold")}function c(){a("#menuMonCompte a[aria-expanded=true]").each(function(b){a(this).trigger("click")}),a("#menuMonCompte > ul").css("display","none"),a("body").trigger("click"),a("#dropdownMonCpte").css("background-color","transparent")}function d(b){b.preventDefault();var c={"margin-left":"-=290px","margin-right":"+=290px"};if(a(window).width()<641)var d={"margin-left":"-=275px","margin-right":"+=275px"};else if(a(window).width()<961)var d={"margin-left":"-=280px","margin-right":"+=275px"};else var d={"margin-left":"-=285px","margin-right":"+=280px"};var f={"margin-left":"auto","margin-right":"auto"};a("#theme-header .navbar-default .container:first-child").animate(d,"slow",function(){a(this).css(f)}),a("#page-content .container:first-child").animate(c,"slow",function(){a(this).css(f)}),a("#theme-footer .container:first-child").animate(c,"slow",function(){a(this).css(f)}),a(".container.footer-partenaires-content").animate(c,"slow",function(){a(this).css(f);
}),a("#sticky-camille").animate(c,"slow",function(){a(this).css(f)}),a("#theme-header .picto-burger").removeClass("active"),a(".navbar-menu-burger-content").animate({"margin-left":"-320px"},"slow",function(){if(a('div[id^="back-home-burger"]').hide(),a(".navbar-menu-burger-content").hide(),a("#theme-header .navbar-titles-header-mob").removeAttr("style"),a(".dropdown-menu").is(":visible")||a("#theme-header .navbar-default").removeAttr("style"),a(".navbar-ma-caf-header").removeAttr("style"),a("#page-content").css("margin-top","20px"),a("body").css("overflow","auto"),a(".content-submenus-lvl2").css("margin-left","280px").hide(),a(".content-sub-menus").css("margin-left","280px").hide(),a(".menu-sub-links").css("margin-left","auto"),a(".menu-links").css("margin-left","auto"),a(document).scrollTop()<=10);e(),g()})}function e(){var b=!1,c=window.location.pathname,d=c.split("/");if(c="",d[1].length>0&&"undefined"!=typeof d[2])if("undefined"!=typeof d[2]&&"vies-de-famille"==d[2])a(".navbar-menu-burger-content .menu-links .menu-item").each(function(){"Viesdefamille"==a(this).text().replace(/\s/g,"")&&a(this).parent().click()});else{for(var e=0;e<5;e++)"undefined"!=typeof d[e]&&(c+=d[e]+"/");if(c=c.substr(0,c.length-1),b=f(c),!b){c="";for(var e=0;e<6;e++)"undefined"!=typeof d[e]&&(c+=d[e]+"/");c=c.substr(0,c.length-1),b=f(c)}}}function f(b){var c=!1;return b.match("^/allocataires/actualites")&&(c=!0,a("[data-toggle-id='sub-menu-content-0").click()),a(".navbar-menu-burger-content .content-sub-menus").each(function(){c||(a(this).find(".content-submenus-lvl2, #submenu-lvl2-content-caf-0").each(function(){c||a(this).find("a").each(function(){if(!c){var d=a(this).attr("href");if(d==b){c=!0;var e=new RegExp("^/allocataires/([0-9a-z.-]+)/actualites$");if(e.test(d))var f=a(this).parent().parent().parent().attr("id");else var f=a(this).parent().parent().attr("id");a("[data-toggle-id='"+f+"']").click();var g=a("#"+f).parent().attr("id");a("[data-toggle-id='"+g+"']").click()}}})}),c||a(this).find(".menu-sub-links a").each(function(){if(!c){var d=a(this).attr("href");if(d==b){c=!0;var e=a(this).parent().parent().attr("id");a("[data-toggle-id='"+e+"']").click()}}}))}),c}function g(){for(var b=a(".navbar-menu-burger-content").is(":visible"),c=["#page-content","#theme-header","#theme-footer",".footer-partenaires-content",".container.infos-txt"],d=["a","button","input"],e=0;e<c.length;e++)for(var f=0;f<d.length;f++)a(c[e]+" "+d[f]).each(function(){b?a(this).attr("tabindex","-1"):a(this).removeAttr("tabindex")});b?a("#sticky-camille").attr("tabindex","-1"):a("#sticky-camille").removeAttr("tabindex")}function h(a){var b="; "+document.cookie,c=b.split("; "+a+"=");if(2==c.length)return c.pop().split(";").shift()}function i(){var a=jQuery;jQuery.get("/sites/default/files/redirect/dispo_wps.json",function(b){1==b&&"undefined"!=typeof h("JSESSIONID")&&"undefined"!=typeof h("LtpaToken2")&&"undefined"!=typeof h("DYN_PERSYS")?jQuery.ajax({url:"/wps/s/getSessionInfos",method:"GET",data:{infos:a(".espace-allocataires").length>0?"yes":"no",JSESSIONID:h("JSESSIONID"),LtpaToken2:h("LtpaToken2"),DYN_PERSYS:h("DYN_PERSYS")},success:function(b){if(0==a(".espace-allocataires").length)"undefined"==typeof b.erreur&&"undefined"!=typeof b.cnxActive&&b.cnxActive?(a("div#sticky-mon-compte a.icon-pic_monC_head-sans").attr("href","/redirect/redirect.php?page=monCompte"),a("div#sticky-mon-compte a.icon-pic-connexion").attr("href",b.jsonDeconnexion),a("div#sticky-mon-compte").show(),a("div#estim_nn_alloc").hide()):a("div#sticky-mon-compte").hide();else if("undefined"==typeof b.erreur&&"undefined"!=typeof b.jsonMenu&&"undefined"!=typeof b.jsonNotification&&"undefined"!=typeof b.cnxActive&&b.cnxActive){a("#theme-header .icon-pic-connexion").parent().show(),a("#theme-header .icon-pic-connexion").parent().attr("href",b.jsonDeconnexion),a("div#estim_nn_alloc").hide(),a("#dropdownMonCpte").click(function(b){a("#menuMonCompte > ul").css("display","block"),a("#dropdownMonCpte").css("background-color","#0099cc")});var c=b.jsonMenu.civilite+" "+b.jsonMenu.nom;a("#dropdownMonCpte .header-monc-name").text(c),"null"!=b.jsonNotification&&"undefined"!=b.jsonNotification.nbAlertes&&b.jsonNotification.nbAlertes>0&&(a("#dropdownMonCpte .header-monc-notif").text(b.jsonNotification.nbAlertes),a("#dropdownMonCpte .header-monc-notif").css("display","block"));var e="";a.each(b.jsonMenu.menuItems,function(a,b){void 0!=b.sousItems?(e+='<li class="ssMenu1"><a id="menuCpt-'+a+'" data-toggle="collapse" data-target="#ssMenu1-'+a+'" aria-expanded="false">'+b.titre,void 0!=b.nbNotif&&parseInt(b.nbNotif)>0&&(e+='<span class="header-monc-notif icon-pic-notification">'+b.nbNotif+"</span>"),e+='</a><div class="picto-suite icon-pic-arrow-right-allocataires"></div>',e+=j(b.sousItems,"menuCpt-"+a,"ssMenu1-"+a),e+="</li>"):(e+='<li class="ssMenu1"><a href="'+b.lien+'">'+b.titre,void 0!=b.nbNotif&&parseInt(b.nbNotif)>0&&(e+='<span class="header-monc-notif icon-pic-notification">'+b.nbNotif+"</span>"),e+="</a></li>")}),e+='<div class="moncomptedeco"><a href="'+b.jsonDeconnexion+'" class="btn btn-info btn-block"><div class="icon-pic_deconnex_menu"></div>DÃ©connexion</a></div>',a(".dropdown-menu").append(e),a("#menuMonCompte a[data-target]").each(function(b){a(this).click(function(b){"false"==a(this).attr("aria-expanded")?(a(this).siblings("div").attr("class","picto-suite icon-pic-arrow-down-allocataires"),a(this).css("background-color","#0099cc"),a(this).css("color","white"),a("#dropdownMonCpte").css("background-color","#204474")):(a(this).siblings("div").attr("class","picto-suite icon-pic-arrow-right-allocataires"),a(this).css("background-color","white"),a(this).css("color","black"))})}),a("#dropdownMonCpte").click(function(b){b.preventDefault();var c=a("#dropdownMonCpte").attr("aria-expanded");if("true"!=c){a(".navbar-menu-burger-content").is(":visible")&&d(b),a("nav.navbar.navbar-default.affix").css("overflow","visible");var e=a("#theme-header > nav").css("height");a(".dropdown-menu").css("top",e-1),a("#overlay-header").show()}else b.stopPropagation(),a("#overlay-header").trigger("click")}),a("#dropdownMonCpte .navbar-header-arrow-bottom").show(),a("#dropdownMonCpte").attr("data-toggle","dropdown")}else a("#theme-header .icon-pic-connexion").parent().hide(),a("#theme-header .navbar-picto-content .header-monc-name").text("MON COMPTE"),a("#dropdownMonCpte .navbar-header-arrow-bottom").hide(),a("#dropdownMonCpte").removeAttr("data-toggle")}}):(a("div#sticky-mon-compte").hide(),a("#header .icon-pic-monC-head-Deco").parent().hide(),a("#dropdownMonCpte .navbar-header-arrow-bottom").hide(),a("#dropdownMonCpte").removeAttr("data-toggle"))})}function j(b,c,d){var e='<ul id="'+d+'" role="menu" aria-labelledby="'+c+'" class="ssMenu2"  style="height: 0px;">';return a.each(b,function(a,b){void 0!=b.sousItems?(e+='<li><a id="ssMenu-'+c+"-"+a+'" class="ssMenu2" data-toggle="collapse" data-target="#ssMenu2-'+a+'" aria-expanded="false">'+b.titre+'</a><div class="picto-suite icon-pic-arrow-right-allocataires"></div></li>',e+=j(b.sousItems,"ssMenu-"+c+"-"+a,"ssMenu2-"+a)):e+='<li><a href="'+b.lien+'" class="ssMenu2">'+b.titre+"</a></li>"}),e+="</ul>"}var k=getCookieGeoloc();if(a("#theme-header .navbar-ma-caf-header .icon-arrow-right").click(function(){a(this).parent().submit()}),"cnaf"!=k){a("#theme-header .navbar-ma-caf-header input").hide(),a("#theme-header .navbar-ma-caf-header .icon-arrow-right").hide();var l=a("#theme-header .navbar-titles-header-mob").height();a("#theme-header .navbar-titles-header-mob").css("height",l-2+"px");var m="Caf "+k.substr(0,2);971!=k&&972!=k&&973!=k&&974!=k&&976!=k||(m="Caf "+k);var n=window.location.pathname.split("/"),o=n[1];"allocataires"!=o&&"partenaires"!=o&&"presse-institutionnel"!=o&&(o="allocataires");var p=a("#liste_all_caf_name_by_caf_id .caf_id_"+k+"_url").text();p='<a href="/allocataires/'+p+'">'+m+"</a>",p+='<div class="ma-caf-header-change-caf icon-pic-geoloc-edit-'+o+'" title="S\'informer sur une autre Caf"></div>',a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name").html(p),a("body > .espace-allocataires").length>0?a.ajax({type:"GET",url:"/get/ajax/caf-menu",data:{caf_id:k},success:function(c){if("undefined"==typeof c.ereur){var d=a("#liste_all_caf_name_by_caf_id .caf_id_"+k+"_url").text(),f='<li class="hidden-xs"><a href="/allocataires/'+d+'">MA CAF</a>';f+='<span class="picto-fleche" data-toggle-id="sub-menu-content-ma-caf">',f+='<span class="icon-pic-arrow-right-allocataires"></span></span></li>';var g='<div class="sub-menu-content" data-toggle-id="sub-menu-content-ma-caf">';g+='<div class="menu-item">Ma Caf</div><div class="picto-suite icon-pic-arrow-right-allocataires"></div></div>';var h='<div id="sub-menu-content-ma-caf" class="content-sub-menus">';h+='<div class="menu-sub-links">',h+='<div class="title-level-1"><div class="picto-suite icon-pic-arrow-left-allocataires"></div><div class="node-title">Toutes les rubriques</div></div>';for(var i=0;i<c.length;i++)c[i].submenu.length>0?h+='<div class="menu-item submenu-lvl2" data-toggle-id="submenu-lvl2-content-caf-'+i+'">'+c[i].title+'<div class="picto-suite icon-pic-arrow-right-allocataires"></div></div>':(h+='<a href="/'+c[i].url+'" class="menu-item">',h+=c[i].title,h+="</a>");h+="</div>";for(var i=0;i<c.length;i++)if(c[i].submenu.length>0){h+=null==c[i].url?'<div id="submenu-lvl2-content-caf-'+i+'" class="content-submenus-lvl2"><div class="menu-sub-links"><div class="title-level-1"><div class="picto-suite icon-pic-arrow-left-allocataires"></div><div class="node-title">Ma Caf</div></div><a class="menu-item" style="cursor:default; background-color:#E0E0DF; color:black;">'+c[i].title+"</a>":'<div id="submenu-lvl2-content-caf-'+i+'" class="content-submenus-lvl2"><div class="menu-sub-links"><div class="title-level-1"><div class="picto-suite icon-pic-arrow-left-allocataires"></div><div class="node-title">Ma Caf</div></div><a class="menu-item" href="/'+c[i].url+'">'+c[i].title+"</a>";for(var j=0;j<c[i].submenu.length;j++){var l=c[i].submenu[j],m=/^http/.exec(l.alias)||/^\//.exec(l.alias)?l.alias:"/"+l.alias;h+='<a class="menu-item" href="'+m+'">'+l.title+"</a>"}h+="</div></div>"}h+="</div>",a("#theme-header .collapsed-second-row .menu-links .hidden-xs").last().after(f),a(g).insertBefore(".sub-menu-content-5"),a(".navbar-menu-burger-content").append(h),b(),e()}}}):(b(),e())}else a("#theme-header .navbar-ma-caf-header input").show(),a("#theme-header .navbar-ma-caf-header .icon-arrow-right").show(),b(),e();a("body").on("click","#theme-header .ma-caf-header-change-caf",function(b){a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name-hidden").html(a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name").html()),a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name").text("Ma Caf"),a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name").show(),a("#theme-header .navbar-ma-caf-header input").show(),a("#theme-header .navbar-ma-caf-header .icon-arrow-right").show(),a("#theme-header .navbar-ma-caf-header input").focus()}),a("#theme-header .navbar-ma-caf-header input").blur(function(){""==a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name-hidden").text()||a("#theme-header .navbar-ma-caf-header input").val()||(a("#theme-header .navbar-ma-caf-header input").val(""),a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name").html(a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name-hidden").html()),a("#theme-header .navbar-ma-caf-header .narvar-ma-caf-name-hidden").html(""),a("#theme-header .navbar-ma-caf-header input").hide(),a("#theme-header .navbar-ma-caf-header .icon-arrow-right").hide())}),a(".nav-burger-cnaf").click(function(b){if(b.preventDefault(),c(),a(".navbar-menu-burger-content").is(":visible"))d(b),a("#overlay-header").hide();else{a("#overlay-header").show();var e={"margin-left":"+=290px","margin-right":"-=290px"};if(a(window).width()<641)var f={"margin-left":"+=275px","margin-right":"-=275px"};else if(a(window).width()<961)var f={"margin-left":"+=280px","margin-right":"-=275px"};else var f={"margin-left":"+=285px","margin-right":"-=280px"};a("#theme-header .navbar-default .container:first-child").animate(f,"slow"),a("#page-content .container:first-child").animate(e,"slow"),a("#sticky-camille").animate(e,"slow"),a("#page-content").css("margin-top","-20px"),a("#theme-header .picto-burger").addClass("active"),a(".navbar-menu-burger-content").show().animate({"margin-left":"-10px"},"slow"),a(document).scrollTop()<=10?a(".navbar-menu-burger-content").css("margin-top",a("#theme-header .container").height()):a(".navbar-menu-burger-content").css("margin-top","0px"),g(),a("body").css("overflow","hidden")}}),a("#overlay-header").click(function(b){a(".navbar-menu-burger-content").is(":visible")&&d(b),c(),a("#overlay-header").hide()}),i(),e()}(jQuery),/*! jQuery UI - v1.12.1 - 2017-01-25
* http://jqueryui.com
* Includes: keycode.js, widgets/datepicker.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */
function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){/*!
 * jQuery UI Datepicker 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */
function b(){var a=window.location.pathname;a=a.split("/");var b="allocataires";return"partenaires"==a[1]?b="partenaires":"presse-institutionnel"==a[1]&&(b="presse-institutionnel"),b}function c(a){for(var b,c;a.length&&a[0]!==document;){if(b=a.css("position"),("absolute"===b||"relative"===b||"fixed"===b)&&(c=parseInt(a.css("zIndex"),10),!isNaN(c)&&0!==c))return c;a=a.parent()}return 0}function d(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},a.extend(this._defaults,this.regional[""]),this.regional.en=a.extend(!0,{},this.regional[""]),this.regional["en-US"]=a.extend(!0,{},this.regional.en),this.dpDiv=e(a("<div id='"+this._mainDivId+"' class='espace-"+b()+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function e(b){var c="button, .ui-datepicker-calendar td a";return b.on("mouseout",c,function(){a(this).removeClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).removeClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&a(this).removeClass("ui-datepicker-next-hover")}).on("mouseover",c,f)}function f(){a.datepicker._isDisabledDatepicker(h.inline?h.dpDiv.parent()[0]:h.input[0])||(a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),a(this).addClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).addClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&a(this).addClass("ui-datepicker-next-hover"))}function g(b,c){a.extend(b,c);for(var d in c)null==c[d]&&(b[d]=c[d]);return b}a.ui=a.ui||{};a.ui.version="1.12.1",a.ui.keyCode={BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38};a.extend(a.ui,{datepicker:{version:"1.12.1"}});var h;a.extend(d.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(a){return g(this._defaults,a||{}),this},_attachDatepicker:function(b,c){var d,e,f;d=b.nodeName.toLowerCase(),e="div"===d||"span"===d,b.id||(this.uuid+=1,b.id="dp"+this.uuid),f=this._newInst(a(b),e),f.settings=a.extend({},c||{}),"input"===d?this._connectDatepicker(b,f):e&&this._inlineDatepicker(b,f)},_newInst:function(c,d){var f=c[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:f,input:c,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:d,dpDiv:d?e(a("<div class='"+this._inlineClass+" espace-"+b()+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(b,c){var d=a(b);c.append=a([]),c.trigger=a([]),d.hasClass(this.markerClassName)||(this._attachments(d,c),d.addClass(this.markerClassName).on("keydown",this._doKeyDown).on("keypress",this._doKeyPress).on("keyup",this._doKeyUp),this._autoSize(c),a.data(b,"datepicker",c),c.settings.disabled&&this._disableDatepicker(b))},_attachments:function(b,c){var d,e,f,g=this._get(c,"appendText"),h=this._get(c,"isRTL");c.append&&c.append.remove(),g&&(c.append=a("<span class='"+this._appendClass+"'>"+g+"</span>"),b[h?"before":"after"](c.append)),b.off("focus",this._showDatepicker),c.trigger&&c.trigger.remove(),d=this._get(c,"showOn"),"focus"!==d&&"both"!==d||b.on("focus",this._showDatepicker),"button"!==d&&"both"!==d||(e=this._get(c,"buttonText"),f=this._get(c,"buttonImage"),c.trigger=a(this._get(c,"buttonImageOnly")?a("<img/>").addClass(this._triggerClass).attr({src:f,alt:e,title:e}):a("<button type='button'></button>").addClass(this._triggerClass).html(f?a("<img/>").attr({src:f,alt:e,title:e}):e)),b[h?"before":"after"](c.trigger),c.trigger.on("click",function(){return a.datepicker._datepickerShowing&&a.datepicker._lastInput===b[0]?a.datepicker._hideDatepicker():a.datepicker._datepickerShowing&&a.datepicker._lastInput!==b[0]?(a.datepicker._hideDatepicker(),a.datepicker._showDatepicker(b[0])):a.datepicker._showDatepicker(b[0]),!1}))},_autoSize:function(a){if(this._get(a,"autoSize")&&!a.inline){var b,c,d,e,f=new Date(2009,11,20),g=this._get(a,"dateFormat");g.match(/[DM]/)&&(b=function(a){for(c=0,d=0,e=0;e<a.length;e++)a[e].length>c&&(c=a[e].length,d=e);return d},f.setMonth(b(this._get(a,g.match(/MM/)?"monthNames":"monthNamesShort"))),f.setDate(b(this._get(a,g.match(/DD/)?"dayNames":"dayNamesShort"))+20-f.getDay())),a.input.attr("size",this._formatDate(a,f).length)}},_inlineDatepicker:function(b,c){var d=a(b);d.hasClass(this.markerClassName)||(d.addClass(this.markerClassName).append(c.dpDiv),a.data(b,"datepicker",c),this._setDate(c,this._getDefaultDate(c),!0),this._updateDatepicker(c),this._updateAlternate(c),c.settings.disabled&&this._disableDatepicker(b),c.dpDiv.css("display","block"))},_dialogDatepicker:function(b,c,d,e,f){var h,i,j,k,l,m=this._dialogInst;return m||(this.uuid+=1,h="dp"+this.uuid,this._dialogInput=a("<input type='text' id='"+h+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.on("keydown",this._doKeyDown),a("body").append(this._dialogInput),m=this._dialogInst=this._newInst(this._dialogInput,!1),m.settings={},a.data(this._dialogInput[0],"datepicker",m)),g(m.settings,e||{}),c=c&&c.constructor===Date?this._formatDate(m,c):c,this._dialogInput.val(c),this._pos=f?f.length?f:[f.pageX,f.pageY]:null,this._pos||(i=document.documentElement.clientWidth,j=document.documentElement.clientHeight,k=document.documentElement.scrollLeft||document.body.scrollLeft,l=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[i/2-100+k,j/2-150+l]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),m.settings.onSelect=d,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),a.blockUI&&a.blockUI(this.dpDiv),a.data(this._dialogInput[0],"datepicker",m),this},_destroyDatepicker:function(b){var c,d=a(b),e=a.data(b,"datepicker");d.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),a.removeData(b,"datepicker"),"input"===c?(e.append.remove(),e.trigger.remove(),d.removeClass(this.markerClassName).off("focus",this._showDatepicker).off("keydown",this._doKeyDown).off("keypress",this._doKeyPress).off("keyup",this._doKeyUp)):"div"!==c&&"span"!==c||d.removeClass(this.markerClassName).empty(),h===e&&(h=null))},_enableDatepicker:function(b){var c,d,e=a(b),f=a.data(b,"datepicker");e.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),"input"===c?(b.disabled=!1,f.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""})):"div"!==c&&"span"!==c||(d=e.children("."+this._inlineClass),d.children().removeClass("ui-state-disabled"),d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)),this._disabledInputs=a.map(this._disabledInputs,function(a){return a===b?null:a}))},_disableDatepicker:function(b){var c,d,e=a(b),f=a.data(b,"datepicker");e.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),"input"===c?(b.disabled=!0,f.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"})):"div"!==c&&"span"!==c||(d=e.children("."+this._inlineClass),d.children().addClass("ui-state-disabled"),d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)),this._disabledInputs=a.map(this._disabledInputs,function(a){return a===b?null:a}),this._disabledInputs[this._disabledInputs.length]=b)},_isDisabledDatepicker:function(a){if(!a)return!1;for(var b=0;b<this._disabledInputs.length;b++)if(this._disabledInputs[b]===a)return!0;return!1},_getInst:function(b){try{return a.data(b,"datepicker")}catch(c){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(b,c,d){var e,f,h,i,j=this._getInst(b);return 2===arguments.length&&"string"==typeof c?"defaults"===c?a.extend({},a.datepicker._defaults):j?"all"===c?a.extend({},j.settings):this._get(j,c):null:(e=c||{},"string"==typeof c&&(e={},e[c]=d),void(j&&(this._curInst===j&&this._hideDatepicker(),f=this._getDateDatepicker(b,!0),h=this._getMinMaxDate(j,"min"),i=this._getMinMaxDate(j,"max"),g(j.settings,e),null!==h&&void 0!==e.dateFormat&&void 0===e.minDate&&(j.settings.minDate=this._formatDate(j,h)),null!==i&&void 0!==e.dateFormat&&void 0===e.maxDate&&(j.settings.maxDate=this._formatDate(j,i)),"disabled"in e&&(e.disabled?this._disableDatepicker(b):this._enableDatepicker(b)),this._attachments(a(b),j),this._autoSize(j),this._setDate(j,f),this._updateAlternate(j),this._updateDatepicker(j))))},_changeDatepicker:function(a,b,c){this._optionDatepicker(a,b,c)},_refreshDatepicker:function(a){var b=this._getInst(a);b&&this._updateDatepicker(b)},_setDateDatepicker:function(a,b){var c=this._getInst(a);c&&(this._setDate(c,b),this._updateDatepicker(c),this._updateAlternate(c))},_getDateDatepicker:function(a,b){var c=this._getInst(a);return c&&!c.inline&&this._setDateFromField(c,b),c?this._getDate(c):null},_doKeyDown:function(b){var c,d,e,f=a.datepicker._getInst(b.target),g=!0,h=f.dpDiv.is(".ui-datepicker-rtl");if(f._keyEvent=!0,a.datepicker._datepickerShowing)switch(b.keyCode){case 9:a.datepicker._hideDatepicker(),g=!1;break;case 13:return e=a("td."+a.datepicker._dayOverClass+":not(."+a.datepicker._currentClass+")",f.dpDiv),e[0]&&a.datepicker._selectDay(b.target,f.selectedMonth,f.selectedYear,e[0]),c=a.datepicker._get(f,"onSelect"),c?(d=a.datepicker._formatDate(f),c.apply(f.input?f.input[0]:null,[d,f])):a.datepicker._hideDatepicker(),!1;case 27:a.datepicker._hideDatepicker();break;case 33:a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(f,"stepBigMonths"):-a.datepicker._get(f,"stepMonths"),"M");break;case 34:a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(f,"stepBigMonths"):+a.datepicker._get(f,"stepMonths"),"M");break;case 35:(b.ctrlKey||b.metaKey)&&a.datepicker._clearDate(b.target),g=b.ctrlKey||b.metaKey;break;case 36:(b.ctrlKey||b.metaKey)&&a.datepicker._gotoToday(b.target),g=b.ctrlKey||b.metaKey;break;case 37:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?1:-1,"D"),g=b.ctrlKey||b.metaKey,b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(f,"stepBigMonths"):-a.datepicker._get(f,"stepMonths"),"M");break;case 38:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,-7,"D"),g=b.ctrlKey||b.metaKey;break;case 39:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?-1:1,"D"),g=b.ctrlKey||b.metaKey,b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(f,"stepBigMonths"):+a.datepicker._get(f,"stepMonths"),"M");break;case 40:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,7,"D"),g=b.ctrlKey||b.metaKey;break;default:g=!1}else 36===b.keyCode&&b.ctrlKey?a.datepicker._showDatepicker(this):g=!1;g&&(b.preventDefault(),b.stopPropagation())},_doKeyPress:function(b){var c,d,e=a.datepicker._getInst(b.target);if(a.datepicker._get(e,"constrainInput"))return c=a.datepicker._possibleChars(a.datepicker._get(e,"dateFormat")),d=String.fromCharCode(null==b.charCode?b.keyCode:b.charCode),b.ctrlKey||b.metaKey||d<" "||!c||c.indexOf(d)>-1},_doKeyUp:function(b){var c,d=a.datepicker._getInst(b.target);if(d.input.val()!==d.lastVal)try{c=a.datepicker.parseDate(a.datepicker._get(d,"dateFormat"),d.input?d.input.val():null,a.datepicker._getFormatConfig(d)),c&&(a.datepicker._setDateFromField(d),a.datepicker._updateAlternate(d),a.datepicker._updateDatepicker(d))}catch(e){}return!0},_showDatepicker:function(b){if(b=b.target||b,"input"!==b.nodeName.toLowerCase()&&(b=a("input",b.parentNode)[0]),!a.datepicker._isDisabledDatepicker(b)&&a.datepicker._lastInput!==b){var d,e,f,h,i,j,k;d=a.datepicker._getInst(b),a.datepicker._curInst&&a.datepicker._curInst!==d&&(a.datepicker._curInst.dpDiv.stop(!0,!0),d&&a.datepicker._datepickerShowing&&a.datepicker._hideDatepicker(a.datepicker._curInst.input[0])),e=a.datepicker._get(d,"beforeShow"),f=e?e.apply(b,[b,d]):{},f!==!1&&(g(d.settings,f),d.lastVal=null,a.datepicker._lastInput=b,a.datepicker._setDateFromField(d),a.datepicker._inDialog&&(b.value=""),a.datepicker._pos||(a.datepicker._pos=a.datepicker._findPos(b),a.datepicker._pos[1]+=b.offsetHeight),h=!1,a(b).parents().each(function(){return h|="fixed"===a(this).css("position"),!h}),i={left:a.datepicker._pos[0],top:a.datepicker._pos[1]},a.datepicker._pos=null,d.dpDiv.empty(),d.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),a.datepicker._updateDatepicker(d),i=a.datepicker._checkOffset(d,i,h),d.dpDiv.css({position:a.datepicker._inDialog&&a.blockUI?"static":h?"fixed":"absolute",display:"none",left:i.left+"px",top:i.top+"px"}),d.inline||(j=a.datepicker._get(d,"showAnim"),k=a.datepicker._get(d,"duration"),d.dpDiv.css("z-index",c(a(b))+4),a.datepicker._datepickerShowing=!0,a.effects&&a.effects.effect[j]?d.dpDiv.show(j,a.datepicker._get(d,"showOptions"),k):d.dpDiv[j||"show"](j?k:null),a.datepicker._shouldFocusInput(d)&&d.input.trigger("focus"),a.datepicker._curInst=d))}},_updateDatepicker:function(b){this.maxRows=4,h=b,b.dpDiv.empty().append(this._generateHTML(b)),this._attachHandlers(b);var c,d=this._getNumberOfMonths(b),e=d[1],g=17,i=b.dpDiv.find("."+this._dayOverClass+" a");i.length>0&&f.apply(i.get(0)),b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),e>1&&b.dpDiv.addClass("ui-datepicker-multi-"+e).css("width",g*e+"em"),b.dpDiv[(1!==d[0]||1!==d[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),b.dpDiv[(this._get(b,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),b===a.datepicker._curInst&&a.datepicker._datepickerShowing&&a.datepicker._shouldFocusInput(b)&&b.input.trigger("focus"),b.yearshtml&&(c=b.yearshtml,setTimeout(function(){c===b.yearshtml&&b.yearshtml&&b.dpDiv.find("select.ui-datepicker-year:first").replaceWith(b.yearshtml),c=b.yearshtml=null},0))},_shouldFocusInput:function(a){return a.input&&a.input.is(":visible")&&!a.input.is(":disabled")&&!a.input.is(":focus")},_checkOffset:function(b,c,d){var e=b.dpDiv.outerWidth(),f=b.dpDiv.outerHeight(),g=b.input?b.input.outerWidth():0,h=b.input?b.input.outerHeight():0,i=document.documentElement.clientWidth+(d?0:a(document).scrollLeft()),j=document.documentElement.clientHeight+(d?0:a(document).scrollTop());return c.left-=this._get(b,"isRTL")?e-g:0,c.left-=d&&c.left===b.input.offset().left?a(document).scrollLeft():0,c.top-=d&&c.top===b.input.offset().top+h?a(document).scrollTop():0,c.left-=Math.min(c.left,c.left+e>i&&i>e?Math.abs(c.left+e-i):0),c.top-=Math.min(c.top,c.top+f>j&&j>f?Math.abs(f+h):0),c},_findPos:function(b){for(var c,d=this._getInst(b),e=this._get(d,"isRTL");b&&("hidden"===b.type||1!==b.nodeType||a.expr.filters.hidden(b));)b=b[e?"previousSibling":"nextSibling"];return c=a(b).offset(),[c.left,c.top]},_hideDatepicker:function(b){var c,d,e,f,g=this._curInst;!g||b&&g!==a.data(b,"datepicker")||this._datepickerShowing&&(c=this._get(g,"showAnim"),d=this._get(g,"duration"),e=function(){a.datepicker._tidyDialog(g)},a.effects&&(a.effects.effect[c]||a.effects[c])?g.dpDiv.hide(c,a.datepicker._get(g,"showOptions"),d,e):g.dpDiv["slideDown"===c?"slideUp":"fadeIn"===c?"fadeOut":"hide"](c?d:null,e),c||e(),this._datepickerShowing=!1,f=this._get(g,"onClose"),f&&f.apply(g.input?g.input[0]:null,[g.input?g.input.val():"",g]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),a.blockUI&&(a.unblockUI(),a("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(a){a.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar")},_checkExternalClick:function(b){if(a.datepicker._curInst){var c=a(b.target),d=a.datepicker._getInst(c[0]);(c[0].id===a.datepicker._mainDivId||0!==c.parents("#"+a.datepicker._mainDivId).length||c.hasClass(a.datepicker.markerClassName)||c.closest("."+a.datepicker._triggerClass).length||!a.datepicker._datepickerShowing||a.datepicker._inDialog&&a.blockUI)&&(!c.hasClass(a.datepicker.markerClassName)||a.datepicker._curInst===d)||a.datepicker._hideDatepicker()}},_adjustDate:function(b,c,d){var e=a(b),f=this._getInst(e[0]);this._isDisabledDatepicker(e[0])||(this._adjustInstDate(f,c+("M"===d?this._get(f,"showCurrentAtPos"):0),d),this._updateDatepicker(f))},_gotoToday:function(b){var c,d=a(b),e=this._getInst(d[0]);this._get(e,"gotoCurrent")&&e.currentDay?(e.selectedDay=e.currentDay,e.drawMonth=e.selectedMonth=e.currentMonth,e.drawYear=e.selectedYear=e.currentYear):(c=new Date,e.selectedDay=c.getDate(),e.drawMonth=e.selectedMonth=c.getMonth(),e.drawYear=e.selectedYear=c.getFullYear()),this._notifyChange(e),this._adjustDate(d)},_selectMonthYear:function(b,c,d){var e=a(b),f=this._getInst(e[0]);f["selected"+("M"===d?"Month":"Year")]=f["draw"+("M"===d?"Month":"Year")]=parseInt(c.options[c.selectedIndex].value,10),this._notifyChange(f),this._adjustDate(e)},_selectDay:function(b,c,d,e){var f,g=a(b);a(e).hasClass(this._unselectableClass)||this._isDisabledDatepicker(g[0])||(f=this._getInst(g[0]),f.selectedDay=f.currentDay=a("a",e).html(),f.selectedMonth=f.currentMonth=c,f.selectedYear=f.currentYear=d,this._selectDate(b,this._formatDate(f,f.currentDay,f.currentMonth,f.currentYear)))},_clearDate:function(b){var c=a(b);this._selectDate(c,"")},_selectDate:function(b,c){var d,e=a(b),f=this._getInst(e[0]);c=null!=c?c:this._formatDate(f),f.input&&f.input.val(c),this._updateAlternate(f),d=this._get(f,"onSelect"),d?d.apply(f.input?f.input[0]:null,[c,f]):f.input&&f.input.trigger("change"),f.inline?this._updateDatepicker(f):(this._hideDatepicker(),this._lastInput=f.input[0],"object"!=typeof f.input[0]&&f.input.trigger("focus"),this._lastInput=null)},_updateAlternate:function(b){var c,d,e,f=this._get(b,"altField");f&&(c=this._get(b,"altFormat")||this._get(b,"dateFormat"),d=this._getDate(b),e=this.formatDate(c,d,this._getFormatConfig(b)),a(f).val(e))},noWeekends:function(a){var b=a.getDay();return[b>0&&b<6,""]},iso8601Week:function(a){var b,c=new Date(a.getTime());return c.setDate(c.getDate()+4-(c.getDay()||7)),b=c.getTime(),c.setMonth(0),c.setDate(1),Math.floor(Math.round((b-c)/864e5)/7)+1},parseDate:function(b,c,d){if(null==b||null==c)throw"Invalid arguments";if(c="object"==typeof c?c.toString():c+"",""===c)return null;var e,f,g,h,i=0,j=(d?d.shortYearCutoff:null)||this._defaults.shortYearCutoff,k="string"!=typeof j?j:(new Date).getFullYear()%100+parseInt(j,10),l=(d?d.dayNamesShort:null)||this._defaults.dayNamesShort,m=(d?d.dayNames:null)||this._defaults.dayNames,n=(d?d.monthNamesShort:null)||this._defaults.monthNamesShort,o=(d?d.monthNames:null)||this._defaults.monthNames,p=-1,q=-1,r=-1,s=-1,t=!1,u=function(a){var c=e+1<b.length&&b.charAt(e+1)===a;return c&&e++,c},v=function(a){var b=u(a),d="@"===a?14:"!"===a?20:"y"===a&&b?4:"o"===a?3:2,e="y"===a?d:1,f=new RegExp("^\\d{"+e+","+d+"}"),g=c.substring(i).match(f);if(!g)throw"Missing number at position "+i;return i+=g[0].length,parseInt(g[0],10)},w=function(b,d,e){var f=-1,g=a.map(u(b)?e:d,function(a,b){return[[b,a]]}).sort(function(a,b){return-(a[1].length-b[1].length)});if(a.each(g,function(a,b){var d=b[1];if(c.substr(i,d.length).toLowerCase()===d.toLowerCase())return f=b[0],i+=d.length,!1}),f!==-1)return f+1;throw"Unknown name at position "+i},x=function(){if(c.charAt(i)!==b.charAt(e))throw"Unexpected literal at position "+i;i++};for(e=0;e<b.length;e++)if(t)"'"!==b.charAt(e)||u("'")?x():t=!1;else switch(b.charAt(e)){case"d":r=v("d");break;case"D":w("D",l,m);break;case"o":s=v("o");break;case"m":q=v("m");break;case"M":q=w("M",n,o);break;case"y":p=v("y");break;case"@":h=new Date(v("@")),p=h.getFullYear(),q=h.getMonth()+1,r=h.getDate();break;case"!":h=new Date((v("!")-this._ticksTo1970)/1e4),p=h.getFullYear(),q=h.getMonth()+1,r=h.getDate();break;case"'":u("'")?x():t=!0;break;default:x()}if(i<c.length&&(g=c.substr(i),!/^\s+/.test(g)))throw"Extra/unparsed characters found in date: "+g;if(p===-1?p=(new Date).getFullYear():p<100&&(p+=(new Date).getFullYear()-(new Date).getFullYear()%100+(p<=k?0:-100)),s>-1)for(q=1,r=s;;){if(f=this._getDaysInMonth(p,q-1),r<=f)break;q++,r-=f}if(h=this._daylightSavingAdjust(new Date(p,q-1,r)),h.getFullYear()!==p||h.getMonth()+1!==q||h.getDate()!==r)throw"Invalid date";return h},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925))*60*60*1e7,formatDate:function(a,b,c){if(!b)return"";var d,e=(c?c.dayNamesShort:null)||this._defaults.dayNamesShort,f=(c?c.dayNames:null)||this._defaults.dayNames,g=(c?c.monthNamesShort:null)||this._defaults.monthNamesShort,h=(c?c.monthNames:null)||this._defaults.monthNames,i=function(b){var c=d+1<a.length&&a.charAt(d+1)===b;return c&&d++,c},j=function(a,b,c){var d=""+b;if(i(a))for(;d.length<c;)d="0"+d;return d},k=function(a,b,c,d){return i(a)?d[b]:c[b]},l="",m=!1;if(b)for(d=0;d<a.length;d++)if(m)"'"!==a.charAt(d)||i("'")?l+=a.charAt(d):m=!1;else switch(a.charAt(d)){case"d":l+=j("d",b.getDate(),2);break;case"D":l+=k("D",b.getDay(),e,f);break;case"o":l+=j("o",Math.round((new Date(b.getFullYear(),b.getMonth(),b.getDate()).getTime()-new Date(b.getFullYear(),0,0).getTime())/864e5),3);break;case"m":l+=j("m",b.getMonth()+1,2);break;case"M":l+=k("M",b.getMonth(),g,h);break;case"y":l+=i("y")?b.getFullYear():(b.getFullYear()%100<10?"0":"")+b.getFullYear()%100;break;case"@":l+=b.getTime();break;case"!":l+=1e4*b.getTime()+this._ticksTo1970;break;case"'":i("'")?l+="'":m=!0;break;default:l+=a.charAt(d)}return l},_possibleChars:function(a){var b,c="",d=!1,e=function(c){var d=b+1<a.length&&a.charAt(b+1)===c;return d&&b++,d};for(b=0;b<a.length;b++)if(d)"'"!==a.charAt(b)||e("'")?c+=a.charAt(b):d=!1;else switch(a.charAt(b)){case"d":case"m":case"y":case"@":c+="0123456789";break;case"D":case"M":return null;case"'":e("'")?c+="'":d=!0;break;default:c+=a.charAt(b)}return c},_get:function(a,b){return void 0!==a.settings[b]?a.settings[b]:this._defaults[b]},_setDateFromField:function(a,b){if(a.input.val()!==a.lastVal){var c=this._get(a,"dateFormat"),d=a.lastVal=a.input?a.input.val():null,e=this._getDefaultDate(a),f=e,g=this._getFormatConfig(a);try{f=this.parseDate(c,d,g)||e}catch(h){d=b?"":d}a.selectedDay=f.getDate(),a.drawMonth=a.selectedMonth=f.getMonth(),a.drawYear=a.selectedYear=f.getFullYear(),a.currentDay=d?f.getDate():0,a.currentMonth=d?f.getMonth():0,a.currentYear=d?f.getFullYear():0,this._adjustInstDate(a)}},_getDefaultDate:function(a){return this._restrictMinMax(a,this._determineDate(a,this._get(a,"defaultDate"),new Date))},_determineDate:function(b,c,d){var e=function(a){var b=new Date;return b.setDate(b.getDate()+a),b},f=function(c){try{return a.datepicker.parseDate(a.datepicker._get(b,"dateFormat"),c,a.datepicker._getFormatConfig(b))}catch(d){}for(var e=(c.toLowerCase().match(/^c/)?a.datepicker._getDate(b):null)||new Date,f=e.getFullYear(),g=e.getMonth(),h=e.getDate(),i=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,j=i.exec(c);j;){switch(j[2]||"d"){case"d":case"D":h+=parseInt(j[1],10);break;case"w":case"W":h+=7*parseInt(j[1],10);break;case"m":case"M":g+=parseInt(j[1],10),h=Math.min(h,a.datepicker._getDaysInMonth(f,g));break;case"y":case"Y":f+=parseInt(j[1],10),h=Math.min(h,a.datepicker._getDaysInMonth(f,g))}j=i.exec(c)}return new Date(f,g,h)},g=null==c||""===c?d:"string"==typeof c?f(c):"number"==typeof c?isNaN(c)?d:e(c):new Date(c.getTime());return g=g&&"Invalid Date"===g.toString()?d:g,g&&(g.setHours(0),g.setMinutes(0),g.setSeconds(0),g.setMilliseconds(0)),this._daylightSavingAdjust(g)},_daylightSavingAdjust:function(a){return a?(a.setHours(a.getHours()>12?a.getHours()+2:0),a):null},_setDate:function(a,b,c){var d=!b,e=a.selectedMonth,f=a.selectedYear,g=this._restrictMinMax(a,this._determineDate(a,b,new Date));a.selectedDay=a.currentDay=g.getDate(),a.drawMonth=a.selectedMonth=a.currentMonth=g.getMonth(),a.drawYear=a.selectedYear=a.currentYear=g.getFullYear(),e===a.selectedMonth&&f===a.selectedYear||c||this._notifyChange(a),this._adjustInstDate(a),a.input&&a.input.val(d?"":this._formatDate(a))},_getDate:function(a){var b=!a.currentYear||a.input&&""===a.input.val()?null:this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay));return b},_attachHandlers:function(b){var c=this._get(b,"stepMonths"),d="#"+b.id.replace(/\\\\/g,"\\");b.dpDiv.find("[data-handler]").map(function(){var b={prev:function(){a.datepicker._adjustDate(d,-c,"M")},next:function(){a.datepicker._adjustDate(d,+c,"M")},hide:function(){a.datepicker._hideDatepicker()},today:function(){a.datepicker._gotoToday(d)},selectDay:function(){return a.datepicker._selectDay(d,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return a.datepicker._selectMonthYear(d,this,"M"),!1},selectYear:function(){return a.datepicker._selectMonthYear(d,this,"Y"),!1}};a(this).on(this.getAttribute("data-event"),b[this.getAttribute("data-handler")])})},_generateHTML:function(a){var c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P=new Date,Q=this._daylightSavingAdjust(new Date(P.getFullYear(),P.getMonth(),P.getDate())),R=this._get(a,"isRTL"),S=this._get(a,"showButtonPanel"),T=this._get(a,"hideIfNoPrevNext"),U=this._get(a,"navigationAsDateFormat"),V=this._getNumberOfMonths(a),W=this._get(a,"showCurrentAtPos"),X=this._get(a,"stepMonths"),Y=1!==V[0]||1!==V[1],Z=this._daylightSavingAdjust(a.currentDay?new Date(a.currentYear,a.currentMonth,a.currentDay):new Date(9999,9,9)),$=this._getMinMaxDate(a,"min"),_=this._getMinMaxDate(a,"max"),aa=a.drawMonth-W,ba=a.drawYear;if(aa<0&&(aa+=12,ba--),_)for(c=this._daylightSavingAdjust(new Date(_.getFullYear(),_.getMonth()-V[0]*V[1]+1,_.getDate())),c=$&&c<$?$:c;this._daylightSavingAdjust(new Date(ba,aa,1))>c;)aa--,aa<0&&(aa=11,ba--);for(a.drawMonth=aa,a.drawYear=ba,d=this._get(a,"prevText"),d=U?this.formatDate(d,this._daylightSavingAdjust(new Date(ba,aa-X,1)),this._getFormatConfig(a)):d,e=this._canAdjustMonth(a,-1,ba,aa)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+d+"'><span class='icon-pic-arrow-left-"+b()+"' alt='"+d+"'></span></a>":T?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+d+"'><span class='ui-icon ui-icon-circle-triangle-"+(R?"e":"w")+"'>"+d+"</span></a>",f=this._get(a,"nextText"),f=U?this.formatDate(f,this._daylightSavingAdjust(new Date(ba,aa+X,1)),this._getFormatConfig(a)):f,g=this._canAdjustMonth(a,1,ba,aa)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+f+"'><span class='icon-pic-arrow-right-"+b()+"' alt='"+f+"'></span></a>":T?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+f+"'><span class='ui-icon ui-icon-circle-triangle-"+(R?"w":"e")+"'>"+f+"</span></a>",h=this._get(a,"currentText"),i=this._get(a,"gotoCurrent")&&a.currentDay?Z:Q,h=U?this.formatDate(h,i,this._getFormatConfig(a)):h,j=a.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(a,"closeText")+"</button>",k=S?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(R?j:"")+(this._isInRange(a,i)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+h+"</button>":"")+(R?"":j)+"</div>":"",l=parseInt(this._get(a,"firstDay"),10),l=isNaN(l)?0:l,m=this._get(a,"showWeek"),n=this._get(a,"dayNames"),o=this._get(a,"dayNamesMin"),p=this._get(a,"monthNames"),q=this._get(a,"monthNamesShort"),r=this._get(a,"beforeShowDay"),s=this._get(a,"showOtherMonths"),t=this._get(a,"selectOtherMonths"),u=this._getDefaultDate(a),v="",x=0;x<V[0];x++){for(y="",this.maxRows=4,z=0;z<V[1];z++){if(A=this._daylightSavingAdjust(new Date(ba,aa,a.selectedDay)),B=" ui-corner-all",C="",Y){if(C+="<div class='ui-datepicker-group",V[1]>1)switch(z){case 0:C+=" ui-datepicker-group-first",B=" ui-corner-"+(R?"right":"left");break;case V[1]-1:C+=" ui-datepicker-group-last",B=" ui-corner-"+(R?"left":"right");break;default:C+=" ui-datepicker-group-middle",B=""}C+="'>"}for(C+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+B+"'>"+(/all|left/.test(B)&&0===x?R?g:e:"")+(/all|right/.test(B)&&0===x?R?e:g:"")+this._generateMonthYearHeader(a,aa,ba,$,_,x>0||z>0,p,q)+"</div><table class='ui-datepicker-calendar'><thead><tr>",D=m?"<th class='ui-datepicker-week-col'>"+this._get(a,"weekHeader")+"</th>":"",w=0;w<7;w++)E=(w+l)%7,D+="<th scope='col'"+((w+l+6)%7>=5?" class='ui-datepicker-week-end'":"")+"><span title='"+n[E]+"'>"+o[E]+"</span></th>";for(C+=D+"</tr></thead><tbody>",F=this._getDaysInMonth(ba,aa),ba===a.selectedYear&&aa===a.selectedMonth&&(a.selectedDay=Math.min(a.selectedDay,F)),G=(this._getFirstDayOfMonth(ba,aa)-l+7)%7,H=Math.ceil((G+F)/7),I=Y&&this.maxRows>H?this.maxRows:H,this.maxRows=I,J=this._daylightSavingAdjust(new Date(ba,aa,1-G)),K=0;K<I;K++){for(C+="<tr>",L=m?"<td class='ui-datepicker-week-col'>"+this._get(a,"calculateWeek")(J)+"</td>":"",w=0;w<7;w++)M=r?r.apply(a.input?a.input[0]:null,[J]):[!0,""],N=J.getMonth()!==aa,O=N&&!t||!M[0]||$&&J<$||_&&J>_,L+="<td class='"+((w+l+6)%7>=5?" ui-datepicker-week-end":"")+(N?" ui-datepicker-other-month":"")+(J.getTime()===A.getTime()&&aa===a.selectedMonth&&a._keyEvent||u.getTime()===J.getTime()&&u.getTime()===A.getTime()?" "+this._dayOverClass:"")+(O?" "+this._unselectableClass+" ui-state-disabled":"")+(N&&!s?"":" "+M[1]+(J.getTime()===Z.getTime()?" "+this._currentClass:"")+(J.getTime()===Q.getTime()?" ui-datepicker-today":""))+"'"+(N&&!s||!M[2]?"":" title='"+M[2].replace(/'/g,"&#39;")+"'")+(O?"":" data-handler='selectDay' data-event='click' data-month='"+J.getMonth()+"' data-year='"+J.getFullYear()+"'")+">"+(N&&!s?"&#xa0;":O?"<span class='ui-state-default'>"+J.getDate()+"</span>":"<a class='ui-state-default"+(J.getTime()===Q.getTime()?" ui-state-highlight":"")+(J.getTime()===Z.getTime()?" ui-state-active":"")+(N?" ui-priority-secondary":"")+"' href='#'>"+J.getDate()+"</a>")+"</td>",J.setDate(J.getDate()+1),J=this._daylightSavingAdjust(J);C+=L+"</tr>"}aa++,aa>11&&(aa=0,ba++),C+="</tbody></table>"+(Y?"</div>"+(V[0]>0&&z===V[1]-1?"<div class='ui-datepicker-row-break'></div>":""):""),y+=C}v+=y}return v+=k,a._keyEvent=!1,
v},_generateMonthYearHeader:function(a,b,c,d,e,f,g,h){var i,j,k,l,m,n,o,p,q=this._get(a,"changeMonth"),r=this._get(a,"changeYear"),s=this._get(a,"showMonthAfterYear"),t="<div class='ui-datepicker-title'>",u="";if(f||!q)u+="<span class='ui-datepicker-month'>"+g[b]+"</span>";else{for(i=d&&d.getFullYear()===c,j=e&&e.getFullYear()===c,u+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",k=0;k<12;k++)(!i||k>=d.getMonth())&&(!j||k<=e.getMonth())&&(u+="<option value='"+k+"'"+(k===b?" selected='selected'":"")+">"+h[k]+"</option>");u+="</select>"}if(s||(t+=u+(!f&&q&&r?"":"&#xa0;")),!a.yearshtml)if(a.yearshtml="",f||!r)t+="<span class='ui-datepicker-year'>"+c+"</span>";else{for(l=this._get(a,"yearRange").split(":"),m=(new Date).getFullYear(),n=function(a){var b=a.match(/c[+\-].*/)?c+parseInt(a.substring(1),10):a.match(/[+\-].*/)?m+parseInt(a,10):parseInt(a,10);return isNaN(b)?m:b},o=n(l[0]),p=Math.max(o,n(l[1]||"")),o=d?Math.max(o,d.getFullYear()):o,p=e?Math.min(p,e.getFullYear()):p,a.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";o<=p;o++)a.yearshtml+="<option value='"+o+"'"+(o===c?" selected='selected'":"")+">"+o+"</option>";a.yearshtml+="</select>",t+=a.yearshtml,a.yearshtml=null}return t+=this._get(a,"yearSuffix"),s&&(t+=(!f&&q&&r?"":"&#xa0;")+u),t+="</div>"},_adjustInstDate:function(a,b,c){var d=a.selectedYear+("Y"===c?b:0),e=a.selectedMonth+("M"===c?b:0),f=Math.min(a.selectedDay,this._getDaysInMonth(d,e))+("D"===c?b:0),g=this._restrictMinMax(a,this._daylightSavingAdjust(new Date(d,e,f)));a.selectedDay=g.getDate(),a.drawMonth=a.selectedMonth=g.getMonth(),a.drawYear=a.selectedYear=g.getFullYear(),"M"!==c&&"Y"!==c||this._notifyChange(a)},_restrictMinMax:function(a,b){var c=this._getMinMaxDate(a,"min"),d=this._getMinMaxDate(a,"max"),e=c&&b<c?c:b;return d&&e>d?d:e},_notifyChange:function(a){var b=this._get(a,"onChangeMonthYear");b&&b.apply(a.input?a.input[0]:null,[a.selectedYear,a.selectedMonth+1,a])},_getNumberOfMonths:function(a){var b=this._get(a,"numberOfMonths");return null==b?[1,1]:"number"==typeof b?[1,b]:b},_getMinMaxDate:function(a,b){return this._determineDate(a,this._get(a,b+"Date"),null)},_getDaysInMonth:function(a,b){return 32-this._daylightSavingAdjust(new Date(a,b,32)).getDate()},_getFirstDayOfMonth:function(a,b){return new Date(a,b,1).getDay()},_canAdjustMonth:function(a,b,c,d){var e=this._getNumberOfMonths(a),f=this._daylightSavingAdjust(new Date(c,d+(b<0?b:e[0]*e[1]),1));return b<0&&f.setDate(this._getDaysInMonth(f.getFullYear(),f.getMonth())),this._isInRange(a,f)},_isInRange:function(a,b){var c,d,e=this._getMinMaxDate(a,"min"),f=this._getMinMaxDate(a,"max"),g=null,h=null,i=this._get(a,"yearRange");return i&&(c=i.split(":"),d=(new Date).getFullYear(),g=parseInt(c[0],10),h=parseInt(c[1],10),c[0].match(/[+\-].*/)&&(g+=d),c[1].match(/[+\-].*/)&&(h+=d)),(!e||b.getTime()>=e.getTime())&&(!f||b.getTime()<=f.getTime())&&(!g||b.getFullYear()>=g)&&(!h||b.getFullYear()<=h)},_getFormatConfig:function(a){var b=this._get(a,"shortYearCutoff");return b="string"!=typeof b?b:(new Date).getFullYear()%100+parseInt(b,10),{shortYearCutoff:b,dayNamesShort:this._get(a,"dayNamesShort"),dayNames:this._get(a,"dayNames"),monthNamesShort:this._get(a,"monthNamesShort"),monthNames:this._get(a,"monthNames")}},_formatDate:function(a,b,c,d){b||(a.currentDay=a.selectedDay,a.currentMonth=a.selectedMonth,a.currentYear=a.selectedYear);var e=b?"object"==typeof b?b:this._daylightSavingAdjust(new Date(d,c,b)):this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay));return this.formatDate(this._get(a,"dateFormat"),e,this._getFormatConfig(a))}}),a.fn.datepicker=function(b){if(!this.length)return this;a.datepicker.initialized||(a(document).on("mousedown",a.datepicker._checkExternalClick),a.datepicker.initialized=!0),0===a("#"+a.datepicker._mainDivId).length&&a("body").append(a.datepicker.dpDiv);var c=Array.prototype.slice.call(arguments,1);return"string"!=typeof b||"isDisabled"!==b&&"getDate"!==b&&"widget"!==b?"option"===b&&2===arguments.length&&"string"==typeof arguments[1]?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c)):this.each(function(){"string"==typeof b?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this].concat(c)):a.datepicker._attachDatepicker(this,b)}):a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c))},a.datepicker=new d,a.datepicker.initialized=!1,a.datepicker.uuid=(new Date).getTime(),a.datepicker.version="1.12.1";a.datepicker}),+function(a){"use strict";if(a(".pagination-articles.not-ajax").length>0){var b=parseInt(a(location).attr("href").split("/").pop());(!a.isNumeric(b)||b>999)&&(b=1);var c=2;if(b<3)var c=5;a(".li-page").each(function(d){var e=parseInt(a(this).text());a.isNumeric(e)&&e!=b&&(e<b-c||e>b+c)&&a(this).hide()})}a("#liste_item_sommaire").length>0&&(a(".toggle-title").click(function(){a(this).parent().find(".list-item").toggle("fast")}),a("#votre_commune").on("keyup",function(){var b=a("#votre_commune").val().toLowerCase(),c=a("#liste_item_sommaire .toggle-title");if(""==b)for(var d=0;d<c.length;d++){var e=a(c[d]);e.parent().show()}else for(var d=0;d<c.length;d++){for(var e=a(c[d]),f=e.text().toLowerCase(),g=[/[\340-\346]/g,/[\350-\353]/g,/[\354-\357]/g,/[\362-\370]/g,/[\371-\374]/g,/[\361]/g,/[\347]/g],h=["a","e","i","o","u","n","c"],i=0;i<g.length;i++)f=f.replace(g[i],h[i]),b=b.replace(g[i],h[i]);f.replace(/ /g,""),f.replace(/\s/g,""),f.replace(/\ /g,""),f=a.trim(f);var j=new RegExp("^"+b+"(.*)$");f.match(j)?e.parent().show():e.parent().hide()}}))}(jQuery),+function(a){"use strict";a("#map_carte_de_france").length>0&&drawMaCarteCaf()}(jQuery),+function(a){"use strict";a(".page-carte").length>0&&drawMaCarteCaf()}(jQuery),+function(a){"use strict";function b(b){var c='<div id="id-pagination"><div class="row">';a.each(b,function(a,b){2!=a&&4!=a&&6!=a||(c+='</div><div class="row">'),c+='<div class="col-md-6">',c+='<div class="article-content-VDF theme-'+b.theme+'">',c+='<div class="article-miniature-VDF">',c+='<a href="/'+b.link+'"><img src="'+b.miniature+'" alt="" style="width:100%" /></a>',c+="</div>",c+='<div class="article-title-VDF"><a href="/'+b.link+'">'+b.title+"</a></div>",c+='<div class="article-summary-VDF">'+b.summary+"... ",c+='<a class="article-link-VDF" href="/'+b.link+'">Lire </a></div>',c+="</div>",c+="</div>"}),c+="</div>",a(".articles-VDF").html(c)}function c(b,d,e,f){var g=5;if(0==e)return a(".error-message").addClass("active"),a(".pagination-articles").html(""),void a(".articles-VDF").html("");a(".error-message").removeClass("active");var h=Math.floor((e+5)/6),i='<ul class="pagination nav">',j=!0,k=!0,l=1;h<=g+2?(j=!1,k=!1,l=2):d<=g+1?(j=!1,l=2):(l=Math.floor((d-2)/g)*g+2,l==h&&(l=h-g),l+g>=h&&(k=!1)),i+=1!=d?'<li data-value="1"><a href="#id-pagination">1</a></li>':'<li class="active" data-value="1"><a href="#id-pagination">1</a></li>',h>1&&(i+=j?l-g==2?'<li data-value="1"><a href="#id-pagination">Â«Â«</a></li>':'<li data-value="'+(l-g)+'"><a href="#id-pagination">Â«Â«</a></li>':'<li class="disabled"><a href="#id-pagination">Â«Â«</a></li>');for(var m=l;m<l+g;m++)m<h&&(i+=m==d?'<li class="active" data-value="'+m+'"><a href="#id-pagination">'+m+"</a></li>":'<li data-value="'+m+'"><a href="#id-pagination">'+m+"</a></li>");h>1&&(i+=k?'<li data-value="'+(l+g)+'"><a href="#id-pagination">Â»Â»</a></li>':'<li class="disabled"><a href="#id-pagination">Â»Â»</a></li>'),h>1&&(i+=d!=h?'<li data-value="'+h+'"><a href="#id-pagination">'+h+"</a></li>":'<li data-value="'+h+'" class="active"><a href="#id-pagination">'+h+"</a></li>"),i+="</ul>",a(".pagination-articles").html(i),a(".pagination-articles ul.pagination li").not(".disabled").click(function(){if(!a(this).hasClass("active")){a(".pagination-articles ul.pagination li.active").removeClass("active"),a(this).addClass("active");var d=parseInt(a(this).attr("data-value"));c(b,d,e,f)}}),f(b,d)}function d(b){a("div.pushmail-info").show(),a("div.modal-footer").show(),a("div.pushmail-content").show(),a("div.pushmail-status").hide(),a("input#email").val(b),a("input#cpostal").val(""),a("div.pushmail-rubrique > label.btn.active").removeClass("active"),e()}function e(){a("input#email").removeClass("error"),a("input#cpostal").removeClass("error"),a("div.error-email").removeClass("active"),a("div.error-cpostal").removeClass("active"),a("div.error-themes").removeClass("active"),a("div.error-email").html(""),a("div.error-cpostal").html(""),a("div.error-themes").html("")}function f(){e();var b=!1,c=a("input#email").val();if(c){var d=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;d.test(c)||(a("input#email").addClass("error"),a("div.error-email").html("Le format de votre adresse mÃ©l est invalide, merci de le modifier."),a("div.error-email").addClass("active"),b=!0)}else a("input#email").addClass("error"),a("div.error-email").html("Veuillez entrer une adresse mÃ©l valide."),a("div.error-email").addClass("active"),b=!0;var f=a("input#cpostal").val();if(f){var d=/^[0-9]{5,6}$/;d.test(f)||(a("input#cpostal").addClass("error"),a("div.error-cpostal").html("Le code postal saisi est incorrect, veuillez le saisir Ã  nouveau."),a("div.error-cpostal").addClass("active"),b=!0)}else a("input#cpostal").addClass("error"),a("div.error-cpostal").html("Veuillez entrer un code postal valide."),a("div.error-cpostal").addClass("active"),b=!0;var h=a("div.pushmail-rubrique > label.btn.active > input.pushmail-theme").length;return 0==h&&(a("div.error-themes").html("Veuillez sÃ©lectionner au moins un abonnement dans la liste proposÃ©e ci-dessous."),a("div.error-themes").addClass("active"),b=!0),b||g(c,f),!1}function g(b,c){var d=a("div.pushmail-rubrique"),e={};d.each(function(){var b=a(this).attr("id").split("-"),c=b[1],d=a(this).find("label.btn.active > input.pushmail-theme");0!=d.length&&(e[c]={},d.each(function(){var b=a(this).val(),d=a(this).attr("id").split("-"),f=d[1];e[c][f]={nom:b}}))}),a.ajax({type:"POST",url:"/pushmail/subscribe",data:{email:b,code_postal:c,subscriptions:JSON.stringify(e)},success:function(b){a("div.pushmail-info").hide(),a("div.modal-footer").hide(),a("div.pushmail-content").hide(),a("div.pushmail-status").html(b.resultat),a("div.pushmail-status").show()}})}var h=getCookieGeoloc();if(a(".node-pages-vdf-accueil").length>0){var i=a(".content-accueil-VDF .nids-slider-VDF").val().split(",");a.ajax({type:"GET",url:"/get/ajax/vdf-content-accueil",dataType:"json",data:{caf_id:h,nids_slider:i},success:function(b){if("undefined"==typeof b.erreur){var c='<div class="row">';a.each(b,function(a,b){2!=a&&4!=a||(c+='</div><div class="row">'),c+='<div class="col-md-6">',c+='<div class="article-content-VDF theme-'+b.theme+'">',c+='<div class="article-miniature-VDF">',c+='<a href="/'+b.link+'"><img src="'+b.miniature+'" alt="" style="width:100%" /></a>',c+="</div>",c+='<div class="article-title-VDF"><a href="/'+b.link+'">'+b.title+"</a></div>",c+='<div class="article-summary-VDF">'+b.summary+"... ",c+='<a class="article-link-VDF" href="/'+b.link+'">Lire </a></div>',c+="</div>",c+="</div>"}),c+="</div>",a(".content-accueil-VDF").html(c),a(window).width()>942&&setTimeout(function(){a(".content-accueil-VDF .row").each(function(){var b=a(this).find(".col-md-6:first .article-content-VDF"),c=a(this).find(".col-md-6:last-child .article-content-VDF");b.height()>c.height()?c.css("height",b.height()):b.height()<c.height()&&b.css("height",c.height())})},400)}else a(".content-accueil-VDF").html("")}}),a(".paves-vdf-articles-les-plus-lus .paves-vdf-articles-les-plus-lus-content").length>0&&a.ajax({type:"GET",url:"/get/ajax/get-articles-most-view",dataType:"json",data:{caf_id:h},success:function(b){if("undefined"==typeof b.vdf_art_plus_lus||0==b.vdf_art_plus_lus.length)a(".paves-vdf-articles-les-plus-lus").parent().hide();else{var c="";if("undefined"!=typeof b.vdf_art_plus_lus&&b.vdf_art_plus_lus.length>0)for(var d=0;d<5;d++)"undefined"!=typeof b.vdf_art_plus_lus[d]&&(c+='<hr><a href="/'+b.vdf_art_plus_lus[d].alias+'">'+b.vdf_art_plus_lus[d].title+"</a>");a(".paves-vdf-articles-les-plus-lus .paves-vdf-articles-les-plus-lus-content").html(c)}}})}if(a(".node-pages-vdf-rubriques").length>0){var j=function(c,d){var e=a(".page-content-node").attr("class").split(" "),f="node-promoted"!=e[3]?e[3].replace("node-",""):e[4].replace("node-",""),g=[];a(".filter-theme-label").each(function(b){if("checked"==a(this).children("input").attr("checked")){var c=a(this).children("span.hidden-theme-nid").text();g.push(c)}}),a.ajax({type:"GET",url:"/get/ajax/vdf-rubrique-articles",dataType:"json",data:{caf_id:c,nids_theme:g,nid_rubrique:f,page_number:d},success:b}),a(".ajax-loading-VDF").hide()},k=[];a(".filter-theme-label").each(function(b){var c=a(this).children("span.hidden-theme-nid").text();k.push(c)}),a(".filter-theme-label").each(function(b){a(this).click(function(){var b=0,d=a(this).children("input").attr("checked");"checked"==d?a(this).children("input").removeAttr("checked"):a(this).children("input").attr("checked","checked"),a(".filter-theme-label.active").each(function(c){b+=parseInt(a(this).children("span.hidden-theme-nb-articles").text())}),a(this).hasClass("active")?b-=parseInt(a(this).children("span.hidden-theme-nb-articles").text()):b+=parseInt(a(this).children("span.hidden-theme-nb-articles").text()),c(h,1,b,j)})}),a.ajax({type:"GET",url:"/get/ajax/vdf-rubrique-nb-articles-by-theme",dataType:"json",data:{caf_id:h,nids_theme:k},success:function(b){var d=b,e=0;a(".filter-theme-label").each(function(b){var c=a(this).children("span.hidden-theme-nid").text();a(this).children("span.filter-theme-nb-articles").text("("+d[c]+")"),a(this).children("span.hidden-theme-nb-articles").text(d[c]),a(this).hasClass("active")&&(e+=parseInt(d[c]))}),c(h,1,e,j),a(".pagination-articles").show()}})}if(a(".node-pages-vdf-articles").length>0){var l=a(".page-content-node").attr("class").split(" "),m="node-promoted"!=l[3]?l[3].replace("node-",""):l[4].replace("node-","");a.ajax({type:"GET",url:"/get/ajax/get-nextprev-article",dataType:"json",data:{caf_id:h,nid:m},success:function(b){"undefined"!=typeof b.next_article&&0!=b.next_article?a(".article-precsuiv-VDF .article-suivant").html(b.next_article).show():a(".article-precsuiv-VDF .article-suivant").html("").hide(),"undefined"!=typeof b.prev_article&&0!=b.prev_article?a(".article-precsuiv-VDF .article-precedent").html(b.prev_article).show():a(".article-precsuiv-VDF .article-precedent").html("").hide()}})}if(a(".page-vdf-mots-cles-articles").length>0){var j=function(c,d){var e=a(".articles-par-mot-cle-nid-VDF").val();a.ajax({type:"GET",url:"/get/ajax/vdf-articles-par-mot-cle",dataType:"json",data:{caf_id:c,nid_mot_cle:e,page_number:d},success:b}),a(".ajax-loading-VDF").hide()},n=parseInt(a(".articles-par-mot-cle-count-VDF").attr("value"));c(h,1,n,j),a(".pagination-articles").show()}a("div.pushmail-content").length>0&&(a("button.pushmail-subscribe").click(function(){d(""),a("#pushmailModal").modal("show")}),a("#vdf-abonnez-vous-submit").click(function(){var b=a("#vdf-abonnez-vous-email").val();""!=b&&"saisir votre mÃ©l"!=b&&(d(b),a("#pushmailModal").modal("show"))}),a("button.pushmail-validate").click(f))}(jQuery),+function(a){"use strict";if(a(".pave-liens-2-colonnes").length>0){var b=0;a(".pave-liens-2-colonnes a.panel-default").each(function(){a(this).height()>b&&(b=a(this).height())}),a(".pave-liens-2-colonnes a.panel-default").each(function(){a(this).height(b)});var c=".pave-liens-2-colonnes a.panel-default .panel-body";a(c).css("position","absolute"),a(c).css("top","50%"),a(c).css("transform","translateY(-50%)")}var d=0;a(".title-dernieres-publi:not(.title-slide)").each(function(){a(this).height()>d&&(d=a(this).height())}),a(".title-dernieres-publi:not(.title-slide)").each(function(){a(this).height(d),a(this).css("margin-bottom","5px")}),a(".pave-services-en-ligne").length>0&&resize_pave_services_en_ligne()}(jQuery),+function(a){"use strict";a('[data-toggle="popover"]').on("inserted.bs.popover",function(){a(this).attr("src","/sites/all/themes/caf_v2/images/pic-aide_bleu.png"),a(".popover-btn-close").click(function(){a('#simu-af-all-content [data-toggle="popover"]').popover("hide")})}),a('[data-toggle="popover"]').on("hide.bs.popover",function(){a(this).attr("src","/sites/all/themes/caf_v2/images/pic-aide_gris.png")}),a('[data-toggle="popover"]').popover({placement:"right"})}(jQuery),+function(a){"use strict";if(a(window).resize(function(){if(a(".carousel").length>0){var b=a(".carousel .carousel-inner .item.active .slide-data-content").height()+15;if(a(".carousel .carousel-indicators").css("bottom",b+"px"),a(".espace-allocataires").length>0&&a(".page-content-accueil").length>0&&a("#slider-cnaf").length>0){var c=a("#slider-cnaf").height();a(".block-caf-theme .bloc-droite-content").css("min-height",c+"px")}}if(a(window).width()>942&&a(".content-accueil-VDF").length>0?a(".content-accueil-VDF .row").each(function(){var b=a(this).find(".col-md-6:first .article-content-VDF"),c=a(this).find(".col-md-6:last-child .article-content-VDF");b.height()>c.height()?c.css("height",b.height()):b.height()<c.height()&&b.css("height",c.height())}):a(".content-accueil-VDF").length>0&&a(".content-accueil-VDF .row").each(function(){a(this).find(".col-md-6 .article-content-VDF").css("height","inherit")}),a("#allocataire-actu-paves-accueil").length>0&&accueil_allocataire_pave_actus_height(),a("#sticky-camille").length>0&&a("#sticky-camille").is(":visible"))if(a(window).width()>942){var d=a(window).width(),e=a(".container:first-child").width(),f=(d-e)/2;a("#sticky-camille").css("margin-right",f+"px")}else a("#sticky-camille").css("margin-right","0px");if(a(".pave-services-en-ligne").length>0&&resize_pave_services_en_ligne(),a(".row-same-height").length>0&&a(".row-same-height .panel").length>0)if(a(window).width()>942){a(".row-same-height").each(function(){if(a(this).find(".col-md-6:first-child .panel").length>0){var b=a(this).find(".col-md-6:first-child .panel"),c=a(this).find(".col-md-6:last-child .panel");if(a(b).height()>a(c).height()){var d=a(b).height();a(c).css("height",d+2+"px");var e=a(c).find("h3").height();if(null==e)var e=a(c).find(".title").height();e+=30,a(c).find(".panel-body-white").css("height",d-e+"px")}else if(a(b).height()<a(c).height()){var d=a(c).height();a(b).css("height",d+2+"px");var e=a(b).find("h3").height();if(null==e)var e=a(c).find(".title").height();e+=30,a(b).find(".panel-body-white").css("height",d-e+"px")}}})}else a(".row-same-height").each(function(){a(this).find(".col-md-6:first-child .panel").css("height","auto").find(".panel-body-white").css("height","auto"),a(this).find(".col-md-6:last-child .panel").css("height","auto").find(".panel-body-white").css("height","auto")})}),a("#sticky-camille").length>0)if(a(window).width()>942&&a("#sticky-camille").is(":visible")){var b=a(window).width(),c=a(".container:first-child").width(),d=(b-c)/2;a("#sticky-camille").css("margin-right",d+"px")}else a("#sticky-camille").css("margin-right","0px")}(jQuery),+function(a){"use strict";function b(a){if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(a))return!1;var b=a.split("/"),c=parseInt(b[0],10),d=parseInt(b[1],10),e=parseInt(b[2],10);if(e<1e3||e>3e3||0==d||d>12)return!1;var f=[31,28,31,30,31,30,31,31,30,31,30,31];return(e%400==0||e%100!=0&&e%4==0)&&(f[1]=29),c>0&&c<=f[d-1]}function c(a){return!jQuery.isArray(a)&&a-parseFloat(a)+1>=0}a("#simu-af-all-content").length>0&&(a("#simu-af-all-content input.simu-af-enfant-date").datepicker({closeText:"Fermer",prevText:"PrÃ©cÃ©dent",nextText:"Suivant",currentText:"Aujourd'hui",monthNames:["janvier","fÃ©vrier","mars","avril","mai","juin","juillet","aoÃ»t","septembre","octobre","novembre","dÃ©cembre"],monthNamesShort:["janv.","fÃ©vr.","mars","avr.","mai","juin","juil.","aoÃ»t","sept.","oct.","nov.","dÃ©c."],dayNames:["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"],dayNamesShort:["dim.","lun.","mar.","mer.","jeu.","ven.","sam."],dayNamesMin:["DIM","LUN","MAR","MER","JEU","VEN","SAM"],weekHeader:"Sem.",dateFormat:"dd/mm/yy",firstDay:1,isRTL:!1,showMonthAfterYear:!1,yearSuffix:"",changeYear:!0,changeMonth:!0,maxDate:"+1m",minDate:"-30y",yearRange:"-30:+01"}),a("#simu-af-all-content .btn-incremental button[type='button']").click(function(){var b=a(this).parent().find("input").val(),c=0;c="+"==a(this).text()?b<8?parseFloat(b)+1:b:b>1?parseFloat(b)-1:1,a(this).parent().find("input").val(c),a("div[id*='child-nb-']").removeClass("has-error").hide(),a("div[id*='simu-af-enfant-num-']").hide();for(var d=1;d<=c;d++)a("#child-nb-"+d).show()}),a("#simu-af-calculer").click(function(){a(".simu-af-result-content").hide(),a("#simu-af-result-value").html(),a("#simu-af-all-content .simu-af-ajax-loading").show(),a(this).hide();var d,e=!1,f=0;a("#simu-af-all-content input[value='Res']").parent().hasClass("active")?(f="R",a("#simu-af-all-content .simu-af-lieu-residence-error").hide(),d=!1):a("#simu-af-all-content input[value='Dom']").parent().hasClass("active")?(f="D",a("#simu-af-all-content .simu-af-lieu-residence-error").hide(),d=!1):(a("#simu-af-all-content .simu-af-lieu-residence-error").show(),d=!0);var g=a("#simu-af-ressources-value").val().replace(/ /g,"").replace(",","."),h=/^\d{0,10}$/;g.length<=0||!c(g)||c(g)&&parseInt(g)<0||!h.test(g)?(a(".simu-af-ressources-content").addClass("has-error"),a(".simu-af-ressources-error").show(),e=!0):(a(".simu-af-ressources-content").removeClass("has-error"),a(".simu-af-ressources-error").hide(),e=!1);var i=0,j=[];a("#simu-af-all-content div[id*='child-nb-']:visible").each(function(){var c=a(this).attr("id").replace("child-nb-",""),d=a(this).find("#simu-af-enfant-num-"+c+"-date").val();b(d)?(a("#simu-af-enfant-num-"+c+"-date-error").hide(),a("#child-nb-"+c).removeClass("has-error"),i++,j.push(d)):(i--,a("#child-nb-"+c).addClass("has-error"),a("#simu-af-enfant-num-"+c+"-date-error").show())}),e||d||i!=j.length?(a("#simu-af-all-content .simu-af-ajax-loading").hide(),a(this).show()):a.ajax({url:"/get/ajax/caf-simu-af-result",type:"GET",dataType:"json",data:{simuAfMontant:g,simuAgeList:j,isDom:f},success:function(b){"undefined"!=typeof b.result?(a(".txt-info-dom").hide(),"D"==f&&1==j.length&&a(".txt-info-dom").show(),a("#simu-af-result-value").html(b.result+"â‚¬"),a(".simu-af-result-content").show(),a("#simu-af-all-content .simu-af-ajax-loading").hide(),a("#simu-af-calculer").show()):(a("#simu-af-result-value").html(),a(".simu-af-result-content").hide(),a("#simu-af-all-content .simu-af-ajax-loading").hide(),a("#simu-af-calculer").show())},error:function(){a(".simu-af-result-content").hide(),a("#simu-af-result-value").html(),a("#simu-af-all-content .simu-af-ajax-loading").hide(),a("#simu-af-calculer").show()}})}))}(jQuery),+function(a){"use strict";if(a("ol.carousel-indicators").length>0&&(a("ol.carousel-indicators > li").length>1&&a("ol.carousel-indicators > li:first").addClass("active"),a(".carousel.slide").length>0)){var b=a(".carousel .carousel-inner .item.active .slide-data-content").height()+15;a(".carousel .carousel-indicators").css("bottom",b+"px")}if(a(".espace-allocataires").length>0&&a(".page-content-accueil").length>0&&a("#slider-cnaf").length>0){var c=a("#slider-cnaf").height();a(".block-caf-theme .bloc-droite-content").css("min-height",c+"px")}a(".carousel.slide").on("slid.bs.carousel",function(){var b=a(".carousel .carousel-inner .item.active .slide-data-content").height()+15;if(a(".carousel .carousel-indicators").css("bottom",b+"px"),a(".espace-allocataires").length>0&&a(".page-content-accueil").length>0&&a("#slider-cnaf").length>0){var c=a("#slider-cnaf").height();a(".block-caf-theme .bloc-droite-content").css("min-height",c+"px")}})}(jQuery),!function(){function a(a,b){function c(b){var c,d=a.arcs[0>b?~b:b],e=d[0];return a.transform?(c=[0,0],d.forEach(function(a){c[0]+=a[0],c[1]+=a[1]})):c=d[d.length-1],0>b?[c,e]:[e,c]}function d(a,b){for(var c in a){var d=a[c];delete b[d.start],delete d.start,delete d.end,d.forEach(function(a){e[0>a?~a:a]=1}),h.push(d)}}var e={},f={},g={},h=[],i=-1;return b.forEach(function(c,d){var e,f=a.arcs[0>c?~c:c];f.length<3&&!f[1][0]&&!f[1][1]&&(e=b[++i],b[i]=c,b[d]=e)}),b.forEach(function(a){var b,d,e=c(a),h=e[0],i=e[1];if(b=g[h])if(delete g[b.end],b.push(a),b.end=i,d=f[i]){delete f[d.start];var j=d===b?b:b.concat(d);f[j.start=b.start]=g[j.end=d.end]=j}else f[b.start]=g[b.end]=b;else if(b=f[i])if(delete f[b.start],b.unshift(a),b.start=h,d=g[h]){delete g[d.end];var k=d===b?b:d.concat(b);f[k.start=d.start]=g[k.end=b.end]=k}else f[b.start]=g[b.end]=b;else b=[a],f[b.start=h]=g[b.end=i]=b}),d(g,f),d(f,g),b.forEach(function(a){e[0>a?~a:a]||h.push([a])}),h}function b(b,c,d){function e(a){var b=0>a?~a:a;(k[b]||(k[b]=[])).push({i:a,g:j})}function f(a){a.forEach(e)}function g(a){a.forEach(f)}function h(a){"GeometryCollection"===a.type?a.geometries.forEach(h):a.type in l&&(j=a,l[a.type](a.arcs))}var i=[];if(arguments.length>1){var j,k=[],l={LineString:f,MultiLineString:g,Polygon:g,MultiPolygon:function(a){a.forEach(g)}};h(c),k.forEach(arguments.length<3?function(a){i.push(a[0].i)}:function(a){d(a[0].g,a[a.length-1].g)&&i.push(a[0].i)})}else for(var m=0,n=b.arcs.length;n>m;++m)i.push(m);return{type:"MultiLineString",arcs:a(b,i)}}function c(b,c){function d(a){a.forEach(function(b){b.forEach(function(b){(g[b=0>b?~b:b]||(g[b]=[])).push(a)})}),h.push(a)}function e(a){return k(f(b,{type:"Polygon",arcs:[a]}).coordinates[0])>0}var g={},h=[],i=[];return c.forEach(function(a){"Polygon"===a.type?d(a.arcs):"MultiPolygon"===a.type&&a.arcs.forEach(d)}),h.forEach(function(a){if(!a._){var b=[],c=[a];for(a._=1,i.push(b);a=c.pop();)b.push(a),a.forEach(function(a){a.forEach(function(a){g[0>a?~a:a].forEach(function(a){a._||(a._=1,c.push(a))})})})}}),h.forEach(function(a){delete a._}),{type:"MultiPolygon",arcs:i.map(function(c){var d=[];if(c.forEach(function(a){a.forEach(function(a){a.forEach(function(a){g[0>a?~a:a].length<2&&d.push(a)})})}),d=a(b,d),(n=d.length)>1)for(var f,h=e(c[0][0]),i=0;n>i;++i)if(h===e(d[i])){f=d[0],d[0]=d[i],d[i]=f;break}return d})}}function d(a,b){return"GeometryCollection"===b.type?{type:"FeatureCollection",features:b.geometries.map(function(b){return e(a,b)})}:e(a,b)}function e(a,b){var c={type:"Feature",id:b.id,properties:b.properties||{},geometry:f(a,b)};return null==b.id&&delete c.id,c}function f(a,b){function c(a,b){b.length&&b.pop();for(var c,d=k[0>a?~a:a],e=0,f=d.length;f>e;++e)b.push(c=d[e].slice()),j(c,e);0>a&&g(b,f)}function d(a){return a=a.slice(),j(a,0),a}function e(a){for(var b=[],d=0,e=a.length;e>d;++d)c(a[d],b);return b.length<2&&b.push(b[0].slice()),b}function f(a){for(var b=e(a);b.length<4;)b.push(b[0].slice());return b}function h(a){return a.map(f)}function i(a){var b=a.type;return"GeometryCollection"===b?{type:b,geometries:a.geometries.map(i)}:b in l?{type:b,coordinates:l[b](a)}:null}var j=p(a.transform),k=a.arcs,l={Point:function(a){return d(a.coordinates)},MultiPoint:function(a){return a.coordinates.map(d)},LineString:function(a){return e(a.arcs)},MultiLineString:function(a){return a.arcs.map(e)},Polygon:function(a){return h(a.arcs)},MultiPolygon:function(a){return a.arcs.map(h)}};return i(b)}function g(a,b){for(var c,d=a.length,e=d-b;e<--d;)c=a[e],a[e++]=a[d],a[d]=c}function h(a,b){for(var c=0,d=a.length;d>c;){var e=c+d>>>1;a[e]<b?c=e+1:d=e}return c}function i(a){function b(a,b){a.forEach(function(a){0>a&&(a=~a);var c=e[a];c?c.push(b):e[a]=[b]})}function c(a,c){a.forEach(function(a){b(a,c)})}function d(a,b){"GeometryCollection"===a.type?a.geometries.forEach(function(a){d(a,b)}):a.type in g&&g[a.type](a.arcs,b)}var e={},f=a.map(function(){return[]}),g={LineString:b,MultiLineString:c,Polygon:c,MultiPolygon:function(a,b){a.forEach(function(a){c(a,b)})}};a.forEach(d);for(var i in e)for(var j=e[i],k=j.length,l=0;k>l;++l)for(var m=l+1;k>m;++m){var n,o=j[l],p=j[m];(n=f[o])[i=h(n,p)]!==p&&n.splice(i,0,p),(n=f[p])[i=h(n,o)]!==o&&n.splice(i,0,o)}return f}function j(a,b){function c(a){f.remove(a),a[1][2]=b(a),f.push(a)}var d=p(a.transform),e=q(a.transform),f=o();return b||(b=l),a.arcs.forEach(function(a){for(var g,h,i=[],j=0,k=0,l=a.length;l>k;++k)h=a[k],d(a[k]=[h[0],h[1],1/0],k);for(var k=1,l=a.length-1;l>k;++k)g=a.slice(k-1,k+2),g[1][2]=b(g),i.push(g),f.push(g);for(var k=0,l=i.length;l>k;++k)g=i[k],g.previous=i[k-1],g.next=i[k+1];for(;g=f.pop();){var m=g.previous,n=g.next;g[1][2]<j?g[1][2]=j:j=g[1][2],m&&(m.next=n,m[2]=g[2],c(m)),n&&(n.previous=m,n[0]=g[0],c(n))}a.forEach(e)}),a}function k(a){for(var b,c=-1,d=a.length,e=a[d-1],f=0;++c<d;)b=e,e=a[c],f+=b[0]*e[1]-b[1]*e[0];return.5*f}function l(a){var b=a[0],c=a[1],d=a[2];return Math.abs((b[0]-d[0])*(c[1]-b[1])-(b[0]-c[0])*(d[1]-b[1]))}function m(a,b){return a[1][2]-b[1][2]}function o(){function a(a,b){for(;b>0;){var c=(b+1>>1)-1,e=d[c];if(m(a,e)>=0)break;d[e._=b]=e,d[a._=b=c]=a}}function b(a,b){for(;;){var c=b+1<<1,f=c-1,g=b,h=d[g];if(e>f&&m(d[f],h)<0&&(h=d[g=f]),e>c&&m(d[c],h)<0&&(h=d[g=c]),g===b)break;d[h._=b]=h,d[a._=b=g]=a}}var c={},d=[],e=0;return c.push=function(b){return a(d[b._=e]=b,e++),e},c.pop=function(){if(!(0>=e)){var a,c=d[0];return--e>0&&(a=d[e],b(d[a._=0]=a,0)),c}},c.remove=function(c){var f,g=c._;if(d[g]===c)return g!==--e&&(f=d[e],(m(f,c)<0?a:b)(d[f._=g]=f,g)),g},c}function p(a){if(!a)return r;var b,c,d=a.scale[0],e=a.scale[1],f=a.translate[0],g=a.translate[1];return function(a,h){h||(b=c=0),a[0]=(b+=a[0])*d+f,a[1]=(c+=a[1])*e+g}}function q(a){if(!a)return r;var b,c,d=a.scale[0],e=a.scale[1],f=a.translate[0],g=a.translate[1];return function(a,h){h||(b=c=0);var i=(a[0]-f)/d|0,j=(a[1]-g)/e|0;a[0]=i-b,a[1]=j-c,b=i,c=j}}function r(){}var s={version:"1.6.19",mesh:function(a){return f(a,b.apply(this,arguments))},meshArcs:b,merge:function(a){return f(a,c.apply(this,arguments))},mergeArcs:c,feature:d,neighbors:i,presimplify:j};"function"==typeof define&&define.amd?define(s):"object"==typeof module&&module.exports?module.exports=s:this.topojson=s}(),+function(a){"use strict";function b(b,d){a(b).each(function(){var b=a(this).attr("href").split("/");b=b[b.length-1],"undefined"!=typeof b&&b.length>0&&/\d/.test(b)&&(a(this).attr("data-toggle","modal"),a(this).attr("data-target","#webform_client_form_modal_"+b),a(this).attr("href","#"),a(this).removeAttr("class"),a.ajax({type:"GET",url:"/post/ajax/form-internaute-webform/"+b,success:function(e){var e=a(e);a(d).after(e),c("#webform-client-form-"+b);var f=a("#webform-client-form-"+b+" .captcha.form-wrapper .fieldset-wrapper").html();"undefined"!=typeof f&&(a("#webform-client-form-"+b+" .captcha.form-wrapper").remove(),a("#webform-client-form-"+b+" .modal-body .form-horizontal").append('<div class="text-center">'+f+"</div>"),a("#edit-captcha-response").addClass("form-control"));var g='<div class="text-center"><button class="btn btn-primary" id="edit-submit-webform">Envoyer</button></div><span class="form-required"> * Renseignement obligatoire </span>';a("#webform_client_form_modal_"+b+" .modal-body").append(g),a("#edit-submit-webform").click(function(d){d.stopPropagation();var e=a("#webform-client-form-"+b);return a(e).find(".form-horizontal").before("<div class='ajax-loading'></div>"),a(e).find(".form-horizontal").hide(),a(e).find(".ajax-loading").show(),a(e).find(".modal-body .text-center").hide(),a.ajax({url:"/post/ajax/form-internaute-webform/"+b,type:a(e).attr("method"),data:a(e).serialize(),success:function(d){var e=a("#webform-client-form-"+b);if(a(e).find(".ajax-loading").remove(),"undefined"!=typeof d.message)a("#webform-client-form-"+b+" .modal-body").html(d.message);else{var f=a("<div />");f.html(d);var g="";if(a(f).find(".messages.error").length>0){var h=a(f).find(".messages.error").html();if(g+='<div class="panel panel-default"><div class="panel-body text-danger"><div class="panel-icon-left icon-pic-attention pull-left"></div>',a(h).find("li").length>0)a(h).find("li").each(function(){g+="<p>"+a(this).text()+"</p>"});else{a(f).find(".messages.error").find("h2").remove();var i=a(f).find(".messages.error").text().replace(/(?:\r\n|\r|\n)/g,"");
g+="<p>"+i+"</p>"}g+="</div></div>",a(f).find(".messages.error").remove()}var j=a(f).find("#webform_client_form_modal_"+b).find(".form-horizontal").html();e.find(".form-horizontal").html(g+j);var k=a(f).find(".captcha.form-wrapper .fieldset-wrapper").html();"undefined"!=typeof k&&(a("#webform-client-form-"+b+" .captcha.form-wrapper").remove(),a("#webform-client-form-"+b+" .modal-body .form-horizontal").append('<div class="text-center">'+k+"</div>"),a("#edit-captcha-response").addClass("form-control")),c("#webform_client_form_modal_"+b),a("#webform_client_form_modal_"+b+" .form-horizontal").show(),a("#webform_client_form_modal_"+b+" .modal-body .text-center").show()}},error:function(c){alert("CMN form internaute error");var d='<div class="panel panel-default"><div class="panel-body text-danger"><div class="panel-icon-left icon-pic-attention pull-left"></div><p>Une erreur innatendue est survenue.</p></div></div>';a("#webform-client-form-"+b+" .modal-body").html(d),console.log(c)}}),!1})}}))})}function c(b){a(b).find("div.fivestar-form-item").each(function(){var b=a(this),c=a('<div class="fivestar-widget clearfix"></div>'),d=a("select",b),e=a('option[value="0"]',b);e.length&&a('<div class="cancel"><a href="#0" title="'+e.text()+'">'+e.text()+"</a></div>").appendTo(c);var f=a("option",b).not('[value="-"], [value="0"]'),g=-1;f.each(function(b,e){var h="star-"+(b+1);h+=(b+1)%2==0?" even":" odd",h+=0==b?" star-first":"",h+=b+1==f.length?" star-last":"",a('<div class="star"><a href="#'+e.value+'" title="'+e.text+'">'+e.text+"</a></div>").addClass(h).appendTo(c),e.value==d.val()&&(g=b+1)}),g!=-1&&c.find(".star").slice(0,g).addClass("on"),c.addClass("fivestar-widget-"+f.length),c.find("a").bind("click",b,function(b){var c=a(this),d=b.data,e=this.hash.replace("#","");a("select",d).val(e).change();var f=0==e?c.parent().parent().find(".star"):c.closest(".star");f.prevAll(".star").andSelf().addClass("on"),f.nextAll(".star").removeClass("on"),0==e&&f.removeClass("on"),b.preventDefault()}).bind("mouseover",b,function(b){var c=a(this),d=(b.data,a(b.target)),e=a(".star",c);if("mouseover"==b.type){var f=e.index(d.parent());e.each(function(b,c){b<=f?a(c).addClass("hover"):a(c).removeClass("hover")})}else e.removeClass("hover")}),c.bind("mouseover mouseout",b,function(b){var c=a(this),d=(b.data,a(b.target)),e=a(".star",c);if("mouseover"==b.type){var f=e.index(d.parent());e.each(function(b,c){b<=f?a(c).addClass("hover"):a(c).removeClass("hover")})}else e.removeClass("hover")}),d.after(c).css("display","none")})}if(a("#modalSatisfaction").on("show.bs.modal",function(b){function c(){var b,c=a('input:radio[name="submitted[utile]"]:checked').val(),d=a('input:radio[name="submitted[facile_a_trouver]"]:checked').val(),e=a('input:radio[name="submitted[facile_a_comprendre]"]:checked').val(),f=!0;if(a("#edit-captcha-response").length>0?b=a("#edit-captcha-response").val():f=!1,0!=c&&1!=c||0!=d&&1!=d||0!=e&&1!=e)return alert("Veuillez renseigner les champs obligatoires"),!1;if(f&&0==b.length)return alert("Veuillez saisir le captcha"),!1;a("#btn-commentaire-submit").hide(),a("#webform-client-form-19835 .ajax-loading").show();var g=jQuery("#webform-client-form-19835"),h=jQuery("#webform-client-form-19835 input[name=captcha_sid]").val(),i=jQuery("#edit-captcha-response").val(),j="";return jQuery.ajax({url:"/captcha/ajax/".concat(h).concat("/").concat(i),data:a(g).serialize(),type:a(g).attr("method"),success:function(b){if(j=b,1==j||0==f){var c=a(a(this).parent(),"input[type='submit']");c.attr("disabled","true"),jQuery.ajax({url:a(g).attr("action"),type:a(g).attr("method"),data:a(g).serialize(),success:function(a){alert("Votre commentaire a bien Ã©tÃ© envoyÃ©."),location.reload()},error:function(){location.reload()}})}else alert("ContrÃ´le de sÃ©curitÃ© incorrect"),jQuery("#edit-captcha-response").val(""),jQuery(".reload-captcha").click(),a("#btn-commentaire-submit").show(),a("#webform-client-form-19835 .ajax-loading").hide()}}),!1}a("#edit-submitted-vos-commentaires").val(""),a("#text_after").empty(),a("#edit-captcha-response").val(""),a("#webform-client-form-19835 .commentaire-satisfaction-choix-multiple-tmp").remove(),a(".webform-component-radios").hide();var d='<div class="commentaire-satisfaction-choix-multiple-tmp">'+a(".commentaire-satisfaction-choix-multiple").html()+"</div>";a("#webform-component-vos-commentaires").before(d),a(d).show(),a("#edit-submitted-vos-commentaires").after('<span id="text_after">Il vous reste <span id="limit_char">500</span> <span id="pluriel">caractÃ¨res disponibles</span></span>'),a("#edit-submitted-vos-commentaires").addClass("form-control"),a("#edit-submitted-vos-commentaires").bind("input propertychange",function(){if(this.value.length>=500){var b=a(this).val(),c=b.substring(0,500);a(this).val(c)}var d=this.value.length,e=500-d;e<=1?a("#pluriel").html("caractÃ¨re disponible"):a("#pluriel").html("caractÃ¨res disponibles"),a("#limit_char").html(e)}),a("#webform-client-form-19835").submit(function(){return b.stopPropagation(),c(),!1}),a('#webform-client-form-19835 input[name="captcha_response"]').keypress(function(a){if(13==a.which)return a.stopPropagation(),c(),!1});var e=a("#webform-client-form-19835 .captcha.form-wrapper .fieldset-wrapper").html();"undefined"!=typeof e&&(a("#webform-client-form-19835 .captcha.form-wrapper").remove(),a("#webform-client-form-19835 .form-submit").before('<div class="text-center">'+e+"</div>"),a("#edit-captcha-response").addClass("form-control"));var f=jQuery("#webform-client-form-19835 .form-submit").hide();a("#webform-client-form-19835 .form-submit").after('<input type="button" class="btn btn-primary" id="btn-commentaire-submit" value="Envoyer" />'),a("#webform-client-form-19835 .form-submit").after('<div class="ajax-loading"></div>'),f.remove(),a("#btn-commentaire-submit").mousedown(function(a){return a.stopPropagation(),c(),!1})}),a("#page-content .page-content-data .field-name-body").length>0){var d="#page-content .page-content-data .field-name-body a.ctools-use-modal",e="#page-content .page-content-data .field-name-body:first";b(d,e)}if(/\/admin\/gestion\-formulaire\-internaute/.test(window.location.pathname)){var d=".view-gestion-formulaire-internaute table.views-table tbody a.ctools-use-modal",e=".view-gestion-formulaire-internaute";b(d,e)}}(jQuery);;
