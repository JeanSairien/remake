// On récupère l'objet canvas

var elem = document.getElementById('canvasElem');


// On récupère le contexte 2D

var context = elem.getContext('2d');

window.addEventListener('load', function () {

// On récupère l'objet canvas

var elem = document.getElementById('canvasElem');

if (!elem || !elem.getContext) {

return;

}

 

// On récupère le contexte 2D

var context = elem.getContext('2d');

if (!context) {

return;

}

 

// Le navigateur est compatible, le contexte a bien été récupéré, on peut continuer...

}, false);

ctx.beginPath();
ctx.rect(x,y,w,h);
ctx.closePath();
ctx.fill();

ctx.fillStyle = "red";
ctx.fillStyle = "#FF0000";
ctx.fillStyle = "rgb(255,0,0)";
ctx.fillStyle = "rgba(255,0,0,1)";

.
// Constantes du jeu
var NBR_LIGNES = 5;
var NBR_BRIQUES_PAR_LIGNE = 8;
var BRIQUE_WIDTH = 48;
var BRIQUE_HEIGHT = 15;
var ESPACE_BRIQUE = 2;

 

// Variables
var tabBriques; // Tableau virtuel contenant les briques
// Fonction permettant de créer les briques du jeu
function creerBriques(ctx, nbrLignes, nbrParLigne, largeur, hauteur, espace) {
// Tableau virtuel: On initialise les lignes de briques
tabBriques = new Array(nbrLignes);
	for (var i=0; i < nbrLignes; i++) {
 	// Tableau virtuel: On initialise les briques de la ligne
		tabBriques[i] = new Array(nbrParLigne);
	// Affichage: On attribue une couleur aux briques de la ligne
		ctx.fillStyle = "rgb("+Math.floor(Math.random()*256)+","+Math.floor(Math.random()*256)+","+Math.floor(Math.random()*256)+")";
		for (var j=0; j < nbrParLigne; j++) {
		// Affichage: On affiche une nouvelle brique
			ctx.fillRect((j*(largeur+espace)),(i*(hauteur+espace)),largeur,hauteur);
		// Tableau virtuel: On attribue à la case actuelle la valeur 1 = Une brique existe encore
			tabBriques[i][j] = 1;
			}

		}

 

// Nos briques sont initialisées.
return 1;
}